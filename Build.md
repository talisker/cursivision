# Building CursiVision

<p>CursiVision builds with Microsoft Visual Studio.</p>

<p>I had been using VS 2015 Community edition, using the Windows 8.1 SDK, up until June 2019 with no issues, and I believe there would still be no issues with it.</p>

<p>However, when I installed 2019 Community edition, it hosed my VS 2015 SDK and/or paths, etc. These things are fixable, it's just irratating to go through with it.
I will say that if you DO want to use 2015 or something earlier than VS 2019, it is a simple matter to change the project files to match up with your VS version. Note near the top of every '.vcxproj file 
there are a couple of elements you'll have to change:</p>

   &lt;PlatformToolset&gt;v142&lt;/PlatformToolset&gt;
   &lt;WindowsTargetPlatformVersion&gt;10.0&lt;/WindowsTargetPlatformVersion&gt;

<p>
142 is for VS 2019, I know that VS 2015 was 140. Inspect a local '.vcxproj file to match your environment (I note these things in case these project files don't open in your version of VS). To use the 8.1 SDK, change the 10.0 to 8.1.
</p>

## Step 1: Choose a root location

<p>All of the repositories you will be cloning need to be siblings of each other. Choose a location on your machine on any drive. Of course, I believe you should create a folder dedicated to this to keep it clean. But, you don't have to.
</p>

## Step 2: Create an environment variable ( GSYSTEM_HOME ) to point to your choosen root

<p>Example, if you create the directory "CursiVision Workspace" on your E: Drive, use Control Panel - System - Advanced system settings - Environment Variables to set:</p>

   GSYSTEM_HOME = E:\CursiVision Workspace

<p>Reboot not necessary, restart Visual Studio would be necessary.</p>

## Step 3: clone the repositories (in that root, remember, these should be siblings)

<ul><li>Common</li>
<li>CursiVision</li>
<li>CursiVision-Signature-Capture-Devices</li>
<li>CursiVision-Toolbox</li>
<li>PDFEnabler</li>
<li>PDFium-Control</li>
<li>Phablet-API</li>
<li>Postscript</li>
<li>Properties</li>
</ul>

## Step 4: Start building

<p>I recommend running Visual Studio as administrator. There are several build steps that register the resulting artifact after building it. This is done to streamline the build/debug cycle. CursiVision makes
heavy use of COM (Component Object Model) and registering components after building them negates the need to build an installer. You can go directly to debug after building each artifact.</p>

<p>To further this notion, you can run the CursiVision installer first thing, then, as you build each COM component, it will be in immediate use by the installed version. Another aspect of the power of COM.</p>

<p>There is no specific order of building these projects (there will be a VS solution file in each of the folders listed above). There are a few interdependencies, however, note that the generated COM files, 
type libraries ('.tlb), interface header and implementation files (..._i.h and ..._i.c respectively) ARE in the repository which means that they are available in their up to date state right away. In addition,
all COM artifacts emit these files into the common place so there is no confusion about versions, etc, as you start to build in a random order.</p>

## Step 4a: Open the installer script

<p>The system uses the open source NSIS installer tool as well as the HM NIS Edit 2.0.3 NSIS editor ( http://hmne.sourceforge.net/ )</p>

<p>If you open the CursiVision installer ![script](Installer%20Support/NSIS/CursiVisionRelease.nsi) and start building it - you will be able to track what is <i>not</i> built yet - because it's errors will 
indicate what files are not yet found.</p>

<p>In any case, if you open every solution under each of the above folders and build them (all 4 configurations, i.e., Debug x86 and x64, and Release x86 and x64), until there are no "not found" issues, you should be okay.
Don't worry, there is not a hugely iterative set of build cycles caused by interdepencies as there are just a few of them (interdependencies).</p>

<p>Two issues that come to mind:</p>

<ul><li>For the PDFium-Control project, you have to navigate to "pdfium/lib/$(Configuration)/$(Platform)" and unzip the pdfium.zip file in place, the actual libs are too big to put on github</li>
<li>When you are building the installer, you may have to wait until the Phablet-API projects are built, then run the NSIS script in it's Installer Support directory (pkDevice.nsi) because the main 
CursiVision installer embeds that installer within itself.</li>
</ul>

<p>I think the best bet is to build in this order:</p>

<ol><li>Common - two projects</li>
<li>Properties - has no dependencies</li>
<li>PDFEnabler - Postscript needs it</li>
<li>Postscript</li>
<li>PDFium-Control</li>
<li>Phablet-API</li>
<li>CursiVision-Signature-Capture-Devices</li>
<li>CursiVision-Toolbox</li>
<li>CursiVision</li>
</ol>