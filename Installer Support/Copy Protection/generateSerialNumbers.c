
#include <windows.h>
#include <stdio.h>
#include <string.h>

   LONG WINAPI ShowDialogs(HWND hwnd, char * szSrcDir, char * szSupport, char * szSerialNum, char * szDbase);

   int main() {
   long j,k,m;
   long digit1,digit2,digit3;
   char alpha[] = {"123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
   char szField2[32];
   char szSerialNumber[11];
   union {
      long field;
      char szField[4];
   } field1;

   union {
      long field;
      char szField[4];
   } field2;

   field1.szField[3] = '\0';
   field2.szField[3] = '\0';
   memset(szSerialNumber,0,sizeof(szSerialNumber));

#if 0
   for ( j = 9; j < strlen(alpha); j++ ) {

      for ( k = strlen(alpha) - 1; k > -1; k-- ) {

         for ( m = 0; m < strlen(alpha); m++ ) {

            field1.szField[0] = alpha[j]; // A - Z
            field1.szField[1] = alpha[k]; // Z - 1
            field1.szField[2] = alpha[m]; // 1 - Z

            field2.szField[0] = field1.szField[2]; // 1 - Z
            field2.szField[1] = field1.szField[0]; // A - Z
            field2.szField[2] = field1.szField[1]; // Z - 1

            digit1 = strchr(alpha,field2.szField[0]) - alpha + 1; // 01 - 35
            digit2 = strchr(alpha,field2.szField[1]) - alpha + 1; // 09 - 35
            digit3 = strchr(alpha,field2.szField[2]) - alpha + 1; // 35 - 01

            sprintf(szField2,"%02d%02d%02d",digit1 + 50,digit2 + 50,digit3 + 50); // 51 - 85, 59 - 85, 85 - 51

            sprintf(szSerialNumber,"%s-%6s",field1.szField,szField2);

            printf("insert into vl_sc_serialnumbers ( serialNumber ) values ('%s');\n",szSerialNumber);

            //if ( ! ShowDialogs(NULL,NULL,NULL,szSerialNumber,NULL) ) return 0;

         }
      }
   }
#else
   for ( j = 9; j < strlen(alpha); j++ ) {

      for ( k = strlen(alpha) - 1; k > -1; k-- ) {

         for ( m = 0; m < strlen(alpha); m++ ) {

            field1.szField[0] = alpha[j]; // A - Z
            field1.szField[1] = alpha[k]; // Z - 1
            field1.szField[2] = alpha[m]; // 1 - Z

            field2.szField[0] = field1.szField[2]; // 1 - Z
            field2.szField[1] = field1.szField[0]; // A - Z
            field2.szField[2] = field1.szField[1]; // Z - 1

            digit1 = strchr(alpha,field2.szField[0]) - alpha + 1; // 01 - 35
            digit2 = strchr(alpha,field2.szField[1]) - alpha + 1; // 09 - 35
            digit3 = strchr(alpha,field2.szField[2]) - alpha + 1; // 35 - 01

            sprintf(szField2,"%02d%02d%02d",digit1 + 3,digit2 + 2,digit3 + 1); // 04 - 38, 11 - 37, 36 - 2

            sprintf(szSerialNumber,"%s-%6s",field1.szField,szField2);

            printf("insert into vl_sc_evaluationserialnumbers ( serialNumber ) values ('%s');\n",szSerialNumber);

            //if ( ! ShowDialogs(NULL,NULL,NULL,szSerialNumber,NULL) ) return 0;

         }
      }
   }
#endif
   return 0;
   }