
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <shlobj.h>
#include <shlguid.h>
#include <msiQuery.h>
#include <time.h>

   static char alpha[] = {"123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"};

   extern "C" long __stdcall ShowDialogs(HWND hwnd, char * szSrcDir, char * szSupport, char * szSerialNumber, char * szDbase) {

   char szTemp[255];
   char szNumber[3];
   long digit1,digit2,digit3;
   char szField1[4];
   char szField2[7];
   char c; 
   HKEY hKey;
   DWORD dwDisposition;

   char *p = getenv("gsystemsinstalleroveride");
   if ( p )
      if ( strcmp("gsystemscompanymachine",p) == 0 ) return 1;

   if ( strlen(szSerialNumber) < 10 ) return -1;

   strcpy(szTemp, szSerialNumber);
   szTemp[0] = toupper(szTemp[0]);
   szTemp[1] = toupper(szTemp[1]);
   szTemp[2] = toupper(szTemp[2]);

   memset(&szField1,0,sizeof(szField1));
   memset(&szField2,0,sizeof(szField2));

   strcpy(szField1,strtok(szTemp,"-" ));
   c = szField1[0];
   szField1[0] = szField1[1];
   szField1[1] = szField1[2];
   szField1[2] = c;

   strcpy(szField2,strtok(NULL,"-" ));

   memset(szNumber,0,sizeof(szNumber));
   strncpy(szNumber,szField2,2);
   digit1 = atol(szNumber) - 50;
   if ( digit1 > strlen(alpha) ) return -1;

   memset(szNumber,0,sizeof(szNumber));
   strncpy(szNumber,szField2 + 2,2);
   digit2 = atol(szNumber) - 50;
   if ( digit2 > strlen(alpha) ) return -1;

   memset(szNumber,0,sizeof(szNumber));
   strncpy(szNumber,szField2 + 4,2);
   digit3 = atol(szNumber) - 50;
   if ( digit3 > strlen(alpha) ) return -1;

   if ( szField1[0] != alpha[digit3 - 1] ) return -1;

   if ( szField1[1] != alpha[digit1 - 1] ) return -1;

   if ( szField1[2] != alpha[digit2 - 1] ) return -1;

   RegCreateKeyEx(HKEY_LOCAL_MACHINE,"\\Software\\InnoVisioNate Inc.\\GSystems Properties Component",0,NULL,0,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);
   RegSetValueEx(hKey,"Serial Number",0,REG_SZ,(BYTE *)szSerialNumber,11);

   return 1;
   }


   extern "C" UINT __stdcall SetHelpShortcut(MSIHANDLE msiHandle) {

/*
   char* p = getenv("windir");
   if ( ! p ) return 1;

   char szInstall[MAX_PATH];
   DWORD dWord = MAX_PATH;
   UINT msi = MsiGetProperty(msiHandle, "INSTALLDIR", szInstall, &dWord);

   char szAllUsers[32];
   dWord = 32;
   MsiGetProperty(msiHandle, "ALLUSERS", szAllUsers, &dWord);

   IShellLink* pShellLink;
   HRESULT hr;
   char szLinkLocation[MAX_PATH];
   char szHelpLocation[MAX_PATH];
   char szLinkRoot[MAX_PATH];
   CoInitialize(NULL);

   hr = CoCreateInstance(CLSID_ShellLink,NULL,CLSCTX_INPROC_SERVER,IID_IShellLink,(void **)&pShellLink); 

   pShellLink -> SetDescription("The GSystems Properties Control help file");

   sprintf(szHelpLocation,"%s\\winhlp32.exe",p);

   pShellLink -> SetPath(szHelpLocation);
   pShellLink -> SetArguments("Properties.hlp");
   pShellLink -> SetWorkingDirectory(szInstall);

   long k;
   if ( ! szAllUsers[0] || szAllUsers[0] == '2' )
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_PROGRAMS,0);
   else
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_COMMON_PROGRAMS,0);

   if ( ! k ) 
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_PROGRAMS,0);

   sprintf(szLinkLocation,"%s\\The GSystems Properties Component\\",szLinkRoot);

   CreateDirectory(szLinkLocation,NULL);

   strcat(szLinkLocation,"help.lnk");

   IPersistFile* pIPersistFile;
   pShellLink -> QueryInterface(IID_IPersistFile,reinterpret_cast<void**>(&pIPersistFile));

   BSTR bstrLink = SysAllocStringLen(NULL,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,szLinkLocation,-1,bstrLink,MAX_PATH);

   pIPersistFile -> Save(bstrLink,TRUE);

   SysFreeString(bstrLink);

   pIPersistFile -> Release();

   pShellLink -> Release();

   CoUninitialize();
*/
   HKEY hKey;                             
   char szTime[32];
   time_t systime;
   DWORD dwDisposition;

   RegCreateKeyEx(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate Inc.\\GSystems Properties Component",0,NULL,REG_OPTION_NON_VOLATILE,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);

   time(&systime);

   memset(szTime,0,sizeof(szTime));

   sprintf(szTime,"%ld",systime);

   RegSetValueEx(hKey,NULL,0,REG_SZ,(BYTE*)szTime,strlen(szTime));

// --------------------------

   RegCreateKeyEx(HKEY_CLASSES_ROOT,"CLSID\\{685223E5-5CF9-4d06-B744-DA4A9A777E68}",0,NULL,REG_OPTION_NON_VOLATILE,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);

   if ( REG_CREATED_NEW_KEY == dwDisposition ) {

      RegCreateKeyEx(hKey,"InProcServer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);

      char szTime[128];
      SYSTEMTIME sysTime;
      memset(&sysTime,0,sizeof(SYSTEMTIME));
      GetSystemTime(&sysTime);
      memset(szTime,0,sizeof(szTime));
      sprintf(szTime,"/%02d/%02d/%02d",sysTime.wMonth,sysTime.wDay,sysTime.wYear);
      RegSetValueEx(hKey,NULL,0,REG_SZ,(BYTE*)szTime,strlen(szTime));

   }

   RegCloseKey(hKey);

// --------------------------

   RegCreateKeyEx(HKEY_CLASSES_ROOT,"CLSID\\{685223E6-5CF9-4d06-B744-DA4A9A777E68}",0,NULL,REG_OPTION_NON_VOLATILE,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);

   if ( REG_CREATED_NEW_KEY == dwDisposition ) {

      RegCreateKeyEx(hKey,"InProcServer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_SET_VALUE,NULL,&hKey,&dwDisposition);

      char szTime[128];
      SYSTEMTIME sysTime;
      memset(&sysTime,0,sizeof(SYSTEMTIME));
      GetSystemTime(&sysTime);
      memset(szTime,0,sizeof(szTime));
      sprintf(szTime,"/%02d/%02d/%02d",sysTime.wMonth,sysTime.wDay,sysTime.wYear);
      RegSetValueEx(hKey,NULL,0,REG_SZ,(BYTE*)szTime,strlen(szTime));

   }

   RegCloseKey(hKey);

   return 1;
   }


   extern "C" UINT __stdcall ClearHelpShortcut(MSIHANDLE msiHandle) {
/*
   DWORD dWord;
   char szAllUsers[32];
   char szLinkRoot[MAX_PATH];
   char szLinkLocation[MAX_PATH];

   dWord = 32;
   MsiGetProperty(msiHandle, "ALLUSERS", szAllUsers, &dWord);

   long k;
   if ( ! szAllUsers[0] || szAllUsers[0] == '2' )
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_PROGRAMS,0);
   else
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_COMMON_PROGRAMS,0);

   if ( ! k ) 
      k = SHGetSpecialFolderPath(NULL,szLinkRoot,CSIDL_PROGRAMS,0);

   sprintf(szLinkLocation,"%s\\The GSystems Properties Component\\help.lnk",szLinkRoot);

   DeleteFile(szLinkLocation);

   sprintf(szLinkLocation,"%s\\The GSystems Properties Component",szLinkRoot);

   RemoveDirectory(szLinkLocation);
*/
   return 1;
   }
