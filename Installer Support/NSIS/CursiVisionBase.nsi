!include LogicLib.nsh

!define PRODUCT_NAME "CursiVision"
!define PRODUCT_VERSION "5"
!define PRODUCT_PUBLISHER "InnoVisioNate"
!define PRODUCT_WEB_SITE "http://www.innovisionate.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\CursiVision.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_MINOR_VERSION "0"
!define PRODUCT_MAJOR_VERSION "5"

!include MUI2.nsh
!include Library.nsh

; MUI Settings

!define MUI_ABORTWARNING
!define MUI_ICON "..\..\CursiVision\Resources\writer.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!define MUI_WELCOMEFINISHPAGE_BITMAP "..\..\CursiVision\Resources\CursiVision-Rotated.bmp"

!define MUI_TEXT_WELCOME_INFO_TEXT "$\r$\n$\r$\nThis wizard will guide you through the installation of $(^NameDA).$\r$\n$\r$\n  $\r$\n$\r$\n$\r$\n$\r$\n$_CLICK"

!define MUI_TEXT_DIRECTORY_TITLE "Please choose the installation location"

!define MUI_TEXT_DIRECTORY_SUBTITLE

!insertmacro MUI_PAGE_WELCOME

;!insertmacro MUI_PAGE_LICENSE "..\EULA.rtf"

!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES

;!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

!define MULTIUSER_EXECUTIONLEVEL Admin

!include MultiUser.nsh

Page custom setRepository "" " - Specify the CursiVision Profile Repository"

Page custom selectPad "" " - Select Your Signature Device"

!ifdef INNER

  !echo "Inner invocation"

  OutFile "$%temp%\tempinstaller.exe"

!else

  !echo "Outer invocation"

  ; Call makensis again, defining INNER.  This writes an installer for us which, when
  ; it is invoked, will just write the uninstaller to some location, and then exit.
  ; Be sure to substitute the name of this script here.

  !system "$\"${NSISDIR}\makensis$\" /DINNER ${INFILE}" = 0

  ; So now run that installer we just created as %TEMP%\tempinstaller.exe.  Since it
  ; calls quit the return value isn't zero.

  !system "$%temp%\tempinstaller.exe" = 2

  ; That will have written an uninstaller binary for us.  Now we sign it with your
  ; favourite code signing tool.

  !ifndef DONT_SIGN
  !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%temp%\CVUninst.exe"'
  !endif

  ; Good.  Now we can carry on writing the real installer.

  OutFile "${OUTFILE}"

!endif

Name "${PRODUCT_NAME}"

InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

ShowInstDetails show

ShowUnInstDetails show

SetPluginUnload  alwaysoff

Var DotNetDirectory
Var SerialNumberDialog
Var Label1
Var Label2
Var snHandle1
Var snHandle2
Var dash
Var SerialNumber1
Var SerialNumber2
Var PostInstallDLL

Function .onInit

!ifdef INNER
  WriteUninstaller "$%temp%\CVUninst.exe"
  Quit
!endif

!insertmacro MULTIUSER_INIT

!if '${ARCH}' == 'x64'

   StrCpy $INSTDIR "$PROGRAMFILES64\InnoVisioNate\CursiVision"

   SetRegView 64

!else

   ;InstallDir "$PROGRAMFILES\InnoVisioNate\CursiVision"
   StrCpy $INSTDIR "$PROGRAMFILES\InnoVisioNate\CursiVision"
   
   SetRegView 32

!endif

   SetOutPath "$INSTDIR"
  
   InitPluginsDir

   File /oname=$PLUGINSDIR\AvailablePads.ini "..\..\Installer Support\NSIS\AvailablePads.ini"
   File /oname=$PLUGINSDIR\UnsupportedPanel.bmp "..\..\Installer Support\NSIS\UnsupportedPanel.bmp"

   Return
   
FunctionEnd

Section "CursiVision" SEC01

  SetOutPath "$INSTDIR"

  SetDetailsView show

  SetOverwrite on

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\Win32\FrontEndControl.exe"

  ExecWait 'FrontEndControl.exe /uninstall "CursiVision Receptor"' $0
  
  DetailPrint "Uninstalling the Receptor service returned: $0"

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVision.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Print Document.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.ocx"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PDFiumControl.ocx"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"
  
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostScript.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Printing Support.dll"

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVisionReceptor.exe"

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\win32\convertToPDF.exe"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\win32\psSupport.dll"
  
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\EmailBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ForwardToReceptor.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ftpBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\GenericBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ImagingBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\NamingBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PrintingBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Replicator.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\SpreadSheetBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\TWAIN ImagingBackEnd.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\VideoBackEnd.dll"

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\zlibwapi.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"

!if '${CONFIGURATION}' == 'Debug'
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\*.pdb"
!endif

!if '${ARCH}' == 'x64'
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\libscriptel-proscript-windows-x64.dll"
!endif

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\libscriptel-proscript-windows-x32.dll"

  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ScriptelCV.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\signotecCV.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\STPadLib.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\WacomCV.dll"
  File "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\WinTabletCV.dll"

!ifndef DONT_SIGN

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD}  "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\Win32\FrontEndControl.exe"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVision.exe"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Print Document.exe"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.ocx"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PDFiumControl.ocx"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostScript.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Printing Support.dll"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVisionReceptor.exe"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\win32\convertToPDF.exe"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\win32\psSupport.dll"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\EmailBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ForwardToReceptor.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ftpBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\GenericBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ImagingBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\NamingBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\PrintingBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\Replicator.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\SpreadSheetBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\TWAIN ImagingBackEnd.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\VideoBackEnd.dll"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"'

   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\ScriptelCV.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\signotecCV.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\STPadLib.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\WacomCV.dll"'
   !system 'signtool sign /tr "http://timestamp.digicert.com" /f "${CERTIFICATE}" /p ${CERTPASSWORD} "$%GSYSTEM_HOME%\Common\Artifacts\${CONFIGURATION}\${ARCH}\WinTabletCV.dll"'

!endif
  
  ExecWait 'cursivision /regcomponents'
  
  ExecWait 'FrontEndControl /install CursiVisionReceptor.exe "CursiVision Receptor"  PDF 17638'
  
  File "GlobalDataStore.ini"
  File "PhabletInstaller1.ini"
  File "PhabletInstaller2.ini"

  SetShellVarContext all

  SetOutPath "$INSTDIR\Help"
  
  SetOverwrite ifnewer

  SetOutPath "$INSTDIR\Print Driver"
  
  SetOverwrite ifnewer

  File /r "..\..\CursiVisionPrintDriver\dist\*.*"
  
  CreateDirectory "$APPDATA\CursiVision"
  
  SetOutPath "$INSTDIR"

  ExecWait 'cursivision /grantfullaccess "$APPDATA\CursiVision"' $0

  DetailPrint "Granting access to the data directory returned: $0"
  
  ${If} $0 != 0

     MessageBox MB_ICONEXCLAMATION "There may have been a problem granting user's access to the directory '$APPDATA\CursiVision'. It is recommended that you provide user's access to this location."

  ${EndIf}

  SetOutPath "$APPDATA\CursiVision"

  SetOverwrite ifnewer

  File /nonfatal /x Thumbs.db /x *TabletPC.* "..\..\CursiVision Distribution Files\*.*"
  
;
; Note: If the appropriate of the following files don't exist, you may need to create the
; installer for the Phablet-API first.
;
; Look in: GSYSTEM_HOME\Phablet-API\Installer Support and run pkDevice.nsi
;

!if '${CONFIGURATION}' == 'Debug'
  File "$%GSYSTEM_HOME%\Phablet-API\Installer Support\pkDeviceDebug.exe"
!else
  File "$%GSYSTEM_HOME%\Phablet-API\Installer Support\pkDevice.exe"
!endif

  File "$%GSYSTEM_HOME%\Phablet-API\PhabletSignaturePad\app\build\outputs\apk\release\PhabletSignaturePad.apk"
  File "$%GSYSTEM_HOME%\Phablet-API\PadKillerAPI\Installer Support\pkAPI.exe"
  File "$%GSYSTEM_HOME%\Phablet-API\PadKillerAPI\Installer Support\pkAPI64.exe"

  CreateDirectory "$APPDATA\CursiVision\Printed Documents"
  CreateDirectory "$APPDATA\CursiVision\Printing Profiles"
  CreateDirectory "$APPDATA\CursiVision\Settings"
  CreateDirectory "$APPDATA\CursiVision\Signed Printed Documents"

  CreateDirectory "$DOCUMENTS\CursiVision Files"
  
  SetOutPath "$INSTDIR"

  ExecWait 'cursivision /installprinter "$INSTDIR"' $0

  DetailPrint "Installing the CursiVision Printer returned: $0"
  
  ${If} $0 != 0

     MessageBox MB_ICONEXCLAMATION "The CursiVision Print Driver may not have installed correctly. The document: 'Installing the CursiVision Print Driver.pdf' has been placed on your desktop"

     SetOutPath "$DESKTOP"

     SetOverwrite ifnewer

     File "..\..\Installer Support\Installing the CursiVision Print Driver.pdf"

${EndIf}

  CreateDirectory "$SMPROGRAMS\CursiVision"

  CreateShortCut "$SMPROGRAMS\CursiVision\CursiVision.lnk" "$INSTDIR\CursiVision.exe"

  CreateShortCut "$DESKTOP\CursiVision.lnk" "$INSTDIR\CursiVision.exe"

  SetOutPath "$INSTDIR"
  
  ExecWait 'cursivision /fixuplinks "$APPDATA\CursiVision" "CursiVision"' $0

  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd

Function selectPad

!define HWND_TOP 0
!define SWP_NOSIZE 1
!define SWP_NOMOVE 2
!define SWP_NOACTIVATE 16

   Push $R0
   Push $R1
   Push $R2
   
   GetDlgItem $1 $HWNDPARENT 1

   ${NSD_SetText} $1 "Next"

   InstallOptions::initDialog "$PLUGINSDIR\AvailablePads.ini"

   Pop $R0
   
   GetDlgItem $R1 $R0 1212 ;1200 + Field number - 1
   
   System::Call 'user32::SetWindowPos(i $R1, i ${HWND_TOP}, i 60, i 65, i 0, i 0, i ${SWP_NOSIZE}|${SWP_NOACTIVATE})'

   GetDlgItem $R1 $R0 1213 ;1200 + Field number - 1

   System::Call 'user32::SetWindowPos(i $R1, i ${HWND_TOP}, i 0, i 0, i 0, i 0, i ${SWP_NOSIZE}|${SWP_NOMOVE}|${SWP_NOACTIVATE})'
   
   SetCtlColors $R1 0x00000000 0x00FFFFFF

   InstallOptions::show

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePads.ini" "Field 3" "State"
   ${If} $R0 == "1"
      ExecWait 'regsvr32 /s "$INSTDIR\WinTabletCV.dll"'
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePads.ini" "Field 4" "State"
   ${If} $R0 == "1"
      ExecWait 'regsvr32 /s "$INSTDIR\ScriptelCV.dll"'
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePads.ini" "Field 5" "State"
   ${If} $R0 == "1"
      ExecWait 'regsvr32 /s "$INSTDIR\WacomCV.dll"'
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePads.ini" "Field 6" "State"
   ${If} $R0 == "1"
      ExecWait 'regsvr32 /s "$INSTDIR\signotecCV.dll"'
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePads.ini" "Field 7" "State"
   ${If} $R0 == "1"
      ExecWait 'regsvr32 /s "$INSTDIR\pkAPI.ocx"'
      ExecWait 'regsvr32 /s "$INSTDIR\pkAPI32.ocx"'
      MessageBox MB_OK "The Phablet Signature Pad device has been chosen. The system will now run the installer that will allow the use of that device on this computer"
!if '${CONFIGURATION}' == 'Debug'
      ExecWait '"$APPDATA\CursiVision\pkDeviceDebug.exe"'
!else
      ExecWait '"$APPDATA\CursiVision\pkDevice.exe"'
!endif
   ${EndIf}

   GetDlgItem $1 $HWNDPARENT 1
   ${NSD_SetText} $1 "More"
   InstallOptions::dialog "$INSTDIR\PhabletInstaller1.ini"
   ${NSD_SetText} $1 "Finish"
   InstallOptions::dialog "$INSTDIR\PhabletInstaller2.ini"

  Delete "$PLUGINSDIR\PhabletInstaller1.ini"
  Delete "$PLUGINSDIR\PhabletInstaller2.ini"

  Delete "$PLUGINSDIR\AvailablePads.ini"
  Delete "$PLUGINSDIR\UnsupportedPanel.bmp"

FunctionEnd

Function setRepository

   Push $R0

   InstallOptions::dialog "$INSTDIR\GlobalDataStore.ini"
   
   Pop $R0
   ReadINIStr $R0 "$INSTDIR\GlobalDataStore.ini" "Field 3" "State"
   
   WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Global Settings Store" $R0

FunctionEnd

Section -AdditionalIcons
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateShortCut "$SMPROGRAMS\CursiVision\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\CursiVision\Uninstall.lnk" "$INSTDIR\CVuninst.exe"
SectionEnd

Section -Post

!ifndef INNER
  SetOutPath $INSTDIR
  File "$%temp%\CVUninst.exe"
  !system 'del "$%temp%\CVUninst.exe"'
  !system 'del "$%temp%\tempinstaller.exe"'
!endif

  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\CursiVision.exe"

  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\CVuninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\CursiVision.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMajor" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMinor" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MajorVersion" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MinorVersion" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation" "$INSTDIR"
  
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Installation Directory" "$INSTDIR"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Printed Documents Directory" "$APPDATA\CursiVision\Printed Documents"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Printing Profiles Directory" "$APPDATA\CursiVision\Printing Profiles"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Signed Printed Documents Directory" "$APPDATA\CursiVision\Signed Printed Documents"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Print Driver Target" "CursiVision"
  
SectionEnd

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) has been removed."
FunctionEnd


Function un.onInit
!insertmacro MULTIUSER_UNINIT
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Do you want to remove $(^Name) ?" IDYES +2
  Abort
FunctionEnd

!ifdef INNER

Section Uninstall

  SetOutPath "$INSTDIR"

  ExecWait 'FrontEndControl /uninstall "CursiVision Receptor"'

  ExecWait 'cursivision /unregcomponents'

  Delete "$INSTDIR\help\*.*"
  Delete "$INSTDIR\Print Driver\*.*"
  Delete "$INSTDIR\Print Driver\amd64\*.*"
  Delete "$INSTDIR\Print Driver\i386\*.*"
  
  RMDir "$INSTDIR\Print Driver\amd64"
  RMDir "$INSTDIR\Print Driver\i386"
  RMDir "$INSTDIR\Print Driver"
  RMDIR "$INSTDIR\help"

  Delete "$INSTDIR\*.*"
  
  RMDir "$SMPROGRAMS\CursiVision"
  
  RMDir "$INSTDIR"

  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT.NTF'
  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT.HLP'
  Delete '$SYSDIR\spool\drivers\x64\PS_SCHM.GDL'
  Delete '$SYSDIR\spool\drivers\x64\PS5UI.DLL'
  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT5.DLL'
  Delete '$SYSDIR\spool\drivers\x64\CursiVisionPrinter.ini'
  Delete '$SYSDIR\spool\drivers\x64\CursiVisionPrinter.dll'
  Delete '$SYSDIR\spool\drivers\x64\CursiVision.ppd'

  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT.NTF'
  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT.HLP'
  Delete '$SYSDIR\spool\drivers\W32X86\PS_SCHM.GDL'
  Delete '$SYSDIR\spool\drivers\W32X86\PS5UI.DLL'
  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT5.DLL'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVisionPrinter.ini'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVisionPrinter.dll'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVision.ppd'
  
  Delete "$DESKTOP\Installing the CursiVision Print Driver.pdf"
  
  Delete "$DESKTOP\CursiVision.lnk"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"

  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"

  DeleteRegKey HKLM "Software\InnoVisioNate\CursiVision"

  SetAutoClose true

  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd

!endif
