
;!define DONT_SIGN

;!define IS_EVALUATION
!define OUTFILE CursiVisionDebug.exe
;!define OUTFILE CursiVisionSetup.exe

!define ARCH x64

!define CONFIGURATION Release

!define PRODUCT_NAME "CursiVision"
!define PRODUCT_VERSION "5"
!define PRODUCT_PUBLISHER "InnoVisioNate"
!define PRODUCT_WEB_SITE "http://www.innovisionate.com"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\CursiVision.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_MINOR_VERSION "0"
!define PRODUCT_MAJOR_VERSION "5"

!include "MUI2.nsh"
!include "Library.nsh"

; MUI Settings

!define MUI_ABORTWARNING
!define MUI_ICON "..\..\writer.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!define MUI_WELCOMEFINISHPAGE_BITMAP "..\..\Resources\CursiVision-Rotated.bmp"

!define MUI_TEXT_WELCOME_INFO_TEXT "$\r$\n$\r$\nThis wizard will guide you through the installation of $(^NameDA).$\r$\n$\r$\n  $\r$\n$\r$\n$\r$\n$\r$\n$_CLICK"

!define MUI_TEXT_DIRECTORY_TITLE "Please choose the installation location"

!define MUI_TEXT_DIRECTORY_SUBTITLE

!insertmacro MUI_PAGE_WELCOME

!insertmacro MUI_PAGE_LICENSE "..\EULA.rtf"

!ifndef IS_EVALUATION

Page custom enterSerialNumber enterSerialNumberLeave

!endif

!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES

;!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

!define MULTIUSER_EXECUTIONLEVEL Admin

!include MultiUser.nsh

!ifndef IS_EVALUATION

Page custom setRepository "" " - Specify the CursiVision Profile Repository"

!endif

Page custom selectPad "" " - Select Your Signature Device"

!ifdef INNER

  !echo "Inner invocation"

  OutFile "D:\temp\tempinstaller.exe"

!else

  !echo "Outer invocation"

  ; Call makensis again, defining INNER.  This writes an installer for us which, when
  ; it is invoked, will just write the uninstaller to some location, and then exit.
  ; Be sure to substitute the name of this script here.

  !system "$\"${NSISDIR}\makensis$\" /DINNER CursiVision.nsi" = 0

  ; So now run that installer we just created as %TEMP%\tempinstaller.exe.  Since it
  ; calls quit the return value isn't zero.

  !system "D:\temp\tempinstaller.exe" = 2

  ; That will have written an uninstaller binary for us.  Now we sign it with your
  ; favourite code signing tool.

  !ifndef DONT_SIGN
  !system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "D:\temp\CVUninst.exe"'
  !endif
  
  ; Good.  Now we can carry on writing the real installer.

  OutFile "${OUTFILE}"

!endif

Name "${PRODUCT_NAME}"

InstallDir "$PROGRAMFILES\InnoVisioNate\CursiVision"

InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""

ShowInstDetails show

ShowUnInstDetails show

SetPluginUnload  alwaysoff

Var DotNetDirectory
Var SerialNumberDialog
Var Label1
Var Label2
Var snHandle1
Var snHandle2
Var dash
Var SerialNumber1
Var SerialNumber2
Var PostInstallDLL

Function .onInit

!ifdef INNER
  WriteUninstaller "D:\temp\CVUninst.exe"
  Quit
!endif

!insertmacro MULTIUSER_INIT

${If} 'x64' == '${ARCH}'

   StrCpy $INSTDIR "$PROGRAMFILES64\InnoVisioNate\CursiVision"

   StrCpy $PostInstallDLL "PostInstall32"

${Else}

   strCpy $PostInstallDLL "PostInstall"
   
${EndIf}

   SetOutPath "$INSTDIR"
  
   InitPluginsDir

   File /oname=$PLUGINSDIR\AvailablePadsPhabletOnly.ini "..\..\Installer Support\NSIS\AvailablePadsPhabletOnly.ini"
   File /oname=$PLUGINSDIR\UnsupportedPanel.bmp "..\..\Installer Support\NSIS\UnsupportedPanel.bmp"

   Return
   
FunctionEnd

!ifndef IS_EVALUATION

Function enterSerialNumber

nsDialogs::Create /NOUNLOAD 1018

Pop $SerialNumberDialog

GetDlgItem $1 $HWNDPARENT 1
EnableWindow $1 0

${NSD_CreateLabel} 8u 8u 90% 12u "Please enter your serial number and click Next."
Pop $Label1

${NSD_CreateLabel} 8u 20u 90% 12u "You can find this number in the e-mail sent to you with download instructions."
Pop $Label2

${NSD_CreateText}  12u 36u 22u 12u "$SerialNumber1"
Pop $snHandle1
${NSD_OnChange} $snHandle1 snText1Changed

${NSD_CreateLabel} 35u 37u 2u 8u "-"
Pop $dash

${NSD_CreateNumber}  38u 36u 32u 12u "$SerialNumber2"
Pop $snHandle2
${NSD_OnChange} $snHandle2 snText2Changed

${NSD_SetFocus} $snHandle1

nsDialogs::Show

FunctionEnd

Function enterSerialNumberLeave

${NSD_GetText} $snHandle1 $SerialNumber1
${NSD_GetText} $snHandle2 $SerialNumber2

WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "License" "$SerialNumber1-$SerialNumber2"

FunctionEnd

Function snText1Changed

Pop $1

${NSD_GetText} $snHandle1 $SerialNumber1
${NSD_GetText} $snHandle2 $SerialNumber2

System::Call '$PostInstallDLL::isPrinterInstalled2(t "$SerialNumber1", t "$SerialNumber2") i .r1'

${If} 1 == $1
   GetDlgItem $1 $HWNDPARENT 1
   EnableWindow $1 1
   return
${EndIf}

StrLen $0 $SerialNumber1

${If} 3 == $0
   ${NSD_SetText} $snHandle2 ""
   ${NSD_SetFocus} $snHandle2
${EndIf}

FunctionEnd

Function snText2Changed

Pop $1

${NSD_GetText} $snHandle1 $SerialNumber1
${NSD_GetText} $snHandle2 $SerialNumber2

System::Call '$PostInstallDLL::isPrinterInstalled2(t "$SerialNumber1", t "$SerialNumber2") i .r1'

${If} 1 == $1
   GetDlgItem $1 $HWNDPARENT 1
   EnableWindow $1 1
   return
${EndIf}

System::Call '$PostInstallDLL::isPrinterInstalled(t "$SerialNumber1-$SerialNumber2") i .r1'

${If} 1 == $1
   GetDlgItem $1 $HWNDPARENT 1
   EnableWindow $1 1
   return
${EndIf}

StrLen $0 $SerialNumber2

${If} 6 == $0
   ${NSD_SetText} $snHandle1 ""
   ${NSD_SetText} $snHandle2 ""
   ${NSD_SetFocus} $snHandle1
${EndIf}

FunctionEnd

!endif

Section "CursiVision" SEC01

  SetOutPath "$INSTDIR"

  SetDetailsView show

  SetOverwrite on

!ifdef INNER
!ifndef DONT_SIGN
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVision.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVisionReceptor.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Print Document.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PDFiumControl.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\EmailBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ForwardToReceptor.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ftpBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\GenericBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ImagingBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\NamingBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall32.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostScript.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Printing Support.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PrintingBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Replicator.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\SpreadSheetBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\TWAIN ImagingBackEnd.dll"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\VideoBackEnd.dll"'
  
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\win32\convertToPDF.exe"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\win32\psSupport.dll"'

!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"'
!system 'signtool sign /tr "http://timestamp.digicert.com" /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"'

!endif
!endif

  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVision.exe"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\CursiVisionReceptor.exe"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Print Document.exe"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.ocx"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PDFiumControl.ocx"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Properties.ocx"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\EmailBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ForwardToReceptor.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ftpBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\GenericBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\ImagingBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\NamingBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pdfEnabler.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall32.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostScript.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Printing Support.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PrintingBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\Replicator.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\SpreadSheetBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\TWAIN ImagingBackEnd.dll"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\VideoBackEnd.dll"

  File "\Common\Artifacts\${CONFIGURATION}\win32\convertToPDF.exe"
  File "\Common\Artifacts\${CONFIGURATION}\win32\psSupport.dll"

  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI.ocx"
  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\pkAPI32.ocx"

  File "\Common\Artifacts\${CONFIGURATION}\${ARCH}\PostInstall*.dll"

  ;File "\CursiVision-Signature-Capture-Devices\Supporting\*.*"
  
  ${If} 'x64' == '${ARCH}'
  ExecWait 'cursivision /regcomponents'
  ${EndIf}
  
  System::Call '$PostInstallDLL::registerAll(t "$INSTDIR") i .r1 ? u'
  
  File "..\..\Installer Support\NSIS\GlobalDataStore.ini"
  ;File "..\..\Installer Support\NSIS\AvailablePadsPhabletOnly.ini"
  File "..\..\Installer Support\NSIS\PhabletInstaller1.ini"
  File "..\..\Installer Support\NSIS\PhabletInstaller2.ini"
  ;File "..\..\Installer Support\NSIS\UnsupportedPanel.bmp"

  SetShellVarContext all

  ;System::Call '$PostInstallDLL::getRegAsmLocation(t "v3.5") t .r1'

  ;StrCpy $DotNetDirectory $1

  ;SetOutPath "$DotNetDirectory"

  ;ExecWait 'RegAsm.exe /nologo /silent /codebase "$INSTDIR\STActiveX.dll"'
  ;ExecWait 'RegAsm.exe /nologo /silent /codebase "$INSTDIR\STFamily.dll"'

  SetOutPath "$INSTDIR\Help"
  
  SetOverwrite ifnewer
  
  File "D:\CursiVision help\CursiVision.chm"
  
  SetOutPath "$INSTDIR\Print Driver"
  
  SetOverwrite ifnewer
  
!ifdef INNER
!ifndef DONT_SIGN
!system '"D:\InnoVisioNate\Code Signing Certificate\Sign Print Driver.cmd"'
!endif
!endif

  File /r "D:\CursiVisionPrintDriver\dist\*.*"
  
  CreateDirectory "$APPDATA\CursiVision"
  
  SetOutPath "$INSTDIR"

  System::Call '$PostInstallDLL::GrantFullAccess(t "$APPDATA\CursiVision") i .r1'

  SetOutPath "$APPDATA\CursiVision"

  SetOverwrite ifnewer

  File /nonfatal /x Thumbs.db /x *TabletPC.* "\CursiVision Distribution Files\*.*"

!ifdef INNER
!ifndef DONT_SIGN
!system 'signtool sign /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "D:\Phablet-API\Installer Support\pkDevice.exe"'
!system 'signtool sign /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "D:\Phablet-API\PadKillerAPI\Installer Support\pkAPI.exe"'
!system 'signtool sign /f "D:\InnoVisioNate\Code Signing Certificate\Nathan Clark.pfx" /p pubfly "D:\Phablet-API\PadKillerAPI\Installer Support\pkAPI64.exe"'
!endif
!endif

  File "D:\Phablet-API\Installer Support\pkDevice.exe"
  File "D:\Phablet-API\PhabletSignaturePad\app\build\outputs\apk\PhabletSignaturePad.apk"
  File "D:\Phablet-API\PadKillerAPI\Installer Support\pkAPI.exe"
  File "D:\Phablet-API\PadKillerAPI\Installer Support\pkAPI64.exe"

  CreateDirectory "$APPDATA\CursiVision\Printed Documents"
  CreateDirectory "$APPDATA\CursiVision\Printing Profiles"
  CreateDirectory "$APPDATA\CursiVision\Settings"
  CreateDirectory "$APPDATA\CursiVision\Signed Printed Documents"

  CreateDirectory "$DOCUMENTS\CursiVision Files"

  SetOutPath "$DESKTOP"
  
  SetOverwrite ifnewer
  
  File "..\..\Installer Support\Installing the CursiVision Print Driver.pdf"
  
  SetOutPath "$INSTDIR"
  
  System::Call '$PostInstallDLL::InstallPrinter(t "$INSTDIR") i .r1'

  ${If} $1 == 1

     SetOutPath "$DESKTOP"

     Delete "Installing the CursiVision Print Driver.pdf"

  ${EndIf}

  CreateDirectory "$SMPROGRAMS\CursiVision"
  CreateShortCut "$SMPROGRAMS\CursiVision\CursiVision.lnk" "$INSTDIR\CursiVision.exe"

  CreateShortCut "$DESKTOP\CursiVision.lnk" "$INSTDIR\CursiVision.exe"

  SetOutPath "$INSTDIR"
  
  System::Call '$PostInstallDLL::fixupLinks(t "$APPDATA\CursiVision", t "CursiVision") i .r1'

  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd

Function selectPad

!define HWND_TOP 0
!define SWP_NOSIZE 1
!define SWP_NOMOVE 2
!define SWP_NOACTIVATE 16

   Push $R0
   Push $R1
   Push $R2
   
   GetDlgItem $1 $HWNDPARENT 1

   ${NSD_SetText} $1 "Next"

   InstallOptions::initDialog "$PLUGINSDIR\AvailablePadsPhabletOnly.ini"

   Pop $R0
   
   GetDlgItem $R1 $R0 1212 ;1200 + Field number - 1
   
   System::Call 'user32::SetWindowPos(i $R1, i ${HWND_TOP}, i 60, i 65, i 0, i 0, i ${SWP_NOSIZE}|${SWP_NOACTIVATE})'

   GetDlgItem $R1 $R0 1213 ;1200 + Field number - 1

   System::Call 'user32::SetWindowPos(i $R1, i ${HWND_TOP}, i 0, i 0, i 0, i 0, i ${SWP_NOSIZE}|${SWP_NOMOVE}|${SWP_NOACTIVATE})'
   
   SetCtlColors $R1 0x00000000 0x00FFFFFF

   InstallOptions::show

!ifdef IS_EVALUATION

   ;RegDll "$INSTDIR\pkAPI.ocx"
   ;RegDll "$INSTDIR\pkAPI32.ocx"
   GetDlgItem $1 $HWNDPARENT 1
   ${NSD_SetText} $1 "More"
   InstallOptions::dialog "$INSTDIR\PhabletInstaller1.ini"
   ${NSD_SetText} $1 "Finish"
   InstallOptions::dialog "$INSTDIR\PhabletInstaller2.ini"

!else

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePadsPhabletOnly.ini" "Field 3" "State"
   ${If} $R0 == "1"
      RegDll "$INSTDIR\TabletPC.ocx"
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePadsPhabletOnly.ini" "Field 4" "State"
   ${If} $R0 == "1"
      RegDll "$INSTDIR\ScriptelST1500.ocx"
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePadsPhabletOnly.ini" "Field 5" "State"
   ${If} $R0 == "1"
      RegDll "$INSTDIR\WacomCV.ocx"
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$PLUGINSDIR\AvailablePadsPhabletOnly.ini" "Field 6" "State"
   ${If} $R0 == "1"
      RegDll "$INSTDIR\signotecCV.ocx"
   ${EndIf}

   Pop $R0
   ReadINIStr $R0 "$INSTDIR\AvailablePads.ini" "Field 7" "State"
   ${If} $R0 == "1"
      RegDll "$INSTDIR\pkAPI.ocx"
      RegDll "$INSTDIR\pkAPI32.ocx"
      GetDlgItem $1 $HWNDPARENT 1
      ${NSD_SetText} $1 "More"
      InstallOptions::dialog "$INSTDIR\PhabletInstaller1.ini"
      ${NSD_SetText} $1 "Finish"
      InstallOptions::dialog "$INSTDIR\PhabletInstaller2.ini"
   ${EndIf}

!endif

  Delete "$INSTDIR\PhabletInstaller1.ini"
  Delete "$INSTDIR\PhabletInstaller2.ini"

  Delete "$PLUGINSDIR\AvailablePadsPhabletOnly.ini"
  Delete "$PLUGINSDIR\UnsupportedPanel.bmp"

FunctionEnd

!ifndef IS_EVALUATION

Function setRepository

   Push $R0

   InstallOptions::dialog "$INSTDIR\GlobalDataStore.ini"
   
   Pop $R0
   ReadINIStr $R0 "$INSTDIR\GlobalDataStore.ini" "Field 3" "State"
   
   WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Global Settings Store" $R0

FunctionEnd

!endif

Section -AdditionalIcons
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateShortCut "$SMPROGRAMS\CursiVision\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\CursiVision\Uninstall.lnk" "$INSTDIR\CVuninst.exe"
SectionEnd

Section -Post

!ifndef INNER
  SetOutPath $INSTDIR
  File "D:\temp\CVUninst.exe"
  !system 'del "D:\temp\CVUninst.exe"'
  !system 'del "D:\temp\tempinstaller.exe"'
!endif

  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\CursiVision.exe"

  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\CVuninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\CursiVision.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMajor" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "VersionMinor" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MajorVersion" "${PRODUCT_MAJOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "MinorVersion" "${PRODUCT_MINOR_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "InstallLocation" "$INSTDIR"
  
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Installation Directory" "$INSTDIR"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Printed Documents Directory" "$APPDATA\CursiVision\Printed Documents"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Printing Profiles Directory" "$APPDATA\CursiVision\Printing Profiles"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Signed Printed Documents Directory" "$APPDATA\CursiVision\Signed Printed Documents"
  WriteRegStr HKLM "Software\InnoVisioNate\CursiVision" "Print Driver Target" "CursiVision"
  
SectionEnd

; Section descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) has been removed."
FunctionEnd


Function un.onInit
!insertmacro MULTIUSER_UNINIT
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Do you want to remove $(^Name) ?" IDYES +2
  Abort
FunctionEnd

!ifdef INNER
Section Uninstall

  SetOutPath "$INSTDIR"

${If} 'x64' == '${ARCH}'
  ExecWait 'cursivision /unregcomponents'
${EndIf}

  System::Call '$PostInstallDLL::unRegisterAll(t "$INSTDIR") i .r1 ? u'

  Delete "$INSTDIR\help\*.*"
  Delete "$INSTDIR\Print Driver\*.*"
  Delete "$INSTDIR\Print Driver\amd64\*.*"
  Delete "$INSTDIR\Print Driver\i386\*.*"
  
  RMDir "$INSTDIR\Print Driver\amd64"
  RMDir "$INSTDIR\Print Driver\i386"
  RMDir "$INSTDIR\Print Driver"
  RMDIR "$INSTDIR\help"

  Delete "$INSTDIR\*.*"
  
  RMDir "$SMPROGRAMS\CursiVision"
  
  RMDir "$INSTDIR"

  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT.NTF'
  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT.HLP'
  Delete '$SYSDIR\spool\drivers\x64\PS_SCHM.GDL'
  Delete '$SYSDIR\spool\drivers\x64\PS5UI.DLL'
  Delete '$SYSDIR\spool\drivers\x64\PSCRIPT5.DLL'
  Delete '$SYSDIR\spool\drivers\x64\CursiVisionPrinter.ini'
  Delete '$SYSDIR\spool\drivers\x64\CursiVisionPrinter.dll'
  Delete '$SYSDIR\spool\drivers\x64\CursiVision.ppd'

  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT.NTF'
  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT.HLP'
  Delete '$SYSDIR\spool\drivers\W32X86\PS_SCHM.GDL'
  Delete '$SYSDIR\spool\drivers\W32X86\PS5UI.DLL'
  Delete '$SYSDIR\spool\drivers\W32X86\PSCRIPT5.DLL'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVisionPrinter.ini'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVisionPrinter.dll'
  Delete '$SYSDIR\spool\drivers\W32X86\CursiVision.ppd'
  
  Delete "$DESKTOP\Installing the CursiVision Print Driver.pdf"
  
  Delete "$DESKTOP\CursiVision.lnk"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"

  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"

  DeleteRegKey HKLM "Software\InnoVisioNate\CursiVision"

  SetAutoClose true

  System::Call 'Shell32::SHChangeNotify(i 0x8000000, i 0, i 0, i 0)'

SectionEnd
!endif