
!verbose 4

!define DONT_SIGN

!define INFILE CursiVisionRelease32.nsi

!define OUTFILE CursiVisionSetup32.exe

!define ARCH Win32

!define CONFIGURATION Release

!include "CursiVisionBase.nsi"
