
!verbose 4

!define DONT_SIGN

!define INFILE CursiVisionRelease.nsi

!define OUTFILE CursiVisionSetup.exe

!define ARCH x64

!define CONFIGURATION Release

!include "CursiVisionBase.nsi"
