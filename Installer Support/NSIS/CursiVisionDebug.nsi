
!verbose 4

!define DONT_SIGN

!define INFILE CursiVisionDebug.nsi

!define OUTFILE CursiVisionDebug.exe

!define ARCH x64

!define CONFIGURATION Debug

!include "CursiVisionBase.nsi"
