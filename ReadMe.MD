# The InnoVisioNate CursiVision System

## Without Unecessary ado - here are the windows installers:

![Pre-built windows x64 installer](Installer%20Support/NSIS/CursiVisionSetup.exe)

If you want a 32bit installer, it is: ![Windows Win32 installer](Installer%20Support/NSIS/CursiVisionSetup32.exe)

To build the system, please refer to ![Build.md](Build.md).

## Powerful and full featured electronic signature capture and document management
----

![](images/Slide1.PNG)

## Automated repeatable processes

<p>
This system is really focused on <i>automation</i>. I developed the technology where document <i>types</i> can be accurately identified.
By "type", I mean a document you might print 100 times every day, but each time with different data. For example, an invoice. CursiVision can recognize invoices
because you can tell it to do so by finding the word "Invoice #" in a certain place, even when the customer data and large parts of each instance of the 
document will be different.
</p>
<p>
Then, stored by that recognized document type is the entire process desired for that type. 
For example, gathering any number of signatures on any page, then post processing the document with any combination of a dozen or so "post" signing activities.
See the <a href="https://github.com/ntclark/CursiVision-Toolbox">CursiVision Toolbox</a> repository, it contains all of the currently developed tools, or, as they are referred to in CursiVision, 
"Back-End" processes.
Indeed, the system does not <i>have</i> to collect signatures at all, perhaps you just want to create a document processing flow of some type. The configurable toolbox 
objects are the way to achieve this.
</p>

<p>
After the initially defined processes are in place, anytime that "type" of document is printed - the whole thing happens automatically, 
without even a mouse click required of any user at the computer. Indeed, since one of the post processing steps could be to forward the 
document to a special cursivision "listener" running on any PC in the world, the entire chain of events itself can be distributed.
</p>

<p>
Imagine Bob in accounting in California, and Mary in New York wants to initiate a purchase. She simply prints the requisition form, any of a 
number of things happen to it there, possibly e-mailing a notice to recieving to keep any eye out for the shipment. After that it automatically 
forwards to Bob in CA, where the document simply appears on his screen, he signs it, and off it goes automatically to the next step.
</p>

<p>
This post processing architecture is the true power of the system. With it, you can do literally anything your organization might need for any 
document management system you might want to build out of it.
</p>

<p>
These processing tools contain the ability to actually use content from every instance of the document type as you see fit. This is hard to picture, 
but absolutely key to the power of the tool, as in the below image, for example, note that field values from this document can be used in a huge number of ways.
</p>

<p>
It is extremely easy to create these post processing steps, any one of the existing ones can be used as a framework for a new tool. Development takes 
but a few short source files and can be done in any language, in any case, C++, C#, and Visual Basic, generally anything that can be used to create a 
COM object that CursiVision can automatically insert into it's toolbox chain.
</p>

![](images/Slide2.PNG)

## Beginning with Signature Capture, evolving to configurable processes

<p>
The system started out as a tool to embed electronic signatures into a soft copy of documents. In fact, it was initially intended for the hotel industry.
I was told about a hotel management software company whose customers demanding electronic signature capture numbered nearly 15 thousand worldwide, and that
said software company would love to use such a system to offer these customers.
</p>

<p>With that in mind, I began development of CursiVision in March of 2009. By the end of that year, I had a viable rock solid solution that could be integrated 
with that management software, and, for that matter, into <strong>any</strong> business system or process on the planet; simply by printing the document out of those
systems. Instant integration doing nothing different than what they had always been doing!</p>

<p>Upon the opportunity to have that management software company test CursiVision, so that they could then turn around and license it to their customers for an outragious sum, 
They requested $7500 of me, before they would even check it out, in order to compensate their engineers for a whole 3 days of testing time!
</p>

<p>So, to hell with them anyway. After that I became more determined that the software would be what I term "off the shelf vertical". In otherwords, every bit as 
industry specific that a particular market segment might want (i.e. hotel management with registration documents), while being so <strong>across</strong> all 
industries. All by offering a strong foundation for easy, seamless, and complete integration of ANY tool someone could dream up, and make the development of 
those tools as easy as they could possibly be.
</p>

<p>And perhaps most importantly - what I wanted most of all was to get the best of what I had done out there to the most people possible at a <i>fair and equitable</i> price.
I have <strong>always</strong> been apalled at the prices charged for software, especially vertical software, when all you get is a buggy stinking pile of crap that is
hard to use, does only a half-assed job for it's assigned tasks, and takes forever and a day to get a necessary change in place (come to think of it, most off the shelf
software is like that too). I wanted to demonstrate that it <strong>is</strong> possible to create defect free powerful software that <strong>does</strong> what it is
intended to do very very well, yet has an eye to the future by providing a barrier free path to it.</p>

<p>
At every step of the way, the industry itself, i.e., signature capture pad manufacturers and resellers, tried their damndest to prevent me from doing this, believe it or not. 
For a long time I thought they were just too myopic to see the possibilities. I could not convey my vision to them and when I tried, they just would not listen. 
It was insane, I had put every fiber of my being into something that could only drive sales much higher, I truly never asked for much more than just let it be my best work
that gets out there - and we'll improve it from then forward. But instead, they want me to provide a "cut-down" version and "we'll upcharge" for the good stuff. AACH!
</p>

<p>
Now you know the driving impetus behind the development of the <a href="https://github.com/ntclark/Phablet-API">Phablet Signature Pad</a>, which is signature capture 
hardware (and, by the way, <strong>much</strong> more!) using the ubiquitous Android tablet (or directly on Windows PC) available for cheap anywhere and which works 
<strong>better</strong> than <i>any</i> signature capture hardware, even ones costing $900. Indeed, I refer to the software in that repository as "PadKiller". 
Even if the pad companies don't want to bring affordable signature capture within reach, let alone care even a little bit about document management, I sure as hell do.
</p>

#### I am very much looking forward to the kind of unique and powerful document processing steps this community is going to foster

## Other things that came out of the development of CursiVision

<p>
In February of 2009, I was asked to provide an estimate of the amount of development time I thought might be required to develop some sort of signature capture
solution. Presumably as I might be hired on a contract basis to do so.
</p>

<p>
I spent about a month looking into that. In fact, I was just looking into (and of course writing) what would be involved in the PDF document manipulation required 
for embedding the signature. I was really quite impressed by the complexity of PDF and instinctively knew that this was not something to create as a "work for hire". 
Rather, this would be an opportunity to create a software product, a goal I have <i>always</i> had. I knew that I would never have the flexibility to go the distance 
with it and would never allow anyone the power to limit my design and features simply on the basis of cost. I turned down any offer to do it for hire, and went 
full bore with this as my product.
</p>

### ![PDFEnabler](../../../PDFEnabler)

<p>Through this initial PDF investigation came the first significant artifact out of this endeavor. The PDFEnabler component is the result of that early work on PDF manipulation. 
</p>

<p>
Did I mention - this <strong>entire</strong> system is highly COM (Component Object Model) centric. The COM integration foundation of this system is extremely 
powerful and all of the separate components, I haven't counted, but there are probably 30 of them, could have uses anywhere you might need similar 
technology, having nothing to do with CursiVision, yet nothing whatsoever prevents you from using them in those places, not even a recompile or rebuild. 
Think on that for a bit.
</p>

<p>
Anyway, the <a href="https://github.com/ntclark/PDFEnabler">PDFEnabler</a> is something you can use to integrate PDF manipulation into a system. 
You should take a look at the <a href="https://github.com/ntclark/PDFEnabler/tree/master/PdfEnabler.odl">interface definitions</a> for it to see what kinds of things you can do with it. Also, it is written in such a way that you could easily add PDF manipulation capabilities
as you see fit.
</p>

### ![CursiVision Print Driver](https://github.com/ntclark/CursiVision/tree/master/CursiVisionPrintDriver)

<p>
The system needed a launch mechanism different than a typical windows application. In other words, I wanted to automate the initiation of signature capture 
without needing a user to open yet another application. The CursiVision Virtual print driver was the obvious solution. It is not, of course, a COM object but 
a user-mode driver following the pattern for a typical windows print driver. It uses the windows pscript5.dll to produce postscript files, and from that, 
it uses the ghostscript open source software to create the PDF file, and finally, it launches CursiVision as the user logged into the machine where the desired signature
capture and post processing series of events occurs.
</p>

<p>
If the system does not recognize a document being printed, it launches a series of dialogs where the user can specify how to recognize the document, where to sign it, 
and select and configure any post processing tools. After that, the whole thing happens automatically.
</p>

<p>
Note the print driver uses the windows WinDDK to build, currently, it is using the Windows 7 version of these tools. It could be migrated to build in Visual Studio.
However, it has been very static and actually hasn't needed to be built for years.
</p>

### ![PostScript Interpreter](../../../PostScript)

<p>
At one point, I needed a way to know, at the twips (1/1440 inch) level of accuracy, where every letter of every word was on the document. I could have taken a 
simpler approach, but I realized that my own PostScript interpreter, from scratch, would be a) fun to write, and b) have a possible life of it's own far beyond 
CursiVision. So, one day I embarked on just that (I have been involved with PostScript since 1985 when I wrote a Tex-like (but better) markup language in PL/I on Vax,
yes, it has been that long).
</p>

<p>
This PostScript interpreter is, I believe, a hidden gem among these repositories. It is in fact, not far removed from code bases that launched viable successful 
companies, such as Adobe and FoxIt. I'll say that it needs font and graphics processing to be complete (it did not need to produce visual results for my purposes) 
but I also learned that font processing is not that difficult in my <a href="https://github.com/ntclark/Graphic">Graphics</a> system, 
and graphics is relatively easy as well. 
</p>

<p>
I'd love to have the time or resources to flesh out this PostScript interpreter into the product it deserves to be. As it is now, however, it is a good tool to 
parse PostScript code for text position, and, for that matter, content.  Other usages would be very easy to achieve. See it's 
<a href="https://github.com/ntclark/PostScript/tree/master/COM%20Implementation/PostScript.odl">interface definitions</a> to understand more about how it works.
</p>

### ![PDFium-Control Host PDF (or HTML) in your System](../../../PDFium-Control)

<p>
CursiVision initially used the Adobe Reader ActiveX control in order to display the PDF in it's user interface for signature capture.
It can't be overstressed how accurate CursiVision had to be in terms of where exactly that document was in the users' running instance of the system.
For example, what page was visible, what part of that page, how much margin was on either side, the top, the bottom. To pixel accuracy, in order to position
the document, the signature capture box, and the resulting signature graphic correctly, CursiVision had to know all of those things.
</p>

<p>
The problem was, the Adobe Reader ActiveX, if it is even around anymore, has an absolutely horrible basic COM interface that offers none of the methods to be
able to know these things correctly. I had to resort to very complex methods, such as counting "non-white" pixels to figure margins, and subclassing Adobe interface
controls to read page numbers, etc, etc. There was absolutely no way that I was going to restrict user's manipulation of that document in the CursiVision UI, for example, 
by forcing a full page in the view rather than scrolling (speaking of which, the non-linearity of the Adobe Reader scroll bar was insanely difficult to grok).
</p>

<p>
Throughout the life of CursiVision, I would say it took months of extra effort to "figure out" how to trick Adobe Reader in such a way I could obtain the position in CursiVision
of the document. Not only that - every iteration of the Reader threw at least one wrench into the mix and caused certain things to malfunction. There was again, absolutely 
no way that I was going to produce a "reader 9 compatible" version, and maybe a different version for Reader X, for example. Never, it was on me to make my system work 
in <strong>all</strong> scenarios. The final straw with Adobe was when the reader went out of process COM, in other words, was implemented as an '.exe rather than a '.dll, 
which means that sub-classing in order to know the content of it's control windows was no longer possible. So to hell with them.
</p>

<p>
Thus was born the <a href="https://github.com/ntclark/PDFium-Control">PDFium Control</a>.
</p>

<p>
This COM object is based on the very complex (I would say overly complex to get started with) Microsoft HTML Control (MSHTML), and marries that with the PDFium open source 
PDF display technology from Google and "hosts" PDF documents in the application.
</p>

<p>
This is again, a hidden gem in these repositories. You can see in it's <a href="https://github.com/ntclark/PDFium-Control/tree/master/COM%20Implementation/PDFiumControl.odl">interface definitions</a>
the things I needed to implement for CursiVision. Given mouse coordinates, for example, exactly what page am I on, and exactly where on that page in PDF coordinates? 
You can also probably see how easy it would be to add things that you need to do while displaying PDF (or HTML) documents.
</p>

<p>
If nothing else - I hope you can see with maybe one or two lines of code - you can appreciate how easy it is to host HTML and PDF in your own application, and, 
while you're at it, see as I do the incredible power and utility of COM technology when it's done correctly.
</p>

<p>
One other thing. I started to implement the Adobe Reader control interface(s) in PDFium-Control. With a bit of justifiable animosity towards the poor quality of 
their version of that interface, I figured I'd one up them and show them how it's done. It would be fun to either do this or see it done at some point.
</p>

<p>
In order to two up them, an additional step could be taken. One merely needs to use the same GUIDS for the interface (IAcroAXDocShim) and the object itself
to essentially "replace" the Adobe implementation. And, the icing on the cake, currently written clients of the Adobe control will suddenly be using the 
PDFium-Control with which you, dear reader, have full and complete "control", pun intended. All of this without requiring even a recomplile of those existing clients.
</p>

<p>
God I'd love to do that!
</p>

### ![The CursiVision Toolbox](../../../CursiVision-Toolbox)

<p>
Here are the "Back-Ends". These are the objects that perform any of the post processing (i.e., after signature capture - if that is desired) steps. For example, 
take a <a href="https://github.com/ntclark/CursiVision-Toolbox/tree/master/VideoBackEnd">picture</a> of the signer and place that picture on the document 
somewhere (or a picture of a rented object on a signed rental agreement).
</p>

<p>
There are currently 14 post-processing tools implemented. Note how these work is you indicate their use in the document signing properties, and you can use any of them
any number of times, and each can save the file to that point, creating a branched set of steps of any complexity.
</p>

<p>
Indeed, in some sense there is an infinite number of these developed, because, one of them is the "generic" back end. With it, the system will call (passing the name and location
of the document created up to that point) any Windows batch or cmd file that you might create to perform any step on that document. 
</p>

<p>
If you look at the <a href="https://github.com/ntclark/CursiVision/tree/master/CursiVision/CursiVision.odl">interface definitions (find ICursiVisionBackEnd)</a> for the 
toolbox items, you'll see how you can build your own such item, in any language that can produce COM objects, by the way. All you have to do is support that interface, 
and upon registering your object, use the CLSID_StdComponentCategoriesMgr feature of COM to indicate that you support it, and voila - your tool is suddenly and seamlsessly
in the cadre of tools that CursiVision knows about.
</p>

<p>
Indeed, your tool will show up in CursiVision's menu of tools, which, by the way, you can use to execute on any PDF document
you have open in CursiVision, not just in a process. For example, you can use CursiVision as an app that is used to combine documents because you can open a document, and then
execute the <a href="https://github.com/ntclark/CursiVision-Toolbox/tree/master/ImagingBackEnd">document scanning tool</a> right off the menu.
</p>

<p>
Heck, with the creation of a simple back-end, you could use CursiVision as an application specifically for some entirely new purpose. I have always maintained, the power of COM is 
something to behold. Of course, you could automate this "new" application with the print driver, but you can also automate it with CursiVision's rich set of command line
arguments that you can find <a href="https://github.com/ntclark/CursiVision/tree/master/CursiVision/winMain.cpp">here</a>.
</p>

<p>
Note that any one of the existing tools could be used as a framework for creating a new tool using C++. They are not complex and the projects are well organized so that
you can see what is going on very easily.
</p>

### ![The InnoVisioNate Properties Component](../../../Properties)

<p>
This one has been around since 1998. One of my pet peeves in software is when it does not remember where you "were" when you last used it and use it again. 
Even now, 20 years later, software is only beginning to embody the concepts I'm talking about. For example, when you open a Word document, wouldn't it be nice if it 
positioned itself in that document exactly where you were when you last worked on it? Some tools will at least open up the series of files you had open last time, 
development IDEs for example, but I think they should go further and position the file(s) as you left them.
</p>

<p>
Thus the Properties component grew out of my desire to provide the ability of developers to implement really solid and complete persistence control in their system(s), including
anything and everything about the system, where windows were, their sizes, every conceivable property, etc. AND, importantly, automating the user's access to setting properties via 
dialogs in an easy and consistent manner. Indeed, the UI mechanism provided by the component, which is centered around the properties of your system, can form the bulk, if not the
entirety of the UI in the whole system.
</p>

<p>
All of that is available in the properties component, in it's nearly 20 years of existence, I think I've had one or two bugs, none of which were serious, yet it is something that
forms the basis, at least starting point, of literally everything I build.