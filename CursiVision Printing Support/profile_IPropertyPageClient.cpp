// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

   long __stdcall Profile::_IGPropertyPageClient::QueryInterface(REFIID riid,void **ppv) {
   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown*>(this); 
   else

   if ( riid == IID_IGPropertyPageClient )
      *ppv = static_cast<IGPropertyPageClient*>(this);
   else

      return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall Profile::_IGPropertyPageClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall Profile::_IGPropertyPageClient::Release() {
   return 1;
   }


   HRESULT Profile::_IGPropertyPageClient::BeforeAllPropertyPages() {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::GetPropertyPagesInfo(long* pCntPages,SAFEARRAY** thePageNames,SAFEARRAY** theHelpDirs,SAFEARRAY** pSize) {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::CreatePropertyPage(long pageNumber,HWND hwndParent,RECT* pRect,BOOL fModal,HWND *pHwnd) {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::Apply() {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::IsPageDirty(long pageNumber,BOOL* isDirty) {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::Help(BSTR bstrHelpDir) {
   return  S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::TranslateAccelerator(long,long* pResult) {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::AfterAllPropertyPages(BOOL userCanceled) {
   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::DestroyPropertyPage(long index) {

   if ( pParent -> hwndPrintingDispositionPage ) 
      DestroyWindow(pParent -> hwndPrintingDispositionPage);
   pParent -> hwndPrintingDispositionPage = NULL;

   pParent -> pIGProperties -> Save();

   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::GetPropertySheetHeader(void *pv) {

   if ( ! pv )
      return E_POINTER;

   static char szLabel[1024];

   Profile *p = NULL;

   if ( pParent ) 
      p = static_cast<Profile *>(pParent);

   if ( p ) {
      sprintf(szLabel,"Profile Properties: %s",p -> szName);
      if ( ! p -> isInitialized )
         p -> completeInitialize();
   } else 
      sprintf(szLabel,"Profile Properties");

   if ( ! p -> allowSaveProfile )
      sprintf(szLabel + strlen(szLabel)," NOTE: Profile Settings will NOT be saved");

   PROPSHEETHEADER *pHeader = reinterpret_cast<PROPSHEETHEADER *>(pv);

   pHeader -> dwFlags = PSH_PROPSHEETPAGE | PSH_NOCONTEXTHELP;
   pHeader -> hInstance = hModule;
   pHeader -> pszIcon = NULL;
   pHeader -> pszCaption = szLabel;
   pHeader -> pfnCallback = NULL;

   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::get_PropertyPageCount(long *pCount) {

   if ( ! pCount )
      return E_POINTER;

   if ( pParent -> theDoodleOptions.szDocumentName[0] ) {
      FILE *fX = fopen(pParent -> theDoodleOptions.szDocumentName,"rb");
      if ( ! fX )
         *pCount = 4;
      else {
         *pCount = 6;
         fclose(fX);
      }
   } else
      *pCount = 4;

   if ( pParent -> pPrintingSupport -> useGlobalProfile && 0 == strcmp(pParent -> pPrintingSupport -> szGlobalProfileName,pParent -> Name()) ) 
      *pCount -= 1;

   return S_OK;
   }


   HRESULT Profile::_IGPropertyPageClient::GetPropertySheets(void *pPages) {

   pParent -> theDoodleOptions.processingDisposition.pParent = pParent;

   if ( pParent -> pTemplateDocument ) {
      delete pParent -> pTemplateDocument;
      pParent -> pTemplateDocument = NULL;
   }

   pParent -> pTemplateDocument = new templateDocument(pParent -> pPrintingSupport -> pICursiVisionServices,pParent -> szOutlinesFileName,pParent -> theDoodleOptions.szDocumentName,NULL,NULL);

   pParent -> pTemplateDocument -> openDocument();

   SIZEL sizelDisplay{0,0};

   pParent -> pTemplateDocument -> GetSinglePagePDFDisplaySize(&sizelDisplay);

   PROPSHEETPAGE *pPropSheetPages = reinterpret_cast<PROPSHEETPAGE *>(pPages);

   pPropSheetPages[0].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[0].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[0].hInstance = hModule;
   pPropSheetPages[0].pszTemplate = MAKEINTRESOURCE(IDD_PROFILE_NAME);
   pPropSheetPages[0].pfnDlgProc = (DLGPROC)Profile::nameHandler;
   pPropSheetPages[0].pszTitle = "Printing Profile Name";
   pPropSheetPages[0].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
   pPropSheetPages[0].pfnCallback = NULL;

   pPropSheetPages[1].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[1].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[1].hInstance = hModule;
   pPropSheetPages[1].pszTemplate = MAKEINTRESOURCE(IDD_DISPOSITION_PROPERTIES);
   pPropSheetPages[1].pfnDlgProc = (DLGPROC)Profile::dispositionSettingsHandler;
   pPropSheetPages[1].pszTitle = DISPOSITION_TITLE;
   pPropSheetPages[1].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
   pPropSheetPages[1].pfnCallback = NULL;

   long nextIndex = 2;

   if ( pParent -> theDoodleOptions.szDocumentName[0] ) {

      FILE *fX = fopen(pParent -> theDoodleOptions.szDocumentName,"rb");

      if ( fX ) {

         fclose(fX);

         if ( pParent -> pPrintingSupport -> useGlobalProfile && 0 == strcmp(pParent -> pPrintingSupport -> szGlobalProfileName,pParent -> Name()) ) {

         } else {

            pPropSheetPages[nextIndex].dwSize = sizeof(PROPSHEETPAGE);
            pPropSheetPages[nextIndex].dwFlags = PSP_USEICONID | PSP_USETITLE | PSP_DLGINDIRECT;
            pPropSheetPages[nextIndex].hInstance = hModule;
            pPropSheetPages[nextIndex].pfnDlgProc = (DLGPROC)Profile::recognitionHandler;
            pPropSheetPages[nextIndex].pszTitle = "Document Recognition";
            pPropSheetPages[nextIndex].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
            pPropSheetPages[nextIndex].pfnCallback = NULL;

#if 0
            pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_CURSIVISION_RECOGNITION),RT_DIALOG));
#else
            //
            //NTC: 09-02-2017: I am aware that the copy of the dialog resource allocated here leaks.
            //
            // For some reason, perhaps by compiling with Visual Studio 2015 (compiler switches ?), changing memory loaded via
            // LoadResource throws an access violation, so the dialog template needs to be copied to allocated memory.
            //
            // However, I am not sure where or when the opportunity would come to delete the copy, perhaps somewhere
            // in the properties component's flow, however that would not know whether the caller just called LoadResource
            // which I don't think should be deleted.
            //
            HRSRC hDialog = FindResource(hModule,MAKEINTRESOURCE(IDD_CURSIVISION_RECOGNITION),RT_DIALOG);
            HGLOBAL hResource = LoadResource(hModule,hDialog);
            void *pResource = LockResource(hResource);
            BYTE *pDialog = new BYTE[SizeofResource(hModule,hDialog)];
            memcpy(pDialog,pResource,SizeofResource(hModule,hDialog));
            pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)pDialog;
#endif

            adjustPropertiesDialogSize(&sizelDisplay,(DLGTEMPLATEEX *)pPropSheetPages[nextIndex].pResource,128);

            nextIndex++;

         }

         pPropSheetPages[nextIndex].dwSize = sizeof(PROPSHEETPAGE);
         pPropSheetPages[nextIndex].dwFlags = PSP_USEICONID | PSP_USETITLE | PSP_DLGINDIRECT;
         pPropSheetPages[nextIndex].hInstance = hModule;
         pPropSheetPages[nextIndex].pfnDlgProc = (DLGPROC)Profile::signingLocationsHandler;
         pPropSheetPages[nextIndex].pszTitle = "Signing Locations";
         pPropSheetPages[nextIndex].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
         pPropSheetPages[nextIndex].pfnCallback = NULL;

#if 0
         pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_SIGNING_LOCATIONS),RT_DIALOG));
#else
         HRSRC hDialog = FindResource(hModule,MAKEINTRESOURCE(IDD_SIGNING_LOCATIONS),RT_DIALOG);
         HGLOBAL hResource = LoadResource(hModule,hDialog);
         void *pResource = LockResource(hResource);
         BYTE *pDialog = new BYTE[SizeofResource(hModule,hDialog)];
         memcpy(pDialog,pResource,SizeofResource(hModule,hDialog));
         pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)pDialog;
#endif

         adjustPropertiesDialogSize(&sizelDisplay,(DLGTEMPLATEEX *)pPropSheetPages[nextIndex].pResource,128);

         nextIndex++;

      }

   }

   pPropSheetPages[nextIndex].pfnDlgProc = (DLGPROC)Profile::fieldsHandler;
   pPropSheetPages[nextIndex].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
   pPropSheetPages[nextIndex].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[nextIndex].dwFlags = PSP_USEICONID | PSP_USETITLE | PSP_DLGINDIRECT;
   pPropSheetPages[nextIndex].hInstance = hModule;
   pPropSheetPages[nextIndex].pszTitle = "Data Fields";
   pPropSheetPages[nextIndex].pfnCallback = NULL;

#if 0
   pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_DATA_FIELDS),RT_DIALOG));
#else
   HRSRC hDialog = FindResource(hModule,MAKEINTRESOURCE(IDD_DATA_FIELDS),RT_DIALOG);
   HGLOBAL hResource = LoadResource(hModule,hDialog);
   void *pResource = LockResource(hResource);
   BYTE *pDialog = new BYTE[SizeofResource(hModule,hDialog)];
   memcpy(pDialog,pResource,SizeofResource(hModule,hDialog));
   pPropSheetPages[nextIndex].pResource = (PROPSHEETPAGE_RESOURCE)pDialog;
#endif

   adjustPropertiesDialogSize(&sizelDisplay,(DLGTEMPLATEEX *)pPropSheetPages[nextIndex].pResource,64);

   nextIndex++;

   pPropSheetPages[nextIndex].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[nextIndex].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[nextIndex].hInstance = hModule;
   pPropSheetPages[nextIndex].pszTemplate = MAKEINTRESOURCE(IDD_BACKENDS);
   pPropSheetPages[nextIndex].pfnDlgProc = (DLGPROC)Profile::additionalBackEndsHandler;
   pPropSheetPages[nextIndex].pszTitle = "Tool Box";
   pPropSheetPages[nextIndex].lParam = (ULONG_PTR)&pParent -> theDoodleOptions.processingDisposition;
   pPropSheetPages[nextIndex].pfnCallback = NULL;

   return S_OK;
   }
