// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include "interfacesToSupportAnEmbeddedObject_IOleInPlaceSite.cpp"