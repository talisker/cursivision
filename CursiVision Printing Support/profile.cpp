// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <limits.h>
#include <map>

#include "PrintingSupport.h"

#include "PostScript_i.h"

   char szProfileInput[PROFILE_BUFFER_SIZE];
   char profileText[PROFILE_BUFFER_SIZE];

   Profile *Profile::pCurrentProfile = NULL;

   Profile::Profile(IUnknown *pUnknownOuter) :
   
      pIGProperties(NULL),
      pIGPropertyPageClient(NULL),

      pIPdfEnabler(NULL),
      pIPdfDocument(NULL),
      hModule_PDFEnabler(NULL),

      pPrintingSupport(NULL),

      recognizeByName(0),

      hwndPrintingDispositionPage(NULL),
      hwndAdditionalBackEnds(NULL),

      pTemplateDocument(NULL),

      skipSignatureCapture(false),

      isStarted(false),
      isInitialized(false),
      allowSaveProfile(false),

      refCount(0)

   {

   pDoodleOptions = &theDoodleOptions;

   memset(szName,0,sizeof(szName));
   memset(szPropertiesFile,0,sizeof(szPropertiesFile));
   memset(szOutlinesFileName,0,sizeof(szOutlinesFileName));
   memset(szSignaturesFileName,0,sizeof(szSignaturesFileName));
   memset(szAssociatedProfileName,0,sizeof(szAssociatedProfileName));
   memset(szProposedNewName,0,sizeof(szProposedNewName));

   memset(expectedRects,0,sizeof(expectedRects));
   memset(expectedText,0,sizeof(expectedText));
   memset(expectedPage,0,sizeof(expectedPage));

   memset(szOutlinesPageRecord,0,sizeof(szOutlinesPageRecord));

   memset(&theDoodleOptions,0,sizeof(theDoodleOptions));

   theDoodleOptions.isPrintingProfileDoodleOptions = true;

   theDoodleOptions.processingDisposition.isGlobalDisposition = false;

   theDoodleOptions.processingDisposition.doSave = true;
   theDoodleOptions.processingDisposition.doRetain = true;
   theDoodleOptions.processingDisposition.saveIn = true;
   theDoodleOptions.processingDisposition.saveMyDocuments = false;

   strcpy(theDoodleOptions.processingDisposition.szFileStorageDirectory,szActiveSignedDocumentsDirectory);

   return;
   }


   HRESULT Profile::Initialize(char *pszOriginalDocument,char *pszProfileName,void *pvDefaultResultDisposition,void *pvPrintingProfile,boolean resetSignatureLocations) {
   preInitialize(pszOriginalDocument,pszProfileName,pvDefaultResultDisposition,pvPrintingProfile,resetSignatureLocations);
   return completeInitialize();
   }


   void Profile::preInitialize(char *pszOriginalDocument,char *pszProfileName,void *pvDefaultResultDisposition,void *pvPrintingProfile,boolean resetSignatureLocations) {

   if ( pvDefaultResultDisposition && ! ( pvDefaultResultDisposition == &theDoodleOptions.processingDisposition ) )
      memcpy(&theDoodleOptions.processingDisposition,(BYTE *)pvDefaultResultDisposition,sizeof(resultDisposition));

   if ( resetSignatureLocations )
      memset(&theDoodleOptions.theLocations,0,sizeof(theDoodleOptions.theLocations));

   if ( pszOriginalDocument )
      strcpy(theDoodleOptions.szDocumentName,pszOriginalDocument);

   pPrintingSupport = (PrintingSupport *)pvPrintingProfile;

   char szTemp[512];

   memset(szTemp,0,sizeof(szTemp));

   memset(szName,0,sizeof(szName));

   strcpy(szTemp,pszProfileName);

   char *p = strrchr(szTemp,'\\');
   if ( ! p )
      p = strrchr(szTemp,'/');
   if ( p ) {
      *p = '\0';
      strcpy(szName,p + 1);
   } else
      strcpy(szName,szTemp);
   p = strrchr(szName,'.');
   if ( p )
      *p = '\0';

   sprintf(szSignaturesFileName,"%s\\%s.signatures",szActiveProfilesDirectory,szName);

   sprintf(szPropertiesFile,"%s\\%s.prf",szActiveProfilesDirectory,szName);

   return;
   }


   HRESULT Profile::completeInitialize() {
   
   isInitialized = true;

   BSTR bstrFileName = SysAllocStringLen(NULL,512);

   MultiByteToWideChar(CP_ACP,0,szPropertiesFile,-1,bstrFileName,512);

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   char szKeepName[MAX_PATH];

   strcpy(szKeepName,szName);

   pIGProperties -> put_FileName(bstrFileName);

   SysFreeString(bstrFileName);

   pIGPropertyPageClient = new _IGPropertyPageClient(this);
   pIGProperties -> AdvisePropertyPageClient(pIGPropertyPageClient);

   pIGProperties -> Add(L"name",NULL);
   pIGProperties -> DirectAccess(L"name",TYPE_SZSTRING,szName,sizeof(szName));

   pIGProperties -> Add(L"doodle options",NULL);
   pIGProperties -> DirectAccess(L"doodle options",TYPE_BINARY,&theDoodleOptions,sizeof(theDoodleOptions));

   pIGProperties -> Add(L"recognize by name",NULL);
   pIGProperties -> DirectAccess(L"recognize by name",TYPE_BINARY,&recognizeByName,sizeof(recognizeByName));

   pIGProperties -> Add(L"expected rects",NULL);
   pIGProperties -> DirectAccess(L"expected rects",TYPE_BINARY,&expectedRects,sizeof(expectedRects));

   pIGProperties -> Add(L"expected text",NULL);
   pIGProperties -> DirectAccess(L"expected text",TYPE_BINARY,&expectedText,sizeof(expectedText));

   pIGProperties -> Add(L"expected page",NULL);
   pIGProperties -> DirectAccess(L"expected page",TYPE_BINARY,&expectedPage,sizeof(expectedPage));

   pIGProperties -> Add(L"outlines file",NULL);
   pIGProperties -> DirectAccess(L"outlines file",TYPE_BINARY,szOutlinesFileName,sizeof(szOutlinesFileName));

   pIGProperties -> Add(L"skip signature capture",NULL);
   pIGProperties -> DirectAccess(L"skip signature capture",TYPE_BINARY,&skipSignatureCapture,sizeof(skipSignatureCapture));

   short bSuccess;

   pIGProperties -> LoadFile(&bSuccess);

   strcpy(szName,szKeepName);

   theDoodleOptions.isPrintingProfileDoodleOptions = true;

   return rc;
   }


   HRESULT Profile::Start() {

   if ( isStarted )
      return S_OK;
   
   if ( ! isInitialized )
      completeInitialize();

   isStarted = true;

   for ( long k = 0; k < theDoodleOptions.processingDisposition.countBackEnds; k++ ) {

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(theDoodleOptions.processingDisposition.backEndGUIDS[k],NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( ! ( S_OK == rc ) ) {
         char szMessage[1024];
         sprintf(szMessage,"The CursiVision Tool (%s)\n\tIs not registered correctly or is non-functional\n\nThe system returned the error code %ld",
                                 theDoodleOptions.processingDisposition.backEndDescriptions[k],rc);
         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);
         continue;
      }

      pICursiVisionBackEnd -> ServicesAdvise(pPrintingSupport -> pICursiVisionServices);

      strcpy(theDoodleOptions.processingDisposition.backEndSettingsFiles[k],szPropertiesFile);

      char *pc = strrchr(theDoodleOptions.processingDisposition.backEndSettingsFiles[k],'.');
      if ( pc )
         *pc = '\0';

      BSTR codeName;
      char szCodeName[32];
      pICursiVisionBackEnd -> get_CodeName(&codeName);

      WideCharToMultiByte(CP_ACP,0,codeName,-1,szCodeName,32,0,0);

      SysFreeString(codeName);

      OLECHAR bstrGUID[64];
      char szGUID[64];

      StringFromGUID2(theDoodleOptions.processingDisposition.backEndInstanceIds[k],bstrGUID,64);

      WideCharToMultiByte(CP_ACP,0,bstrGUID,-1,szGUID,64,0,0);

      sprintf(theDoodleOptions.processingDisposition.backEndSettingsFiles[k] + strlen(theDoodleOptions.processingDisposition.backEndSettingsFiles[k]),"_%s%s.settings",szCodeName,szGUID);

      BSTR bstrSettingsFile = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,theDoodleOptions.processingDisposition.backEndSettingsFiles[k],-1,bstrSettingsFile,MAX_PATH);
      pICursiVisionBackEnd -> put_PropertiesFileName(bstrSettingsFile);
      SysFreeString(bstrSettingsFile);

      pICursiVisionBackEnd -> Release();

   }

   return S_OK;
   }


   Profile::~Profile() {

   if ( pIGProperties && szPropertiesFile[0] )
      pIGProperties -> Save();

   if ( pIGProperties )
      pIGProperties -> Release();

   if ( pIGPropertyPageClient )
      delete pIGPropertyPageClient;;

   closePDFEnabler(NULL,NULL);

   if ( pTemplateDocument ) 
      delete pTemplateDocument;

   pTemplateDocument = NULL;

   return;
   }


   long Profile::Save(char *pszNewName) {

   if ( ! allowSaveProfile )
      return 0L;

   if ( pszNewName ) {

      for ( std::list<IPrintingSupportProfile *>::iterator it = pPrintingSupport -> profiles.begin(); it != pPrintingSupport -> profiles.end(); it++ ) {
         IPrintingSupportProfile *pProfile = *it;
         Profile *px = static_cast<Profile *>(pProfile);
         if ( px == this )
            continue;
         if ( 0 == strcmp(pszNewName,px -> szName) ) {
            char szMessage[1024];
            sprintf(szMessage,"A profile with the name:\n\n\t%s\n\nAlready exists. Please delete that profile if you want to reuse this name",pszNewName);
            MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
            return 0L;
         }
      }

      rename(pszNewName);

   }

   pIGProperties -> Save();

   return 1L;
   }



   HRESULT Profile::Destroy(BOOL keepOutlines) {

   for ( long k = 0; k < theDoodleOptions.processingDisposition.countBackEnds; k++ ) {

      char szTemp[MAX_PATH];

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(theDoodleOptions.processingDisposition.backEndGUIDS[k],NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( ! pICursiVisionBackEnd )
         continue;

      pICursiVisionBackEnd -> ServicesAdvise(pPrintingSupport -> pICursiVisionServices);

      strcpy(szTemp,szPropertiesFile);

      char *pc = strrchr(szTemp,'.');
      if ( pc )
         *pc = '\0';

      BSTR codeName;
      char szCodeName[32];
      pICursiVisionBackEnd -> get_CodeName(&codeName);

      WideCharToMultiByte(CP_ACP,0,codeName,-1,szCodeName,32,0,0);

      SysFreeString(codeName);

      OLECHAR bstrGUID[64];
      char szGUID[64];

      StringFromGUID2(theDoodleOptions.processingDisposition.backEndInstanceIds[k],bstrGUID,64);

      WideCharToMultiByte(CP_ACP,0,bstrGUID,-1,szGUID,64,0,0);

      sprintf(szTemp + strlen(szTemp),"_%s%s.settings",szCodeName,szGUID);

      DeleteFile(szTemp);

      pICursiVisionBackEnd -> Release();

   }

   DeleteFile(szPropertiesFile);
   szPropertiesFile[0] = '\0';

   if ( ! keepOutlines ) {
      DeleteFile(szOutlinesFileName);
      szOutlinesFileName[0] = '\0';
   }

   DeleteFile(szSignaturesFileName);
   szSignaturesFileName[0] = '\0';

   char *psz = GetDispositionSettingsFileName();
   if ( psz )
      DeleteFile(psz);

   return S_OK;
   }


   bool Profile::readOutlines(RECT **ppPDFEntries,long **ppPDFPages,
                                    char *pszOriginalDocument,char *pszOutlinesFile,
                                    char **ppszTextValues,long *pCountEntries,
                                    long *pCXPage,long *pCYPage) {


   FILE *fProfile = NULL;
   char szCurrentOutlinesFile[MAX_PATH];
   char szPrintedDocument[MAX_PATH];

   if ( pszOutlinesFile ) {
      strcpy(szCurrentOutlinesFile,pszOutlinesFile);
      strcpy(szPrintedDocument,pszOriginalDocument);
   } else {
      strcpy(szCurrentOutlinesFile,szOutlinesFileName);
      strcpy(szPrintedDocument,DocumentName());
   }

   if ( pCountEntries )
      *pCountEntries = 0L;

   fProfile = fopen(szCurrentOutlinesFile,"rb");

   if ( ! fProfile )
      return false;

   long pageNumber = -1L,pageEntries = 0L,nextPageOffset,cxPage,cyPage;
   char bIgnore[5];

   long rc = (long)fread(szOutlinesPageRecord,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   if ( rc < OUTLINES_PAGE_RECORD_SIZE ) {
      fclose(fProfile);
      return false;
   }
      
   rc = sscanf(szOutlinesPageRecord,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);

   if ( ! ( 6 == rc ) ) {
      fclose(fProfile);
      return false;
   }

   if ( pCXPage )
      *pCXPage = cxPage;

   if ( pCYPage )
      *pCYPage = cyPage;

   long countEntries = 0L;
   long countBytes = 0L;

   do {

      countEntries += pageEntries;

      fseek(fProfile,nextPageOffset,SEEK_SET);

      rc = (long)fread(szOutlinesPageRecord,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szOutlinesPageRecord,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);
      else
         break;

   } while ( 6 == rc && 0 != nextPageOffset );

   fclose(fProfile);

   *ppPDFEntries = new RECT[(countEntries + 1)];
   *ppPDFPages = new long[(countEntries + 1)];

   memset(*ppPDFEntries,0,(countEntries + 1) * sizeof(RECT));
   memset(*ppPDFPages,0,(countEntries + 1) * sizeof(long));

   char *pText = NULL;

   if ( ppszTextValues ) {
      *ppszTextValues = new char[(countEntries + 1) * 33];
      memset(*ppszTextValues,0,33 * (countEntries + 1) * sizeof(char));
      pText = *ppszTextValues;
   }

   if ( pCountEntries )
      *pCountEntries = countEntries;

   RECT *pPDF = *ppPDFEntries;
   long *pPage = *ppPDFPages;

   fProfile = fopen(szCurrentOutlinesFile,"rb");

   long rcx = (long)fread(szOutlinesPageRecord,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   rcx = sscanf(szOutlinesPageRecord,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);

   do {

      for ( long k = 0; k < pageEntries; k++, pPDF++, pPage++ ) {

         fread(szProfileInput,1,OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE,fProfile);

         sscanf(szProfileInput,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,&pPDF -> left,&pPDF -> bottom,&pPDF -> right,&pPDF -> top,&countBytes);

         fread(profileText,countBytes + 1,1,fProfile);

         if ( pText ) {
            strncpy(pText,profileText,min(countBytes,32));
            pText += 33;
         }

         *pPage = pageNumber;

      }
   
      rc = (long)fread(szOutlinesPageRecord,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szOutlinesPageRecord,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);
      else
         break;

   } while ( 6 == rc && 0 != nextPageOffset );

   fclose(fProfile);

   return true;
   }


   long Profile::rename(char *pszNewName) {

   for ( std::list<IPrintingSupportProfile *>::iterator it = pPrintingSupport -> profiles.begin(); it != pPrintingSupport -> profiles.end(); it++ ) {
      IPrintingSupportProfile *pProfile = *it;
      Profile *px = static_cast<Profile *>(pProfile);
      if ( px == this )
         continue;
      if ( 0 == _stricmp(pszNewName,px -> szName) ) {
         char szMessage[1024];
         sprintf(szMessage,"A profile with the name:\n\n\t%s\n\nAlready exists. Please delete that profile if you want to reuse this name",pszNewName);
         MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
         return 0L;
      }
   }

   char szNewName[512];
   char szTemp[512];
   char szNewPropertiesFileName[512];
   char szNewSignaturesFileName[512];
   char szNewOutlinesFileName[512];

   char szDispositionFileName[512];
   char szNewDispositionFileName[512];

   memset(szTemp,0,sizeof(szTemp));

   strcpy(szTemp,pszNewName);

   char *p = strrchr(szTemp,'\\');
   if ( ! p )
      p = strrchr(szTemp,'/');
   if ( p ) {
      *p = '\0';
      strcpy(szNewName,p + 1);
   } else
      strcpy(szNewName,szTemp);
   p = strrchr(szNewName,'.');
   if ( p )
      *p = '\0';

   sprintf(szNewPropertiesFileName,"%s\\%s.prf",szActiveProfilesDirectory,szNewName);

   sprintf(szNewSignaturesFileName,"%s\\%s.signatures",szActiveProfilesDirectory,szNewName);

   sprintf(szDispositionFileName,"%s\\%s.disposition",szActiveProfilesDirectory,szName);

   sprintf(szNewDispositionFileName,"%s\\%s.disposition",szActiveProfilesDirectory,szNewName);

   sprintf(szNewOutlinesFileName,"%s\\%s.profile",szActiveProfilesDirectory,szNewName);

   BSTR bstrFileName = SysAllocStringLen(NULL,512);
   MultiByteToWideChar(CP_ACP,0,szNewPropertiesFileName,-1,bstrFileName,512);

   pIGProperties -> put_FileName(bstrFileName);

   SysFreeString(bstrFileName);

   DeleteFile(szNewPropertiesFileName);
   CopyFile(szPropertiesFile,szNewPropertiesFileName,FALSE);
   DeleteFile(szPropertiesFile);

   DeleteFile(szNewOutlinesFileName);
   CopyFile(szOutlinesFileName,szNewOutlinesFileName,FALSE);
   DeleteFile(szOutlinesFileName);

   DeleteFile(szNewSignaturesFileName);
   CopyFile(szSignaturesFileName,szNewSignaturesFileName,FALSE);
   DeleteFile(szSignaturesFileName);

   DeleteFile(szNewDispositionFileName);
   CopyFile(szDispositionFileName,szNewDispositionFileName,FALSE);
   DeleteFile(szDispositionFileName);

   strcpy(szOutlinesFileName,szNewOutlinesFileName);

   pIGProperties -> Save();

   char szOldPropertiesFile[MAX_PATH];

   strcpy(szOldPropertiesFile,szPropertiesFile);

   strcpy(szName,szNewName);
   strcpy(szPropertiesFile,szNewPropertiesFileName);
   strcpy(szSignaturesFileName,szNewSignaturesFileName);

   for ( long k = 0; k < theDoodleOptions.processingDisposition.countBackEnds; k++ ) {

      char szTemp[MAX_PATH];

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(theDoodleOptions.processingDisposition.backEndGUIDS[k],NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( ! pICursiVisionBackEnd )
         continue;

      pICursiVisionBackEnd -> ServicesAdvise(pPrintingSupport -> pICursiVisionServices);

      strcpy(szTemp,szPropertiesFile);

      char *pc = strrchr(szTemp,'.');
      if ( pc )
         *pc = '\0';

      BSTR codeName;
      char szCodeName[32];
      pICursiVisionBackEnd -> get_CodeName(&codeName);

      WideCharToMultiByte(CP_ACP,0,codeName,-1,szCodeName,32,0,0);

      SysFreeString(codeName);

      OLECHAR bstrGUID[64];
      char szGUID[64];

      StringFromGUID2(theDoodleOptions.processingDisposition.backEndInstanceIds[k],bstrGUID,64);

      WideCharToMultiByte(CP_ACP,0,bstrGUID,-1,szGUID,64,0,0);

      sprintf(szTemp + strlen(szTemp),"_%s%s.settings",szCodeName,szGUID);

      DeleteFile(szTemp);
      CopyFile(theDoodleOptions.processingDisposition.backEndSettingsFiles[k],szTemp,FALSE);
      DeleteFile(theDoodleOptions.processingDisposition.backEndSettingsFiles[k]);

      strcpy(theDoodleOptions.processingDisposition.backEndSettingsFiles[k],szTemp);

      pICursiVisionBackEnd -> Release();

   }

   return 1L;
   }


   void Profile::closePDFEnabler(HWND hwndView,WNDPROC defaultHandler) {

   if ( hwndView )
      SetWindowLongPtr(hwndView,GWLP_WNDPROC,(ULONG_PTR)defaultHandler);

   if ( hModule_PDFEnabler )
      FreeLibrary(hModule_PDFEnabler);

   hModule_PDFEnabler = NULL;

   pIPdfEnabler = NULL;
   pIPdfDocument = NULL;

   return;
   }


   void Profile::setupPDFEnabler(bool force,char *pszDocument) {

   closePDFEnabler(NULL,NULL);

#if 1

   hModule_PDFEnabler = LoadLibrary(szPDFEnablerDLL);

   IClassFactory *pIClassFactory = NULL;
   long (__stdcall *cf)(REFCLSID rclsid, REFIID riid, void **ppObject) = NULL;
   cf = (long (__stdcall *)(REFCLSID rclsid, REFIID riid, void **ppObject))GetProcAddress(hModule_PDFEnabler,"DllGetClassObject");
   cf(CLSID_PdfEnabler,IID_IClassFactory,reinterpret_cast<void **>(&pIClassFactory)); 
   pIClassFactory -> CreateInstance(NULL,IID_IPdfEnabler,reinterpret_cast<void **>(&pIPdfEnabler));

#else

   HRESULT rc = CoCreateInstance(CLSID_PdfEnabler,NULL,CLSCTX_INPROC_SERVER,IID_IPdfEnabler,reinterpret_cast<void **>(&pIPdfEnabler));

#endif

   pIPdfEnabler -> Document(&pIPdfDocument);

   BSTR bstrDocument = NULL;

   if ( pszDocument ) {
      bstrDocument = SysAllocStringLen(NULL,(DWORD)strlen(pszDocument));
      MultiByteToWideChar(CP_ACP,0,pszDocument,-1,bstrDocument,(DWORD)strlen(pszDocument));
   } else {
      bstrDocument = SysAllocStringLen(NULL,(DWORD)strlen(pDoodleOptions -> szDocumentName));
      MultiByteToWideChar(CP_ACP,0,pDoodleOptions -> szDocumentName,-1,bstrDocument,(DWORD)strlen(pDoodleOptions -> szDocumentName));
   }

   pIPdfDocument -> Open(bstrDocument,NULL,NULL);

   SysFreeString(bstrDocument);

   return;
   }


   HRESULT Profile::PushProperties() {
   return pIGProperties -> Push();
   }

   HRESULT Profile::PopProperties() {
   return pIGProperties -> Pop();
   }

   HRESULT Profile::DiscardProperties() {
   return pIGProperties -> Discard();
   }

   HRESULT Profile::SaveProperties() {
   if ( ! allowSaveProfile )
      return S_OK;
   BSTR bstrFileName = NULL;
   pIGProperties -> get_FileName(&bstrFileName);
   if ( ! bstrFileName || 0 == bstrFileName[0] ) {
      return E_UNEXPECTED;
   }
   if ( theDoodleOptions.countRects > 1 )
      theDoodleOptions.processingDisposition.doContinuousDoodle = true;
   return pIGProperties -> Save();
   }

