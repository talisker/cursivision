// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define IDD_PRINTER_FIELDS          800
#define IDD_FIELDS_LABEL            820
#define IDD_PROFILE_NAME            100

#define IDDI_PRINTING_VIEW_SCROLL   801
#define IDDI_PRINTING_FIELDS_LABEL  802

#define IDDI_PROFILE_NAME           101

#define IDS_INSTRUCTIONS_REGIONS             1
#define IDS_INSTRUCTIONS_REGIONS_POINT       2
#define IDS_INSTRUCTIONS_FIELDS              3
#define IDS_INSTRUCTIONS_FIELDS_POINT        4
#define IDS_INSTRUCTIONS_FIELDS_NOTPDF       5
#define IDS_INSTRUCTIONS_FIELDS_POINT_NOTPDF 6

#define IDS_ADOBE_NOREGIONS                  7
#define IDS_ADOBE_NOREGIONS_LABELS           8
#define IDS_RECOGNIZE_BY_NAME                9

#define IDDI_CV_RECOGNITION_BYNAME           103
#define IDDI_CV_RECOGNITION_INSTRUCTIONS     104
#define IDDI_CV_LOCATIONS_RESET              105
#define IDDI_CV_LIMIT_REACHED                106
#define IDDI_SIGNING_LOCATIONS_SKIP_SIGNING  207

#define IDDI_INHERITED_DOCUMENT_TEXT                     961
#define IDDI_INHERITED_DOCUMENT_PROPERTIES               962
