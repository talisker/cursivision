// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"
#include "resource.h"

#include <shellapi.h>
#include <shTypes.h>
#include <shlobj.h>

extern "C" void disableAll(HWND hwnd,long *pExceptions);
extern "C" void enableAll(HWND hwnd,long *pExceptions);

#define USE_PROPSHEETS

#define OBJECT_WITH_PROPERTIES Profile
#define CURSIVISION_SERVICES_INTERFACE pObject -> pPrintingSupport -> pICursiVisionServices

#define ALL_BACKENDS_LIST pObject -> pPrintingSupport -> allBackEnds

#define PARENT_OBJECT_PREFERRED_SETTINGS_FILE_NAME pObject -> szPropertiesFile

#define ADDITIONAL_INITIALIZATION                                                                                       \
      IPrintingSupportProfile *px = NULL;                                                                               \
      if ( CURSIVISION_SERVICES_INTERFACE )                                                                             \
         CURSIVISION_SERVICES_INTERFACE -> get_PrintingSupportProfile(&px);                                             \
      if ( ! CURSIVISION_SERVICES_INTERFACE -> IsAdministrator() && px ) {                                              \
         RECT rc = {0};                                                                                                 \
         GetClientRect(hwnd,&rc);                                                                                       \
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,rc.bottom - 32,0,0,SWP_NOSIZE);   \
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);    \
         EnableWindow(hwnd,FALSE);                                                                                      \
      } else                                                                                                            \
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

   struct buttonPair {
      HWND hwndList,hwndUse,hwndProperties,hwndOrder;
      GUID objectId;
      char szSettingsFileName[MAX_PATH];
      GUID instanceId;
      IUnknown *pIUnknown_Object;
   };

   static buttonPair buttonPairs[32];

   static HWND hwndTopList = NULL;
   static HWND hwndBottomList = NULL;

   static long nativeTopListHeight = 0;
   static long nativeTopListWidth = 0;
   static long nativeWidth = 0;
   static long nativeHeight = 0;
   static long countAvailableBackEnds = 0;

   LRESULT CALLBACK Profile::additionalBackEndsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

#include "additionalBackEndsBody.cpp"

   return LRESULT(FALSE);
   }

#include "additionalBackEnds_ListViewHandler.cpp"

