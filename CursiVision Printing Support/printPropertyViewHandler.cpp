/*
                       Copyright (c) 2009 Nathan T. Clark
*/

#include "PrintingSupport.h"

#include "..\resource.h"

   LRESULT CALLBACK PrintingSupport::printPropertyViewHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   PrintingSupport *p = (PrintingSupport *)GetWindowLong(hwnd,GWL_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      SetWindowLong(hwnd,GWL_USERDATA,lParam);

      p = (PrintingSupport *)lParam;

      if ( ! p )
         break;

      SetWindowPos(hwnd,HWND_TOP,p -> rcViewDialog.left,p -> rcViewDialog.top,p -> rcViewDialog.right - p -> rcViewDialog.left,p -> rcViewDialog.bottom - p -> rcViewDialog.top,0L);

      SetWindowLong(GetDlgItem(hwnd,IDDI_PRINTING_VIEW_VIEW),GWL_WNDPROC,(long)PrintingSupport::printPropertyViewHandler);

      }
      return LRESULT(FALSE);

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_PRINTING_VIEW_OK: {

         EndDialog(hwnd,1);

         }
         break;

      }

      }
      break;

   case WM_MOVE: 
      GetWindowRect(hwnd,&p -> rcViewDialog);
      break;

   case WM_SIZE: {

      long cx = LOWORD(lParam);
      long cy = HIWORD(lParam);

      RECT rcAdjust = {0,0,0,0};
      AdjustWindowRectEx(&rcAdjust,GetWindowLong(hwnd,GWL_STYLE),FALSE,GetWindowLong(hwnd,GWL_EXSTYLE));

      p -> rcViewDialog.right = p -> rcViewDialog.left + cx - rcAdjust.left + rcAdjust.right;
      p -> rcViewDialog.bottom = p -> rcViewDialog.top + cy - rcAdjust.top + rcAdjust.bottom;

      RECT rcText,rcParent;

      GetWindowRect(hwnd,&rcParent);
      GetWindowRect(GetDlgItem(hwnd,IDDI_PRINTING_VIEW_VIEW),&rcText);

      rcText.top -= rcParent.top;
      rcText.left -= rcParent.left;
      rcText.right -= rcParent.left;
      rcText.bottom -= rcParent.top;

      SetWindowPos(GetDlgItem(hwnd,IDDI_PRINTING_VIEW_OK),HWND_TOP,8,cy - 32,0,0,SWP_NOSIZE);
//      SetWindowPos(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_CANCEL),HWND_TOP,60,cy - 32,0,0,SWP_NOSIZE);
      SetWindowPos(GetDlgItem(hwnd,IDDI_PRINTING_VIEW_VIEW),HWND_TOP,0,0,cx - 32, cy - 48 - rcText.top,SWP_NOMOVE);

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }
   