/*

                       Copyright (c) 1999,2000,2001,2002,2009 Nathan T. Clark

*/

#include <windows.h>
#include <stdio.h>

#include "PrintingSupport.h"

#include "..\resource.h"

#ifdef USE_GPROPERTIES
#else

   extern "C" int GetDocumentsLocation(HWND hwnd,char *);

   long __stdcall Profile::_IPropertyPage::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL;
  
   if ( riid == IID_IUnknown ) 
      *ppv = static_cast<void *>(this);
   else

      if ( riid == IID_IPropertyPage )
         *ppv = static_cast<IPropertyPage *>(this);
      else
 
         return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef(); 
   return S_OK; 
   }
   unsigned long __stdcall Profile::_IPropertyPage::AddRef() {
   return ++refCount;
   }
   unsigned long __stdcall Profile::_IPropertyPage::Release() {
   if ( --refCount  ) return refCount;
   delete this;
   return 0;
   }
 
 
   HRESULT Profile::_IPropertyPage::SetPageSite(IPropertyPageSite* pPageSite) {
   return S_OK;
   }

   
   HRESULT Profile::_IPropertyPage::Activate(HWND hwndParent,const RECT* prc,BOOL fModal) {

   char szLocalMyDocumentsDirectory[MAX_PATH];

   memset(szLocalMyDocumentsDirectory,0,sizeof(szLocalMyDocumentsDirectory));

   GetDocumentsLocation(NULL,szLocalMyDocumentsDirectory);

   if ( szLocalMyDocumentsDirectory[0] ) {
      if ( 0 == strncmp(szLocalMyDocumentsDirectory,pParent -> processingDisposition.szFileStorageDirectory,min(strlen(szLocalMyDocumentsDirectory),strlen(pParent -> processingDisposition.szFileStorageDirectory))) ) {
         pParent -> processingDisposition.saveMyDocuments = true;
         pParent -> processingDisposition.saveIn = false;
      }
   }

   if ( ! pParent -> hwndPrintingDispositionPage ) {
      DLGTEMPLATE *dt = (DLGTEMPLATE *)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_DISPOSITION_PROPERTIES),RT_DIALOG));
      pParent -> hwndPrintingDispositionPage = CreateDialogIndirectParam(hModule,dt,(HWND)hwndParent,(DLGPROC)Profile::dispositionSettingsHandler,(long)&pParent -> processingDisposition);
   } else {
      SetParent(pParent -> hwndPrintingDispositionPage,(HWND)hwndParent);
   }

   SetWindowPos(pParent -> hwndPrintingDispositionPage,HWND_TOP,prc -> left,prc -> top,prc -> right - prc -> left,prc -> bottom - prc -> top,0L);

   pParent -> pProfileProperties -> Push();
   pParent -> pProfileProperties -> Push();

   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::Deactivate() {
   if ( pParent -> hwndPrintingDispositionPage ) {
      DestroyWindow(pParent -> hwndPrintingDispositionPage);
      pParent -> hwndPrintingDispositionPage = NULL;
   }
   return S_OK;
   }



   HRESULT Profile::_IPropertyPage::GetPageInfo(PROPPAGEINFO *pPageInfo) {

   memset(pPageInfo,0,sizeof(PROPPAGEINFO));

   pPageInfo -> cb = sizeof(PROPPAGEINFO);

   pPageInfo -> pszTitle = (BSTR)CoTaskMemAlloc(256);
   memset(pPageInfo -> pszTitle,0,256);

   BSTR pn = SysAllocString(L"Profile Disposition");
   memcpy(pPageInfo -> pszTitle,pn,wcslen(pn) * sizeof(OLECHAR));

   pPageInfo -> size.cx = 350;
   pPageInfo -> size.cy = 350;

   SysFreeString(pn);

   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::SetObjects(ULONG cObjects,IUnknown** pUnk) {
   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::Show(UINT nCmdShow) {
   ShowWindow(pParent -> hwndPrintingDispositionPage,nCmdShow);
   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::Move(const RECT* prc) {
   SetWindowPos(pParent -> hwndPrintingDispositionPage,HWND_TOP,prc -> left,prc -> top,prc -> right - prc -> left,prc -> bottom - prc -> top,0L);
   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::IsPageDirty() {
   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::Apply() {
   SendMessage(pParent -> hwndPrintingDispositionPage,WM_COMMAND,MAKELPARAM(IDDI_DISPOSITION_ACCEPT,0),0L);
   pParent -> pProfileProperties -> Discard();
   pParent -> pProfileProperties -> Push();
   return S_OK;
   }


   HRESULT Profile::_IPropertyPage::Help(LPCOLESTR pszHelpDir) {
   return E_FAIL;
   }


   HRESULT Profile::_IPropertyPage::TranslateAccelerator(MSG* pMsg) {
   return S_FALSE;
   }
#endif