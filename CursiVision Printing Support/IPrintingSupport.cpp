// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <olectl.h>
#include <commctrl.h>

#include "PrintingSupport.h"

#include "PDFEnabler_i.h"

   HRESULT __stdcall PrintingSupport::TakeMainWindow(HWND hwnd) {
   hwndMainFrame = hwnd;
   return S_OK;
   }


   HRESULT __stdcall PrintingSupport::TakeDocumentInfo(char *pszPDFFileName,void *pvDisposition,void *pvIPdfDocument,boolean createGlobalPrintingProfile) {

   if ( pvDisposition )
      memcpy(&defaultResultDisposition,(BYTE *)pvDisposition,sizeof(resultDisposition));
   else
      memset(&defaultResultDisposition,0,sizeof(resultDisposition));

   defaultResultDisposition.isGlobalDisposition = false;

   char szRootName[512];
   char szTemp[512];

   memset(szRootName,0,sizeof(szRootName));
   memset(szTemp,0,sizeof(szTemp));

   strncpy(szTemp,pszPDFFileName,sizeof(szTemp) - 1);

   char *p = strrchr(szTemp,'\\');
   if ( ! p )
      p = strrchr(szTemp,'/');

   if ( p )
      strcpy(szRootName,p + 1);
   else
      strcpy(szRootName,szTemp);

   p = strrchr(szRootName,'.');
   if ( p )
      *p = '\0';

   if ( szGlobalDataStore[0] ) {
      sprintf(szPrintedDocumentFileName,"%s\\Printed Documents\\%s.pdf",szGlobalDataStore,szRootName);
      sprintf(szPrintedDocumentOutlinesFileName,"%s\\Printing Profiles\\%s.profile",szGlobalDataStore,szRootName);
      CopyFile(pszPDFFileName,szPrintedDocumentFileName,FALSE);
      sprintf(szTemp,"%s\\%s.profile",szPrintDriverProfilesDirectory,szRootName);
      CopyFile(szTemp,szPrintedDocumentOutlinesFileName,FALSE);
   } else {
      strcpy(szPrintedDocumentFileName,pszPDFFileName);
      sprintf(szPrintedDocumentOutlinesFileName,"%s\\%s.profile",szPrintDriverProfilesDirectory,szRootName);
   }

   if ( createGlobalPrintingProfile ) {
      defaultResultDisposition.isGlobalDisposition = true;
      strcpy(szGlobalProfileName,"Default Printing Profile");
      addProfile(szPrintedDocumentFileName,szGlobalProfileName,&defaultResultDisposition,true);
      char szNewOutlinesFileName[MAX_PATH];
      sprintf(szNewOutlinesFileName,"%s\\%s.profile",szActiveProfilesDirectory,szGlobalProfileName);
      CopyFile(szPrintedDocumentOutlinesFileName,szNewOutlinesFileName,FALSE);
      DeleteFile(szPrintedDocumentOutlinesFileName);
      strcpy(szPrintedDocumentOutlinesFileName,szNewOutlinesFileName);
      Profile *px = static_cast<Profile *>(pActiveProfile);
      px -> SetOutlinesFileName(szPrintedDocumentOutlinesFileName);
      useGlobalProfile = true;
      bool keepAllowSave = px -> GetAllowSaveProfile();
      px -> SetAllowSaveProfile(true);
      pIGProperties -> Save();
      px -> SetAllowSaveProfile(keepAllowSave);
   }

   strcpy(szPotentialProfileName,szRootName);

   if ( doManualSelection ) {

      preferredProfileIndex = -1L;

      isPrintJobActive = true;

      isManualSelection = true;

      pICursiVisionServices -> SetIsAdministrator(TRUE);

      IUnknown *pIUnknown = NULL;
      QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
      pIGProperties -> ShowProperties(hwndMainFrame,pIUnknown);
      pIUnknown -> Release();

      isManualSelection = false;

      if ( userCanceledActivePrintJob )
         return E_FAIL;

      pICursiVisionServices -> put_PrintingSupportProfile(pActiveProfile);

      return S_OK;

   }

   FILE *fProfile = fopen(szPrintedDocumentOutlinesFileName,"rb");

   long countEntries = 0;

   if ( fProfile ) {

      long pageNumber,nextPageOffset,cxPage,cyPage;
      char bIgnore[5];

      long rc = fscanf(fProfile,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

      fclose(fProfile);

      if ( rc < 6 || 0 == countEntries )

         pICursiVisionServices -> GenerateOutlines(szPrintedDocumentFileName,szPrintedDocumentOutlinesFileName,1,-1L,&countEntries);

      else

         if ( strncmp("AUTO",bIgnore,4) )

            pICursiVisionServices -> ConvertPixelsToPDF(szPrintedDocumentFileName,szPrintedDocumentOutlinesFileName,1,-1L);
//
//NTC: 09-16-2014: I'm going to disable putting the outlines into the document for now.
// This affects the retrieveOrFindPDFFields functions (also one in ICursiVisionServices) - which will never "retrieve" 
// these fields from the document but will go on to "find" the fields.
//
#if 0
      IPdfEnabler *pIPdfEnabler = NULL;

      if ( S_OK == CoCreateInstance(CLSID_PdfEnabler,NULL,CLSCTX_INPROC_SERVER,IID_IPdfEnabler,reinterpret_cast<void **>(&pIPdfEnabler)) ) {

         IPdfDocument *pIPdfDocument = NULL;

         if ( S_OK == pIPdfEnabler -> Document(&pIPdfDocument) ) {

            BSTR bstrFile = SysAllocStringLen(NULL,MAX_PATH);

            MultiByteToWideChar(CP_ACP,0,szPrintedDocumentFileName,-1,bstrFile,MAX_PATH);

            if ( S_OK == pIPdfDocument -> Open(bstrFile,NULL,NULL) ) {

               IPdfPage *pIPdfPage = NULL;

               if ( S_OK == pIPdfDocument -> Page(1,NULL,&pIPdfPage) )  {

                  BSTR bstrOutlines = SysAllocStringLen(NULL,MAX_PATH);

                  MultiByteToWideChar(CP_ACP,0,szPrintedDocumentOutlinesFileName,-1,bstrOutlines,MAX_PATH);

                  pIPdfPage -> AddBinaryObjectFromFile(L"cvOutlines",bstrOutlines);

                  if ( pvIPdfDocument ) {
                     IPdfDocument *pIPdfDocumentClient = reinterpret_cast<IPdfDocument *>(pvIPdfDocument);
                     IPdfPage *pIPdfPageClient = NULL;
                     pIPdfDocumentClient -> Page(1,NULL,&pIPdfPageClient);
                     pIPdfPageClient -> AddBinaryObjectFromFile(L"cvOutlines",bstrOutlines);
                  }

                  pIPdfDocument -> Write(bstrFile);

                  SysFreeString(bstrOutlines);

               }

            }

            pIPdfDocument -> Release();

         }

         pIPdfEnabler -> Release();

      }
#endif

   } else

      pICursiVisionServices -> GenerateOutlines(szPrintedDocumentFileName,szPrintedDocumentOutlinesFileName,1,-1L,&countEntries);

   pActiveProfile = NULL;

   long profileIndex = 0;

   for ( IPrintingSupportProfile *pProfile : profiles ) {

      if ( pProfile -> RecognizeByName() ) {

         if ( 0 == strcmp(szPrintedDocumentFileName,pProfile -> DocumentName()) ) {
            pActiveProfile = pProfile;
            break;
         }

         continue;

      }

      if ( 1L == pProfile -> Match(szPrintedDocumentFileName,szPrintedDocumentOutlinesFileName) ) {
         pActiveProfile = pProfile;
         break;
      } 

   }

   if ( ! pActiveProfile && szGlobalProfileName[0] && useGlobalProfile ) {

      profileIndex = 0;

      for ( IPrintingSupportProfile *pProfile : profiles ) {

         if ( 0 == strcmp(szGlobalProfileName,pProfile -> Name()) ) {
            pActiveProfile = pProfile;
            break;
         }

      }

   }

   if ( szGlobalProfileName[0] && useGlobalProfile && ! pActiveProfile ) {
      memset(szGlobalProfileName,0,sizeof(szGlobalProfileName));
      useGlobalProfile = false;
   }

#if 0
   // 
   // If the system does not find the profile by contents, it can fall back to recognizing
   // it by name.
   //
   if ( ! pActiveProfile ) {

      profileIndex = 0;

      for ( IPrintingSupportProfile *pProfile : profiles ) {

         if ( 0 == strcmp(szRootName,pProfile -> Name()) ) {
            pActiveProfile = pProfile;
            break;
         }
      
      }

   }
#endif

   hwndPrintingDocumentPage = NULL;

   realAdminPrivileges = pICursiVisionServices -> IsAdministrator() ? true : false;

   if ( pActiveProfile ) {

      pICursiVisionServices -> put_PrintingSupportProfile(pActiveProfile);

      Profile *px = static_cast<Profile *>(pActiveProfile);

      strcpy(px -> DoodleOptions() -> szDocumentName,szPrintedDocumentFileName);

#if 0
      char szNewOutlinesFileName[MAX_PATH];

      sprintf(szNewOutlinesFileName,"%s\\%s.profile",szActiveProfilesDirectory,pActiveProfile -> Name());

      CopyFile(szPrintedDocumentOutlinesFileName,szNewOutlinesFileName,FALSE);

      DeleteFile(szPrintedDocumentOutlinesFileName);

      strcpy(szPrintedDocumentOutlinesFileName,szNewOutlinesFileName);
#endif

      px -> SetOutlinesFileName(szPrintedDocumentOutlinesFileName);

      bool keepAllowSave = px -> GetAllowSaveProfile();

      px -> SetAllowSaveProfile(true);

      px -> SaveProperties();

      px -> SetAllowSaveProfile(keepAllowSave);

      long n = GetWindowTextLength(hwndMainFrame);
      char *pszTitle = new char[n + 256];
      GetWindowText(hwndMainFrame,pszTitle,n + 1);
      sprintf(pszTitle + strlen(pszTitle)," - Signature requested. Profile: %s",pActiveProfile -> Name());
      SetWindowText(hwndMainFrame,pszTitle);
      delete [] pszTitle;

      if ( px -> GetSignaturesFileName()[0] )
         DeleteFile(px -> GetSignaturesFileName());

      if ( szGlobalProfileName[0] && useGlobalProfile && 0 == strcmp(szGlobalProfileName,pActiveProfile -> Name()) ) {
         memset(&px -> DoodleOptions() -> theLocations,0,sizeof(px -> DoodleOptions() -> theLocations));
         pActiveProfile -> Start();
         return S_OK;
      }

      if ( allowNonAdminProfileSettings )
         pICursiVisionServices -> SetIsAdministrator(TRUE);

      if ( ! pActiveProfile -> IsDefined() && ! pActiveProfile -> RecognizeByName() ) {

         pICursiVisionServices -> SetIsAdministrator(TRUE);

         px -> SetAllowSaveProfile(true);

         preferredProfileIndex = profileIndex;
         isPrintJobActive = true;
         IUnknown *pIUnknown = NULL;
         QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
         pIGProperties -> ShowProperties(hwndMainFrame,pIUnknown);
         pIUnknown -> Release();

      } else

         pActiveProfile -> Start();

      if ( 0 == px -> DoodleOptions() -> countRects ) {
         pICursiVisionServices -> SetIsAdministrator(TRUE);
         px -> SetAllowSaveProfile(true);
      }

      if ( userCanceledActivePrintJob ) {
         pICursiVisionServices -> put_PrintingSupportProfile(NULL);
         return E_FAIL;
      }

      return S_OK;

   }
 
   pICursiVisionServices -> SetIsAdministrator(TRUE);

   addProfile(szPrintedDocumentFileName,szPrintedDocumentFileName,&defaultResultDisposition,false);

   profileIndex = (long)profiles.size() - 1;

   Profile *px = static_cast<Profile *>(pActiveProfile);

   px -> SetOutlinesFileName(szPrintedDocumentOutlinesFileName);

   preferredProfileIndex = profileIndex;

   isNewProfile = true;
   isPrintJobActive = true;

   IUnknown *pIUnknown = NULL;
   QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
   pIGProperties -> ShowProperties(hwndMainFrame,pIUnknown);
   pIUnknown -> Release();

   preferredProfileIndex = -1L;

   isNewProfile = false;

   if ( userCanceledActivePrintJob ) {
      pICursiVisionServices -> put_PrintingSupportProfile(NULL);
      if ( pActiveProfile ) {
         pActiveProfile -> Destroy();
         profiles.remove(pActiveProfile);
         pActiveProfile -> Release();
      }
      return E_FAIL;
   }

   pICursiVisionServices -> put_PrintingSupportProfile(pActiveProfile);

   return S_OK;
   }


   HRESULT __stdcall PrintingSupport::GetPropertiesWindow(HWND *pHWNDProperties) {
   if ( ! pHWNDProperties )
      return E_POINTER;
   *pHWNDProperties = hwndPrintingDocumentPage;
   return S_OK;
   }


   HRESULT __stdcall PrintingSupport::ServicesAdvise(void *pvCursiVisionServices) {
   pICursiVisionServices = reinterpret_cast<ICursiVisionServices *>(pvCursiVisionServices);
   return S_OK;
   }