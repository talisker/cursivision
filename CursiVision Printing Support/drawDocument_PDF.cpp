
/*
                       Copyright (c) 2009,2010,2011 Nathan T. Clark
*/

#include "PrintingSupport.h"

#include "..\resource.h"

   void fieldsPackage::drawDocument(HDC hdc,RECT *pRect) {

   if ( ! pRect ) {
      RECT rcClient;
      GetClientRect(hwndView,&rcClient);
      FillRect(hdc,&rcClient,(HBRUSH)(COLOR_APPWORKSPACE + 1));
   } else
      FillRect(hdc,pRect,(HBRUSH)(COLOR_APPWORKSPACE + 1));

   FILE *fProfile = fopen(pProfile -> szOutlinesFileName,"rb");

   char szTemp[MAX_PATH];

   if ( fProfile ) {

      long pageNumber,nextPageOffset;
      BYTE bIgnore[5];

      fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

      if ( 0 == cxPage && ! pProfile -> pIPdfEnabler ) {
         fclose(fProfile);
         return;
      }

      if ( 0 == cxPage ) {
         RECT rcPDFPage;
         IPdfPage *pIPdfPage = NULL;
         pProfile -> pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);
         pIPdfPage -> DisplayedPageSize(&rcPDFPage);
         cxPage = rcPDFPage.right - rcPDFPage.left;
         cyPage = rcPDFPage.bottom - rcPDFPage.top;
      } else {
         if ( ! ( 0 == cxPage ) && ! ( -1L == currentPageNumber ) ) {
            while ( ! ( pageNumber == currentPageNumber ) ) {
               if ( 0 == nextPageOffset ) {
                  countEntries = 0L;
                  break;
               }
               fseek(fProfile,nextPageOffset,SEEK_SET);
               if ( EOF == fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset) ) {
                  countEntries = 0L;
                  break;
               }
            }
         }
      }

   } else {

      if ( ! pProfile -> pIPdfEnabler )
         return;

      RECT rcPDFPage;
      IPdfPage *pIPdfPage = NULL;
      pProfile -> pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);
      pIPdfPage -> DisplayedPageSize(&rcPDFPage);
      cxPage = rcPDFPage.right - rcPDFPage.left;
      cyPage = rcPDFPage.bottom - rcPDFPage.top;

   }

   RECT rcView;

   GetClientRect(hwndView,&rcView);

   rcPage.left = 8;
   rcPage.top = 8;
   rcPage.bottom = rcPage.top + (rcView.bottom - rcView.top - 2 * 8);
   rcPage.right = rcPage.left + (long)((double)(rcPage.bottom - rcPage.top) * (double)cxPage / (double)cyPage);

   if ( rcPage.right > rcView.right - rcView.left ) {
      rcPage.left = 8;
      rcPage.top = 8;
      rcPage.right = rcView.right - rcView.left - 16;
      rcPage.bottom = (long)((double)(rcView.right - rcView.left - 16) * (double)cyPage / (double)cxPage);
   }

   // rcView units are in pixels
   // rcPage units are in pixels

   offsetX = (rcView.right - rcView.left) / 2; // offsetX = middle of window in pixels
   offsetX -= (rcPage.right - rcPage.left) / 2; // offsetX = left edge of page in pixels

   offsetY = (rcView.bottom - rcView.top) / 2;
   offsetY -= (rcPage.bottom - rcPage.top) / 2;

   long offsetXPixels = offsetX;
   long offsetYPixels = offsetY;

   rcPage.left += offsetX - 8;
   rcPage.right += offsetX - 8;
   rcPage.top += offsetY - 8;
   rcPage.bottom += offsetY - 8;

   parentOffset.left = parentOffsetBase.left + rcPage.left;
   parentOffset.top = parentOffsetBase.top + rcPage.top;
   parentOffset.right = parentOffset.left + rcPage.right - rcPage.left;
   parentOffset.bottom = parentOffset.top + rcPage.bottom - rcPage.top;

   HBRUSH white = CreateSolidBrush(RGB(255,255,255));

   FillRect(hdc,&rcPage,white);

   DeleteObject(white);

   RECT rcParsingPage;

   memcpy(&rcParsingPage,&rcPage,sizeof(RECT));

   RECT rcFMS,rcParent,rcAdjust = {0,0,0,0};
   GetWindowRect(hwndView,&rcFMS);
   GetWindowRect(GetParent(hwndView),&rcParent);
   AdjustWindowRect(&rcAdjust,GetWindowLong(GetParent(hwndView),GWL_STYLE),FALSE);

   rcPage.left += rcFMS.left - rcParent.left + rcAdjust.left;
   rcPage.top += rcFMS.top - rcParent.top + rcAdjust.top;
   rcPage.right += rcFMS.left - rcParent.left + rcAdjust.left;
   rcPage.bottom += rcFMS.top - rcParent.top + rcAdjust.top;

   offsetX = offsetX * cxPage / (rcPage.right - rcPage.left); // offsetX = left edge of page in page units
   offsetY = offsetY * cyPage / (rcPage.bottom - rcPage.top); // offsetY = top edge of page in page units

   if ( 0 == countEntries && pProfile -> pIPdfEnabler ) {

      if ( fProfile )
         fclose(fProfile);

#if 1
      countEntries = pProfile -> findPDFFields(currentPageNumber,hwndView,hdc,&rcParsingPage);
#else
      countEntries = pProfile -> findPDFFields(pProfile -> pDoodleOptions -> szDocumentName,pProfile -> szOutlinesFileName,currentPageNumber);
#endif

      fProfile = fopen(pProfile -> szOutlinesFileName,"rb");

      if ( countEntries ) {
         long pageNumber,nextPageOffset,toss;
         BYTE bIgnore[5];
         fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&toss,&nextPageOffset);
      }

   }

   SetMapMode(hdc,MM_ANISOTROPIC);

   SetWindowExtEx(hdc,cxPage,cyPage,NULL);
   SetViewportExtEx(hdc,rcPage.right - rcPage.left,rcPage.bottom - rcPage.top,NULL);

   HPEN hPen = CreatePen(PS_SOLID,1,RGB(255,255,255));
   HGDIOBJ oldPen = SelectObject(hdc,hPen);
   
   SetROP2(hdc,R2_XORPEN);

   if ( pPotentialRegions )
      delete [] pPotentialRegions;

   pPotentialRegions = NULL;

   if ( countEntries )
      pPotentialRegions = new RECT[countEntries];

   RECT *p = pPotentialRegions;

   for ( long k = 0; k < countEntries; k++ ) {

      fgets(szTemp,MAX_PATH,fProfile);

      sscanf(szTemp,"%04ld,%04ld,%04ld,%04ld",&p -> left,&p -> top,&p -> right,&p -> bottom);

      MoveToEx(hdc,offsetX + p -> left,p -> top + offsetY,NULL);
      LineTo(hdc,offsetX + p -> right,p -> top + offsetY);
      LineTo(hdc,offsetX + p -> right,p -> bottom + offsetY);
      LineTo(hdc,offsetX + p -> left,p -> bottom + offsetY);
      LineTo(hdc,offsetX + p -> left,p -> top + offsetY);
   
      p++;

   }

   DeleteObject(SelectObject(hdc,oldPen));

   hPen = CreatePen(PS_SOLID,1,RGB(0,228,0));
   oldPen = SelectObject(hdc,hPen);
   
   SetROP2(hdc,R2_COPYPEN);

   offsetXPixels = offsetX * (rcPage.right - rcPage.left) / cxPage;
   offsetYPixels = offsetY * (rcPage.bottom - rcPage.top) / cyPage;

   if ( pPages ) {

      long regionCount = countRegions();

      for ( long k = 0; k < regionCount; k++ ) {

         if ( ! ( pPages[k] == currentPageNumber ) )
            continue;

         RECT rcText;
         rcText.left = offsetX + pRegions[k].left;
         rcText.right = offsetX + pRegions[k].right;
         rcText.top = offsetY + pRegions[k].top;
         rcText.bottom = offsetY + pRegions[k].bottom;

         MoveToEx(hdc,rcText.left,rcText.top,NULL);
         LineTo(hdc,rcText.right,rcText.top);
         LineTo(hdc,rcText.right,rcText.bottom);
         LineTo(hdc,rcText.left,rcText.bottom);
         LineTo(hdc,rcText.left,rcText.top);

      }

   }

   DeleteObject(SelectObject(hdc,oldPen));

   SetMapMode(hdc,MM_TEXT);

   if ( pLabels && pPages ) {

      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

      HGDIOBJ oldFont = SelectObject(hdc,hGUIFont);

      long regionCount = countRegions();

      for ( long k = 0; k < regionCount; k++ ) {

         if ( ! ( pPages[k] == currentPageNumber ) )
            continue;

         char *psz = pLabels + 32 * k;
         if ( ! psz[0] )
            continue;

         RECT rcText;
         rcText.left = offsetX + pRegions[k].left;
         rcText.right = offsetX + pRegions[k].right;
         rcText.top = offsetY + pRegions[k].top;
         rcText.bottom = offsetY + pRegions[k].bottom;

         POINT pt = {rcText.left,rcText.top};

         SetMapMode(hdc,MM_ANISOTROPIC);

         SetWindowExtEx(hdc,cxPage,cyPage,NULL);
         SetViewportExtEx(hdc,rcPage.right - rcPage.left,rcPage.bottom - rcPage.top,NULL);

         LPtoDP(hdc,&pt,1);

         rcText.left = pt.x;
         rcText.top = pt.y;

         pt.x = rcText.right;
         pt.y = rcText.bottom;

         LPtoDP(hdc,&pt,1);

         rcText.right = pt.x;
         rcText.bottom = pt.y;

         SetMapMode(hdc,MM_TEXT);

         DrawTextEx(hdc,psz,strlen(psz),&rcText,DT_CENTER | DT_VCENTER | 0*DT_SINGLELINE,NULL);

      }

      SelectObject(hdc,oldFont);

   }

   if ( ! fProfile || 0 == countEntries ) {

      SetTextColor(hdc,RGB(228,0,0));

      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

      HGDIOBJ oldFont = SelectObject(hdc,(HGDIOBJ)hGUIFont);

      RECT rcText;
      rcText.left = offsetXPixels + 16;
      rcText.top = offsetYPixels + 16;
      rcText.right = rcText.left + rcPage.right - rcPage.left - 32;
      rcText.bottom = rcText.top + rcPage.bottom - rcPage.top - 32;

      char szText[1024];

      if ( pLabels )
         LoadString(hModule,IDS_ADOBE_NOREGIONS_LABELS,szText,1024);
      else
         LoadString(hModule,IDS_ADOBE_NOREGIONS,szText,1024);

      DrawText(hdc,szText,-1,&rcText,DT_VCENTER | DT_WORDBREAK);

      SelectObject(hdc,oldFont);

   }

   if ( fProfile )
      fclose(fProfile);

   return;
   }