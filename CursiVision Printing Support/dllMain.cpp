// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <windows.h>
#include <shlwapi.h>
#include <htmlhelp.h>
#include <ShlObj.h>
#include <olectl.h>
#include <stdio.h>

#define DEFINE_DATA

#include "utilities.h"

#include "PrintingSupport.h"

#include "Properties_i.c"
#include "PDFEnabler_i.c"
#include "PrintingSupport_i.c"
#include "PDFiumControl_i.c"

   OLECHAR wstrModuleName[256];

   extern "C" BOOL WINAPI DllMain(HINSTANCE hI, DWORD dwReason, LPVOID) {

   switch ( dwReason ) {

   case DLL_PROCESS_ATTACH: {

      hModule = hI;

      GetModuleFileName(hModule,szModuleName,1024);

      memset(wstrModuleName,0,sizeof(wstrModuleName));

      MultiByteToWideChar(CP_ACP, 0, szModuleName, -1, wstrModuleName, (DWORD)strlen(szModuleName));  

      char szTemp[MAX_PATH];

      GetCommonAppDataLocation(NULL,szTemp);

      sprintf(szApplicationDataDirectory,"%s\\CursiVision",szTemp);

      sprintf(szPrintDriverProfilesDirectory,"%s\\CursiVision\\Printing Profiles",szTemp);

      sprintf(szPrintDriverPrintedDocumentsDirectory,"%s\\CursiVision\\Printed Documents",szTemp);

      sprintf(szPrintDriverSignedDocumentsDirectory,"%s\\CursiVision\\Signed Printed Documents",szTemp);

      GetDocumentsLocation(NULL,szTemp);

      sprintf(szMySignedDocumentsDirectory,"%s\\My Signed Documents",szTemp);

      sprintf(szUserDirectory,"%s\\CursiVision Files",szTemp);

      strcpy(szPDFEnablerDLL,szModuleName);

      char *p = strrchr(szPDFEnablerDLL,'\\');
      *p = '\0';

      sprintf(szPDFEnablerDLL + strlen(szPDFEnablerDLL),"\\PdfEnabler.dll");

      }

      break;
  
   case DLL_PROCESS_DETACH:
      break;
  
   }
  
   return TRUE;
   }
  

   extern "C" int __cdecl DoHelp(HWND hwnd,char *szHelpFile) {
   HWND hwndHelp = HtmlHelpA(hwnd,szHelpFile,HH_DISPLAY_TOPIC,NULL);
   return 0;
   }


   class Factory : public IClassFactory {
   public:
  
      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();
      STDMETHOD (CreateInstance)(IUnknown *punkOuter, REFIID riid, void **ppv);
      STDMETHOD (LockServer)(BOOL fLock);
  
      Factory(const CLSID *pcid) : refCount(0) {memcpy(&clsid,pcid,sizeof(CLSID));};
      ~Factory() {};
  
   private:
      int refCount;
      CLSID clsid;
   };
  
  
   static Factory supportFactory(&CLSID_CursiVisionPrintingSupport);
   static Factory profileFactory(&CLSID_CursiVisionPrintingSupportProfile);
  

   STDAPI DllCanUnloadNow(void) {
   return S_FALSE;
   }
  
  
   STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppObject) {
   *ppObject = NULL;
   if ( CLSID_CursiVisionPrintingSupport != rclsid && CLSID_CursiVisionPrintingSupportProfile != rclsid ) 
      return CLASS_E_CLASSNOTAVAILABLE;
   if ( CLSID_CursiVisionPrintingSupport == rclsid )
      return supportFactory.QueryInterface(riid,ppObject);
   return profileFactory.QueryInterface(riid,ppObject);
   }
  
  
   char *OBJECT_NAME;
   char *OBJECT_NAME_V;
   GUID OBJECT_CLSID;

   STDAPI DllRegisterServer() {

   char szBase[MAX_PATH];
   char szTemp[MAX_PATH];

   GetCommonAppDataLocation(NULL,szBase);

   sprintf(szTemp,"%s\\CursiVision",szBase);
   CreateDirectory(szTemp,NULL);

   sprintf(szTemp,"%s\\CursiVision\\Settings",szBase);
   CreateDirectory(szTemp,NULL);

   sprintf(szTemp,"%s\\CursiVision\\Printing Profiles",szBase);
   CreateDirectory(szTemp,NULL);

   sprintf(szTemp,"%s\\CursiVision\\Printed Documents",szBase);
   CreateDirectory(szTemp,NULL);

   sprintf(szTemp,"%s\\CursiVision\\Signed Printed Documents",szBase);
   CreateDirectory(szTemp,NULL);

   GetDocumentsLocation(NULL,szBase);

   sprintf(szTemp,"%s\\My Signed Documents",szBase);
   CreateDirectory(szTemp,NULL);

   sprintf(szTemp,"%s\\CursiVision Files",szBase);
   CreateDirectory(szTemp,NULL);

   static long cycle = 0;

   char *OBJECT_VERSION;
   GUID OBJECT_LIBID;
   char *OBJECT_DESCRIPTION;

   if ( ! cycle ) {

      OBJECT_NAME = "InnoVisioNate.CursiVisionPrintingSupport";
      OBJECT_NAME_V = "InnoVisioNate.CursiVisionPrintingSupport.1";
      OBJECT_VERSION = "1.0";
      memcpy(&OBJECT_CLSID,&CLSID_CursiVisionPrintingSupport,sizeof(GUID));
      memcpy(&OBJECT_LIBID,&LIBID_CursiVisionPrintingSupport,sizeof(GUID));
      OBJECT_DESCRIPTION = "InnoVisioNate CursiVision Printing Support Object";

   } else {

      OBJECT_NAME = "InnoVisioNate.CursiVisionPrintingSupportProfile";
      OBJECT_NAME_V = "InnoVisioNate.CursiVisionPrintingSupportProfile.1";
      OBJECT_VERSION = "1.0";
      memcpy(&OBJECT_CLSID,&CLSID_CursiVisionPrintingSupportProfile,sizeof(GUID));
      memcpy(&OBJECT_LIBID,&LIBID_CursiVisionPrintingSupport,sizeof(GUID));
      OBJECT_DESCRIPTION = "InnoVisioNate CursiVision Printing Support Profile Object";

   }

   HRESULT rc = S_OK;
   ITypeLib *ptLib;
   HKEY keyHandle,clsidHandle;
   DWORD disposition;
   char szCLSID[256];
   LPOLESTR oleString;
  
   StringFromCLSID(OBJECT_CLSID,&oleString);
   WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);
  
   if ( S_OK != LoadTypeLib(wstrModuleName,&ptLib) )
      rc = ResultFromScode(SELFREG_E_TYPELIB);
   else
      if ( S_OK != RegisterTypeLib(ptLib,wstrModuleName,NULL) )
         rc = ResultFromScode(SELFREG_E_TYPELIB);

   RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);
  
      RegCreateKeyEx(keyHandle,szCLSID,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&clsidHandle,&disposition);
      sprintf(szTemp,OBJECT_DESCRIPTION);
      RegSetValueEx(clsidHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"Control");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"ProgID");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"InprocServer");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
  
      sprintf(szTemp,"InprocServer32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
//      RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Free",5);
//      RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Apartment",9);
      RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Both",5);
  
      sprintf(szTemp,"LocalServer");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szModuleName,(DWORD)strlen(szModuleName));
    
      sprintf(szTemp,"TypeLib");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
    
      StringFromCLSID(OBJECT_LIBID,&oleString);
      WideCharToMultiByte(CP_ACP,0,oleString,-1,szTemp,256,0,0);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
        
      sprintf(szTemp,"ToolboxBitmap32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
//      sprintf(szTemp,"%s, 1",szModuleName);
//      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,strlen(szModuleName));
  
      sprintf(szTemp,"Version");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_VERSION);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"MiscStatus");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"0");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"1");
      RegCreateKeyEx(keyHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"%ld",
                 OLEMISC_ALWAYSRUN |
                 OLEMISC_ACTIVATEWHENVISIBLE | 
                 OLEMISC_RECOMPOSEONRESIZE | 
                 OLEMISC_INSIDEOUT |
                 OLEMISC_SETCLIENTSITEFIRST |
                 OLEMISC_CANTLINKINSIDE );
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,"CurVer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME_V,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,"CLSID",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szCLSID,(DWORD)strlen(szCLSID));
  
   if ( ! cycle ) {

      cycle = 1;

      return DllRegisterServer();

   }

   return S_OK;
   }
  
  
   STDAPI DllUnregisterServer() {

   static long cycle = 0L;

   if ( ! cycle ) {

      OBJECT_NAME = "InnoVisioNate.CursiVisionPrintingSupport";
      OBJECT_NAME_V = "InnoVisioNate.CursiVisionPrintingSupport.1";
      memcpy(&OBJECT_CLSID,&CLSID_CursiVisionPrintingSupport,sizeof(GUID));

   } else {

      OBJECT_NAME = "InnoVisioNate.CursiVisionPrintingSupportProfile";
      OBJECT_NAME_V = "InnoVisioNate.CursiVisionPrintingSupportProfile.1";
      memcpy(&OBJECT_CLSID,&CLSID_CursiVisionPrintingSupportProfile,sizeof(GUID));

   }

   HRESULT rc = S_OK;
   HKEY keyHandle;
   char szCLSID[256];
   LPOLESTR oleString;
  
   StringFromCLSID(OBJECT_CLSID,&oleString);
   WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);

   RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);

   rc = SHDeleteKey(keyHandle,szCLSID);

   rc = SHDeleteKey(HKEY_CLASSES_ROOT,OBJECT_NAME);

   rc = SHDeleteKey(HKEY_CLASSES_ROOT,OBJECT_NAME_V);

   if ( ! cycle ) {

      cycle = 1;

      return DllUnregisterServer();

   }

   return S_OK;
   }
  
  
   long __stdcall Factory::QueryInterface(REFIID iid, void **ppv) { 
   *ppv = NULL; 
   if ( iid == IID_IUnknown || iid == IID_IClassFactory ) 
      *ppv = this; 
   else 
      return E_NOINTERFACE; 
   AddRef(); 
   return S_OK; 
   } 
  
  
   unsigned long __stdcall Factory::AddRef() { 
   return ++refCount; 
   } 
  
  
   unsigned long __stdcall Factory::Release() { 
   return --refCount;
   } 
  
  
   HRESULT STDMETHODCALLTYPE Factory::CreateInstance(IUnknown *punkOuter, REFIID riid, void **ppv) { 
   HRESULT hres;
   *ppv = NULL; 
   if ( CLSID_CursiVisionPrintingSupport == clsid ) {
      PrintingSupport *pef = new PrintingSupport(punkOuter);
      hres = pef -> QueryInterface(riid,ppv);
      if ( ! *ppv ) 
         delete pef;
   } else {
      Profile *pef = new Profile(punkOuter);
      hres = pef -> QueryInterface(riid,ppv);
      if ( ! *ppv ) 
         delete pef;
   }
   return hres;
   } 
  
  
   long __stdcall Factory::LockServer(int fLock) { 
   return S_OK; 
   }