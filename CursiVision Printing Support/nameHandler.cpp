// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

   LRESULT CALLBACK Profile::nameHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   Profile *pProfile = NULL;
   if ( p )
      pProfile = (Profile *)(p -> pParent);

   switch ( msg ) {

   case WM_INITDIALOG: {
      PROPSHEETPAGE *pPage = reinterpret_cast<PROPSHEETPAGE *>(lParam);
      p = (resultDisposition *)pPage -> lParam;
      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);
      pProfile = (Profile *)(p -> pParent);
      SetDlgItemText(hwnd,IDDI_PROFILE_NAME,pProfile -> Name());
      memset(pProfile -> szProposedNewName,0,sizeof(pProfile -> szProposedNewName));

      IPrintingSupportProfile *px = NULL;

      if ( pProfile -> pPrintingSupport -> pICursiVisionServices ) 
         pProfile -> pPrintingSupport -> pICursiVisionServices -> get_PrintingSupportProfile(&px);
      if ( ! pProfile -> pPrintingSupport -> pICursiVisionServices -> IsAdministrator() && px ) {
         RECT rc = {0};
         GetClientRect(hwnd,&rc);
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,rc.bottom - 32,0,0,SWP_NOSIZE);
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);
         EnableWindow(hwnd,FALSE);
      } else
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

      }
      return LRESULT(FALSE);

   case WM_NOTIFY: {

      NMHDR *pNotifyHeader = (NMHDR *)lParam;

      switch ( pNotifyHeader -> code ) {

      case PSN_APPLY: {

         PSHNOTIFY *pNotify = (PSHNOTIFY *)lParam;

         if ( pNotify -> lParam ) {

            char szTemp[512];

            GetDlgItemText(hwnd,IDDI_PROFILE_NAME,szTemp,512);

            if ( _stricmp(szTemp,pProfile -> Name()) ) {

               bool isValid = true;

               for ( std::list<IPrintingSupportProfile *>::iterator it = pProfile -> pPrintingSupport -> profiles.begin(); it != pProfile -> pPrintingSupport -> profiles.end(); it++ ) {
                  IPrintingSupportProfile *pOtherProfile = *it;
                  Profile *px = static_cast<Profile *>(pOtherProfile);
                  if ( px == pProfile )
                     continue;
                  if ( 0 == strcmp(szTemp,px -> szName) ) {
                     char szMessage[1024];
                     sprintf(szMessage,"A profile with the name:\n\n\t%s\n\nAlready exists. Please delete that profile if you want to reuse this name",szTemp);
                     MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
                     isValid = false;
                     break;
                  }
               }

               if ( ! isValid ) {
                  SetWindowLongPtr(hwnd,DWLP_MSGRESULT,PSNRET_INVALID);
                  return (LRESULT)TRUE;
               }

               strcpy(pProfile -> szProposedNewName,szTemp);

            }

         }

         SetWindowLongPtr(hwnd,DWLP_MSGRESULT,PSNRET_NOERROR);

         }
         return (LRESULT)TRUE;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }
