// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include <olectl.h>


   extern char szProfileInput[];


   HRESULT __stdcall Profile::ClearTargets() {
   if ( szSignaturesFileName[0] )
      DeleteFile(szSignaturesFileName);
   return S_OK;
   }

   
   HRESULT __stdcall Profile::SaveTarget(long targetIndex,RECT *pTarget,long page,long adobePageNumber) {

   if ( ! allowSaveProfile )
      return S_OK;

   if ( ! pTarget )
      return E_POINTER;

   if ( ! theDoodleOptions.processingDisposition.doRemember )
      return S_OK;

   if ( TARGET_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   if ( theDoodleOptions.theLocations[targetIndex].documentRect.left == theDoodleOptions.theLocations[targetIndex].documentRect.right )
      memcpy(&theDoodleOptions.theLocations[targetIndex].documentRect,pTarget,sizeof(RECT));

   theDoodleOptions.theLocations[targetIndex].zzpdfPageNumber = page;
   theDoodleOptions.theLocations[targetIndex].pdfAdobePageNumber = adobePageNumber;

   theDoodleOptions.countRects = max(theDoodleOptions.countRects,targetIndex + 1);

   return S_OK;
   }

   
   HRESULT __stdcall Profile::ReplaceTarget(long targetIndex,RECT *pTarget,long page,long adobePageNumber) {

   if ( ! pTarget )
      return E_POINTER;

   if ( ! allowSaveProfile )
      return S_OK;

   if ( ! theDoodleOptions.processingDisposition.doRemember )
      return S_OK;

   if ( TARGET_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   memcpy(&theDoodleOptions.theLocations[targetIndex].documentRect,pTarget,sizeof(RECT));

   theDoodleOptions.theLocations[targetIndex].zzpdfPageNumber = page;

   theDoodleOptions.theLocations[targetIndex].pdfAdobePageNumber = adobePageNumber;

   theDoodleOptions.countRects = max(theDoodleOptions.countRects,targetIndex + 1);

   return S_OK;
   }


   HRESULT __stdcall Profile::GetTarget(long targetIndex,RECT *pTarget,long *pPage,long *pAdobePageNumber) {

   if ( ! pTarget )
      return E_POINTER;

   if ( ! theDoodleOptions.processingDisposition.doRemember )
      return E_FAIL;

   if ( TARGET_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   if ( targetIndex > theDoodleOptions.countRects - 1 )
      return E_FAIL;

   if ( theDoodleOptions.theLocations[targetIndex].documentRect.left == theDoodleOptions.theLocations[targetIndex].documentRect.right )
      return E_FAIL;

   memcpy(pTarget,&theDoodleOptions.theLocations[targetIndex].documentRect,sizeof(RECT));

   if ( pPage )
      *pPage = theDoodleOptions.theLocations[targetIndex].zzpdfPageNumber;

   if ( pAdobePageNumber )
      *pAdobePageNumber = theDoodleOptions.theLocations[targetIndex].pdfAdobePageNumber;

   return S_OK;
   }


   HRESULT __stdcall Profile::RemoveTarget(long index) {
   MessageBox(NULL,"Implement Profile::RemoveTarget","Programmer Error",MB_OK);
   return E_FAIL;
   }


   HRESULT __stdcall Profile::Begin() {
   return S_OK;
   }


   HRESULT __stdcall Profile::GetResultDisposition(void **ppVoid) {
   if ( ! ppVoid ) 
      return E_POINTER;
   *ppVoid = reinterpret_cast<void *>(&theDoodleOptions.processingDisposition);
   return S_OK;
   }


   HRESULT __stdcall Profile::GetDoodleProperties(void **ppVoid) {
   if ( ! ppVoid ) 
      return E_POINTER;
   *ppVoid = reinterpret_cast<void *>(&theDoodleOptions);
   return S_OK;
   }


   char * __stdcall Profile::GetDispositionSettingsFileName() {

   static char szSettingsName[MAX_PATH];

   BSTR bstrFileName;

   pIGProperties -> get_FileName(&bstrFileName);

   WideCharToMultiByte(CP_ACP,0,bstrFileName,-1,szSettingsName,MAX_PATH,0,0);

   char *p = strrchr(szSettingsName,'.');

   if ( p )
      *p = '\0';

   p = strrchr(szSettingsName,'\\');
   if ( ! p )
      p = strrchr(szSettingsName,'/');
   if ( p )
      *p = '\0';

   sprintf(szSettingsName + strlen(szSettingsName),"\\%s.disposition",NULL == p ? "" : p + 1);

   if ( ! allowSaveProfile ) {
   
      FILE *fExists = fopen(szSettingsName,"rb");

      if ( fExists ) {
         fclose(fExists);
         return szSettingsName;
      }

   }

   IGProperties *pProperties;

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pProperties));

   pProperties -> Add(L"result disposition",NULL);

   pProperties -> DirectAccess(L"result disposition",TYPE_BINARY,&theDoodleOptions.processingDisposition,sizeof(resultDisposition));
   
   BSTR bstrNewFileName = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szSettingsName,-1,bstrNewFileName,MAX_PATH);

   pProperties -> put_FileName(bstrNewFileName);

   pProperties -> Save();

   pProperties -> Release();

   SysFreeString(bstrNewFileName);

   return szSettingsName;
   }


   HRESULT __stdcall Profile::ShowProperties(HWND hwnd,IUnknown *pObject ) {
   pIGProperties -> ShowProperties(hwnd,pObject);
   if ( szProposedNewName[0] )
      rename(szProposedNewName);
   return S_OK;
   }


   long Profile::Match(char *pszOriginalDocument,char *pszOutlinesFile) {

   if ( ! isInitialized )
      completeInitialize();

   if ( ! IsDefined() )
      return 0;

   long countEntries;

   RECT *pPDFEntries = NULL;
   long *pPDFPages = NULL;
   char *pTextValues = NULL;

   bool wereEntries = readOutlines(&pPDFEntries,&pPDFPages,pszOriginalDocument,pszOutlinesFile,&pTextValues,&countEntries,NULL,NULL);

   if ( ! wereEntries ) {
      if ( pPDFEntries )
         delete [] pPDFEntries;
      if ( pPDFPages )
         delete [] pPDFPages;
      if ( pTextValues )
         delete [] pTextValues;
      return -1L;
   }

   RECT *p = expectedRects;
   char *pText = expectedText;
   long *pExpectedPage = expectedPage;

   while ( p -> left || p -> right ) {

      RECT *pProfileRect = pPDFEntries;
      char *pProfileText = pTextValues;
      long *pProfilePage = pPDFPages;

      bool matchFound = false;

      char szCompositeText[1024];
      memset(szCompositeText,0,sizeof(szCompositeText));

      for ( long k = 0; k < countEntries; k++, pProfileRect++, pProfilePage++, pProfileText += 33 ) {

         if ( *pProfilePage != *pExpectedPage )
            continue;

         if ( pProfileRect -> left >= p -> left && pProfileRect -> right <= p -> right &&
                  pProfileRect -> top <= p -> top && pProfileRect -> bottom >= p -> bottom ) {

            long n = max(0,1023 - (long)strlen(szCompositeText));

            if ( n > 0 )
               strncpy(szCompositeText + strlen(szCompositeText),pProfileText,n);

            matchFound = true;

            continue;

         } else {

            if ( abs(p -> left - pProfileRect -> left) > PDF_RECT_LEFT_SLOP )
               continue;

            if ( abs(p -> top - pProfileRect -> top) > PDF_RECT_TOP_SLOP || abs(p -> bottom - pProfileRect -> bottom) > PDF_RECT_TOP_SLOP )
               continue;

         }

         if ( 0 == strncmp(pProfileText,pText,strlen(pText)) ) {
            matchFound = true;
            break;
         }

      }

      if ( ! matchFound ) {
         if ( pPDFEntries )
            delete [] pPDFEntries;
         if ( pPDFPages )
            delete [] pPDFPages;
         if ( pTextValues )
            delete [] pTextValues;
         return 0;
      }

      if ( szCompositeText[0] ) {
         if ( memcmp(szCompositeText,pText,strlen(pText)) ) {
            if ( pPDFEntries )
               delete [] pPDFEntries;
            if ( pPDFPages )
               delete [] pPDFPages;
            if ( pTextValues )
               delete [] pTextValues;
            return 0;
         }
      }

      p++;
      pText += 33;
      pExpectedPage++;

   }

   if ( pPDFEntries )
      delete [] pPDFEntries;

   if ( pPDFPages )
      delete [] pPDFPages;

   if ( pTextValues )
      delete [] pTextValues;

   return 1L;
   }


#if 1

   char * __stdcall Profile::SignatureGraphicFileName() {
   return szSignaturesFileName;
   }

#else

   long __stdcall Profile::SavedSignatureGraphicCount() {
   FILE *fSignatures = fopen(szSignaturesFileName,"rb");
   if ( ! fSignatures )
      return 0L;
   long signatureCount = 0L;
   fread(&signatureCount,sizeof(long),1,fSignatures);
   fclose(fSignatures);
   return signatureCount;
   }

   HRESULT __stdcall Profile::SaveSignatureGraphic(void *pvSignatureData) {

   signatureGraphic *pSG = reinterpret_cast<signatureGraphic *>(pvSignatureData);

   FILE *fSignatures = fopen(szSignaturesFileName,"r+b");

   long signatureCount = 0L;

   if ( fSignatures ) {
      fread(&signatureCount,sizeof(long),1,fSignatures);
      fseek(fSignatures,0,SEEK_END);
   } else
      fSignatures = fopen(szSignaturesFileName,"w+b");

   signatureCount++;

   long currentPosition = ftell(fSignatures);

   fseek(fSignatures,0,SEEK_SET);

   fwrite(&signatureCount,sizeof(long),1,fSignatures);

   if ( currentPosition )
      fseek(fSignatures,currentPosition,SEEK_SET);
   else
      currentPosition = sizeof(long);

   long nextOffset = 0L;

   fwrite(&nextOffset,sizeof(long),1,fSignatures);

   double oldScaleX = pSG -> scaleToPDFX;
   double oldScaleY = pSG -> scaleToPDFY;
   long oldWindowsOriginX = pSG -> windowsOriginX;
   long oldWindowsOriginY = pSG -> windowsOriginY;
   long oldWindowsWidth = pSG -> windowsWidth;
   long oldWindowsHeight = pSG -> windowsHeight;
   long oldOffsetX = pSG -> offsetX;
   long oldOffsetY = pSG -> offsetY;

   pSG -> scaleToPDFX = 1.0;
   pSG -> scaleToPDFY = 1.0;
   pSG -> windowsOriginX = (long)((double)pSG -> windowsOriginX * oldScaleX);
   pSG -> windowsOriginY = (long)((double)pSG -> windowsOriginY * oldScaleY);
   pSG -> windowsWidth = (long)((double)pSG -> windowsWidth * oldScaleX);
   pSG -> windowsHeight = (long)((double)pSG -> windowsHeight * oldScaleY);
   pSG -> offsetX = (long)((double)pSG -> offsetX * oldScaleX);
   pSG -> offsetY = (long)((double)pSG -> offsetY * oldScaleY);

   fwrite(pSG,sizeof(signatureGraphic),1,fSignatures);

   for ( long k = 0; k < pSG -> totalPoints; k++ ) {
      fwrite(&pSG -> pSignatureDataPage[k],sizeof(long),1,fSignatures);
      fwrite(&pSG -> pSignaturePageLabel[k * 8],8 * sizeof(char),1,fSignatures);
      double x = pSG -> pSignatureDataX[k];
      if ( 0.0 != x ) {
         x *= oldScaleX;
         x += pSG -> offsetX;
      }
      fwrite(&x,sizeof(double),1,fSignatures);
      double y = pSG -> pSignatureDataY[k];
      if ( 0.0 != y ) {
         y *= oldScaleY;
         y += pSG -> offsetY;
      }
      fwrite(&y,sizeof(double),1,fSignatures);
   }

   pSG -> scaleToPDFX = oldScaleX;
   pSG -> scaleToPDFY = oldScaleY;
   pSG -> windowsOriginX = oldWindowsOriginX;
   pSG -> windowsOriginY = oldWindowsOriginY;
   pSG -> windowsWidth = oldWindowsWidth;
   pSG -> windowsHeight = oldWindowsHeight;
   pSG -> offsetX = oldOffsetX;
   pSG -> offsetY = oldOffsetY;

   nextOffset = ftell(fSignatures);
   fseek(fSignatures,currentPosition,SEEK_SET);
   fwrite(&nextOffset,sizeof(long),1,fSignatures);

   fclose(fSignatures);

   return S_OK;
   }

   void *Profile::GetSignatureGraphic(long index) {

   FILE *fSignatures = fopen(szSignaturesFileName,"rb");

   if ( ! fSignatures )
      return NULL;

   long countSignatures = 0L;
   long nextOffset = 0L;

   fread(&countSignatures,sizeof(long),1,fSignatures);
   fread(&nextOffset,sizeof(long),1,fSignatures);

   signatureGraphic *pSG = new signatureGraphic();

   for ( long k = 0; k < index; k++ ) {
      fseek(fSignatures,nextOffset,SEEK_SET);
      fread(&nextOffset,sizeof(long),1,fSignatures);
   }

   fread(pSG,sizeof(signatureGraphic),1,fSignatures);

   pSG -> pSignatureDataX = new double[pSG -> totalPoints];
   pSG -> pSignatureDataY = new double[pSG -> totalPoints];
   pSG -> pSignatureDataPage = new long[pSG -> totalPoints];
   pSG -> pSignaturePageLabel = new char[pSG -> totalPoints * 8];
   memset(pSG -> pSignaturePageLabel,0,(pSG -> totalPoints * 8) * sizeof(char));

   for ( long k = 0; k < pSG -> totalPoints; k++ ) {
      fread(&pSG -> pSignatureDataPage[k],sizeof(long),1,fSignatures);
      fread(&pSG -> pSignaturePageLabel[k * 8],8 * sizeof(char),1,fSignatures);
      fread(&pSG -> pSignatureDataX[k],sizeof(double),1,fSignatures);
      fread(&pSG -> pSignatureDataY[k],sizeof(double),1,fSignatures);
   }

   fclose(fSignatures);

   return reinterpret_cast<void *>(pSG);
   }
#endif


   void *Profile::GetTextOutlines(long pageNumber,long *pReportedPageWidth,long *pReportedPageHeight) {

   FILE *fProfile = fopen(szOutlinesFileName,"rb");

   if ( ! fProfile ) {
      long countEntries = 0L;
      pPrintingSupport -> pICursiVisionServices -> GenerateOutlines(pDoodleOptions -> szDocumentName,szOutlinesFileName,1,3,&countEntries);
      if ( 0 == countEntries )
         return NULL;
      fProfile = fopen(szOutlinesFileName,"rb");
   }

   long pn,cxPage,cyPage,countEntries,nextPageOffset;
   char bIgnore[5];

   fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pn,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   bool isPDFAuto = false;
   long rectOffset = 0;
   if ( 0 == strncmp(bIgnore,"AUTO",4) ) {
      isPDFAuto = true;
      rectOffset = 21;
   }

   while ( ! ( pn == pageNumber ) ) {
      fseek(fProfile,nextPageOffset,SEEK_SET);
      long rc = fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pn,&cxPage,&cyPage,&countEntries,&nextPageOffset);
      if ( rc < 6 ) {
         fclose(fProfile);
         DeleteFile(szOutlinesFileName);
         return GetTextOutlines(pageNumber,pReportedPageWidth,pReportedPageHeight);
      }
      if ( 0 == nextPageOffset )
         break;
   }
   
   *pReportedPageWidth = cxPage;
   *pReportedPageHeight = cyPage;

   RECT *pRectangles = new RECT[countEntries + 1];
   
   memset(pRectangles,0,(countEntries + 1) * sizeof(RECT));

   RECT *p = pRectangles;

   char szTemp[2048];
   for ( long k = 0; k < countEntries; k++ ) {
      fgets(szTemp,2048,fProfile);
      sscanf(&szTemp[rectOffset],"%04ld,%04ld,%04ld,%04ld",&p -> left,&p -> bottom,&p -> right,&p -> top);
      p++;
   }

   fclose(fProfile);

   return reinterpret_cast<void *>(pRectangles);
   }


   void Profile::FreeTextOutlines(void *pvRectangles) {
   RECT *pRects = reinterpret_cast<RECT *>(pvRectangles);
   delete [] pRects;
   return;
   }