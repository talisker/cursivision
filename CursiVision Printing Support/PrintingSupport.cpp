// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include <process.h>

#define DEFINE_DATA

#include "CursiVision_i.h"
#include "CursiVision_i.c"

   IPrintingSupportProfile *PrintingSupport::pActiveProfile = NULL;

   PrintingSupport::PrintingSupport(IUnknown *pUnkOuter) :

      pIGProperties(NULL),
      pIGPropertyPageClient(NULL),

      pICursiVisionServices(NULL),

      hwndPrintingDevicePage(NULL),
      hwndPrintingDocumentPage(NULL),

      preferredProfileIndex(-1L),
      countProfiles(0),

      useGlobalProfile(false),
      usePDFRenderer(true),
      doManualSelection(false),

      isNewProfile(false),
      isManualSelection(false),
      isPrintJobActive(false),
      userCanceledActivePrintJob(false),

      isProfileDirectoryParsed(false),

      allowNonAdminProfileSettings(true),
      realAdminPrivileges(false),

      pTemplateDocument(NULL),

      pInitializeCallback(NULL),
      pCallerObject(NULL),

      refCount(0)

   {

   rcEditDialog.left = 32;
   rcEditDialog.top = 32;
   rcEditDialog.right = 768 - 32;
   rcEditDialog.bottom = 993 - 32;

   memset(&defaultResultDisposition,0,sizeof(resultDisposition));

   memset(szPrintedDocumentFileName,0,sizeof(szPrintedDocumentFileName));
   memset(szPrintedDocumentOutlinesFileName,0,sizeof(szPrintedDocumentOutlinesFileName));
   memset(szPotentialProfileName,0,sizeof(szPotentialProfileName));

   defaultResultDisposition.isGlobalDisposition = false;

   defaultResultDisposition.doSave = true;
   defaultResultDisposition.doRetain = true;
   defaultResultDisposition.saveIn = true;
   defaultResultDisposition.saveMyDocuments = false;

   strcpy(defaultResultDisposition.szFileStorageDirectory,szPrintDriverSignedDocumentsDirectory);

   HKEY hKeySettings = NULL;

   RegOpenKeyEx(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate\\CursiVision",0L,KEY_QUERY_VALUE,&hKeySettings);
   
   if ( ! ( NULL == hKeySettings ) ) {

      DWORD cb = MAX_PATH;
      DWORD dwType = REG_SZ;
      RegQueryValueEx(hKeySettings,"Global Settings Store",NULL,&dwType,(BYTE *)&szGlobalDataStore,&cb);

      RegCloseKey(hKeySettings);

   } else

      szGlobalDataStore[0] = '\0';

   if ( szGlobalDataStore[0] ) {

      char szTemp[1024];

      sprintf(szTemp,"%s\\Settings",szGlobalDataStore);

      BOOL rc = CreateDirectory(szTemp,NULL);

      if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
         sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szGlobalDataStore,GetLastError());
         MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
      } else {

         sprintf(szTemp,"%s\\Printing Profiles",szGlobalDataStore);
         rc = CreateDirectory(szTemp,NULL);

         if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
            sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szGlobalDataStore,GetLastError());
            MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
         } else {

            sprintf(szTemp,"%s\\Printed Documents",szGlobalDataStore);
            rc = CreateDirectory(szTemp,NULL);

            if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
               sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szGlobalDataStore,GetLastError());
               MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
            } else {
               sprintf(szTemp,"%s\\Signed Printed Documents",szGlobalDataStore);
               rc = CreateDirectory(szTemp,NULL);
               if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
                  sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szGlobalDataStore,GetLastError());
                  MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
               }

            }

         }

      }

   }

   ICatInformation *pICatInformation = NULL;

   HRESULT rc = CoCreateInstance(CLSID_StdComponentCategoriesMgr,NULL,CLSCTX_ALL,IID_ICatInformation,reinterpret_cast<void **>(&pICatInformation));

   IEnumCLSID *pIEnumCLSID = NULL;

   CATID catIds[] = { IID_ICursiVisionBackEnd };

   pICatInformation -> EnumClassesOfCategories(1,catIds,0,NULL,&pIEnumCLSID);

   CATEGORYINFO catInfo;

   memset(&catInfo,0,sizeof(CATEGORYINFO));

   unsigned long countFound;
   CLSID clsid;

//#define CREATE_BACKENDS 1
//
//NTC: 09-22-2018: Currently the system relies on all backends being instantiated at system startup time - which is a little bit slow, 
// especially if a backend does a lot of processing and/or IO when it initializes.
// This should be changed to create a back-end at the point of use, and to get the description of the back-end from the registry rather
// than from it's "Description" property.
//
   while ( S_OK == pIEnumCLSID -> Next(1,&clsid,&countFound) ) {

      if ( ! countFound )
         break;

      BSTR bstrClsid;

      StringFromCLSID(clsid,&bstrClsid);

#if CREATE_BACKENDS
      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      rc = CoCreateInstance(clsid,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( S_OK != rc )
         continue;

      pICursiVisionBackEnd -> ServicesAdvise(pICursiVisionServices);
#endif

      backEndPackage *pBackendPackage = new backEndPackage();

      memset(pBackendPackage,0,sizeof(backEndPackage));

#if CREATE_BACKENDS
      BSTR bstrDescription = NULL;

      pICursiVisionBackEnd -> get_Description(&bstrDescription);

      CoTaskMemFree(bstrClsid);

      WideCharToMultiByte(CP_ACP,0,bstrDescription,-1,pBackendPackage -> szDescription,MAX_PATH,0,0);

      SysFreeString(bstrDescription);

      pICursiVisionBackEnd -> get_CodeName(&bstrDescription);

      WideCharToMultiByte(CP_ACP,0,bstrDescription,-1,pBackendPackage -> szCodeName,32,0,0);

      SysFreeString(bstrDescription);
#else

      char szCLSID[256];

      WideCharToMultiByte(CP_ACP,0,bstrClsid,-1,szCLSID,256,0,0);

      HKEY keyHandle;
      HKEY clsidHandle;
      DWORD dwType = REG_SZ;
      DWORD dwLength = 256;

      RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_QUERY_VALUE,&keyHandle);
      RegOpenKeyEx(keyHandle,szCLSID,0,KEY_QUERY_VALUE,&clsidHandle);
      RegQueryValueEx(clsidHandle,NULL,NULL,&dwType,(BYTE *)pBackendPackage -> szDescription,&dwLength);

#endif

      memcpy(&pBackendPackage -> objectId,&clsid,sizeof(GUID));

      allBackEnds.insert(allBackEnds.end(),pBackendPackage);

   }

   char szTemp[512];

   pIGPropertyPageClient = new _IGPropertyPageClient(this);

   CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   if ( ! pIGProperties ) {
      sprintf(szTemp,"The Properties control is not registered.\nYou may need to reinstall CursiVision.");
      MessageBox(NULL,szTemp,"Error!",MB_ICONEXCLAMATION);
      return;
   }

   memset(szGlobalProfileName,0,sizeof(szGlobalProfileName));

   sprintf(szTemp,"%s\\Settings\\CursiVisionPrintingSupport.settings",szApplicationDataDirectory);

   BSTR bstrFileName = SysAllocStringLen(NULL,512);

   MultiByteToWideChar(CP_ACP,0,szTemp,-1,bstrFileName,512);

   pIGProperties -> put_FileName(bstrFileName);

   pIGProperties -> AdvisePropertyPageClient(pIGPropertyPageClient,true);

   SysFreeString(bstrFileName);

   pIGProperties -> Add(L"edit dialog position",NULL);
   pIGProperties -> DirectAccess(L"edit dialog position",TYPE_BINARY,&rcEditDialog,sizeof(RECT));

   pIGProperties -> Add(L"use global profile",NULL);
   pIGProperties -> DirectAccess(L"use global profile",TYPE_BINARY,&useGlobalProfile,sizeof(useGlobalProfile));

   pIGProperties -> Add(L"global profile name",NULL);
   pIGProperties -> DirectAccess(L"global profile name",TYPE_BINARY,szGlobalProfileName,sizeof(szGlobalProfileName));

   pIGProperties -> Add(L"default result disposition",NULL);
   pIGProperties -> DirectAccess(L"default result disposition",TYPE_BINARY,&defaultResultDisposition,sizeof(defaultResultDisposition));

   pIGProperties -> Add(L"use PDF renderer",NULL);
   pIGProperties -> DirectAccess(L"use PDF renderer",TYPE_BINARY,&usePDFRenderer,sizeof(usePDFRenderer));

   pIGProperties -> Add(L"do manual selection",NULL);
   pIGProperties -> DirectAccess(L"do manual selection",TYPE_BINARY,&doManualSelection,sizeof(doManualSelection));

   pIGProperties -> Add(L"allow non-admin edits",NULL);
   pIGProperties -> DirectAccess(L"allow non-admin edits",TYPE_BINARY,&allowNonAdminProfileSettings,sizeof(allowNonAdminProfileSettings));

   short bSuccess;

   pIGProperties -> LoadFile(&bSuccess);

   if ( szGlobalDataStore[0] ) {
      sprintf(szActiveProfilesDirectory,"%s\\Printing Profiles",szGlobalDataStore);
      sprintf(szActiveSignedDocumentsDirectory,"%s\\Signed Printed Documents",szGlobalDataStore);
   } else {
      strcpy(szActiveProfilesDirectory,szPrintDriverProfilesDirectory);
      strcpy(szActiveSignedDocumentsDirectory,szPrintDriverSignedDocumentsDirectory);
   }

   return;
   }


   PrintingSupport::~PrintingSupport() {

   if ( pIGProperties ) {
      pIGProperties -> Save();
      pIGProperties -> Release();
   }

   if ( pIGPropertyPageClient )
      pIGPropertyPageClient -> Release();

   for ( std::list<IPrintingSupportProfile *>::iterator it = profiles.begin(); it != profiles.end(); it++ ) {
      IPrintingSupportProfile *pProfile = (*it);
      pProfile -> Release();
   }

   for ( std::list<backEndPackage *>::iterator it = allBackEnds.begin(); it != allBackEnds.end(); it++ )
      delete (*it);
   
   allBackEnds.clear();

   if ( pTemplateDocument )
      delete pTemplateDocument;

   pTemplateDocument = NULL;

   return;
   }


   HRESULT PrintingSupport::InitializeThreaded(void (__stdcall * pCallback)(void *),void *pvCallerObj) {
   pInitializeCallback = pCallback;
   pCallerObject = pvCallerObj;
   unsigned threadAddress;
   _beginthreadex(NULL,16384,addAllProfiles,(void *)this,0L,&threadAddress);
   return S_OK;
   }


   unsigned int __stdcall PrintingSupport::addAllProfiles(void *pv) {

   CoInitialize(NULL);

   PrintingSupport *pThis = reinterpret_cast<PrintingSupport *>(pv);
   
   pThis -> isProfileDirectoryParsed = true;

   WIN32_FIND_DATA wfd;
   char szTemp[512];

   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   sprintf(szTemp,"%s\\*.prf",szActiveProfilesDirectory);

   HANDLE hFiles = FindFirstFile(szTemp,&wfd);
   
   pThis -> countProfiles = 0;

   if ( INVALID_HANDLE_VALUE != hFiles ) do {

      if ( wfd.cFileName[0] == '.' )
         continue;

      pThis -> countProfiles++;

      pThis -> addProfile(NULL,wfd.cFileName,&pThis -> defaultResultDisposition,false);

   } while ( FindNextFile(hFiles,&wfd) );

   pThis -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);

   if ( pThis -> pInitializeCallback )
      pThis -> pInitializeCallback(pThis -> pCallerObject);

   return 0;
   }


   long PrintingSupport::addProfile(char *pszOriginalDocument,char *pszName,resultDisposition *pDefaultCursiVisionDisposition,bool resetSignatureLocations) {
   CoCreateInstance(CLSID_CursiVisionPrintingSupportProfile,NULL,CLSCTX_INPROC_SERVER,IID_IPrintingSupportProfile,reinterpret_cast<void**>(&pActiveProfile));
   if ( S_OK == pActiveProfile -> Initialize(pszOriginalDocument,pszName,pDefaultCursiVisionDisposition,(void *)this,resetSignatureLocations) )
      profiles.insert(profiles.end(),pActiveProfile);
   else {
      pActiveProfile -> Release();
      pActiveProfile = NULL;
   }
   return (long)profiles.size();
   }


   LRESULT CALLBACK redTextHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_PAINT: {
      PAINTSTRUCT ps = {0};
      BeginPaint(hwnd,&ps);
      char szText[1024];
      GetWindowText(hwnd,szText,1024);
      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
      SelectObject(ps.hdc,hGUIFont);
      SetTextColor(ps.hdc,RGB(255,0,0));
      SetBkColor(ps.hdc,GetSysColor(COLOR_MENU));
      DrawText(ps.hdc,szText,(int)strlen(szText),&ps.rcPaint,DT_TOP);
      EndPaint(hwnd,&ps);
      }
      break;

   default:
      break;

   }

   return CallWindowProc(defaultTextHandler,hwnd,msg,wParam,lParam);
   }