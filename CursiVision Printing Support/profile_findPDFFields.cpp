// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <io.h>

#include "PrintingSupport.h"

#include "PostScript_i.h"

#include "pdfEnablerReceiveText.h"

   long Profile::retrieveOrFindPDFFields(long currentPageNumber,HDC hdcView,RECT *prcPageRect) {

#if 1

   return findPDFFields(currentPageNumber,hdcView,prcPageRect);

#else

   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   if ( NULL == pIPdfPage ) 
      return 0L;

   BSTR bstrOutlines = SysAllocStringLen(NULL,MAX_PATH);
         
   wcscpy(bstrOutlines,_wtempnam(NULL,NULL));

   if ( S_OK != pIPdfPage -> GetBinaryObjectToFile(L"cvOutlines",bstrOutlines) ) {
      DeleteFileW(bstrOutlines);
      SysFreeString(bstrOutlines);
      return findPDFFields(currentPageNumber,hdcView,prcPageRect);
   }

   char szTemp[MAX_PATH];

   WideCharToMultiByte(CP_ACP,0,bstrOutlines,-1,szTemp,MAX_PATH,0,0);

   DeleteFile(szOutlinesFileName);

   CopyFile(szTemp,szOutlinesFileName,FALSE);

   SysFreeString(bstrOutlines);

   FILE *fProfile = fopen(szOutlinesFileName,"rb");

   long pageNumber,nextPageOffset,countEntries,cxPage,cyPage;
   BYTE bIgnore[5];

   fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   while ( ! ( pageNumber == currentPageNumber ) ) {
      fseek(fProfile,nextPageOffset,SEEK_SET);
      long rc = fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);
      if ( rc < 6 ) {
         fclose(fProfile);
         return 0L;
      }
   }

   if ( ! ( NULL == hdcView ) ) {

      SetMapMode(hdcView,MM_ANISOTROPIC);

      SetWindowExtEx(hdcView,cxPage,cyPage,NULL);

      SetViewportExtEx(hdcView,prcPageRect -> right - prcPageRect -> left,prcPageRect -> bottom - prcPageRect -> top,NULL);

      HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
      HGDIOBJ oldPen = SelectObject(hdcView,hPen);

      char szProfile[2048];

      for ( long k = 0; k < countEntries; k++ ) {

         if ( NULL == fgets(szProfile,2048,fProfile) ) {
            fclose(fProfile);
            DeleteObject(SelectObject(hdcView,oldPen));
            return countEntries;
         }

         RECT rcPDF;
         sscanf(szProfile,"%04ld,%04ld,%04ld,%04ld",&rcPDF.left,&rcPDF.bottom,&rcPDF.right,&rcPDF.top);

         rcPDF.top = cyPage - rcPDF.top;
         rcPDF.bottom = cyPage - rcPDF.bottom;

         Rectangle(hdcView,rcPDF.left,rcPDF.top,rcPDF.right,rcPDF.bottom);

      }

      DeleteObject(SelectObject(hdcView,oldPen));

      SetMapMode(hdcView,MM_TEXT);

   }

   fclose(fProfile);

   return countEntries;
#endif
   }


   long Profile::findPDFFields(long currentPageNumber,HDC hdcView,RECT *prcPageRect) {

   RECT rcPDFPage;
   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   pIPdfPage -> DisplayedPageSize(&rcPDFPage);

   long cxPage = rcPDFPage.right - rcPDFPage.left;
   long cyPage = rcPDFPage.bottom - rcPDFPage.top;

   struct recieveText *pGetText = new recieveText(szOutlinesFileName,currentPageNumber,cxPage,cyPage);

   HRESULT rc = pIPdfDocument -> ParseText(currentPageNumber,hdcView,prcPageRect,reinterpret_cast<void *>(pGetText));

   long countEntries = pGetText -> countFound;

   pGetText -> close();

   delete pGetText;

   if ( ! ( S_OK == rc ) ) {
      char *pszError;
      pIPdfDocument -> GetLastError(&pszError);
      MessageBox(NULL,pszError,"Error!",MB_ICONEXCLAMATION | MB_TOPMOST);
      countEntries = 0L;
   }

   return countEntries;
   }


   long Profile::retrieveOrFindPDFFields(char *pszTargetOutlinesFile,long currentPageNumber) {

#if 1

   return findPDFFields(pszTargetOutlinesFile,currentPageNumber);

#else

   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   if ( NULL == pIPdfPage ) 
      return 0L;

   BSTR bstrOutlines = SysAllocStringLen(NULL,MAX_PATH);
         
   wcscpy(bstrOutlines,_wtempnam(NULL,NULL));

   if ( S_OK != pIPdfPage -> GetBinaryObjectToFile(L"cvOutlines",bstrOutlines) ) {
      DeleteFileW(bstrOutlines);
      SysFreeString(bstrOutlines);
      return findPDFFields(pszTargetOutlinesFile,currentPageNumber);
   }

   char szTemp[MAX_PATH];

   WideCharToMultiByte(CP_ACP,0,bstrOutlines,-1,szTemp,MAX_PATH,0,0);

   DeleteFile(pszTargetOutlinesFile);

   CopyFile(szTemp,pszTargetOutlinesFile,FALSE);

   SysFreeString(bstrOutlines);

   FILE *fProfile = fopen(pszTargetOutlinesFile,"rb");

   long pageNumber,nextPageOffset,countEntries,cxPage,cyPage;
   BYTE bIgnore[5];

   fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   while ( ! ( pageNumber == currentPageNumber ) ) {
      fseek(fProfile,nextPageOffset,SEEK_SET);
      long rc = fscanf(fProfile,"%05s%04ld:%04ld-%04ld%06ld-%08ld\n",bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);
      if ( rc < 6 ) {
         fclose(fProfile);
         return 0L;
      }
   }

   fclose(fProfile);

   return countEntries;
#endif
   }


   long Profile::findPDFFields(char *pszTargetOutlinesFile,long currentPageNumber) {

   RECT rcPDFPage;

   IPdfPage *pIPdfPage = NULL;
   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   if ( NULL == pIPdfPage ) 
      return 0L;

   pIPdfPage -> DisplayedPageSize(&rcPDFPage);

   long cxPage = rcPDFPage.right - rcPDFPage.left;
   long cyPage = rcPDFPage.bottom - rcPDFPage.top;

   struct recieveText *pGetText = new recieveText(pszTargetOutlinesFile,currentPageNumber,cxPage,cyPage);

   HDC hdcView = CreateCompatibleDC(NULL);

   HRESULT rc = pIPdfDocument -> ParseText(currentPageNumber,hdcView,&rcPDFPage,reinterpret_cast<void *>(pGetText));

   DeleteDC(hdcView);

   long countEntries = pGetText -> countFound;

   pGetText -> close();

   delete pGetText;

   if ( ! ( S_OK == rc ) ) {
      char *pszError;
      pIPdfDocument -> GetLastError(&pszError);
      MessageBox(NULL,pszError,"Error!",MB_ICONEXCLAMATION | MB_TOPMOST);
      countEntries = 0L;
   }

   return countEntries;
   }
