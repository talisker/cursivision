// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

   long __stdcall PrintingSupport::_IGPropertyPageClient::QueryInterface(REFIID riid,void **ppv) {
   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown*>(this); 
   else

   if ( riid == IID_IDispatch )
      *ppv = this;
   else

   if ( riid == IID_IGPropertyPageClient )
      *ppv = static_cast<IGPropertyPageClient*>(this);
   else
 
      return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall PrintingSupport::_IGPropertyPageClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall PrintingSupport::_IGPropertyPageClient::Release() {
   return 1;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::BeforeAllPropertyPages() {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::GetPropertyPagesInfo(long* pCntPages,SAFEARRAY** thePageNames,SAFEARRAY** theHelpDirs,SAFEARRAY** pSize) {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::CreatePropertyPage(long pageNumber,HWND hwndParent,RECT* pRect,BOOL fModal,HWND * pHwnd) {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::Apply() {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::IsPageDirty(long pageNumber,BOOL* isDirty) {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::Help(BSTR bstrHelpDir) {
   return  S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::TranslateAccelerator(long,long* pResult) {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::AfterAllPropertyPages(BOOL userCanceled) {
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::DestroyPropertyPage(long index) {

   if ( pParent -> hwndPrintingDocumentPage ) 
      DestroyWindow(pParent -> hwndPrintingDocumentPage);

   pParent -> hwndPrintingDocumentPage = NULL;

   if ( pParent -> hwndPrintingDevicePage ) 
      DestroyWindow(pParent -> hwndPrintingDevicePage);
   pParent -> hwndPrintingDevicePage = NULL;

   pParent -> pIGProperties -> Save();

   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::GetPropertySheetHeader(void *pv) {

   if ( ! pv )
      return E_POINTER;

   PROPSHEETHEADER *pHeader = reinterpret_cast<PROPSHEETHEADER *>(pv);

   pHeader -> dwFlags = PSH_PROPSHEETPAGE | PSH_NOCONTEXTHELP;
   pHeader -> hInstance = hModule;
   pHeader -> pszIcon = NULL;
   pHeader -> pszCaption = "Print to sign properties";
   pHeader -> pfnCallback = NULL;

   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::get_PropertyPageCount(long *pCount) {
   if ( ! pCount )
      return E_POINTER;
   *pCount = 1;
   return S_OK;
   }


   HRESULT PrintingSupport::_IGPropertyPageClient::GetPropertySheets(void *pPages) {

   if ( pParent -> pTemplateDocument ) {
      delete pParent -> pTemplateDocument;
      pParent -> pTemplateDocument = NULL;
   }

   pParent -> pTemplateDocument = new templateDocument(pParent -> pICursiVisionServices,NULL,NULL,NULL,NULL);

   PROPSHEETPAGE *pPropSheetPages = reinterpret_cast<PROPSHEETPAGE *>(pPages);

   pPropSheetPages[0].dwFlags = PSP_USETITLE;
   pPropSheetPages[0].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[0].hInstance = hModule;
   pPropSheetPages[0].pszTemplate = MAKEINTRESOURCE(IDD_PRINTER_DEVICE_PAGE);
   pPropSheetPages[0].pfnDlgProc = (DLGPROC)PrintingSupport::printPropertyPageHandler;
   pPropSheetPages[0].pszTitle = "Print to Sign";
   pPropSheetPages[0].lParam = (LONG_PTR)pParent;
   pPropSheetPages[0].pfnCallback = NULL;

   return S_OK;
   }
