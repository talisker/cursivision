// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"
#include "resource.h"

#include "SignaturePad_i.h"
#include "SignaturePad_i.c"

#define OBJECT_WITH_PROPERTIES Profile
#define CURSIVISION_SERVICES_INTERFACE pObject -> pPrintingSupport -> pICursiVisionServices

#include "signingLocationsDefines.h"

doodleOptionProperties *pDoodleOptionProps = NULL;

#define DOODLE_PROPERTIES_PTR  pDoodleOptionProps = pObject -> pDoodleOptions;

#define MODULE_HANDLE hModule

extern HINSTANCE hModule;

#define ADDITIONAL_INITIALIZATION                                                                              \
      IPrintingSupportProfile *px = NULL;                                                                      \
      if ( CURSIVISION_SERVICES_INTERFACE )                                                                    \
         CURSIVISION_SERVICES_INTERFACE -> get_PrintingSupportProfile(&px);                                    \
      if ( ! CURSIVISION_SERVICES_INTERFACE -> IsAdministrator() && px ) {                                     \
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,112,0,0,SWP_NOSIZE);  \
         ShowWindow(GetDlgItem(hwnd,IDDI_CV_LOCATIONS_ADDITIONAL_INFO),SW_HIDE);                               \
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);    \
         EnableWindow(hwnd,FALSE);                                                                                      \
      } else                                                                                                            \
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);                                   \
      SendMessage(GetDlgItem(hwnd,IDDI_SIGNING_LOCATIONS_SKIP_SIGNING),BM_SETCHECK,pObject -> skipSignatureCapture ? BST_CHECKED : BST_UNCHECKED, 0L);   \
      EnableWindow(GetDlgItem(hwnd,IDDI_CV_LOCATIONS_RESET),pObject -> skipSignatureCapture ? FALSE : TRUE);                        \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_CONTINUOUS_DOODLE_LEARN),pObject -> skipSignatureCapture ? FALSE : TRUE);       \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_CONTINUOUS_DOODLE_ON),pObject -> skipSignatureCapture ? FALSE : TRUE);          \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_CONTINUOUS_DOODLE_OFF),pObject -> skipSignatureCapture ? FALSE : TRUE);         \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_REMEMBER),pObject -> skipSignatureCapture ? FALSE : TRUE);

   LRESULT CALLBACK Profile::signingLocationsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

#include "signingLocationsBody.cpp"

   return LRESULT(FALSE);
   }

#include "signingLocationsSupport.cpp"
