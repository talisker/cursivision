// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

   // IUnknown

   long __stdcall PrintingSupport::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL;

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);

   if ( riid == IID_IPrintingSupport )
      *ppv = static_cast<IPrintingSupport *>(this);

   if ( *ppv ) {
      AddRef();
      return S_OK;
   }

   if ( ! ( riid == IID_IGPropertyPageClient ) && ! ( IID_IPersistStream == riid ) && ! ( IID_ISpecifyPropertyPages == riid ) )
      return E_NOINTERFACE;

   if ( 0 == profiles.size() && ! isProfileDirectoryParsed )
      addAllProfiles(reinterpret_cast<void *>(this));

#if 0
   if ( riid == IID_IGPropertyPageSupport )
      return pIGProperties -> QueryInterface(riid,ppv);
   else
#endif

   if ( riid == IID_IGPropertyPageClient )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

   if ( IID_IPersistStream == riid ) 
      return pIGProperties -> QueryInterface(riid,ppv);
   else

   if ( IID_ISpecifyPropertyPages == riid )
      return pIGProperties -> QueryInterface(riid,ppv);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall PrintingSupport::AddRef() {
   refCount++;
   return refCount;
   }

   unsigned long __stdcall PrintingSupport::Release() { 
   refCount--;
   if ( ! refCount ) {
      delete this;
      return 0;
   }
   return refCount;
   }