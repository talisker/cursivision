// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include <shellapi.h>
#include <shTypes.h>
#include <shlobj.h>
#include <olectl.h>

bool isTheGlobalProfile = false;

BOOL CALLBACK doMoveDown(HWND hwndTest,LPARAM lParam);

   static templateDocument::tdUI *pTemplateDocumentUI = NULL;

   LRESULT CALLBACK PrintingSupport::printPropertyPageHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   PrintingSupport *p = (PrintingSupport *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      PROPSHEETPAGE *pPage = (PROPSHEETPAGE *)lParam;
      p = (PrintingSupport *)pPage -> lParam;

      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);

      if ( ! p )
         break;

      if ( 0 == p -> profiles.size()&& ! p -> isProfileDirectoryParsed )
         p -> addAllProfiles(reinterpret_cast<void *>(p));

      p -> pIGProperties -> Push();

      p -> pIGProperties -> Push();

      ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_ALLOW_NON_ADMIN_ACCESS),SW_HIDE);

      SendMessage(GetDlgItem(hwnd,IDDI_PRINTING_ALLOW_NON_ADMIN_ACCESS),BM_SETCHECK,p -> allowNonAdminProfileSettings ? BST_CHECKED : BST_UNCHECKED,0L);

      if ( ! p -> isNewProfile && ! p -> isManualSelection ) {

         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN_LABEL),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE_LABEL),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE),SW_HIDE);

         if ( p -> realAdminPrivileges && p -> pICursiVisionServices -> EnforceNonAdministrativeRestrictions() ) {
            RECT rcParent = {0};
            RECT rcRestore = {0};
            GetWindowRect(hwnd,&rcParent);
            GetWindowRect(GetDlgItem(hwnd,IDDI_PRINTING_ALLOW_NON_ADMIN_ACCESS),&rcRestore);
            EnumChildWindows(hwnd,doMoveDown,(LPARAM)32);      
            SetWindowPos(GetDlgItem(hwnd,IDDI_PRINTING_ALLOW_NON_ADMIN_ACCESS),HWND_TOP,rcRestore.left - rcParent.left,rcRestore.top - rcParent.top,0,0,SWP_NOSIZE | SWP_SHOWWINDOW);
         }

      } else if ( p -> isManualSelection ) {

         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN_LABEL),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE_LABEL),SW_SHOW);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE),SW_SHOW);
         EnumChildWindows(hwnd,doMoveDown,(LPARAM)32);

      } else {

         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN_LABEL),SW_SHOW);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_JUST_SIGN),SW_SHOW);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE_LABEL),SW_HIDE);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE),SW_HIDE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILES),FALSE);
         EnumChildWindows(hwnd,doMoveDown,(LPARAM)32);

      }

      long profileCount = (long)p -> profiles.size();

      char szText[1024];

      if ( 0 == profileCount ) {

         LoadString(hModule,IDDI_PRINTING_DEVICE_SKETCH_VIEW,szText,1024);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_NAME),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DISPOSITION),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_DELETE),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILES),FALSE);

         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS),SW_SHOW);
         ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),SW_HIDE);

      } else

         LoadString(hModule,IDS_RECOGNIZE_BY_NAME,szText,1024);

      SetDlgItemText(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS,szText);

      p -> pActiveProfile = NULL;

      p -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);

      long n = 0;

      for ( std::list<IPrintingSupportProfile *>::iterator it = p -> profiles.begin(); it != p -> profiles.end(); it++, n++ ) {
         IPrintingSupportProfile *pProfile = (*it);
         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_INSERTSTRING,(WPARAM)n,(LPARAM)pProfile -> Name());
         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETITEMDATA,(WPARAM)n,(LPARAM)pProfile);
      }

      if ( -1L != p -> preferredProfileIndex )
         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETCURSEL,p -> preferredProfileIndex,0L);
      else  
         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETCURSEL,0L,0L);

      if ( profileCount ) 
         PostMessage(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_PRINTING_DEVICE_PROFILES,CBN_SELCHANGE),0L);

#ifndef _DEBUG
      SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
#endif

      SendMessage(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_SELECTION),BM_SETCHECK,(WPARAM)p -> doManualSelection ? BST_CHECKED : BST_UNCHECKED,0L);

      pTemplateDocumentUI = p -> pTemplateDocument -> createView(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),0,0,NULL);

#ifndef _DEBUG
      SetTimer(hwnd,TIMER_EVENT_BRING_TO_TOP,500,NULL);
#endif

      if ( -1L != p -> preferredProfileIndex )
         PostMessage(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_PRINTING_DEVICE_DISPOSITION,0),0L);

      if ( ! p -> allowNonAdminProfileSettings && ! p -> pICursiVisionServices -> IsAdministrator() ) {
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_DELETE),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),FALSE);
      } else if ( p -> allowNonAdminProfileSettings )
         p -> pICursiVisionServices -> SetIsAdministrator(TRUE);

      EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DISPOSITION),TRUE);

      }
      return LRESULT(FALSE);


#ifndef _DEBUG
   case WM_TIMER:
      if ( TIMER_EVENT_BRING_TO_TOP == wParam ) {
         SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
         break;
      }
      break;
#endif

   case WM_DESTROY: {
      p -> pIGProperties -> Save();
      pTemplateDocumentUI -> releaseView();
      }
      break;

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME: {

         if ( p -> pActiveProfile ) {

            long checkState = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME,BM_GETCHECK,0L,0L);

            p -> pActiveProfile -> SetRecognizeByName( BST_CHECKED == checkState );

            if ( p -> pActiveProfile -> RecognizeByName() ) {
               ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS),SW_SHOW);
               ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),SW_HIDE);
            } else {
               ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS),SW_HIDE);
               ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),SW_SHOW);
            }

         }
         }
         break;

      case IDDI_PRINTING_DEVICE_DISPOSITION: {

         if ( ! p -> pActiveProfile ) 
            break;

         char szOldName[512];

         strcpy(szOldName,p -> pActiveProfile -> Name());

         IUnknown *pIUnknown = NULL;

         p -> pActiveProfile -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

         p -> pICursiVisionServices -> put_PrintingSupportProfile(p -> pActiveProfile);

         p -> pActiveProfile -> ShowProperties(hwnd,pIUnknown);

         p -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);

         Profile *px = reinterpret_cast<Profile *>(p -> pActiveProfile);

         px -> SetAllowSaveProfile(true);

         if ( strcmp(szOldName,p -> pActiveProfile -> Name()) ) {

            long currentIndex = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCURSEL,0L,0L);
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_DELETESTRING,(WPARAM)currentIndex,0L);
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_INSERTSTRING,(WPARAM)currentIndex,(LPARAM)p -> pActiveProfile -> Name());
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETITEMDATA,(WPARAM)currentIndex,(LPARAM)p -> pActiveProfile);
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETCURSEL,(WPARAM)currentIndex,0L);

            if ( p -> useGlobalProfile && 0 == strcmp(p -> szGlobalProfileName,szOldName) )
               strcpy(p -> szGlobalProfileName,p -> pActiveProfile -> Name());

         }

         p -> pActiveProfile -> Save(NULL);

         pIUnknown -> Release();

         }
         break;

      case IDDI_PRINTING_DEVICE_JUST_SIGN: {

         long currentIndex = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCURSEL,0L,0L);

         if ( currentIndex == p -> preferredProfileIndex ) {
            p -> profiles.remove(p -> pActiveProfile);
            p -> pActiveProfile -> Destroy();
            p -> pActiveProfile -> Release();
            p -> pActiveProfile = NULL;
            p -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);
            SendMessage(GetParent(hwnd),PSM_PRESSBUTTON,(WPARAM)PSBTN_OK,0L);
         }

         }
         break;

      case IDDI_PRINTING_DEVICE_MANUAL_CREATE: {

         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_MANUAL_CREATE),FALSE);

         long n = 0;
         for ( std::list<IPrintingSupportProfile *>::iterator it = p -> profiles.begin(); it != p -> profiles.end(); it++, n++ ) {
            IPrintingSupportProfile *pProfile = (*it);
            if ( 0 == strcmp(p -> szPotentialProfileName,pProfile -> Name()) ) {
               p -> profiles.remove(pProfile);
               pProfile -> Destroy(true);
               SendMessage(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILES),CB_DELETESTRING,(WPARAM)n,0L);
               break;
            }
         }

         p -> addProfile(p -> szPrintedDocumentFileName,p -> szPrintedDocumentFileName,&p -> defaultResultDisposition,false);

         IPrintingSupportProfile *pProfile = p -> profiles.back();

         Profile *px = static_cast<Profile *>(pActiveProfile);

         px -> SetOutlinesFileName(p -> szPrintedDocumentOutlinesFileName);

         long newIndex = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_INSERTSTRING,(WPARAM)-1L,(LPARAM)pProfile -> Name());

         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETITEMDATA,(WPARAM)newIndex,(LPARAM)pProfile);

         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETCURSEL,newIndex,0L);

         PostMessage(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_PRINTING_DEVICE_PROFILES,CBN_SELCHANGE),0L);
         }
         break;

      case IDDI_PRINTING_DEVICE_PROFILE_DELETE: {

         if ( ! p -> pActiveProfile )
            break;

         long currentIndex = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCURSEL,0L,0L);

         char szName[64];
         char szMessage[256];
         GetDlgItemText(hwnd,IDDI_PRINTING_DEVICE_PROFILES,szName,64);
         sprintf(szMessage,"Are you sure you want to delete profile: %s",szName);
         if ( IDYES != MessageBox(hwnd,szMessage,"Note!",MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2 | MB_TOPMOST) )
            break;

         long countItems = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCOUNT,0L,0L);

         if ( p -> useGlobalProfile ) {
            if ( 0 == strcmp(p -> szGlobalProfileName,p -> pActiveProfile -> Name()) ) {
               p -> useGlobalProfile = false;
               memset(p -> szGlobalProfileName,0,sizeof(p -> szGlobalProfileName));
            }
         }

         p -> profiles.remove(p -> pActiveProfile);

         p -> pActiveProfile -> Destroy();

         p -> pActiveProfile -> Release();

         p -> pActiveProfile = NULL;

         p -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);

         long newIndex = currentIndex - 1;

         if ( currentIndex > countItems - 1 )
            newIndex = 0;

         if ( newIndex < 0 )
            newIndex = 0;

         if ( 1 == countItems ) {
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_RESETCONTENT,0L,0L);
            char szText[1024];
            LoadString(hModule,IDDI_PRINTING_DEVICE_SKETCH_VIEW,szText,1024);
            SetDlgItemText(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW,szText);
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),FALSE);
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DISPOSITION),FALSE);
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_DELETE),FALSE);
            p -> pActiveProfile = NULL;
            p -> pICursiVisionServices -> put_PrintingSupportProfile(NULL);
            break;
         }

         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_DELETESTRING,currentIndex,0L);

         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_SETCURSEL,newIndex,0L);

         PostMessage(hwnd,WM_COMMAND,MAKEWPARAM(IDDI_PRINTING_DEVICE_PROFILES,CBN_SELCHANGE),0L);

         }
         break;


      case IDDI_PRINTING_DEVICE_PROFILE_SET_GLOBAL: {
         long checkState = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_SET_GLOBAL,BM_GETCHECK,0L,0L);
         if ( BST_CHECKED == checkState ) {
            p -> useGlobalProfile = true;
            strcpy(p -> szGlobalProfileName,p -> pActiveProfile -> Name());
         } else {
            p -> useGlobalProfile = false;
            memset(p -> szGlobalProfileName,0,sizeof(p -> szGlobalProfileName));
         }
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),! p -> useGlobalProfile);
         break;
         }

      case IDDI_PRINTING_DEVICE_PROFILES: {

         if ( CBN_SELCHANGE != HIWORD(wParam) )
            break;

         long newIndex = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCURSEL,0L,0L);

         p -> pActiveProfile = (IPrintingSupportProfile *)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETITEMDATA,(WPARAM)newIndex,0L);

         p -> pActiveProfile -> Start();

         Profile *px = static_cast<Profile *>(p -> pActiveProfile);

         px -> SetAllowSaveProfile(true);

         FILE *fExists = NULL;

         if ( ! ( NULL == p -> pActiveProfile -> OutlinesFileName() ) )
            fExists = fopen(p -> pActiveProfile -> OutlinesFileName(),"rb");

         if ( ! fExists ) {

            fExists = fopen(p -> pActiveProfile -> DocumentName(),"rb");

            if ( fExists ) {
               fclose(fExists);
               long countEntries = 0L;
               px -> GetPrintingSupport() -> pICursiVisionServices -> GenerateOutlines(p -> pActiveProfile -> DocumentName(),p -> pActiveProfile -> OutlinesFileName(),1,3,&countEntries);
            }

         } else

            fclose(fExists);

         p -> pTemplateDocument -> openDocument(p -> pActiveProfile -> DocumentName(),p -> pActiveProfile -> OutlinesFileName());

         SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME,BM_SETCHECK,p -> pActiveProfile -> RecognizeByName() ? BST_CHECKED : BST_UNCHECKED,0L);

         if ( p -> useGlobalProfile ) {
            if ( p -> szGlobalProfileName[0] && 0 == strcmp(p -> pActiveProfile -> Name(),p -> szGlobalProfileName) ) {
               SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_SET_GLOBAL,BM_SETCHECK,(WPARAM)BST_CHECKED,0L);
               isTheGlobalProfile = true;
            } else {
               SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_SET_GLOBAL,BM_SETCHECK,0L,0L);
               isTheGlobalProfile = false;
            }
         } else
            SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILE_SET_GLOBAL,BM_SETCHECK,0L,0L);


         long countItems = (long)SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_PROFILES,CB_GETCOUNT,0L,0L);

         if ( p -> useGlobalProfile && ! isTheGlobalProfile ) {


         } else {

            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DOODLE_INSTRUCTIONS_1),TRUE);
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DOODLE_INSTRUCTIONS_2),TRUE);

            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DISPOSITION),countItems ? TRUE : FALSE);

            SetWindowText(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_DOODLE_INSTRUCTIONS_1),
                              "When you close this Dialog, CursiVision will enter Writing mode. "
                              "Move the mouse over the document to the location you want to sign, then click the mouse button.");

         }

         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_DELETE),countItems ? TRUE : FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),countItems ? TRUE : FALSE);

         if ( p -> useGlobalProfile && isTheGlobalProfile  ) 
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),FALSE);

         if ( p -> pActiveProfile -> RecognizeByName() ) {
            ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS),SW_SHOW);
            ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),SW_HIDE);
         } else {
            ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_BY_NAME_INSTRUCTIONS),SW_HIDE);
            ShowWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_SKETCH_VIEW),SW_SHOW);
         }

         if ( ! p -> allowNonAdminProfileSettings && ! p -> pICursiVisionServices -> IsAdministrator() ) {
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_DELETE),FALSE);
            EnableWindow(GetDlgItem(hwnd,IDDI_PRINTING_DEVICE_PROFILE_USE_FILENAME),FALSE);
         }

         }
         break;

      default:
         break;
      }

      }
      break;


   case WM_NOTIFY: {

      NMHDR *pNotifyHeader = (NMHDR *)lParam;

      switch ( pNotifyHeader -> code ) {

      case PSN_KILLACTIVE: {
         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,FALSE);
         }
         break;

      case PSN_APPLY: {

         PSHNOTIFY *pNotify = (PSHNOTIFY *)lParam;

         if ( pNotify -> lParam ) {

            p -> doManualSelection = BST_CHECKED == SendDlgItemMessage(hwnd,IDDI_PRINTING_DEVICE_MANUAL_SELECTION,BM_GETCHECK,0L,0L);

            p -> allowNonAdminProfileSettings = (BST_CHECKED == SendMessage(GetDlgItem(hwnd,IDDI_PRINTING_ALLOW_NON_ADMIN_ACCESS),BM_GETCHECK,0L,0L));

            p -> pIGProperties -> Save();
            p -> pIGProperties -> Discard();
            p -> pIGProperties -> Discard();

            if ( p -> pActiveProfile && p -> isPrintJobActive ) {

               Profile *px = static_cast<Profile *>(p -> pActiveProfile);

               HRESULT rc = px -> SaveProperties();

               if ( ! ( S_OK == rc ) ) {
                  char szMessage[1024];
                  sprintf(szMessage,"There was an error saving the printing profile to:\n\n\t%s\n\nPlease check that appropriate privileges to this location are provided to the current login.\n\nWindows reported the error code: %x",szActiveProfilesDirectory,rc);
                  MessageBox(NULL,szMessage,"Error",MB_ICONEXCLAMATION | MB_OK | MB_TOPMOST);
               }

// The following was taken out at some point - and replaced with the above.
// Check whether profile names can be changed and if so - whether the files should be moved.
#if 0
               if ( ! px -> pDoodleOptions -> szDocumentName[0] || strcmp(px -> pDoodleOptions -> szDocumentName,p -> szPrintedDocumentFileName) ) {
                  if ( px -> pDoodleOptions -> szDocumentName[0] )
                     DeleteFile(px -> pDoodleOptions -> szDocumentName);
                  strcpy(px -> pDoodleOptions -> szDocumentName,p -> szPrintedDocumentFileName);
               }

               if ( ! px -> szOutlinesFileName[0] || strcmp(px -> szOutlinesFileName,p -> szPrintedDocumentOutlinesFileName) ) {
                  FILE *fX = fopen(p -> szPrintedDocumentOutlinesFileName,"rb");
                  if ( fX ) {
                     fclose(fX);
                     if ( px -> szOutlinesFileName[0] ) 
                        DeleteFile(px -> szOutlinesFileName);
                     MoveFileEx(p -> szPrintedDocumentOutlinesFileName,px -> szOutlinesFileName,MOVEFILE_REPLACE_EXISTING);
                  }
               }

               px -> SaveProperties();
#endif
            }

            p -> isPrintJobActive = false;

            p -> userCanceledActivePrintJob = false;

         } 

         SetWindowLongPtr(pNotifyHeader -> hwndFrom,DWLP_MSGRESULT,PSNRET_NOERROR);

         }
         break;

      case PSN_QUERYCANCEL: {
         p -> pIGProperties -> Discard();
         p -> pIGProperties -> Push();
         p -> userCanceledActivePrintJob = true;
         }
         break;

      case PSN_RESET: {
         p -> pIGProperties -> Pop();
         p -> pIGProperties -> Pop();
         }
         break;

      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }


   BOOL CALLBACK doMoveDown(HWND hwndTest,LPARAM lParam) {
   RECT rcParent,rcCurrent;
   GetWindowRect(GetParent(hwndTest),&rcParent);
   GetWindowRect(hwndTest,&rcCurrent);
   SetWindowPos(hwndTest,NULL,rcCurrent.left - rcParent.left,rcCurrent.top - rcParent.top + (int)lParam,0,0,SWP_NOZORDER | SWP_NOSIZE);
   return TRUE;
   }
