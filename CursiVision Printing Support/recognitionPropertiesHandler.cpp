// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#define OBJECT_WITH_PROPERTIES Profile
#define CURSIVISION_SERVICES_INTERFACE pObject -> pPrintingSupport -> pICursiVisionServices

#include "recognitionPropertiesDefines.h"

#define ADDITIONAL_INITIALIZATION                                                                                       \
      IPrintingSupportProfile *px = NULL;                                                                               \
      if ( CURSIVISION_SERVICES_INTERFACE )                                                                             \
         CURSIVISION_SERVICES_INTERFACE -> get_PrintingSupportProfile(&px);                                             \
      if ( ! CURSIVISION_SERVICES_INTERFACE -> IsAdministrator() && px ) {                                              \
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,54,0,0,SWP_NOSIZE);            \
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);    \
         EnableWindow(hwnd,FALSE);                                                                                      \
      } else                                                                                                            \
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

   LRESULT CALLBACK Profile::recognitionHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

#include "recognitionPropertiesBody.cpp"

   return LRESULT(FALSE);
   }

#include "recognitionPropertiesSupport.cpp"