/*

                       Copyright (c) 2009 Nathan T. Clark

*/

#include "PrintingSupport.h"

#include "..\resource.h"


   long __stdcall Profile::_ISpecifyPropertyPages::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown*>(this); 
   else

   if ( riid == IID_ISpecifyPropertyPages )
      *ppv = static_cast<ISpecifyPropertyPages*>(this);
   else

      return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall Profile::_ISpecifyPropertyPages::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall Profile::_ISpecifyPropertyPages::Release() {
   return 1;
   }


   HRESULT Profile::_ISpecifyPropertyPages::GetPages(CAUUID *pPages) {

//   pParent -> pProfileProperties -> Push();
//   pParent -> pProfileProperties -> Push();

   pPages -> cElems = 1;
   pPages -> pElems = reinterpret_cast<GUID*>(CoTaskMemAlloc(pPages -> cElems * sizeof(GUID)));

   memset(pPages -> pElems,0,pPages -> cElems * sizeof(GUID));
   memcpy(&pPages -> pElems[0],const_cast<GUID *>(&CLSID_CursiVisionPrintingSupportProfile),sizeof(GUID));

//   ppPropertyPageUnknowns = new IUnknown*[pCAUUID -> cElems + 1];
//   memset(ppPropertyPageUnknowns,0,(pCAUUID -> cElems + 1) * sizeof(IUnknown*));


   return S_OK;
   }
