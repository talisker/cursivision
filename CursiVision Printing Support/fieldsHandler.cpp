// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include "resource.h"

#include "dispositionSettingsDefines.h"

#define OBJECT_WITH_PROPERTIES Profile
#define CURSIVISION_SERVICES_INTERFACE pObject -> pPrintingSupport -> pICursiVisionServices

static doodleOptionProperties *pDoodleOptionProps = NULL;

#define DOODLE_PROPERTIES_PTR pDoodleOptionProps = &pObject -> theDoodleOptions;

#define ADDITIONAL_INITIALIZATION                                                                                       \
      IPrintingSupportProfile *px = NULL;                                                                               \
      if ( CURSIVISION_SERVICES_INTERFACE )                                                                             \
         CURSIVISION_SERVICES_INTERFACE -> get_PrintingSupportProfile(&px);                                             \
      if ( ! CURSIVISION_SERVICES_INTERFACE -> IsAdministrator() && px ) {                                              \
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,60,0,0,SWP_NOSIZE);    \
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);    \
         EnableWindow(hwnd,FALSE);                                                                                      \
      } else                                                                                                            \
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

#include "fieldsHandlerDefines.h"

   LRESULT CALLBACK Profile::fieldsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

#include "fieldsHandlerBody.cpp"

   return LRESULT(FALSE);
   }

#include "fieldsHandlerSupport.cpp"