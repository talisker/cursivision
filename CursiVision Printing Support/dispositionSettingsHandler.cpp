// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include <shellapi.h>
#include <shTypes.h>
#include <shlobj.h>

BOOL CALLBACK doMoveDown(HWND hwndTest,LPARAM lParam);

#define OBJECT_WITH_PROPERTIES Profile
#define CURSIVISION_SERVICES_INTERFACE pObject -> pPrintingSupport -> pICursiVisionServices

#define USE_PROPSHEETS

#include "dispositionSettingsDefines.h"

#define ADDITIONAL_INITIALIZATION                                                                                          \
      IPrintingSupportProfile *px = NULL;                                                                                  \
      if ( CURSIVISION_SERVICES_INTERFACE )                                                                                \
         CURSIVISION_SERVICES_INTERFACE -> get_PrintingSupportProfile(&px);                                                \
      if ( ! CURSIVISION_SERVICES_INTERFACE -> IsAdministrator() && px ) {                                                 \
         RECT rc = {0};                                                                                                    \
         GetClientRect(hwnd,&rc);                                                                                          \
         SetWindowPos(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),HWND_TOP,8,rc.bottom - 32,0,0,SWP_NOSIZE);   \
         defaultTextHandler = (WNDPROC)SetWindowLongPtr(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),GWLP_WNDPROC,(ULONG_PTR)redTextHandler);    \
         EnableWindow(hwnd,FALSE);                                                                                         \
      } else                                                                                                               \
         ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);                                      \
      ShowWindow(GetDlgItem(hwnd,IDDI_DO_REMEMBER_LABEL),SW_HIDE);                                                         \
      EnumChildWindows(hwnd,doMoveDown,(LPARAM)-64);

   LRESULT CALLBACK Profile::dispositionSettingsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

#include "dispositionSettingsBody.cpp"

   return LRESULT(FALSE);
   }

   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doMoveUp(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doHide(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doShow(HWND hwndTest,LPARAM lParam);

   extern "C" void disableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doDisable,(LPARAM)pExceptions);
   return;
   }

   extern "C" void enableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doEnable,(LPARAM)pExceptions);
   return;
   }

   extern "C" void moveUpAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doMoveUp,(LPARAM)pExceptions);
   return;
   }

   extern "C" void hideAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doHide,(LPARAM)pExceptions);
   return;
   }

   extern "C" void showAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doShow,(LPARAM)pExceptions);
   return;
   }

   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,TRUE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,FALSE);
   return TRUE;
   }


   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,FALSE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,TRUE);
   return TRUE;
   }


   BOOL CALLBACK doMoveUp(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   RECT rcParent,rcCurrent;
   GetWindowRect(GetParent(hwndTest),&rcParent);
   GetWindowRect(hwndTest,&rcCurrent);
   SetWindowPos(hwndTest,NULL,rcCurrent.left - rcParent.left,rcCurrent.top - 32 - rcParent.top,0,0,SWP_NOZORDER | SWP_NOSIZE);
   return TRUE;
   }

   BOOL CALLBACK doHide(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   ShowWindow(hwndTest,SW_HIDE);
   return TRUE;
   }

   BOOL CALLBACK doShow(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   ShowWindow(hwndTest,SW_SHOW);
   return TRUE;
   }