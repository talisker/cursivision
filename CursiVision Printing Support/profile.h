// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "PrintingSupport.h"

#include <commctrl.h>

#define MAX_TEXT_RECT_COUNT   128
#define MAX_TEXT_TEXT_SPACE   8192

#include "resultDisposition.h"
#include "printingSupportResource.h"

#include "writingLocation.h"
#include "doodleOptionsProperties.h"

#include "Properties_i.h"
#include "PDFEnabler_i.h"

#include "PrintingSupport_i.h"
#include "SignaturePad_i.h"
#include "CursiVision_i.h"

#include "signatureGraphic.h"

#include "templateDocument.h"

#include "PrintingProfileDefines.h"

#define TARGET_RECT_COUNT 32

#define PROFILE_BUFFER_SIZE   16384

#define PDF_RECT_LEFT_SLOP        4
#define PDF_RECT_TOP_SLOP         8

   class PrintingSupport;

   class Profile : public IPrintingSupportProfile {

   public:

      Profile(IUnknown *pIUnknownOuter);

      ~Profile();

      STDMETHOD(PushProperties)();
      STDMETHOD(PopProperties)();
      STDMETHOD(DiscardProperties)();
      STDMETHOD(SaveProperties)();

      // IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      // IPrintingSupportProfile

      HRESULT __stdcall Initialize(char *pszOriginalDocument,char *pszProfileName,void *pvDefaultResultDisposition,void *pvPrintingProfile,boolean resetSignatureLocations);
      HRESULT __stdcall Start();

      HRESULT __stdcall ClearTargets();
      HRESULT __stdcall SaveTarget(long targetIndex,RECT *,long pageNumber,long adobePageNumber);
      HRESULT __stdcall ReplaceTarget(long targetIndex,RECT *,long pageNumber,long adobePageNumber);
      HRESULT __stdcall GetTarget(long targetIndex,RECT *,long *pPageNumber,long *pAdobePageNumber);
      HRESULT __stdcall RemoveTarget(long targetIndex);
      HRESULT __stdcall Begin();
      HRESULT __stdcall GetResultDisposition(void **);
      HRESULT __stdcall GetDoodleProperties(void **);

      HRESULT __stdcall ShowProperties(HWND hwndOwner,IUnknown *);

      long __stdcall Match(char *pszOriginalDocument,char *pszOutlinesFile);

      char * __stdcall Name() { return szName; };

      BOOL __stdcall IsDefined() { return ( ! ( 0L == expectedRects[0].left ) || ! ( 0L == expectedRects[0].right ) ); };

      BOOL __stdcall RecognizeByName() { return recognizeByName; };

      long __stdcall SetRecognizeByName(BOOL setVal) { recognizeByName = setVal; return 0L; };

      long __stdcall SigningRectangleCount() { if ( ! theDoodleOptions.processingDisposition.doRemember ) return 0L; return theDoodleOptions.countRects; };

      long __stdcall ClearSigningRects() { theDoodleOptions.countRects = 0L; memset(&theDoodleOptions.theLocations,0,sizeof(theDoodleOptions.theLocations)); return 0L; };

      long __stdcall Save(char *pszNewName = NULL);

      HRESULT __stdcall Destroy(BOOL keepOutlines = false);

      char * __stdcall GetDispositionSettingsFileName();

      char * __stdcall SignatureGraphicFileName();

      void * __stdcall GetTextOutlines(long pageNumber,long *pReportedPageWidth,long *pReportedPageHeight);

      void __stdcall FreeTextOutlines(void *pvTextOutlines);

      char * __stdcall DocumentName() {
         if ( ! pDoodleOptions ) 
            return NULL; 
         if ( ! pDoodleOptions -> szDocumentName[0] ) 
            return NULL; 
         return pDoodleOptions -> szDocumentName;
      };

      char * __stdcall OutlinesFileName() {
         if ( ! szOutlinesFileName[0] ) 
            return NULL;
         return szOutlinesFileName;
      };

      void __stdcall UnlockSave() {
         allowSaveProfile = true;
         return;
      };

      BOOL __stdcall DoSignatureCapture() { return ! skipSignatureCapture; }

      //  IPropertyPageClient

      class _IGPropertyPageClient : public IGPropertyPageClient {
      public:

         _IGPropertyPageClient(Profile *p) : pParent(p),refCount(0) {};
         ~_IGPropertyPageClient() {};

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         STDMETHOD(BeforeAllPropertyPages)();
         STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** stringHelpDirs,SAFEARRAY** pSize);
         STDMETHOD(CreatePropertyPage)(long,HWND,RECT*,BOOL,HWND *pHwndPropertyPage);
         STDMETHOD(Apply)();
         STDMETHOD(IsPageDirty)(long,BOOL*);
         STDMETHOD(Help)(BSTR bstrHelpDir);
         STDMETHOD(TranslateAccelerator)(long,long*);
         STDMETHOD(AfterAllPropertyPages)(BOOL);
         STDMETHOD(DestroyPropertyPage)(long);

         STDMETHOD(GetPropertySheetHeader)(void *pHeader);
         STDMETHOD(get_PropertyPageCount)(long *pCount);
         STDMETHOD(GetPropertySheets)(void *pSheets);

         Profile *pParent;
         long refCount;
         friend class _IPropertyPage;

      } *pIGPropertyPageClient;

      char *copy();

      bool readOutlines(RECT **ppPDFEntries,long **ppPDFPages,
                           char *pszOriginalDocument = NULL,char *pszOutlinesFile = NULL,
                           char **ppszTextValues = NULL,long *pCountEntries = NULL,
                           long *pCXPage = NULL,long *pCYPage = NULL);

      void preInitialize(char *pszOriginalDocument,char *pszProfileName,void *pvDefaultResultDisposition,void *pvPrintingProfile,boolean resetSignatureLocations);
      HRESULT completeInitialize();

      long rename(char *pszNewName);

      void setupPDFEnabler(bool force,char *pszDocument = NULL);
      void closePDFEnabler(HWND hwndView,WNDPROC defaultHandler = NULL);

      long retrieveOrFindPDFFields(char *pszTargetOutlinesFile,long currentPageNumber);
      long findPDFFields(char *pszTargetOutlinesFile,long currentPageNumber);

      long retrieveOrFindPDFFields(long currentPageNumber,HDC hdcView,RECT *prcPageRect);
      long findPDFFields(long currentPageNumber,HDC hdcView,RECT *prcPageRect);

      doodleOptionProperties *DoodleOptions() { return pDoodleOptions; };

      void SetAllowSaveProfile(bool v) { allowSaveProfile = v; };
      bool GetAllowSaveProfile() { return allowSaveProfile; };

      void SetOutlinesFileName(char *psz) { 
         strcpy(szOutlinesFileName,psz); 
      };

      void SetSignaturesFileName(char *psz) {
         strcpy(szSignaturesFileName,psz);
      };

      char * GetSignaturesFileName() { return szSignaturesFileName; };

      PrintingSupport *GetPrintingSupport() { return pPrintingSupport; };

   private:

      IGProperties *pIGProperties;

      IPdfEnabler *pIPdfEnabler;
      IPdfDocument *pIPdfDocument;
      HMODULE hModule_PDFEnabler;

      PrintingSupport *pPrintingSupport;

      char szName[512];
      char szPropertiesFile[512];
      char szOutlinesFileName[512];
      char szSignaturesFileName[512];
      char szAssociatedProfileName[512];
      char szProposedNewName[128];

      char szOutlinesPageRecord[OUTLINES_PAGE_RECORD_SIZE + 1];

      RECT expectedRects[MAX_TEXT_RECT_COUNT];
      char expectedText[MAX_TEXT_TEXT_SPACE];
      long expectedPage[MAX_TEXT_RECT_COUNT];

      long recognizeByName;
      bool skipSignatureCapture;

      bool isStarted;
      bool isInitialized;
      bool allowSaveProfile;

      doodleOptionProperties theDoodleOptions;
      doodleOptionProperties *pDoodleOptions;

      templateDocument *pTemplateDocument;

      HWND hwndPrintingDispositionPage,hwndAdditionalBackEnds;

      long refCount;

      static _IPropertyPage *pIPropertyPage;

      static Profile *pCurrentProfile;

      static LRESULT CALLBACK dispositionSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalBackEndsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalSaveOptionsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK listViewHandler(HWND,UINT,WPARAM,LPARAM);

      static LRESULT CALLBACK nameHandler(HWND,UINT,WPARAM,LPARAM);

      static LRESULT CALLBACK recognitionHandler(HWND,UINT,WPARAM,LPARAM);

      static LRESULT CALLBACK signingLocationsHandler(HWND,UINT,WPARAM,LPARAM);

      static LRESULT CALLBACK fieldsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

      friend class _IPropertyPage;
      friend class _IGPropertyPageClient;

   };
