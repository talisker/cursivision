// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#pragma warning(disable:4786)
#pragma warning(disable:4275)
#pragma warning(disable:4251)

#include <windows.h>
#include <list>

#include <stdio.h>
#include <objbase.h>
#include <Richedit.h>
#include <comcat.h>

#include "printingSupportResource.h"
#include "resource.h"
#include "profile.h"

#include "resultDisposition.h"

#include "utilities.h"

#undef szHomeDirectory

#include "Properties_i.h"
#include "PrintingSupport_i.h"
#include "SignaturePad_i.h"
#include "CursiVision_i.h"

#ifdef DEFINE_DATA

   char szModuleName[512];
   char szApplicationDataDirectory[512];
   char szGlobalDataStore[512];
   char szMySignedDocumentsDirectory[512];
   char szUserDirectory[512];

   char szPrintDriverSignedDocumentsDirectory[512];
   char szPrintDriverProfilesDirectory[512];
   char szPrintDriverPrintedDocumentsDirectory[512];

   char szActiveProfilesDirectory[512];
   char szActiveSignedDocumentsDirectory[512];

   char szPDFEnablerDLL[512];
   HINSTANCE hModule;
   WNDPROC nativeListViewHandler;
   HWND hwndMainFrame;
   WNDPROC defaultTextHandler = NULL;

#else

   extern char szModuleName[];
   extern char szApplicationDataDirectory[];
   extern char szGlobalDataStore[];
   extern char szMySignedDocumentsDirectory[];
   extern char szUserDirectory[];

   extern char szPrintDriverSignedDocumentsDirectory[];
   extern char szPrintDriverProfilesDirectory[];
   extern char szPrintDriverPrintedDocumentsDirectory[];

   extern char szActiveProfilesDirectory[];
   extern char szActiveSignedDocumentsDirectory[];

   extern char szPDFEnablerDLL[];
   extern HINSTANCE hModule;
   extern WNDPROC nativeListViewHandler;
   extern HWND hwndMainFrame;
   extern WNDPROC defaultTextHandler;

#endif

#define TIMER_EVENT_BRING_TO_TOP    1

//class profile;

class PrintingSupport : public IPrintingSupport {

   public:

      PrintingSupport(IUnknown *pUnkOuter);
      ~PrintingSupport();

      // IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

   private:

      HRESULT __stdcall TakeMainWindow(HWND hwndMainWindow);
      HRESULT __stdcall TakeDocumentInfo(char *pszPDFFileName,void *pvDefaultDispositionSettings,void *pvIPdfDocument,boolean createGlobalPrintingProfile);
      HRESULT __stdcall GetPropertiesWindow(HWND *pHWNDWindow);

      HRESULT __stdcall ServicesAdvise(void *pvICursiVisionServices);

      BOOL __stdcall NonAdminCanChange() { return allowNonAdminProfileSettings; };

      HRESULT __stdcall InitializeThreaded(void (__stdcall * pCallback)(void *pCallerObject),void *pvCallerObj);

      // IPropertyPageClient

      class _IGPropertyPageClient : public IGPropertyPageClient {
      public:

         _IGPropertyPageClient(PrintingSupport *p) : pParent(p),refCount(0) {};
         ~_IGPropertyPageClient() {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         STDMETHOD(BeforeAllPropertyPages)();
         STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** stringHelpDirs,SAFEARRAY** pSize);
         STDMETHOD(CreatePropertyPage)(long,HWND,RECT*,BOOL,HWND *hwndPropertyPage);
         STDMETHOD(Apply)();
         STDMETHOD(IsPageDirty)(long,BOOL*);
         STDMETHOD(Help)(BSTR bstrHelpDir);
         STDMETHOD(TranslateAccelerator)(long,long*);
         STDMETHOD(AfterAllPropertyPages)(BOOL);
         STDMETHOD(DestroyPropertyPage)(long);

         STDMETHOD(GetPropertySheetHeader)(void *pHeader);
         STDMETHOD(get_PropertyPageCount)(long *pCount);
         STDMETHOD(GetPropertySheets)(void *pSheets);

         PrintingSupport *pParent;
         long refCount;

      } *pIGPropertyPageClient;

      static IPrintingSupportProfile *ActiveProfile() { return pActiveProfile; };

   private:

      static unsigned int __stdcall  addAllProfiles(void *);

      long addProfile(char *pszOriginalDocumentName,char *pszName,resultDisposition *pDefaultDisposition,bool resetSignatureLocations);

      long refCount;

      IGProperties *pIGProperties;
      ICursiVisionServices *pICursiVisionServices;

      static char *pszWorkingArea;

      HWND hwndPrintingDocumentPage,hwndPrintingDevicePage;
      RECT rcEditDialog;

      static IPrintingSupportProfile *pActiveProfile;

      char szPrintedDocumentFileName[MAX_PATH];
      char szPrintedDocumentOutlinesFileName[MAX_PATH];
      char szPotentialProfileName[128];

      long countProfiles;
      long preferredProfileIndex;
      bool useGlobalProfile;
	   char szGlobalProfileName[64];

      bool isNewProfile;
      bool isManualSelection;
      bool isPrintJobActive;
      bool userCanceledActivePrintJob;

      bool usePDFRenderer;
      bool doManualSelection;

      bool allowNonAdminProfileSettings;
      bool realAdminPrivileges;

      bool isProfileDirectoryParsed;

      std::list<IPrintingSupportProfile *> profiles;
      std::list<backEndPackage *> allBackEnds;

      void (__stdcall *pInitializeCallback)(void *pvCallerObject);
      void *pCallerObject;
      
      templateDocument *pTemplateDocument;

      struct resultDisposition defaultResultDisposition;

      static LRESULT CALLBACK printPropertyPageHandler(HWND,UINT,WPARAM,LPARAM);

      friend class _IGPropertyPageClient;
      friend class Profile;

};

int GetLocation(HWND hwnd,long key,char *szFolderLocation);
LRESULT CALLBACK redTextHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

