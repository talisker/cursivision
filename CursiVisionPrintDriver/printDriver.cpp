
#define DEFINE_DATA

#include "printDriver.h"

#include <time.h>

physicalDevice *pPhysicalDevice = NULL;

CRITICAL_SECTION printDriverCriticalSection;

char szTemporaryFileName[STATIC_STRING_SIZE];
char szTemporaryRectanglesFileName[STATIC_STRING_SIZE];
char szDesiredFileName[STATIC_STRING_SIZE];
char szDesiredRectanglesFileName[STATIC_STRING_SIZE];
char szTargetExecutable[STATIC_STRING_SIZE];

char szDocumentDirectory[STATIC_STRING_SIZE];
char szProfilesDirectory[STATIC_STRING_SIZE];
char szOrientation[STATIC_STRING_SIZE];
char szExecutableName[STATIC_STRING_SIZE];

HANDLE PrintDriver::currentOutputFile = INVALID_HANDLE_VALUE;
HANDLE PrintDriver::currentRectanglesFile = INVALID_HANDLE_VALUE;

long PrintDriver::totalBytesWritten = 0;
bool PrintDriver::startDocumentHasBeenCalled = false;
bool PrintDriver::endDocumentHasBeenCalled = false;

PrintDriver::PrintDriver() : 

    refCount(1),

    pIPrintOemDriverPS(NULL),
    pIPrintCorePS2(NULL),
    pIPrintCoreHelperPS(NULL)

{
    hookFunctions[0].iFunc = INDEX_DrvStartDoc;
    hookFunctions[0].pfn = (PFN)startDocument;

    hookFunctions[1].iFunc = INDEX_DrvStartPage;
    hookFunctions[1].pfn = (PFN)startPage;

    hookFunctions[2].iFunc = INDEX_DrvTextOut;
    hookFunctions[2].pfn = (PFN)textOut;

    hookFunctions[3].iFunc = INDEX_DrvEndDoc;
    hookFunctions[3].pfn = (PFN)endDocument;

    hookFunctions[4].iFunc = INDEX_DrvLineTo;
    hookFunctions[4].pfn = (PFN)lineTo;

    hookFunctions[5].iFunc = INDEX_DrvStrokePath;
    hookFunctions[5].pfn = (PFN)strokePath;

    hookFunctions[6].iFunc = INDEX_DrvFillPath;
    hookFunctions[6].pfn = (PFN)fillPath;

    hookFunctions[7].iFunc = INDEX_DrvStrokeAndFillPath;
    hookFunctions[7].pfn = (PFN)strokeAndFillPath;

    hookFunctions[8].iFunc = -1L;
    hookFunctions[8].pfn = NULL;

    memset(szDocumentDirectory,0,sizeof(szDocumentDirectory));
    memset(szProfilesDirectory,0,sizeof(szProfilesDirectory));
    memset(szOrientation,0,sizeof(szOrientation));

DEBUG_LOG_THIS(pPhysicalDevice,"The PrintDriver Constructor is called","");

    return;
}


PrintDriver::~PrintDriver() {

    if ( pIPrintOemDriverPS ) {
        pIPrintOemDriverPS -> Release();
        pIPrintOemDriverPS = NULL;
    }

    if ( pIPrintCorePS2 ) {
        pIPrintCorePS2 -> Release();
        pIPrintCorePS2 = NULL;
    }

    if ( pIPrintCoreHelperPS ) {
        pIPrintCoreHelperPS -> Release();
        pIPrintCoreHelperPS = NULL;
    }

DEBUG_LOG_THIS(pPhysicalDevice,"The PrintDriver Destructor is exiting","");
   
   return;
}


HRESULT __stdcall PrintDriver::QueryInterface(const IID& riid, void** ppv) {

    *ppv = NULL;

    if ( IID_IUnknown == riid ) {

        *ppv = static_cast<IUnknown *>(this);

    } else {

        if ( IID_IPrintOemPS == riid ) {

            *ppv = static_cast<IPrintOemPS *>(this);

        } else {

            if ( IID_IPrintOemPS2 == riid ) {

                *ppv = static_cast<IPrintOemPS2 *>(this);

            } else {

                return E_NOINTERFACE;

            }
        }
    }

    AddRef();
    return S_OK;
}


ULONG __stdcall PrintDriver::AddRef() {
   return refCount++;
   }


ULONG __stdcall PrintDriver::Release() {
   long rc = --refCount;
   if ( 0 == refCount )
       delete this;
   return rc;
   }


HRESULT __stdcall PrintDriver::GetInfo(DWORD dwMode,PVOID pBuffer,DWORD cbSize,PDWORD pCbNeeded) {

DEBUG_LOG_THIS(pPhysicalDevice,"The PrintDriver GetInfo is called with dwMode = %ld",dwMode);

    *pCbNeeded = sizeof(DWORD);

    if ( NULL == pBuffer )
        return S_OK;

    switch ( dwMode ) {

    case OEMGI_GETSIGNATURE:
        *(DWORD *)pBuffer = OEM_SIGNATURE;
        break;

    case OEMGI_GETINTERFACEVERSION:
        break;

    case OEMGI_GETREQUESTEDHELPERINTERFACES:
        *(DWORD *)pBuffer = OEMPUBLISH_IPRINTCOREHELPER;
        break;

    case OEMGI_GETVERSION:
        *(DWORD *)pBuffer = OEM_VERSION;
        break;

    case OEMGI_GETPUBLISHERINFO:
        return E_NOTIMPL;

    default:
        *pCbNeeded = 0;
        SetLastError(ERROR_NOT_SUPPORTED);
        return E_FAIL;
    }

    return S_OK;

}


HRESULT __stdcall PrintDriver::PublishDriverInterface(IUnknown *pIUnknown) {

    if ( NULL == pIPrintCorePS2 ) {
        pIUnknown -> QueryInterface(IID_IPrintCorePS2, reinterpret_cast<void **>(&pIPrintCorePS2));
        if ( pIPrintCorePS2 )
            return S_OK;
    }

    if ( NULL == pIPrintOemDriverPS )
        pIUnknown -> QueryInterface(IID_IPrintOemDriverPS, reinterpret_cast<void **>(&pIPrintOemDriverPS));

    if ( NULL == pIPrintCoreHelperPS )
        pIUnknown -> QueryInterface(IID_IPrintCoreHelperPS, reinterpret_cast<void **>(&pIPrintCoreHelperPS));

   return S_OK;

}


HRESULT __stdcall PrintDriver::EnableDriver(DWORD dwDriverVersion,DWORD cbSize,DRVENABLEDATA *pDriverEnableData) {

    pDriverEnableData -> iDriverVersion = PRINTER_OEMINTF_VERSION;
    pDriverEnableData -> c = MAX_DDI_HOOKS;
    pDriverEnableData -> pdrvfn = (DRVFN *)hookFunctions;

DEBUG_LOG_THIS(pPhysicalDevice,"EnableDriver exiting","");

    return S_OK;
}


HRESULT __stdcall PrintDriver::DisableDriver() {

DEBUG_LOG_THIS(pPhysicalDevice,"DisableDriver exiting","");

    return S_OK;
}


HRESULT __stdcall PrintDriver::EnablePDEV(PDEVOBJ pDevObject,PWSTR pPrinterName,ULONG cPatterns,HSURF *phsurfPatterns,ULONG cjGdiInfo,
                                            GDIINFO *pGdiInfo,ULONG cjDevInfo,DEVINFO *pDevInfo,DRVENABLEDATA *pDriverEnableData,PDEVOEM *pDevOEM) {

#if 0
   if ( pPhysicalDevice )
      delete pPhysicalDevice;
#else

   if ( ! pPhysicalDevice )
      pPhysicalDevice = new physicalDevice();

   pPhysicalDevice -> countFunctions = pDriverEnableData -> c;

DEBUG_LOG_THIS(pPhysicalDevice,"EnablePDEV entering. %ld functions are available",pPhysicalDevice -> countFunctions);

#endif

   for ( long k = 0; 1; k++ ) {
      if ( -1L == hookFunctions[k].iFunc )
         break;
      for ( long j = 0; j < (long)pDriverEnableData -> c; j++ ) {
         if ( hookFunctions[k].iFunc == pDriverEnableData -> pdrvfn[j].iFunc ) {
            pPhysicalDevice -> pHooks[k].iFunc = pDriverEnableData -> pdrvfn[j].iFunc;
            pPhysicalDevice -> pHooks[k].pfn = pDriverEnableData -> pdrvfn[j].pfn;
            break;
         }
      }
   }

   if ( pDevOEM )
      *pDevOEM = (PDEVOEM)pPhysicalDevice;

DEBUG_LOG_THIS(pPhysicalDevice,"EnablePDEV exiting","");

    return S_OK;
}


HRESULT __stdcall PrintDriver::ResetPDEV(DEVOBJ *pDevOld,DEVOBJ *pDevNew) {

   if ( pDevNew && pDevOld ) 
//      memcpy(pDevNew,pDevOld,sizeof(DEVOBJ));
      pDevNew -> pdevOEM = pDevOld -> pdevOEM;

DEBUG_LOG_THIS(pPhysicalDevice,"ResetPDEV exiting","");

   return S_OK;
   }


   HRESULT __stdcall PrintDriver::DisablePDEV(PDEVOBJ pDevObj) {
   DEBUG_LOG_THIS(pPhysicalDevice,"DisablePDEV exiting","");
   return S_OK;
   }


   HRESULT __stdcall PrintDriver::DevMode(DWORD dwMode,OEMDMPARAM *pOemDMParam) {

   DEBUG_LOG_THIS(pPhysicalDevice,"entering DevMode dwMode = %ld",dwMode);

   OEMDEV *pOEMDevIn = (OEMDEV *)pOemDMParam -> pOEMDMIn;
   OEMDEV *pOEMDevOut = (OEMDEV *)pOemDMParam -> pOEMDMOut;

   switch ( dwMode ) {
   case OEMDM_SIZE:
       pOemDMParam -> cbBufSize = sizeof(OEMDEV);
       break;

   case OEMDM_DEFAULT:
       pOemDMParam -> cbBufSize = sizeof(OEMDEV);
       pOEMDevOut -> dmOEMExtra.dwSize = sizeof(OEMDEV);
       pOEMDevOut -> dmOEMExtra.dwSignature = OEM_SIGNATURE;
       pOEMDevOut -> dmOEMExtra.dwVersion = OEM_VERSION;
       break;

   case OEMDM_CONVERT:
       pOemDMParam -> cbBufSize = sizeof(OEMDEV);
       break;

   case OEMDM_MERGE:
       pOemDMParam -> cbBufSize = sizeof(OEMDEV);
       pOEMDevOut -> dmOEMExtra.dwSize = sizeof(OEMDEV);
       pOEMDevOut -> dmOEMExtra.dwSignature = OEM_SIGNATURE;
       pOEMDevOut -> dmOEMExtra.dwVersion = OEM_VERSION;
       break;
   }

   DEBUG_LOG_THIS(pPhysicalDevice,"DevMode exiting","");

   return S_OK;
   }


   HRESULT __stdcall PrintDriver::Command(PDEVOBJ pDevObj,DWORD dwIndex,PVOID pData,DWORD cbSize,OUT DWORD *pdwResult) {
   return E_NOTIMPL;
   }


   HRESULT __stdcall PrintDriver::GetPDEVAdjustment(DEVOBJ *pDevObj,DWORD dwAdjustType,void *pBuffer,DWORD cbBuffer,BOOL *pbAdjustmentDone) {
   *pbAdjustmentDone = FALSE;
   return S_OK;
   }


   HRESULT __stdcall PrintDriver::WritePrinter(DEVOBJ *pDevObj,void *pBuffer,DWORD cbBuffer,DWORD *pcbWritten) {

   DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter is Called","")

   *pcbWritten = 0;

   if ( NULL == pBuffer ) {
      DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter pBuffer is NULL","")
      return S_OK;
   }

   DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter pBuffer is NOT NULL","")

   if ( strstr((char *)pBuffer,"%%Orientation:") ) {

      char *p = strstr((char *)pBuffer,"%%Orientation:") + strlen("%%Orientation:");
      while ( *p && *p != 0x0A && *p != 0x0D )
         p++;

      char pKeep = *p;
      *p = '\0';

      strcpy_s(szOrientation,sizeof(szOrientation),strstr((char *)pBuffer,"%%Orientation:") + strlen("%%Orientation:"));

      *p = pKeep;

   }

   if ( ! ( INVALID_HANDLE_VALUE == /*pPhysicalDevice -> */currentOutputFile ) ) {

      DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter wants to write %ld bytes",cbBuffer)

      WriteFile(/*pPhysicalDevice -> */currentOutputFile,pBuffer,cbBuffer,pcbWritten,NULL);

      /*pPhysicalDevice -> */totalBytesWritten += *pcbWritten;

      DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter did write %ld bytes",*pcbWritten)

   } else {

      DEBUG_LOG_THIS(pPhysicalDevice,"WritePrinter: output file is null","")

      *pcbWritten = cbBuffer;

    }

    return S_OK;
}


  void ASCIIHexEncode(char *pszInput,long valueSize,char **ppszResult) {

   *ppszResult = new char[2 * valueSize + 1];
   memset(*ppszResult,0,(2 * valueSize + 1) * sizeof(char));

   char *p = pszInput;
   char *pEnd = p + valueSize;
   char *pTarget = *ppszResult;

   while ( p < pEnd ) {
  
      *pTarget = (*p & 0xF0) >> 4;
      *pTarget += (*pTarget > 9 ? 'a' - 10 : '0');

      pTarget++;

      *pTarget = (*p & 0x0F);
      *pTarget += (*pTarget > 9 ? 'a' - 10 : '0');
   
      pTarget++;

      p++;

   }

   return;
   }