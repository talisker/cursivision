
#include "printDriver.h"
#include <time.h>

   void ASCIIHexEncode(char *pszText,long countBytes,char **ppResult);

   static long pageWidth = 0L;
   static long pageHeight = 0L;

   BOOL APIENTRY PrintDriver::startDocument(SURFOBJ *pso,PWSTR pwszDocName,DWORD dwJobId) {

   if ( ! pwszDocName )
      return (((PFN_DrvStartDoc)(pPhysicalDevice -> pHooks[UD_DrvStartDoc].pfn))(pso,pwszDocName,dwJobId));

   EnterCriticalSection(&printDriverCriticalSection);

#if 0
   if ( ! ( INVALID_HANDLE_VALUE == pPhysicalDevice -> currentOutputFile  ) ) {

      if ( /*pPhysicalDevice -> */szTemporaryFileName[0] ) {
         CloseHandle(pPhysicalDevice -> currentOutputFile);
         pPhysicalDevice -> currentOutputFile = INVALID_HANDLE_VALUE;
         memset(/*pPhysicalDevice -> */szTemporaryFileName,0,sizeof(/*pPhysicalDevice -> */szTemporaryFileName));
      }

      if ( /*pPhysicalDevice -> */szTemporaryRectanglesFileName[0] ) {
         CloseHandle(pPhysicalDevice -> currentRectanglesFile);
         pPhysicalDevice -> currentRectanglesFile = INVALID_HANDLE_VALUE;
         memset(/*pPhysicalDevice -> */szTemporaryRectanglesFileName,0,sizeof(/*pPhysicalDevice -> */szTemporaryRectanglesFileName));
       }

   }
#endif

   pPhysicalDevice -> pageNumber = 0;

   /*pPhysicalDevice -> */startDocumentHasBeenCalled = true;

   if ( ! szDocumentDirectory[0] || ! szProfilesDirectory[0] || ! szTargetExecutable[0] ) {

      HKEY hKey = NULL;

      RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate\\CursiVision", 0, KEY_QUERY_VALUE | KEY_WOW64_32KEY , &hKey);

      if ( ! hKey ) {
         RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate\\CursiVision", 0, KEY_QUERY_VALUE | KEY_WOW64_64KEY , &hKey);
         if ( ! hKey ) {
            DEBUG_LOG("%s", "HKEY_LOCAL_MACHINE/Software/InnoVisioNate/CursiVision does not exist")
         }
      }

      if ( hKey ) {

         DWORD cb = MAX_PATH;
         DWORD dwType = REG_SZ;

         RegQueryValueExA(hKey,"Printed Documents Directory",NULL,&dwType,(BYTE *)szDocumentDirectory,&cb);

         cb = MAX_PATH;
         dwType = REG_SZ;
         RegQueryValueExA(hKey,"Printing Profiles Directory",NULL,&dwType,(BYTE *)szProfilesDirectory,&cb);

         cb = MAX_PATH;
         dwType = REG_SZ;
         RegQueryValueExA(hKey,"Installation Directory",NULL,&dwType,(BYTE *)szExecutableName,&cb);

         cb = MAX_PATH;
         dwType = REG_SZ;
         RegQueryValueExA(hKey, "Print Driver Target", NULL, &dwType,(BYTE *)szTargetExecutable,&cb);

         if ( szTargetExecutable[0] )
            sprintf_s(szExecutableName + strlen(szExecutableName),STATIC_STRING_SIZE,"\\%s.exe",szTargetExecutable);
         else
            sprintf_s(szExecutableName + strlen(szExecutableName),STATIC_STRING_SIZE,"\\CursiVision.exe");

         RegCloseKey(hKey);

      }

   }

   if ( szDocumentDirectory[0] && INVALID_HANDLE_VALUE == /*pPhysicalDevice -> */currentOutputFile ) {

      char szFMSBSTRs[MAX_PATH];

      WideCharToMultiByte(CP_ACP,0,pwszDocName,-1,szFMSBSTRs,MAX_PATH,0,0);

      char *pDot = strrchr(szFMSBSTRs,'.');
      if ( pDot )
         *pDot = '\0';

      char *pSlash = strrchr(szFMSBSTRs,'\\');

      if ( ! pSlash )
          pSlash = strrchr(szFMSBSTRs,'/');

      if ( pSlash )
         sprintf_s(/*pPhysicalDevice -> */szDesiredFileName,STATIC_STRING_SIZE,"%s\\%s.ps",szDocumentDirectory,pSlash + 1);
      else
         sprintf_s(/*pPhysicalDevice -> */szDesiredFileName,STATIC_STRING_SIZE,"%s\\%s.ps",szDocumentDirectory,szFMSBSTRs);

      if ( pSlash )
          *pSlash = '\\';

      if ( pDot )
          *pDot = '.';

       sprintf_s(/*pPhysicalDevice -> */szTemporaryFileName,STATIC_STRING_SIZE,"%s\\%s",szDocumentDirectory,tmpnam(NULL));

       /*pPhysicalDevice -> */currentOutputFile = CreateFileA(/*pPhysicalDevice -> */szTemporaryFileName,GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS,NULL);

   }

   if ( szProfilesDirectory[0] && INVALID_HANDLE_VALUE == /*pPhysicalDevice -> */currentRectanglesFile ) {

      char szFMSBSTRs[MAX_PATH];

      WideCharToMultiByte(CP_ACP,0,pwszDocName,-1,szFMSBSTRs,MAX_PATH,0,0);

      char *pDot = strrchr(szFMSBSTRs,'.');
      if ( pDot )
          *pDot = '\0';

      char *pSlash = strrchr(szFMSBSTRs,'\\');
      if ( ! pSlash )
          pSlash = strrchr(szFMSBSTRs,'/');

      if ( pSlash )
          sprintf_s(/*pPhysicalDevice -> */szDesiredRectanglesFileName,STATIC_STRING_SIZE,"%s\\%s.profile",szProfilesDirectory,pSlash + 1);
      else
          sprintf_s(/*pPhysicalDevice -> */szDesiredRectanglesFileName,STATIC_STRING_SIZE,"%s\\%s.profile",szProfilesDirectory,szFMSBSTRs);

      if ( pSlash )
          *pSlash = '\\';
      if ( pDot )
          *pDot = '.';

       sprintf_s(/*pPhysicalDevice -> */szTemporaryRectanglesFileName,STATIC_STRING_SIZE,"%s\\%s",szDocumentDirectory,tmpnam(NULL));

       /*pPhysicalDevice -> */currentRectanglesFile = CreateFileA(/*pPhysicalDevice -> */szTemporaryRectanglesFileName,GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS,NULL);

   }

   BOOL rc = (((PFN_DrvStartDoc)(pPhysicalDevice -> pHooks[UD_DrvStartDoc].pfn))(pso,pwszDocName,dwJobId));

   LeaveCriticalSection(&printDriverCriticalSection);

   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.1 (%s)",/*pPhysicalDevice -> */szTemporaryFileName)
   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.2 (%s)",/*pPhysicalDevice -> */szTemporaryRectanglesFileName)
   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.3 (%s)",/*pPhysicalDevice -> */szDesiredFileName)
   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.4 (%s)",/*pPhysicalDevice -> */szDesiredRectanglesFileName)
   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.5 (%s)",szProfilesDirectory)
   DEBUG_LOG_THIS(pPhysicalDevice,"startDocument exits 1.6 (%s)",szDocumentDirectory)

   return TRUE;
}


BOOL APIENTRY PrintDriver::startPage(SURFOBJ *pSurfaceObject) {

   char szTemp[1024];
   DWORD cb;

   EnterCriticalSection(&printDriverCriticalSection);

   if ( pPhysicalDevice -> pageNumber ) {
       DWORD currentFilePosition = SetFilePointer(/*pPhysicalDevice -> */currentRectanglesFile,0,NULL,FILE_CURRENT);
       cb = sprintf_s(szTemp,1024,"%06ld-%08ld",pPhysicalDevice -> pageEntryCount,currentFilePosition);
       SetFilePointer(/*pPhysicalDevice -> */currentRectanglesFile,pPhysicalDevice -> startPageFilePosition,NULL,FILE_BEGIN);
       WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,szTemp,cb,&cb,NULL);
       SetFilePointer(/*pPhysicalDevice -> */currentRectanglesFile,currentFilePosition,NULL,FILE_BEGIN);
   }

   pPhysicalDevice -> pageNumber++;
   pPhysicalDevice -> pageEntryCount = 0;

   pageWidth = pSurfaceObject -> sizlBitmap.cx;
   pageHeight = pSurfaceObject -> sizlBitmap.cy;

   cb = sprintf_s(szTemp,1024,"Page %04ld:%04ld-%04ld",pPhysicalDevice -> pageNumber,pageWidth,pageHeight);
   WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,szTemp,cb,&cb,NULL);

   pPhysicalDevice -> startPageFilePosition = SetFilePointer(/*pPhysicalDevice -> */currentRectanglesFile,0,NULL,FILE_CURRENT);

   cb = sprintf_s(szTemp,1024,"%06ld-%08ld%c",0,0,0x0A);
   WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,szTemp,cb,&cb,NULL);

   BOOL rc = ((PFN_DrvStartPage)(pPhysicalDevice -> pHooks[UD_DrvStartPage].pfn))(pSurfaceObject);

   DEBUG_LOG_THIS(pPhysicalDevice,"startPage is returning (%ld)",rc)

   LeaveCriticalSection(&printDriverCriticalSection);

   return TRUE;
   }


   BOOL APIENTRY PrintDriver::textOut(SURFOBJ *pso,STROBJ *pStringObject,FONTOBJ *pfo,CLIPOBJ *pco,RECTL *prclExtra,RECTL *prclOpaque,BRUSHOBJ *pboFore,BRUSHOBJ *pboOpaque,POINTL *pptlOrg,MIX mix) {

   DEBUG_LOG_THIS(pPhysicalDevice,"textOut is called",0)

   if ( pStringObject && pStringObject -> cGlyphs && pStringObject -> pwszOrg ) {

      char *pszText = new char[pStringObject -> cGlyphs + 3];

      memset(pszText,0,(pStringObject -> cGlyphs + 3) * sizeof(char));

      char szTemp[128];

      long x1 = max(0,min(pStringObject -> rclBkGround.left,pageWidth));
      long y1 = max(0,min(pStringObject -> rclBkGround.top,pageHeight));
      long x2 = max(0,min(pStringObject -> rclBkGround.right,pageWidth));
      long y2 = max(0,min(pStringObject -> rclBkGround.bottom,pageHeight));

      DWORD cb = sprintf_s(szTemp,128,"%04ld,%04ld,%04ld,%04ld",x1,y1,x2,y2);

DEBUG_LOG_THIS(pPhysicalDevice,szTemp,0);

      WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,szTemp,cb,&cb,NULL);

      WideCharToMultiByte(CP_ACP,0,pStringObject -> pwszOrg,pStringObject -> cGlyphs,pszText,pStringObject -> cGlyphs,0,0);

      char *pszEncoded = NULL;

      ASCIIHexEncode(pszText,pStringObject -> cGlyphs,&pszEncoded);

      cb = strlen(pszEncoded);

      cb = sprintf_s(szTemp,128,":%04ld",cb);

      WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,szTemp,cb,&cb,NULL);

      cb = strlen(pszEncoded);

      WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,pszEncoded,cb,&cb,NULL);

      pszText[0] = 0x0A;

      cb = 1;

      WriteFile(/*pPhysicalDevice -> */currentRectanglesFile,pszText,cb,&cb,NULL);

      delete [] pszText;
      delete [] pszEncoded;

      pPhysicalDevice -> pageEntryCount++;

   }

   BOOL rc = ((PFN_DrvTextOut)(pPhysicalDevice -> pHooks[UD_DrvTextOut].pfn))(pso,pStringObject,pfo,pco,prclExtra,prclOpaque,pboFore,pboOpaque,pptlOrg,mix);

   return rc;
   }


   BOOL APIENTRY PrintDriver::lineTo(SURFOBJ *pso,CLIPOBJ *pco,BRUSHOBJ *pbo,LONG long1,LONG long2,LONG long3,LONG long4,RECTL *pRect,MIX mix) {

DEBUG_LOG_THIS(pPhysicalDevice,"lineTo is called",0)

   BOOL rc = ((PFN_DrvLineTo)(pPhysicalDevice -> pHooks[UD_DrvLineTo].pfn))(pso,pco,pbo,long1,long2,long3,long4,pRect,mix);

   return rc;
   }


   BOOL APIENTRY PrintDriver::strokePath(SURFOBJ *pso,PATHOBJ *ppo,CLIPOBJ *pco,XFORMOBJ *pxo,BRUSHOBJ *pbo,POINTL *pptlBrushOrg,LINEATTRS *plineattrs,MIX mix) {

DEBUG_LOG_THIS(pPhysicalDevice,"strokePath is called",0)

   BOOL rc = ((PFN_DrvStrokePath)(pPhysicalDevice -> pHooks[UD_DrvStrokePath].pfn))(pso,ppo,pco,pxo,pbo,pptlBrushOrg,plineattrs,mix);

   return rc;
   }


   BOOL APIENTRY PrintDriver::fillPath(SURFOBJ *sfo,PATHOBJ *ppobj,CLIPOBJ *pcobj,BRUSHOBJ *pbrushobj,POINTL *pptl,MIX mix,FLONG flong) {

DEBUG_LOG_THIS(pPhysicalDevice,"fillPath is called",0)

   BOOL rc = ((PFN_DrvFillPath)(pPhysicalDevice -> pHooks[UD_DrvFillPath].pfn))(sfo,ppobj,pcobj,pbrushobj,pptl,mix,flong);

   return rc;
   }


   BOOL APIENTRY PrintDriver::strokeAndFillPath(SURFOBJ *pso,PATHOBJ *ppo,CLIPOBJ *pco,XFORMOBJ *pxo,BRUSHOBJ *pboStroke,LINEATTRS *plineattrs,BRUSHOBJ *pboFill,POINTL *pptlBrushOrg,MIX mixFill,FLONG flOptions) {

DEBUG_LOG_THIS(pPhysicalDevice,"strokeAndFillPath is called",0)

   BOOL rc = ((PFN_DrvStrokeAndFillPath)(pPhysicalDevice -> pHooks[UD_DrvStrokeAndFillPath].pfn))(pso,ppo,pco,pxo,pboStroke,plineattrs,pboFill,pptlBrushOrg,mixFill,flOptions);

   return rc;
   }
