
#include "printDriver.h"

#include <time.h>
#include <process.h>

BOOL LaunchProcess(char *, char *);


BOOL APIENTRY PrintDriver::endDocument(SURFOBJ *pso,FLONG fl) {

   BOOL rc = ((PFN_DrvEndDoc)(pPhysicalDevice -> pHooks[UD_DrvEndDoc].pfn))(pso,fl);

   endDocumentHasBeenCalled = true;

   DEBUG_LOG_THIS(pPhysicalDevice," endDocument: TotalBytesWritten %ld",totalBytesWritten);
   DEBUG_LOG_THIS(pPhysicalDevice," endDocument: startDocumentHasBeenCalled %ld",startDocumentHasBeenCalled);
   DEBUG_LOG_THIS(pPhysicalDevice," endDocument: endDocumentHasBeenCalled %ld",endDocumentHasBeenCalled);

   if ( 0 != totalBytesWritten && startDocumentHasBeenCalled ) {

      if ( pPhysicalDevice -> pageNumber && INVALID_HANDLE_VALUE != currentRectanglesFile ) {
         char szTemp[MAX_PATH];
         DWORD cb;
         DWORD currentFilePosition = SetFilePointer(currentRectanglesFile,0,NULL,FILE_CURRENT);
         cb = sprintf_s(szTemp,MAX_PATH,"%06ld-%08ld",pPhysicalDevice -> pageEntryCount,currentFilePosition);
         SetFilePointer(currentRectanglesFile,pPhysicalDevice -> startPageFilePosition,NULL,FILE_BEGIN);
         WriteFile(currentRectanglesFile,szTemp,cb,&cb,NULL);
         SetFilePointer(currentRectanglesFile,currentFilePosition,NULL,FILE_BEGIN);
      }

      if ( szTemporaryFileName[0] ) {

         CloseHandle(currentOutputFile);

         char *p = strrchr(szDesiredFileName,'\\');

         if ( ! p )
            p = strrchr(szDesiredFileName,'/');

         if ( p ) {
            p++;
            while ( *p ) {
               if ( strchr("\\/:*?\"<>|",*p) )
                  *p = '_';
               p++;
            } 
         }

         DeleteFileA(szDesiredFileName);

         BOOL rc = MoveFileExA(szTemporaryFileName,szDesiredFileName,MOVEFILE_REPLACE_EXISTING);
         long sequence = 0;
         while ( ! rc ) {
            char szNewName[MAX_PATH];
            sprintf_s(szNewName,sizeof(szNewName),szDesiredFileName);
            char *pDot = strchr(szNewName,'.');
            if ( pDot )
               *pDot = '\0';
            sprintf_s(szNewName + strlen(szNewName),sizeof(szNewName),"%ld.ps",++sequence);
            rc = MoveFileExA(szTemporaryFileName,szNewName,MOVEFILE_REPLACE_EXISTING);
            if ( rc ) 
               sprintf_s(szDesiredFileName,STATIC_STRING_SIZE,"%s",szNewName);
         }

         currentOutputFile = INVALID_HANDLE_VALUE;
         memset(szTemporaryFileName,0,STATIC_STRING_SIZE);

      }

      if ( szTemporaryRectanglesFileName[0] ) {

         CloseHandle(currentRectanglesFile);

         char *p = strrchr(szDesiredRectanglesFileName,'\\');

         if ( ! p )
            p = strrchr(szDesiredRectanglesFileName,'/');

         if ( p ) {
            p++;
            while ( *p ) {
               if ( strchr("\\/:*?\"<>|",*p) )
                  *p = '_';
               p++;
            } 
         }

         DeleteFileA(szDesiredRectanglesFileName);

         BOOL rc = MoveFileExA(szTemporaryRectanglesFileName,szDesiredRectanglesFileName,MOVEFILE_REPLACE_EXISTING);

         long sequence = 0;
         while ( ! rc ) {
            char szNewName[MAX_PATH];
            sprintf_s(szNewName,sizeof(szNewName),szDesiredRectanglesFileName);
            char *pDot = strrchr(szNewName,'.');
            if ( pDot )
               *pDot = '\0';
            sprintf_s(szNewName + strlen(szNewName),sizeof(szNewName),"%ld.profile",++sequence);
            rc = MoveFileExA(szTemporaryRectanglesFileName,szNewName,MOVEFILE_REPLACE_EXISTING);
            if ( rc ) 
               sprintf_s(szDesiredRectanglesFileName,STATIC_STRING_SIZE,"%s",szNewName);
         }
         currentRectanglesFile = INVALID_HANDLE_VALUE;
         memset(szTemporaryRectanglesFileName,0,STATIC_STRING_SIZE);
      }

      if ( ! pPhysicalDevice -> pageEntryCount && szOrientation[0] ) {

         char szOldFileName[MAX_PATH];
         char szNewFileName[MAX_PATH];

         if ( szDesiredRectanglesFileName[0] ) {

            strcpy_s(szOldFileName,sizeof(szOldFileName),szDesiredRectanglesFileName);
            char *p = strrchr(szOldFileName,'.');
            if ( p )
               *p = '\0';
            if ( strstr(szOrientation,"andscape") )
               sprintf_s(szNewFileName,sizeof(szNewFileName),"%s-%s.profile",szOldFileName,"landscape");
            else
               sprintf_s(szNewFileName,sizeof(szNewFileName),"%s-%s.profile",szOldFileName,"portrait");
            if ( p )
               *p = '.';
            DeleteFileA(szNewFileName);
            BOOL rc = MoveFileExA(szOldFileName,szNewFileName,MOVEFILE_REPLACE_EXISTING);
            strcpy_s(szDesiredRectanglesFileName,STATIC_STRING_SIZE,szNewFileName);
         }

         if ( szDesiredFileName[0] ) {

            strcpy_s(szOldFileName,sizeof(szOldFileName),szDesiredFileName);

            char *p = strrchr(szOldFileName,'.');
            if ( p )
               *p = '\0';
            if ( strstr(szOrientation,"andscape") )
               sprintf_s(szNewFileName,sizeof(szNewFileName),"%s-%s.ps",szOldFileName,"landscape");
            else
               sprintf_s(szNewFileName,sizeof(szNewFileName),"%s-%s.ps",szOldFileName,"portrait");
            if ( p )
               *p = '.';

            DeleteFileA(szNewFileName);
            BOOL rc = MoveFileExA(szOldFileName,szNewFileName,MOVEFILE_REPLACE_EXISTING);
            strcpy_s(szDesiredFileName,STATIC_STRING_SIZE,szNewFileName);
         }

      }

      totalBytesWritten = 0L;
      startDocumentHasBeenCalled = 0L;
      endDocumentHasBeenCalled = 0L;

      char szOutput[MAX_PATH];

      sprintf_s(szOutput, MAX_PATH, "\"%s\"", szDesiredFileName);

      OSVERSIONINFO osVersionInfo;

      memset(&osVersionInfo, 0, sizeof(OSVERSIONINFO));

      osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

      GetVersionEx(&osVersionInfo);

      char szX[256];
      sprintf_s(szX,sizeof(szX),"%ld-%ld %s", osVersionInfo.dwMajorVersion, osVersionInfo.dwMinorVersion, osVersionInfo.szCSDVersion);

      DEBUG_LOG_THIS(pPhysicalDevice, "OSVersion: %s", szX)

      if ( 4 < osVersionInfo.dwMajorVersion ) {

         char szArguments[1024];

         sprintf_s(szArguments, 1024, "\"%s\" /print %s", szExecutableName, szOutput);

         LaunchProcess(szExecutableName, szArguments);

      } else {

         char szArguments[MAX_PATH];

         sprintf_s(szArguments, MAX_PATH, "\"%s\" /print", szExecutableName);

         _spawnl(_P_NOWAIT, szExecutableName, szArguments, szOutput, NULL);

      }

   }

   DEBUG_LOG_THIS(pPhysicalDevice," endDocument: exiting","");

   return TRUE;
}

