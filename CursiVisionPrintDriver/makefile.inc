INSTALLDIR=.

COPYDLL:
!if ( 1 == 0 )
      signtool sign /v /s CursiVisionStore /n "CursiVision Test" $(OBJ_PATH)\$(O)\$(TARGETNAME).dll
      signtool sign /v /s CursiVisionStore /n "CursiVision Test" $(OBJ_PATH)\dist\CursiVision.cat
!endif
      if not exist $(INSTALLDIR)\$(_BUILDARCH) (md $(INSTALLDIR)\$(_BUILDARCH) )
      if exist $(OBJ_PATH)\$(O)\$(TARGETNAME).dll copy $(OBJ_PATH)\$(O)\$(TARGETNAME).dll $(INSTALLDIR)\$(_BUILDARCH)

!if "$(_BUILDARCH)" == "x86"
        if exist $(OBJ_PATH)\$(O)\$(TARGETNAME).dll copy $(OBJ_PATH)\$(O)\$(TARGETNAME).dll $(OBJ_PATH)\dist\i386\$(TARGETNAME).dll                
!else
        if exist $(OBJ_PATH)\$(O)\$(TARGETNAME).dll copy $(OBJ_PATH)\$(O)\$(TARGETNAME).dll $(OBJ_PATH)\dist\$(_BUILDARCH)\$(TARGETNAME).dll                
!endif
#       if exist $(OBJ_PATH)\$(O)\$(TARGETNAME).dll copy $(OBJ_PATH)\$(O)\$(TARGETNAME).dll $(OBJ_PATH)\dist\$(TARGETNAME).dll                
