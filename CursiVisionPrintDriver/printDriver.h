//
// 9-28-2011: I made another stab at getting Adobe to call through to some of the 
// hooks that I need it to call (DrvTextOut), but I cannot (so far) get it to 
// call them. I added a few new hooks to see if it would call them, and am leaving
// those changes in this source - but am going to revert to the driver binaries
// that are currently in production since February 2010 because these new hooks
// did not solve the problem.
//
// Therefore, the source is newer than the binaries but there is no reason to revert
// the source, nor is there a compelling reason to update the binaries.
//
// To build the driver - use the WinDDK command line:
//
//    build /c /f
//
#pragma once

#define USER_MODE_DRIVER
#define INITGUID

#if ( ! defined(WINVER) || (WINVER < 0x0500) )
#undef WINVER
#define WINVER          0x0500
#endif
#if ( ! defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x0500) )
#undef _WIN32_WINNT
#define _WIN32_WINNT    0x0500
#endif

extern "C" {

#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <objbase.h>

#include <windef.h>
#include <winerror.h>
#include <winbase.h>
#include <wingdi.h>

#include <winddi.h>

#include <tchar.h>
#include <excpt.h>
#include <assert.h>

#include <printoem.h>
#include <prcomoem.h>

}

#define OEM_SIGNATURE 'MSFT'
#define OEM_VERSION 0x00000001L

#define DO_HOOKS

struct OEMDEV {
    OEM_DMEXTRAHEADER dmOEMExtra;
};

#define STATIC_STRING_SIZE 512

struct physicalDevice {

   physicalDevice() : 

         pageNumber(0),
         pageEntryCount(0),
         startPageFilePosition(0),

         countFunctions(0)

         { 
         memset(pHooks,0,128 * sizeof(DRVFN)); 
         };

   DRVFN pHooks[128];

   long pageNumber;
   long pageEntryCount;
   DWORD startPageFilePosition;

   long countFunctions;

};

void convertToPDF(char *pszInputFileName);

extern physicalDevice *pPhysicalDevice;

extern CRITICAL_SECTION printDriverCriticalSection;

extern char szTemporaryFileName[];
extern char szTemporaryRectanglesFileName[];
extern char szDesiredFileName[];
extern char szDesiredRectanglesFileName[];
extern char szTargetExecutable[];

extern char szDocumentDirectory[];
extern char szProfilesDirectory[];
extern char szOrientation[];
extern char szExecutableName[];


class PrintDriver : public IPrintOemPS2 {

public:

   PrintDriver();
   ~PrintDriver();

   STDMETHOD(QueryInterface)(REFIID riid, LPVOID FAR* ppvObj);
   STDMETHOD_(ULONG,AddRef)();
   STDMETHOD_(ULONG,Release)();

private:

   STDMETHOD(PublishDriverInterface)( IUnknown *pIUnknown);
   STDMETHOD(EnableDriver)(DWORD DriverVersion,DWORD cbSize,PDRVENABLEDATA pded);
   STDMETHOD(DisableDriver)();
   STDMETHOD(EnablePDEV)(PDEVOBJ pdevobj,__in PWSTR pPrinterName,ULONG cPatterns,HSURF *phsurfPatterns,ULONG cjGdiInfo,
                                   GDIINFO *pGdiInfo,ULONG cjDevInfo,DEVINFO *pDevInfo,DRVENABLEDATA *pded,OUT PDEVOEM *pDevOem);
   STDMETHOD(DisablePDEV)(PDEVOBJ pdevobj);
   STDMETHOD(ResetPDEV)(PDEVOBJ pdevobjOld,PDEVOBJ pdevobjNew);
   STDMETHOD(GetInfo)(DWORD dwMode,PVOID pBuffer,DWORD cbSize,PDWORD pcbNeeded);
   STDMETHOD(DevMode)(DWORD dwMode,POEMDMPARAM pOemDMParam);
   STDMETHOD(Command)(PDEVOBJ pdevobj,DWORD dwIndex,PVOID pData,DWORD cbSize,OUT DWORD *pdwResult);

   STDMETHOD(GetPDEVAdjustment)(DEVOBJ *pDevObj,DWORD dwAdjustType,VOID *pBuffer,DWORD cbBuffer,BOOL *pbAdjustmentDone);
   STDMETHOD(WritePrinter)(DEVOBJ *pDevObj,VOID *pBuffer,DWORD cbBuffer,DWORD *pcbWritten);

   long refCount;

   IPrintOemDriverPS *pIPrintOemDriverPS;
   IPrintCorePS2 *pIPrintCorePS2;
   IPrintCoreHelperPS *pIPrintCoreHelperPS;

   DRVFN hookFunctions[32];

   static HANDLE currentOutputFile;
   static HANDLE currentRectanglesFile;

   static long totalBytesWritten;

   static bool startDocumentHasBeenCalled,endDocumentHasBeenCalled;

   static BOOL APIENTRY startDocument(SURFOBJ *pso,PWSTR pwszDocName,DWORD dwJobId);
   static BOOL APIENTRY endDocument(SURFOBJ *pso,FLONG fl);
   static BOOL APIENTRY textOut(SURFOBJ *pso,STROBJ *pstro,FONTOBJ *pfo,CLIPOBJ *pco,RECTL *prclExtra,RECTL *prclOpaque,BRUSHOBJ *pboFore,BRUSHOBJ *pboOpaque,POINTL *pptlOrg,MIX mix);
   static BOOL APIENTRY startPage(SURFOBJ *pso);

   static BOOL APIENTRY lineTo(SURFOBJ *pso,CLIPOBJ *pco,BRUSHOBJ *pbo,LONG long1,LONG long2,LONG long3,LONG long4,RECTL *pRect,MIX mix);
   static BOOL APIENTRY strokePath(SURFOBJ *pso,PATHOBJ *ppo,CLIPOBJ *pco,XFORMOBJ *pxo,BRUSHOBJ *pbo,POINTL *pptlBrushOrg,LINEATTRS *plineattrs,MIX mix);
   static BOOL APIENTRY fillPath(SURFOBJ *,PATHOBJ *,CLIPOBJ *,BRUSHOBJ *,POINTL *,MIX,FLONG);
   static BOOL APIENTRY strokeAndFillPath(SURFOBJ *pso,PATHOBJ *ppo,CLIPOBJ *pco,XFORMOBJ *pxo,BRUSHOBJ *pboStroke,LINEATTRS *plineattrs,BRUSHOBJ *pboFill,POINTL *pptlBrushOrg,MIX mixFill,FLONG flOptions);

   friend struct physicalDevice;

};

typedef enum tag_Hooks {

    UD_DrvStartDoc = 0,
    UD_DrvStartPage,
    UD_DrvTextOut,
    UD_DrvEndDoc,

    UD_DrvLineTo,
    UD_DrvStrokePath,
    UD_DrvFillPath,
    UD_DrvStrokeAndFillPath,

#if 0
    UD_DrvDrawEscape,
    UD_DrvQueryDeviceSupport,
#endif

    MAX_DDI_HOOKS,

} ENUMHOOKS;

#if 1

#define DEBUG_LOG(f,s)
#define DEBUG_LOG_THIS(t,f,s)
#define DEBUG_LOG_THIS_W(t,f,s)

#else

#define DEBUG_LOG(f,s) \
{\
char szTime[32];\
_strtime(szTime);\
FILE *fX = fopen("C:\\TEMP\\gothere","at");\
fprintf(fX,"%s ",__TIME__);\
fprintf(fX,"%s: ",szTime);\
fprintf(fX,(f),(s));\
fprintf(fX,"\n");\
fclose(fX);\
}

#define DEBUG_LOG_THIS(t,f,s) \
{\
char szTime[32];\
_strtime(szTime);\
FILE *fX = fopen("C:\\TEMP\\gothere","at");\
fprintf(fX,"%s ",__TIME__);\
fprintf(fX,"%s: ",szTime);\
fprintf(fX," (%p) ",t);\
fprintf(fX,(f),(s));\
fprintf(fX,"\n");\
fclose(fX);\
}

#define DEBUG_LOG_THIS_W(t,f,s) \
{\
char szTime[32];\
_strtime(szTime);\
FILE *fX = fopen("C:\\TEMP\\gothere","at");\
fprintf(fX,"%s ",__TIME__);\
fwprintf(fX,L"%s: ",szTime);\
fwprintf(fX,L" (%p) ",t);\
fwprintf(fX,(f),(s));\
fwprintf(fX,L"\n");\
fclose(fX);\
}

#endif