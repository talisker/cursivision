
#include "printDriver.h"

#include <time.h>

static long serverLocks = 0L;

extern "C" BOOL WINAPI DllMain(HINSTANCE hInst, WORD wReason, LPVOID lpReserved) {

   switch ( wReason ) {
   case DLL_PROCESS_ATTACH:
      InitializeCriticalSection(&printDriverCriticalSection);
      break;

   case DLL_THREAD_ATTACH:
      break;

   case DLL_PROCESS_DETACH:
      break;

   case DLL_THREAD_DETACH:
      break;

   }

   return TRUE;
}


class PrintDriverClassFactory : public IClassFactory {

public:

    STDMETHOD(QueryInterface) (REFIID riid, LPVOID FAR* ppvObj);
    STDMETHOD_(ULONG,AddRef)();
    STDMETHOD_(ULONG,Release)();

    STDMETHOD(CreateInstance)(LPUNKNOWN pUnkOuter,REFIID riid,LPVOID FAR* ppvObject);
    STDMETHOD(LockServer)(BOOL bLock);

    PrintDriverClassFactory();
    ~PrintDriverClassFactory();

protected:

    long refCount;

};


PrintDriverClassFactory::PrintDriverClassFactory() : 
    refCount(1) 
{
}

PrintDriverClassFactory::~PrintDriverClassFactory() {
}


HRESULT __stdcall PrintDriverClassFactory::QueryInterface(const IID& iid, void** ppv) {

    *ppv = NULL;
    if ( IID_IUnknown != iid && IID_IClassFactory != iid ) 
        return E_NOINTERFACE;

    *ppv = static_cast<PrintDriverClassFactory*>(this);

    AddRef();

    return S_OK;
}


ULONG __stdcall PrintDriverClassFactory::AddRef() {
    return ++refCount;
}


ULONG __stdcall PrintDriverClassFactory::Release() {
   long rc = --refCount;
   if ( 0 == rc )
      delete this;
   return rc;
}

HRESULT __stdcall PrintDriverClassFactory::CreateInstance(IUnknown* pUnknownOuter,const IID& iid,void** ppv) {

    if ( NULL == ppv )
        return E_POINTER;

    *ppv = NULL;

    if ( NULL != pUnknownOuter )
        return CLASS_E_NOAGGREGATION;

    PrintDriver *pPrintDriver = new PrintDriver;

    if ( NULL == pPrintDriver )
        return E_OUTOFMEMORY;

    HRESULT hr = pPrintDriver -> QueryInterface(iid, ppv);

    pPrintDriver -> Release();

    return hr;
}


HRESULT __stdcall PrintDriverClassFactory::LockServer(BOOL bLock) {
    if ( bLock )
        serverLocks++;
    else
        serverLocks--;
    return S_OK;
}


STDAPI DllCanUnloadNow() {
   if ( 0 == serverLocks )
      return S_OK;
   return S_FALSE;      
}


STDAPI DllGetClassObject(const CLSID& clsid,const IID& iid, void** ppv) {

    if ( NULL == ppv )
        return E_POINTER;

    *ppv = NULL;

    if ( CLSID_OEMRENDER != clsid ) 
        return CLASS_E_CLASSNOTAVAILABLE;

    PrintDriverClassFactory* pPrintDriverClassFactory = new PrintDriverClassFactory;

    if ( NULL == pPrintDriverClassFactory )
        return E_OUTOFMEMORY;

    HRESULT hr = pPrintDriverClassFactory -> QueryInterface(iid, ppv);

    pPrintDriverClassFactory -> Release();

    return hr;
}
