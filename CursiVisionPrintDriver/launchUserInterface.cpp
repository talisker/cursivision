
#include <windows.h>
//#include <WtsApi32.h>
#include <Userenv.h>
#include <time.h>

#include "printDriver.h"

   BOOL LaunchProcess(char *pszExecutable,char *pszArguments) {

   BOOL bReturn = TRUE;

   HANDLE hToken,hTokenDup;

   void *pEnvironment = NULL;

   if ( ! OpenThreadToken(GetCurrentThread(),TOKEN_DUPLICATE,TRUE,&hToken) ) {
      if ( ! OpenProcessToken(GetCurrentProcess(),TOKEN_DUPLICATE,&hToken) ) {
         DEBUG_LOG_THIS(NULL,"BOTH OpenThreadToken and OpenProcessToken failed","")
         return FALSE;
      }
   }

   if ( ! DuplicateTokenEx(hToken,TOKEN_IMPERSONATE | TOKEN_READ | TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE,NULL,SecurityImpersonation,TokenPrimary,&hTokenDup) ) {
      DEBUG_LOG_THIS(NULL,"DuplicateTokenEx failed","")
      return FALSE;
   }

   if ( ! CreateEnvironmentBlock(&pEnvironment, hTokenDup, FALSE) ) {
      DEBUG_LOG_THIS(NULL,"CreateEnvironmentBlock failed","")
     CloseHandle(hTokenDup);
     return FALSE;
   }

   STARTUPINFO startUpInfo;
   PROCESS_INFORMATION processInfo;

   ZeroMemory(&startUpInfo,sizeof(STARTUPINFO));
   ZeroMemory(&processInfo,sizeof(PROCESS_INFORMATION));

   startUpInfo.cb = sizeof(STARTUPINFO);
   startUpInfo.wShowWindow = SW_SHOW;
   startUpInfo.lpDesktop = L"Winsta0\\Default";
   startUpInfo.dwFlags = STARTF_USESHOWWINDOW;
   startUpInfo.wShowWindow = SW_SHOW;

   BSTR bstrArguments = SysAllocStringLen(NULL,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,pszArguments,-1,bstrArguments,MAX_PATH);

   DEBUG_LOG("%s","Attempt to launch:")
   DEBUG_LOG("%s",pszExecutable)
   DEBUG_LOG("%s",pszArguments)
  
   if ( ! CreateProcessAsUser(hTokenDup,NULL,bstrArguments,NULL,NULL,TRUE,
                                    CREATE_UNICODE_ENVIRONMENT | 0 * CREATE_SUSPENDED | CREATE_PRESERVE_CODE_AUTHZ_LEVEL,
                                    pEnvironment,NULL,&startUpInfo,&processInfo) )  {

      DEBUG_LOG_THIS(NULL,"Create Process as user failed","")
      bReturn = FALSE;
   }

#if 1
//   AllowSetForegroundWindow(processInfo.dwProcessId);

//   ResumeThread(processInfo.hThread);
#endif

   RevertToSelf( );

   CloseHandle(hToken);

   if ( pEnvironment )
      DestroyEnvironmentBlock(pEnvironment);

   CloseHandle(hTokenDup);
   
   SysFreeString(bstrArguments);

   return bReturn;
}