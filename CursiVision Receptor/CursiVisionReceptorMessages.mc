
; // ***** CursiVisionReceptor.mc *****

MessageIdTypedef=DWORD

SeverityNames=(
   Success=0x0:STATUS_SEVERITY_SUCCESS
   Informational=0x1:STATUS_SEVERITY_INFORMATIONAL
   Warning=0x2:STATUS_SEVERITY_WARNING
   Error=0x3:STATUS_SEVERITY_ERROR
   )


FacilityNames=(
   System=0x0:FACILITY_SYSTEM
   Runtime=0x2:FACILITY_RUNTIME
   Stubs=0x3:FACILITY_STUBS
   Io=0x4:FACILITY_IO_ERROR_CODE
   )

LanguageNames=(English=0x409:MSG00409)

; // The following are message definitions.

MessageId=0x1
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_STOPPED
Language=English
The CursiVision Receptor Service has been Stopped.
.

MessageId=0x2
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_PROCESS_LAUNCH_SUCCEEDED
Language=English
The Receptor service successfully launched the CursiVision system.
%1
.

MessageId=0x3
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_PROCESS_LAUNCH_FAILED
Language=English
The Receptor Service could not launch the CursiVision process.
%1
.

MessageId=0x4
Severity=Error
Facility=Runtime
SymbolicName=COULDNT_START
Language=English
The CursiVision Receptor Service could not be started.
.

MessageId=0x5
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_EVENT_SERVICE_STOPPING
Language=English
The CursiVision Receptor service is shutting down.
.

MessageId=0x6
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_INFORMATION_FILE_SAVED
Language=English
%1
.

MessageId=0x7
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_CANTLOAD_SETTINGS
Language=English
%1
.

MessageId=0x8
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE
Language=English
%1
.

MessageId=0x9
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_CREATE_SOCKET
Language=English
%1
.

MessageId=0x10
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_BIND_SOCKET
Language=English
%1
.

MessageId=0x11
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_LISTEN_PORT
Language=English
%1
.

MessageId=0x12
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_ACCEPT
Language=English
%1
.

MessageId=0x13
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_INFORMATION_WAITING
Language=English
%1
.

MessageId=0x14
Severity=Informational
Facility=Runtime
SymbolicName=EVENTLOG_INFORMATION_CONNECTION_ACCEPTED
Language=English
%1
.

MessageId=0x15
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR
Language=English
%1
.

MessageId=0x16
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_WRONG_BYTES_SENT
Language=English
%1
.

MessageId=0x17
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_GO_SENT
Language=English
%1
.

MessageId=0x18
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_FILE_SENT
Language=English
%1
.

MessageId=0x19
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_NO_DESTINATION_NAME_SPECIFIED
Language=English
%1
.

MessageId=0x20
Severity=Error
Facility=Runtime
SymbolicName=EVENTLOG_ERROR_FILE_EXISTENCE_CLASH
Language=English
%1
.

