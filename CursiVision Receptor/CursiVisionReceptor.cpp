
#include <Ws2tcpip.h>
#include <Windows.h>
#include <stdio.h>
#include <process.h>

#include "CursiVisionReceptor.h"
#include "messages.h"

char szExecutableName[1024] = {0};
char szProgramDataDirectory[MAX_PATH] = {0};

SERVICE_STATUS_HANDLE hServiceStatus = NULL;
SERVICE_STATUS serviceStatus = {0};

VOID WINAPI serviceCursiVision(DWORD,LPTSTR *);
DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext);
void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint);
unsigned int __stdcall serviceLoop(void *);
HANDLE hServiceThread = NULL;

void launchProcess(char *pszArguments);
BOOL LaunchProcess(char *pszCommandLine);

   extern "C" int main(int argc,char *argv[]) {
 
   SERVICE_TABLE_ENTRY dispatchTable[] = { {SERVICE_NAME_W,(LPSERVICE_MAIN_FUNCTION)serviceCursiVision}, {NULL,NULL} };

   if ( ! StartServiceCtrlDispatcher( dispatchTable ))
      logEvent(L"The " SERVICE_NAME_W L" could not be started.",NULL,EVENTLOG_INFORMATION_TYPE,COULDNT_START); 

   return 0;
   }


   VOID WINAPI serviceCursiVision(DWORD argc,LPTSTR *argv) {

   hServiceStatus = RegisterServiceCtrlHandlerEx(SERVICE_NAME_W,serviceHandler,NULL);

   serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS; 
   serviceStatus.dwServiceSpecificExitCode = 0;    

   reportStatus(SERVICE_START_PENDING,NO_ERROR,3000);

   unsigned threadAddress;
   hServiceThread = (HANDLE)_beginthreadex(NULL,16384,serviceLoop,NULL,CREATE_SUSPENDED,&threadAddress);

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   HKEY hKey = NULL;

   if ( ! szExecutableName[0] ) {

      RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software",0,KEY_QUERY_VALUE,&hKey);
      RegOpenKeyExA(hKey,"InnoVisioNate",0,KEY_READ,&hKey);
      RegOpenKeyExA(hKey,"CursiVision",0,KEY_READ,&hKey);

      if ( hKey ) {

         DWORD cb = MAX_PATH;
         DWORD dwType = REG_SZ;

         char szTarget[MAX_PATH];

         HRESULT rc = RegQueryValueExA(hKey,"Installation Directory",NULL,&dwType,(BYTE *)szExecutableName,&cb);

         cb = MAX_PATH;

         rc = RegQueryValueExA(hKey,"Print Driver Target",NULL,&dwType,(BYTE *)szTarget,&cb);

         sprintf(szExecutableName + strlen(szExecutableName),"\\%s.exe",szTarget);

         cb = MAX_PATH;
         dwType = REG_SZ;
         RegQueryValueExA(hKey,"Printed Documents Directory",NULL,&dwType,(BYTE *)szProgramDataDirectory,&cb);

         szProgramDataDirectory[cb - 2] = '\0';
         char *p = strrchr(szProgramDataDirectory,'\\');
         if ( ! p )
            p = strrchr(szProgramDataDirectory,'/');
         if ( p )
            *p = '\0';

         RegCloseKey(hKey);

      }

      if ( ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE,"System",0,KEY_SET_VALUE,&hKey) ) {

         HKEY hKey2;
         if ( ERROR_SUCCESS == RegOpenKeyExA(hKey,"CurrentControlSet\\Services\\EventLog\\Application",0,KEY_SET_VALUE,&hKey2) ) {

            HKEY hKey3;
            RegCreateKeyExA(hKey2,"CursiVisionReceptor",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKey3,NULL);

            char szTemp[MAX_PATH];
            GetModuleFileNameA(NULL,szTemp,MAX_PATH);

            DWORD cb = (DWORD)strlen(szTemp) + 1;
            RegSetValueExA(hKey3,"EventMessageFile",0L,REG_EXPAND_SZ,(BYTE *)szTemp,cb);

            RegCloseKey(hKey3);
            RegCloseKey(hKey2);

         }

         RegCloseKey(hKey);

      }

      if ( ! hKey )
         logEvent(L"The CursiVisionReceptor service was not able to read the CursiVision installation information from the Registry. CursiVision may be installed incorrectly.",
                     NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_SERVICE_STOPPING);
   }

   ResumeThread(hServiceThread);

   return;
   }



   DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext) {

   switch ( dwControl ) {

   case SERVICE_CONTROL_STOP:
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      TerminateThread(hServiceThread,0L);
      logEvent(L"The CursiVisionReceptor service is closing down.",NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_SERVICE_STOPPING);
      return NO_ERROR;

   }

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   return NO_ERROR;
   }


   void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint) {

   static DWORD dwCheckPoint = 1;

   serviceStatus.dwCurrentState = dwCurrentState;
   serviceStatus.dwWin32ExitCode = dwWin32ExitCode;
   serviceStatus.dwWaitHint = dwWaitHint;

   if ( SERVICE_START_PENDING == dwCurrentState )
      serviceStatus.dwControlsAccepted = 0;
   else 
      serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

   if ( SERVICE_RUNNING == dwCurrentState || SERVICE_STOPPED == dwCurrentState )
      serviceStatus.dwCheckPoint = 0;
    else 
      serviceStatus.dwCheckPoint = dwCheckPoint++;

   SetServiceStatus(hServiceStatus,&serviceStatus);

   return;
   }
   

   void logEvent(char *pszMessage,char *pszAdditionalInfo,DWORD eventType,DWORD eventNumber) {
   TCHAR message[1024];
   MultiByteToWideChar(CP_ACP,0,pszMessage,-1,message,1023);
   if ( pszAdditionalInfo ) {
      TCHAR additionalInfo[1024];
      MultiByteToWideChar(CP_ACP,0,pszAdditionalInfo,-1,additionalInfo,1023);
      logEvent(message,additionalInfo,eventType,eventNumber);
   } else
      logEvent(message,NULL,eventType,eventNumber);
   return;
   }


   void logEvent(LPTSTR szFunction,LPTSTR additionalInfo,DWORD eventType,DWORD eventNumber) { 
   HANDLE hEventSource = RegisterEventSource(NULL,SERVICE_NAME_W);
   if ( NULL == hEventSource ) 
      return;
   LPCTSTR lpszStrings[2];
   lpszStrings[0] = szFunction;
   if ( additionalInfo ) {
      lpszStrings[1] = additionalInfo;
      ReportEvent(hEventSource,(WORD)eventType,0,eventNumber,NULL,2,0,lpszStrings,NULL);
   } else 
      ReportEvent(hEventSource,(WORD)eventType,0,eventNumber,NULL,1,0,lpszStrings,NULL);
   DeregisterEventSource(hEventSource);
   return;
   }


   void launchProcess(char *pszCommand) {

   char szCommand[1024];

   sprintf(szCommand,"%s %s",szExecutableName,pszCommand);

   BOOL rc = LaunchProcess(szCommand);

   TCHAR message[1024],sx[1024];
   MultiByteToWideChar(CP_ACP,0,szCommand,-1,sx,256);

   TCHAR additionalInfo[1024];
   if ( ! rc ) {
      swprintf(message,L"Launch failed: %s",sx);
      swprintf(additionalInfo,L"The windows error code is %ld",GetLastError());
      logEvent(message,additionalInfo,EVENTLOG_ERROR_TYPE,EVENTLOG_EVENT_PROCESS_LAUNCH_FAILED);
   } else {
      swprintf(message,L"Launch succeeded: %s",sx);
      swprintf(additionalInfo,L"The launch command is: \n%s. GLE: %ld",sx,GetLastError());
      logEvent(message,additionalInfo,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_PROCESS_LAUNCH_SUCCEEDED);
   }

   return;
   }