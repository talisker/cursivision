
#include <windows.h>
#include <WtsApi32.h>
#include <Userenv.h>
#include <time.h>
#include <stdio.h>

BOOL LaunchProcess(char *pszArguments) {

   BOOL bReturn = TRUE;

   HANDLE hToken,hTokenDup;
   void *pEnvironment = NULL;

   DWORD sessionId = WTSGetActiveConsoleSessionId();
   if ( ! WTSQueryUserToken(sessionId,&hToken) )
      return FALSE;

   if ( ! DuplicateTokenEx(hToken,MAXIMUM_ALLOWED,NULL,SecurityImpersonation,TokenPrimary,&hTokenDup) ) 
      return FALSE;

   CloseHandle(hToken);

   if ( ! CreateEnvironmentBlock(&pEnvironment, hTokenDup, FALSE) ) {
     CloseHandle(hTokenDup);
     return FALSE;
   }

   STARTUPINFO startUpInfo;
   PROCESS_INFORMATION processInfo;

   ZeroMemory(&startUpInfo,sizeof(STARTUPINFO));
   ZeroMemory(&processInfo,sizeof(PROCESS_INFORMATION));

   startUpInfo.cb = sizeof(STARTUPINFO);
   startUpInfo.wShowWindow = SW_SHOW;
   startUpInfo.lpDesktop = L"Winsta0\\Default";
   startUpInfo.dwFlags = STARTF_USESHOWWINDOW;
   startUpInfo.wShowWindow = SW_SHOW;

   BSTR bstrArguments = SysAllocStringLen(NULL,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,pszArguments,-1,bstrArguments,MAX_PATH);

   if ( ! CreateProcessAsUser(hTokenDup,NULL,bstrArguments,NULL,NULL,TRUE,
                                    CREATE_UNICODE_ENVIRONMENT | NORMAL_PRIORITY_CLASS | CREATE_PRESERVE_CODE_AUTHZ_LEVEL,
                                    pEnvironment,NULL,&startUpInfo,&processInfo) )  {
      bReturn = FALSE;
   }

   RevertToSelf();

   if ( pEnvironment )
      DestroyEnvironmentBlock(pEnvironment);

   CloseHandle(hTokenDup);
   
   SysFreeString(bstrArguments);

   return bReturn;
   }