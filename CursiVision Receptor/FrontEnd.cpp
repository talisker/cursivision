
#include <Ws2tcpip.h>
#include <Windows.h>
#include <stdio.h>
#include <process.h>

#include "FrontEnd.h"

char szExecutableName[1024] = {0};
char szProgramDataDirectory[MAX_PATH] = {0};

SERVICE_STATUS_HANDLE hServiceStatus = NULL;
SERVICE_STATUS serviceStatus = {0};

VOID WINAPI serviceCursiVision(DWORD,LPTSTR *);
DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext);
void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint);
void logEvent(LPTSTR szFunction);
unsigned int __stdcall serviceLoop(void *);
HANDLE hServiceThread = NULL;

void launchProcess(char *pszArguments);
BOOL LaunchProcess(char *pszCommandLine);

   extern "C" int main(int argc,char *argv[]) {
 
   SERVICE_TABLE_ENTRY dispatchTable[] = { {SERVICE_NAME_W,(LPSERVICE_MAIN_FUNCTION)serviceCursiVision}, {NULL,NULL} };

   if ( ! StartServiceCtrlDispatcher( dispatchTable )) { 
      logEvent(L"The " SERVICE_NAME_W L" has entered the stopped state"); 
   } 

   return 0;
   }

   VOID WINAPI serviceCursiVision(DWORD argc,LPTSTR *argv) {

   hServiceStatus = RegisterServiceCtrlHandlerEx(SERVICE_NAME_W,serviceHandler,NULL);

   serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS; 
   serviceStatus.dwServiceSpecificExitCode = 0;    

   reportStatus(SERVICE_START_PENDING,NO_ERROR,3000);

   unsigned threadAddress;
   hServiceThread = (HANDLE)_beginthreadex(NULL,16384,serviceLoop,NULL,CREATE_SUSPENDED,&threadAddress);

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   HKEY hKey = NULL;

   if ( ! szExecutableName[0] ) {

      if ( ERROR_SUCCESS == RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software",0,KEY_QUERY_VALUE,&hKey) ) {
          if ( ERROR_SUCCESS != RegOpenKeyExA(hKey,"InnoVisioNate",0,KEY_READ,&hKey) ) {
             RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software",0,KEY_QUERY_VALUE | KEY_WOW64_32KEY,&hKey);
             RegOpenKeyExA(hKey,"InnoVisioNate",0,KEY_READ | KEY_WOW64_32KEY,&hKey);
          }
      } else {
          RegOpenKeyExA(HKEY_LOCAL_MACHINE,"Software",0,KEY_QUERY_VALUE | KEY_WOW64_32KEY,&hKey);
          RegOpenKeyExA(hKey,"InnoVisioNate",0,KEY_READ | KEY_WOW64_32KEY,&hKey);
      }

      if ( hKey ) {

         DWORD cb = MAX_PATH;
         DWORD dwType = REG_SZ;

         RegQueryValueExA(hKey,"Installation Directory",NULL,&dwType,(BYTE *)szExecutableName,&cb);
         sprintf(szExecutableName + strlen(szExecutableName),"\\CursiVision.exe");

         cb = MAX_PATH;
         dwType = REG_SZ;
         RegQueryValueExA(hKey,"Printed Documents Directory",NULL,&dwType,(BYTE *)szProgramDataDirectory,&cb);

         szProgramDataDirectory[cb - 2] = '\0';
         char *p = strrchr(szProgramDataDirectory,'\\');
         if ( ! p )
            p = strrchr(szProgramDataDirectory,'/');
         if ( p )
            *p = '\0';

         RegCloseKey(hKey);
      }

   }

   ResumeThread(hServiceThread);

   return;
   }



   DWORD WINAPI serviceHandler(DWORD dwControl,DWORD dwEventType,LPVOID pEventData,LPVOID pContext) {

   switch ( dwControl ) {

//   case SERVICE_CONTROL_START:
//      reportStatus(SERVICE_CONTROL_START,NO_ERROR,0);
//      return NO_ERROR;

   case SERVICE_CONTROL_STOP:
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      TerminateThread(hServiceThread,0L);
      logEvent(L"The CursiVisionReceptor is closing down.");
      return NO_ERROR;

   }

   reportStatus(SERVICE_RUNNING,NO_ERROR,0);

   return NO_ERROR;
   }


   void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint) {

   static DWORD dwCheckPoint = 1;

   serviceStatus.dwCurrentState = dwCurrentState;
   serviceStatus.dwWin32ExitCode = dwWin32ExitCode;
   serviceStatus.dwWaitHint = dwWaitHint;

   if ( SERVICE_START_PENDING == dwCurrentState )
      serviceStatus.dwControlsAccepted = 0;
   else 
      serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

   if ( SERVICE_RUNNING == dwCurrentState || SERVICE_STOPPED == dwCurrentState )
      serviceStatus.dwCheckPoint = 0;
    else 
      serviceStatus.dwCheckPoint = dwCheckPoint++;

   SetServiceStatus(hServiceStatus,&serviceStatus);

   return;
   }
   

   void logEvent(char *pszMessage) {
   TCHAR message[1024];
   MultiByteToWideChar(CP_ACP,0,pszMessage,-1,message,1023);
   logEvent(message);
   return;
   }


   void logEvent(LPTSTR szFunction) { 

   HANDLE hEventSource = RegisterEventSource(NULL,SERVICE_NAME_W);

   if ( NULL == hEventSource ) 
      return;

   LPCTSTR lpszStrings[2];

   lpszStrings[0] = SERVICE_NAME_W;
   lpszStrings[1] = szFunction;

   ReportEvent(hEventSource,EVENTLOG_SUCCESS,0,SERVICE_ERROR,NULL,2,0,lpszStrings,NULL);

   DeregisterEventSource(hEventSource);

   return;
   }


   void launchProcess(char *pszCommand) {

   char szCommand[1024];

   sprintf(szCommand,"%s %s",szExecutableName,pszCommand);

   BOOL rc = LaunchProcess(szCommand);

   TCHAR message[1024],sx[1024];
   MultiByteToWideChar(CP_ACP,0,szCommand,-1,sx,256);

   if ( ! rc ) 
      swprintf(message,L"Launch failed: %s",sx);
   else
      swprintf(message,L"Launch succeeded: %s",sx);

   logEvent(message);

   return;
   }