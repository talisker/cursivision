
#include <Ws2tcpip.h>
#include <Windows.h>
#include <stdio.h>
#include <process.h>

#include "CursiVisionReceptor.h"

#include "messages.h"

extern char szProgramDataDirectory[];

void reportStatus(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint);
void processFile(char *pszInputFile,char *pszTargetDirectory,char *pszPreferredName,char *pszProfileFile);

void launchProcess(char *pszArguments);
BOOL LaunchProcess(char *pszCommandLine);

extern SERVICE_STATUS serviceStatus;

   unsigned int __stdcall serviceLoop(void *) {

   WSADATA wsaData;
   TCHAR message[NI_MAXHOST];
   char szInput[1024],szCommand[2048],szTemp[256];
   char szTempFileName[MAX_PATH],szFileName[MAX_PATH],szPreferredName[MAX_PATH],szStorageLocation[MAX_PATH];
   char szTempProfileFileName[MAX_PATH];
   char szHostName[NI_MAXHOST];
   char szForwardServer[64];
   char szResponse[1024],szStatus[2048];

   if ( WSAStartup(MAKEWORD(2,2),&wsaData) ) {
      TCHAR message[128];
      swprintf(message,L"The CursiVision Receptor could not initialize the windows socket library. RC = %ld",WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_SOCKET_SUPPORT_INITIALIZE);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      return 0;
   }

   addrinfo addressInfo;
   addrinfo *pResolvedAddressInfo;
   sockaddr sockAddr;

   memset(&addressInfo,0,sizeof(addrinfo));
   memset(&sockAddr,0,sizeof(sockaddr));

   addressInfo.ai_flags = AI_PASSIVE;
   addressInfo.ai_family = AF_INET;
   addressInfo.ai_socktype = SOCK_STREAM;
   addressInfo.ai_protocol = IPPROTO_TCP;
   addressInfo.ai_addrlen = 0;
   addressInfo.ai_addr = NULL;
   addressInfo.ai_canonname = NULL;
   addressInfo.ai_next = NULL;

   getaddrinfo(NULL,SERVICE_PORT_A,&addressInfo,&pResolvedAddressInfo);

   getnameinfo(pResolvedAddressInfo -> ai_addr,(socklen_t)pResolvedAddressInfo -> ai_addrlen,szHostName,NI_MAXHOST,NULL,0L,0L);

   SOCKET theSocket = socket(pResolvedAddressInfo -> ai_family,pResolvedAddressInfo -> ai_socktype,pResolvedAddressInfo -> ai_protocol);

   if ( INVALID_SOCKET == theSocket ) {
      TCHAR message[128];
      swprintf(message,L"The CursiVisionReceptor could not create a socket. RC = %ld",WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_CREATE_SOCKET);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      freeaddrinfo(pResolvedAddressInfo);
      return 0;
   }

   if ( SOCKET_ERROR == bind(theSocket,pResolvedAddressInfo -> ai_addr,(int)pResolvedAddressInfo -> ai_addrlen) ) {
      TCHAR message[128];
      swprintf(message,L"The CursiVisionReceptor could not bind the socket on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_BIND_SOCKET);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      freeaddrinfo(pResolvedAddressInfo);
      return 0;
   }

   if ( SOCKET_ERROR == listen(theSocket,SOMAXCONN) ) {
      TCHAR message[128];
      swprintf(message,L"The CursiVisionReceptor could not listen on the socket on port  %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
      logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_LISTEN_PORT);
      reportStatus(SERVICE_STOPPED,NO_ERROR,0);
      freeaddrinfo(pResolvedAddressInfo);
      return 0;
   }

   BSTR bstrHostName = SysAllocStringLen(NULL,NI_MAXHOST);
   MultiByteToWideChar(CP_ACP,0,szHostName,-1,bstrHostName,NI_MAXHOST);

   while ( SERVICE_STOPPED != serviceStatus.dwCurrentState ) {

      swprintf(message,L"The CursiVisionReceptor is awaiting commands on %s:%s.",bstrHostName,SERVICE_PORT_W);
      logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_WAITING);

      SOCKET clientSocket = accept(theSocket,NULL,NULL);

      swprintf(message,L"The CursiVision Receptor has accepted a connection on %s.",SERVICE_PORT_W);
      logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_CONNECTION_ACCEPTED);

      if ( SOCKET_ERROR == clientSocket ) {
         TCHAR message[128];
         swprintf(message,L"The CursiVisionReceptor failed during socket accept on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
         logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_ACCEPT);
         reportStatus(SERVICE_STOPPED,NO_ERROR,0);
         freeaddrinfo(pResolvedAddressInfo);
         return 0;
      }

      memset(szInput,0,sizeof(szInput));
      memset(szCommand,0,sizeof(szCommand));
      memset(szResponse,0,sizeof(szResponse));
      memset(szFileName,0,sizeof(szFileName));
      memset(szTempFileName,0,sizeof(szTempFileName));
      memset(szTempProfileFileName,0,sizeof(szTempProfileFileName));
      memset(szPreferredName,0,sizeof(szPreferredName));
      memset(szStorageLocation,0,sizeof(szStorageLocation));
      memset(szStatus,0,sizeof(szStatus));
      memset(szForwardServer,0,sizeof(szForwardServer));

      bool overWritePermitted = false;
      bool goSent = false;

      while ( SERVICE_STOPPED != serviceStatus.dwCurrentState ) {

         memset(szInput,0,sizeof(szInput));

         long rc = recv(clientSocket,szInput,1024,0L);

         if ( rc < 0 ) {
            swprintf(message,L"The CursiVision Receptor encountered an error recieving data on port %s. RC = %ld",SERVICE_PORT_W,WSAGetLastError());
            logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_SOCKET_RECIEVE_ERROR);
            break;
         }

         if ( 0 == rc )
            break;

         if ( 0 == strncmp(szInput,"go",4) ) {
            goSent = true;
            break;
         }

         if ( 0 == strncmp(szInput,"help",4) ) {
            sprintf(szResponse,"\n"
                              "filesize # <- to submit the binary contents of a file of # bytes\n"
                              "overwrite <- Optional: To permit an existing file of the same name to be overwritten.\n"
                              "\tIf not specified, the presence of a file with the same name in the server's program data location will cause job rejection.\n"
                              "name theName <- Required: Set the name of the resulting file on the server before signing\n"
                              "\tNote, directory information is ignored. The file will be placed in the program data directory\n"
                              "store theLocation <- Optional: Store the signed document on the server, in theLocation specified, without bringing it up to sign.\n"
                              "\tThis option is used to simply save the document on the server without having to publish a network drive to the server on the client.\n"
                              "\tUsing this option also circumvents document signing on the server machine - it is intended only to affect storage of the document.\n"
                              "go <- to exit and submit the document for signing. After submitting done - read the response to determine success or failure.");
            send(clientSocket,szResponse,(DWORD)strlen(szResponse),0);
         }

         if ( 0 == strncmp(szInput,"overwrite",9) ) {
            overWritePermitted = true;
            send(clientSocket,"ok",2,0);
            continue;
         }

         if ( 0 == strncmp(szInput,"name",4) ) {
            char *pStart = szInput + 5;
            char *p = strrchr(szInput,'\\');
            if ( ! p )
               p = strrchr(szInput,'/');
            if ( p )
               *p = '\0';
            p = pStart;
            while ( *p && ' ' == *p )
               p++;
            strcpy(szPreferredName,p);
            send(clientSocket,"ok",2,0);
            continue;
         }

         if ( 0 == strncmp(szInput,"store",5) ) {
            char *pStart = szInput + 6;
            while ( *pStart && ' ' == *pStart )
               pStart++;
            strcpy(szStorageLocation,pStart);
            send(clientSocket,"ok",2,0);
            continue;
         }

         if ( 0 == strncmp(szInput,"forward",7) ) {
            char *pStart = szInput + 8;
            char *p = strrchr(szInput,'\\');
            if ( ! p )
               p = strrchr(szInput,'/');
            if ( p )
               *p = '\0';
            p = pStart;
            while ( *p && ' ' == *p )
               p++;
            strcpy(szForwardServer,p);
            send(clientSocket,"ok",2,0);
            continue;
         }

         bool fileRecievingFailed = false;

         if ( 0 == strncmp(szInput,"filesize",8) ) {

            char *p = szInput + 9;
            long fileSize = atol(p);

            sprintf(szTemp,"send %ld bytes",fileSize);
            send(clientSocket,szTemp,(DWORD)strlen(szTemp),0L);

            long totalBytes = 0;
            BYTE *pBinary = new BYTE[fileSize];
            memset(pBinary,0,fileSize * sizeof(BYTE));
            while ( totalBytes < fileSize ) {
               rc = recv(clientSocket,(char *)pBinary + totalBytes,fileSize - totalBytes,0L);
               if ( 0 > rc ) {
                  swprintf(message,L"The CursiVisionReceptor expected to receive %ld bytes but the client only sent %ld before closing the connection.",fileSize,totalBytes);
                  logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_WRONG_BYTES_SENT);
                  fileRecievingFailed = true;
                  break;
               }
               totalBytes += rc;
            }

            if ( fileRecievingFailed ) {
               delete [] pBinary;
               break;
            }

            FILE *fPDF = NULL;
            sprintf(szTempFileName,"%s",_tempnam(NULL,NULL));
            fPDF = fopen(szTempFileName,"wb");
            fwrite(pBinary,fileSize,1,fPDF);
            fclose(fPDF);

            delete [] pBinary;

            send(clientSocket,"received",8,0L);

            continue;

         } 

         if ( fileRecievingFailed ) {
            shutdown(clientSocket,SD_SEND);
            continue;
         }

         if ( 0 == strncmp(szInput,"profilesize",11) ) {

            char *p = szInput + 12;
            long fileSize = atol(p);

            sprintf(szTemp,"send %ld bytes",fileSize);
            send(clientSocket,szTemp,(DWORD)strlen(szTemp),0L);

            long totalBytes = 0;
            BYTE *pBinary = new BYTE[fileSize];
            memset(pBinary,0,fileSize * sizeof(BYTE));
            while ( totalBytes < fileSize ) {
               rc = recv(clientSocket,(char *)pBinary + totalBytes,fileSize - totalBytes,0L);
               if ( 0 > rc ) {
                  swprintf(message,L"The CursiVisionReceptor expected to receive %ld bytes but the client only sent %ld before closing the connection.",fileSize,totalBytes);
                  logEvent(message,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_WRONG_BYTES_SENT);
                  fileRecievingFailed = true;
                  break;
               }
               totalBytes += rc;
            }

            if ( fileRecievingFailed ) {
               delete [] pBinary;
               break;
            }

            FILE *fProfile = NULL;
            sprintf(szTempProfileFileName,"%s",_tempnam(NULL,NULL));
            fProfile = fopen(szTempProfileFileName,"wb");
            fwrite(pBinary,fileSize,1,fProfile);
            fclose(fProfile);

            delete [] pBinary;

            send(clientSocket,"received",8,0L);

            continue;

         } 

         if ( fileRecievingFailed ) {
            shutdown(clientSocket,SD_SEND);
            continue;
         }

         send(clientSocket,"\ninvalid - send help for information\n",(DWORD)strlen("\ninvalid - send help for information\n"),0L);

      }

      if ( ! goSent ) {
         sprintf(szInput,"failure: The client never sent \"go\" to submit the job for signing.");
         send(clientSocket,szInput,(DWORD)strlen(szInput),0L);
         shutdown(clientSocket,SD_SEND);
         logEvent(szInput,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_GO_SENT);
         shutdown(clientSocket,SD_SEND);
         continue;
      }

      if ( ! szTempFileName[0] ) {
         sprintf(szInput,"failure: No file has been sent. Send \"filesize: # bytes\" then send those bytes raw.");
         send(clientSocket,szInput,(DWORD)strlen(szInput),0L);
         shutdown(clientSocket,SD_SEND);
         logEvent(szInput,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_FILE_SENT);
         shutdown(clientSocket,SD_SEND);
         continue;
      }

      if ( ! szPreferredName[0] && ! szStorageLocation[0] ) {
         sprintf(szInput,"failure: The destination file name was not specified. Send \"name <fileName>\" without the quotes.");
         send(clientSocket,szInput,(DWORD)strlen(szInput),0L);
         shutdown(clientSocket,SD_SEND);
         DeleteFileA(szTempFileName);
         logEvent(szInput,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_NO_DESTINATION_NAME_SPECIFIED);
         shutdown(clientSocket,SD_SEND);
         continue;
      }

      if ( szStorageLocation[0] )
         strcpy(szFileName,szStorageLocation);
      else
         sprintf(szFileName,"%s\\%s",szProgramDataDirectory,szPreferredName);

      if ( ! overWritePermitted && ! szStorageLocation[0] ) {
         FILE *fTest = fopen(szFileName,"rb");
         if ( fTest ) {
            fclose(fTest);
            sprintf(szInput,"failure: A file with the same name already exists in the server's destination. Send \"overwrite\" without the quotes to allow a file to be overwritten.");
            send(clientSocket,szInput,(DWORD)strlen(szInput),0L);
            shutdown(clientSocket,SD_SEND);
            DeleteFileA(szTempFileName);
            logEvent(szInput,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_FILE_EXISTENCE_CLASH);
            shutdown(clientSocket,SD_SEND);
            continue;
         }
      }

      if ( szStorageLocation[0] )
         processFile(szTempFileName,szStorageLocation,szPreferredName,szTempProfileFileName);
      else
         CopyFileA(szTempFileName,szFileName,FALSE);

      DeleteFileA(szTempFileName);

      send(clientSocket,"ok",2,0L);

      shutdown(clientSocket,SD_SEND);

      if ( szStorageLocation[0] ) 
         continue;

      sprintf(szCommand + strlen(szCommand),"/File \"%s\"",szFileName);
      if ( szTempProfileFileName[0] )
         sprintf(szCommand + strlen(szCommand)," /Disposition \"%s\"",szTempProfileFileName);

      if ( szForwardServer[0] )
         sprintf(szCommand + strlen(szCommand)," /Forward \"%s\"",szForwardServer);

      launchProcess(szCommand);

   }

   swprintf(message,L"The CursiVisionReceptor is shutting down.");
   logEvent(message,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_EVENT_SERVICE_STOPPING);

   return 0;
   }
