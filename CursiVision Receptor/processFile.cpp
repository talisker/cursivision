
#include <Windows.h>
#include <stdio.h>
#include <time.h>

#include "CursiVisionReceptor.h"

#include "resultDisposition.h"
#include "Properties_i.h"
#include "Properties_i.c"
#include "messages.h"

void processFile(char *pszInputFile,char *pszTargetDirectory,char *pszPreferredName,char *pszProfileFile) {

   if ( ! pszProfileFile || ! pszProfileFile[0] ) {

      char szFileName[MAX_PATH];

      char *p = strrchr(pszPreferredName,'\\');
      
      if ( ! p )
         p = strrchr(pszPreferredName,'/');
      if ( ! p )
         p = pszPreferredName - 1;

      sprintf(szFileName,"%s\\%s",pszTargetDirectory,p + 1);

      CopyFileA(pszInputFile,szFileName,FALSE);

      char szTemp[1024];
      sprintf(szTemp,"The CursiVision Receptor saved the signed document to: %s",szFileName);
      logEvent(szTemp,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_FILE_SAVED);

      return;

   }

   IGProperties *pIGProperties = NULL;

   CoInitialize(NULL);

   resultDisposition theDisposition;

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   pIGProperties -> Add(L"result disposition",NULL);

   pIGProperties -> DirectAccess(L"result disposition",TYPE_BINARY,&theDisposition,sizeof(resultDisposition));

   BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,pszProfileFile,-1,bstrFileName,MAX_PATH);

   pIGProperties -> put_FileName(bstrFileName);

   VARIANT_BOOL loadSuccess;

   pIGProperties -> LoadFile(&loadSuccess);

   if ( ! loadSuccess ) {
      SysFreeString(bstrFileName);
      char szTemp[1024];
      sprintf(szTemp,"The CursiVision Receptor was not able to load the settings from %s",pszProfileFile);
      logEvent(szTemp,NULL,EVENTLOG_ERROR_TYPE,EVENTLOG_ERROR_CANTLOAD_SETTINGS);
      processFile(pszInputFile,pszTargetDirectory,pszPreferredName,NULL);
      return;
   }

   pIGProperties -> Release();

   CoUninitialize();

   SysFreeString(bstrFileName);

   char szUltimateDirectory[MAX_PATH];
   
   char szResultFile[MAX_PATH];
   memset(szResultFile,0,sizeof(szResultFile));
   
   if ( pszPreferredName )
      sprintf(szResultFile,"%s\\%s",pszTargetDirectory,pszPreferredName);
   else
      sprintf(szResultFile,"%s\\",pszTargetDirectory);

   strcpy(szUltimateDirectory,szResultFile);

   if ( theDisposition.saveInMonthYear || theDisposition.saveInDayMonth ) {

      char szBaseName[MAX_PATH];

      strcpy(szUltimateDirectory,szResultFile);
      char *p = strrchr(szUltimateDirectory,'\\');
      if ( ! p )
         p = strrchr(szUltimateDirectory,'/');
      if ( p )
         *p = '\0';
      else 
         p = szUltimateDirectory - 1;

      strcpy(szBaseName,p + 1);
      p = strrchr(szBaseName,'.');
      if ( p ) 
         *p = '\0';

      if ( theDisposition.saveInMonthYear ) {
         time_t t;
         time(&t);
         char *p = asctime(localtime(&t));
         p[7] = '\0';
         p[24] = '\0';
         sprintf(szUltimateDirectory + strlen(szUltimateDirectory),"\\%s-%s",p + 4,p + 20);
         long rc = CreateDirectoryA(szUltimateDirectory,0L);
      }

      if ( theDisposition.saveInDayMonth ) {
         time_t t;
         time(&t);
         char *p = asctime(localtime(&t));
         p[3] = '\0';
         p[10] = '\0';
         sprintf(szUltimateDirectory + strlen(szUltimateDirectory),"\\%s-%s",p,p + 8);
         long rc = CreateDirectoryA(szUltimateDirectory,0L);
      }

      sprintf(szResultFile,"%s\\%s.pdf",szUltimateDirectory,szBaseName);

   }

   CopyFileA(pszInputFile,szResultFile,FALSE);

   char szTemp[1024];
   sprintf(szTemp,"The CursiVision Receptor saved the signed document to: %s",szResultFile);
   logEvent(szTemp,NULL,EVENTLOG_INFORMATION_TYPE,EVENTLOG_INFORMATION_FILE_SAVED);

   return;
}
