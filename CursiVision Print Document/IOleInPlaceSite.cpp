
#include "Print Document.h"

   long __stdcall _IOleInPlaceSite::QueryInterface(REFIID riid,void **ppv) {

   if ( IID_IUnknown == riid ) 
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( IID_IOleInPlaceSite == riid ) 
      *ppv = static_cast<IOleInPlaceSite *>(this);
   else

   if ( IID_IOleInPlaceSiteEx == riid ) 
      *ppv = static_cast<IOleInPlaceSiteEx *>(this);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }
   unsigned long __stdcall _IOleInPlaceSite::AddRef() {
   return ++refCount;
   }
   unsigned long __stdcall _IOleInPlaceSite::Release() {
   return --refCount;
   }


   HRESULT _IOleInPlaceSite::GetWindow(HWND *gwh) {
   *gwh = hwndHost;
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::ContextSensitiveHelp(BOOL fEnterMode) {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::CanInPlaceActivate() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::OnInPlaceActivate() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::OnUIActivate() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::GetWindowContext(IOleInPlaceFrame **ppIOleInPlaceFrame,
                                                               IOleInPlaceUIWindow **ppIOleInPlaceUIWindow,
                                                               RECT *pRectPosition,RECT *pRectClip,
                                                               OLEINPLACEFRAMEINFO *pOleInPlaceFrameInfo) {
   *ppIOleInPlaceFrame = pIOleInPlaceFrame;
   *ppIOleInPlaceUIWindow = static_cast<IOleInPlaceUIWindow *>(pIOleInPlaceFrame);

   RECT rectHost;
   GetWindowRect(hwndHost,&rectHost);
   rectHost.right = rectHost.right - rectHost.left;
   rectHost.bottom = rectHost.bottom - rectHost.top;
   rectHost.left = 0;
   rectHost.top = 0;

   memcpy(pRectPosition,&rectHost,sizeof(RECT));
   memcpy(pRectClip,pRectPosition,sizeof(RECT));

   memset(pOleInPlaceFrameInfo,0,sizeof(OLEINPLACEFRAMEINFO));

   pOleInPlaceFrameInfo -> cb = sizeof(OLEINPLACEFRAMEINFO);
   pOleInPlaceFrameInfo -> fMDIApp = FALSE;
   pOleInPlaceFrameInfo -> hwndFrame = hwndHost;
   pOleInPlaceFrameInfo -> haccel = NULL;
   pOleInPlaceFrameInfo -> cAccelEntries = NULL;   

   return S_OK;
   }


   HRESULT _IOleInPlaceSite::Scroll(SIZE) {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::OnUIDeactivate(BOOL) {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::OnInPlaceDeactivate() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::DiscardUndoState() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::DeactivateAndUndo() {
   return S_OK;
   }


   HRESULT _IOleInPlaceSite::OnPosRectChange(const RECT*) {
   return S_OK;
   }

   HRESULT _IOleInPlaceSite::OnInPlaceActivateEx(BOOL *pRedraw,DWORD dwFlags) {
   *pRedraw = FALSE;
   return S_OK;
   }

   HRESULT _IOleInPlaceSite::OnInPlaceDeactivateEx(BOOL fNoRedraw) {
   return S_OK;
   }

   HRESULT _IOleInPlaceSite::RequestUIActivate() {
   return S_OK;
   }


