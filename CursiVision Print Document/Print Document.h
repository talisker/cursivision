
#pragma once

#pragma warning(disable:4786)
#pragma warning(disable:4275)
#pragma warning(disable:4251)

#include <windows.h>

#include <commdlg.h>
#include <stdio.h>
#include <commctrl.h>

#include "utilities.h"

#include "resource.h"

#include "PDFiumControl_i.h"

#define IDX_FILENAME             0
#define IDX_PRINT_TO             1
#define IDX_SETTINGS_FILENAME    2
#define IDX_COPIES               3
#define IDX_FILENAME_ROOT        4

#define STATUS_BAR_HEIGHT  20

#define WM_PRINT_CLEANUP (WM_USER + 1)

class _IOleInPlaceFrame : public IOleInPlaceFrame {

public:

   _IOleInPlaceFrame(HWND hostWindow) : hwndHost(hostWindow), refCount(0) { AddRef(); };

   STDMETHOD(QueryInterface)(REFIID riid,void **ppv);

private:

   STDMETHOD_ (ULONG, AddRef)();
   STDMETHOD_ (ULONG, Release)();

   STDMETHOD(GetWindow)(HWND *);
   STDMETHOD(ContextSensitiveHelp)(BOOL);
   STDMETHOD(GetBorder)(RECT *);
   STDMETHOD(RequestBorderSpace)(LPCBORDERWIDTHS);
   STDMETHOD(SetBorderSpace)(LPCBORDERWIDTHS);
   STDMETHOD(SetActiveObject)(IOleInPlaceActiveObject *,LPCOLESTR);
   STDMETHOD(InsertMenus)(HMENU,OLEMENUGROUPWIDTHS *);
   STDMETHOD(SetMenu)(HMENU,HOLEMENU,HWND);
   STDMETHOD(RemoveMenus)(HMENU);
   STDMETHOD(SetStatusText)(LPCOLESTR);
   STDMETHOD(EnableModeless)(BOOL);
   STDMETHOD(TranslateAccelerator)(MSG *,WORD);

protected:

   HWND hwndHost;

   long refCount;

};


class _IOleInPlaceSite : public IOleInPlaceSiteEx {

public:

   _IOleInPlaceSite(HWND hostWindow, IOleInPlaceFrame *ipf) :
               hwndHost(hostWindow), pIOleInPlaceFrame(ipf), refCount(0) { ipf -> AddRef(); AddRef(); };

   STDMETHOD(QueryInterface)(REFIID riid,void **ppv);

private:

   STDMETHOD_ (ULONG, AddRef)();
   STDMETHOD_ (ULONG, Release)();

   STDMETHOD(GetWindow)(HWND*);
   STDMETHOD(ContextSensitiveHelp)(BOOL);
   STDMETHOD(CanInPlaceActivate)();
   STDMETHOD(OnInPlaceActivate)();
   STDMETHOD(OnUIActivate)();
   STDMETHOD(GetWindowContext)(IOleInPlaceFrame**,IOleInPlaceUIWindow**,RECT* position,RECT* clip,OLEINPLACEFRAMEINFO*);
   STDMETHOD(Scroll)(SIZE);
   STDMETHOD(OnUIDeactivate)(BOOL);
   STDMETHOD(OnInPlaceDeactivate)();
   STDMETHOD(DiscardUndoState)();
   STDMETHOD(DeactivateAndUndo)();
   STDMETHOD(OnPosRectChange)(const RECT*);
   STDMETHOD(OnInPlaceActivateEx)(BOOL *,DWORD);
   STDMETHOD(OnInPlaceDeactivateEx)(BOOL);
   STDMETHOD(RequestUIActivate)();

protected:

   HWND hwndHost;
   IOleInPlaceFrame *pIOleInPlaceFrame;

   long refCount;

};


class _IOleClientSite : public IOleClientSite {

public:

   _IOleClientSite(IOleInPlaceSite *pis,IOleInPlaceFrame *pif) : pIOleInPlaceSite(pis),pIOleInPlaceFrame(pif), refCount(0) {
          pif -> AddRef(); pis -> AddRef(); AddRef(); };
   ~_IOleClientSite() {};

   //   IUnknown

   STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
   STDMETHOD_ (ULONG, AddRef)();
   STDMETHOD_ (ULONG, Release)();

   long _stdcall SaveObject();
   long _stdcall GetMoniker(DWORD,DWORD,IMoniker**);
   long _stdcall GetContainer(IOleContainer**);
   long _stdcall ShowObject();
   long _stdcall OnShowWindow(BOOL);
   long _stdcall RequestNewObjectLayout();

   IOleInPlaceSite *pIOleInPlaceSite;
   IOleInPlaceFrame *pIOleInPlaceFrame;

   long refCount;

};

#ifdef DEFINE_DATA

   HWND hwndMainFrame,hwndDialog,hwndStatusBar,hwndCancel;
   HINSTANCE hInstance;

   char szApplicationDataDirectory[MAX_PATH];
   char szProgramDirectory[MAX_PATH];
   char szApplicationName[MAX_PATH];
   char szArguments[5][MAX_PATH];
   char szDefaultPrinter[MAX_PATH];

   IPDFiumControl *pIPDFiumControl = NULL;

   _IOleInPlaceFrame *pIOleInPlaceFrame_Reader;
   _IOleInPlaceSite *pIOleInPlaceSite_Reader;
   _IOleClientSite *pIOleClientSite_Reader;
   IOleObject *pIOleObject_PDFiumControl;
   IOleInPlaceActiveObject *pIOleInPlaceActiveObject_Reader;
   IOleInPlaceObject *pIOleInPlaceObject_Reader;

   BYTE savedPrinterInfo[32768];

#else

   extern HWND hwndMainFrame,hwndDialog,hwndStatusBar,hwndCancel;
   extern HINSTANCE hInstance;

   extern RECT rcMainFrame;

   extern char szApplicationDataDirectory[];
   extern char szProgramDirectory[];
   extern char szApplicationName[];
   extern char szArguments[5][MAX_PATH];
   extern char szDefaultPrinter[];

   extern IPDFiumControl *pIPDFiumControl;

   extern _IOleInPlaceFrame *pIOleInPlaceFrame_Reader;
   extern _IOleInPlaceSite *pIOleInPlaceSite_Reader;
   extern _IOleClientSite *pIOleClientSite_Reader;
   extern IOleObject *pIOleObject_PDFiumControl;
   extern IOleInPlaceActiveObject *pIOleInPlaceActiveObject_Reader;
   extern IOleInPlaceObject *pIOleInPlaceObject_Reader;

   BYTE savedPrinterInfo[];

#endif
