
#define DEFINE_DATA

#include <olectl.h>
#include <shlwapi.h>
#include <shlobj.h>

#include "Print Document.h"

#include "PDFiumControl_i.c"

   OLECHAR wstrModuleName[256];

   int initWindows(HINSTANCE);

   LRESULT CALLBACK handler(HWND,UINT,WPARAM,LPARAM);
   LRESULT CALLBACK dialogHandler(HWND,UINT,WPARAM,LPARAM);

   RECT rcMainFrame = {16,16,384,194};

   int __stdcall WinMain(HINSTANCE hInst,HINSTANCE hInstancePrevious,LPSTR lpCmdLine,int nCmdShow) {

   char *pCommandLine = GetCommandLine();
   char **argv;
   int argc = 0;

   long n = (long)strlen(pCommandLine) + 1;
   BSTR bstrCommandLine = SysAllocStringLen(NULL,n);
   MultiByteToWideChar(CP_ACP,0,pCommandLine,-1,bstrCommandLine,n);
   BSTR *pArgs = CommandLineToArgvW(bstrCommandLine,&argc);
   argv = new char *[argc];
   for ( long k = 0; k < argc; k++ ) {
      argv[k] = new char[wcslen(pArgs[k]) + 1];
      memset(argv[k],0,(wcslen(pArgs[k]) + 1) * sizeof(char));
      WideCharToMultiByte(CP_ACP,0,pArgs[k],-1,argv[k],(long)wcslen(pArgs[k]),0,0);
   }

   GetModuleFileName(NULL,szApplicationName,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,szApplicationName,-1,wstrModuleName,MAX_PATH);

   strcpy(szProgramDirectory,szApplicationName);

   char *p = strrchr(szProgramDirectory,'\\');
   if ( ! p )
      p = strrchr(szProgramDirectory,'/');
   if ( p )
      *p = '\0';

   CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

   char szTemp[MAX_PATH];

   GetCommonAppDataLocation(NULL,szTemp);

   sprintf(szApplicationDataDirectory,"%s\\CursiVision",szTemp);

   CreateDirectory(szApplicationDataDirectory,NULL);

   hInstance = hInst;

   INITCOMMONCONTROLSEX icex;
   icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
   icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES | ICC_TAB_CLASSES | ICC_LISTVIEW_CLASSES | ICC_PROGRESS_CLASS;
   InitCommonControlsEx(&icex);

   long argumentMatchCount = 0;

   memset(szArguments,0,sizeof(szArguments));

   long argUsed[32];
   memset(argUsed,0,32 * sizeof(long));

   char switches[][16] = {"/file ","/printto ","/settings","/copies","/defaultprinter",""};
   char switchesAlternate[][16] = {"-file ","-printto ","-settings","-copies","-defaultprinter",""};

   for ( long argIndex = 0; argIndex < argc; argIndex++ ) {

      if ( argUsed[argIndex] )
         continue;

      char *p;

      char *pLowerCase = new char[strlen(argv[argIndex]) + 2];

      memset(pLowerCase,0,(strlen(argv[argIndex]) + 2)* sizeof(char));

      strcpy(pLowerCase,argv[argIndex]);

      p = pLowerCase;

      while ( *p ) {
         *p = tolower(*p);
         p++;
      }
   
      *p = ' ';

      for ( long k = 0; 1; k++ ) {

         if ( ! switches[k][0] )
            break;

         if ( ( p = strstr(pLowerCase,switches[k]) ) || ( p = strstr(pLowerCase,switchesAlternate[k]) ) ) {
   
            p += strlen(switches[k]);
   
            while ( *p && ' ' == *p )
               p++;
   
            if ( ! *p ) {

               if ( argIndex < argc - 1 ) {
                  p = argv[argIndex + 1];
                  argUsed[argIndex + 1] = 1;
               }

            }

            if ( 0 == strcmp("/defaultprinter",switches[k]) ) 

               sprintf(szArguments[IDX_PRINT_TO],"<default>");

            else {

               char *pStart = p;
  
               if ( '\"' == *pStart ) {
      
                  pStart++;
                  if ( '\"' == *pStart )
                     pStart++;
                  char *pEnd = pStart;
                  while ( *pEnd && '\"' != *pEnd )
                     pEnd++;
                  if ( ! *pEnd ) {
                     MessageBox(NULL,"There is an unterminated \" in the passed specification.","Error!",MB_OK | MB_ICONEXCLAMATION);
                     exit(0);
                  }
      
                  *pEnd = '\0';
  
               }
      
               strcpy(szArguments[k],pStart);
  
            }

            argumentMatchCount++;

         }

      }

   }

   if ( ! szArguments[IDX_FILENAME][0] || ! szArguments[IDX_PRINT_TO][0] ) {

      if ( ! szArguments[IDX_FILENAME][0] ) {

         OPENFILENAME openFileName;
         char szFilter[MAX_PATH],szFile[MAX_PATH];
         char szUserDirectory[MAX_PATH];

         GetDocumentsLocation(NULL,szUserDirectory);

         memset(szFilter,0,sizeof(szFilter));
         memset(szFile,0,sizeof(szFile));
         memset(&openFileName,0,sizeof(OPENFILENAME));

         sprintf(szFilter,"Acrobat Documents");
         long k = (long)strlen(szFilter) + sprintf(szFilter + (long)strlen(szFilter) + 1,"*.pdf");
         k = k + sprintf(szFilter + k + 2,"All Files");
         sprintf(szFilter + k + 3,"*.*");

         openFileName.lStructSize = sizeof(OPENFILENAME);
         openFileName.hwndOwner = NULL;
         openFileName.Flags = OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_PATHMUSTEXIST;
         openFileName.lpstrFilter = szFilter;
         openFileName.lpstrFile = szFile;
         openFileName.lpstrDefExt = "xls";
         openFileName.nFilterIndex = 1;
         openFileName.nMaxFile = MAX_PATH;
         openFileName.lpstrTitle = "Select the existing pdf file";
         openFileName.lpstrInitialDir = szUserDirectory;

         if ( ! GetOpenFileName(&openFileName) ) {
            char szTemp[1024];
            sprintf(szTemp,"PrintDocument Invalid arguments. Use:\n\n\tPrintDocument /File \"<inputFile>\" /PrintTo \"<printerName>\"");
            MessageBox(NULL,szTemp,"PrintDocument Error!",MB_OK | MB_ICONEXCLAMATION);
            exit(0);
         }

         strcpy(szArguments[IDX_FILENAME],openFileName.lpstrFile);

      }

      if ( ! szArguments[IDX_PRINT_TO][0] ) {

         PRINTDLG printDialog = {0};

         printDialog.lStructSize = sizeof(PRINTDLG);
         printDialog.hwndOwner = NULL;
         printDialog.Flags = PD_PRINTSETUP | PD_USELARGETEMPLATE;
         
         if ( ! PrintDlg(&printDialog) ) {
            char szTemp[1024];
            sprintf(szTemp,"PrintDocument Invalid arguments. Use:\n\n\tPrintDocument /File \"<inputFile>\" /PrintTo \"<printerName>\"");
            MessageBox(NULL,szTemp,"PrintDocument Error!",MB_OK | MB_ICONEXCLAMATION);
            exit(0);
         }

         long devModeSize = (long)GlobalSize(printDialog.hDevNames);

         DEVNAMES *pDevNames = (DEVNAMES *)GlobalLock(printDialog.hDevNames);

         BYTE *pDevMode = (BYTE *)((BYTE *)pDevNames + pDevNames -> wDeviceOffset);

         strcpy(szArguments[IDX_PRINT_TO],(char *)pDevMode);

      }

   }

   FILE *fx = fopen(szArguments[IDX_FILENAME],"rb");

   if ( ! fx ) {
      char szTemp[1024];
      sprintf(szTemp,"The file \"%s\"\n\ncannot be found.\n\nPlease specify the name of an existing PDF File",szArguments[IDX_FILENAME]);
      MessageBox(NULL,szTemp,"PrintDocument Error!",MB_OK | MB_ICONEXCLAMATION);
      exit(0);
   }

   fclose(fx);

   p = strrchr(szArguments[IDX_FILENAME],'\\');
   if ( ! p )
      p = strrchr(szArguments[IDX_FILENAME],'/');
   if ( ! p )
      p = &szArguments[IDX_FILENAME][0] - 1;

   strcpy(szArguments[IDX_FILENAME_ROOT],p + 1);

   if ( szArguments[IDX_SETTINGS_FILENAME][0] ) {

      fx = fopen(szArguments[IDX_SETTINGS_FILENAME],"rb");

      if ( ! fx ) {
         char szTemp[1024];
         sprintf(szTemp,"The file \"%s\"\n\ncannot be found.\n\nPlease specify the name of the printer settings file",szArguments[IDX_SETTINGS_FILENAME]);
         MessageBox(NULL,szTemp,"PrintDocument Error!",MB_OK | MB_ICONEXCLAMATION);
         exit(0);
      } 

      fclose(fx);      

   }

   if ( ! szArguments[IDX_COPIES][0] )
      sprintf(szArguments[IDX_COPIES],"1");

   initWindows(hInstance);

   MSG qmessage;

   PostMessage(hwndMainFrame,WM_PRINT_ONLY,0L,0L);

   while ( GetMessage(&qmessage,(HWND)NULL,0L,0L) )
      DispatchMessage(&qmessage);

   DestroyWindow(hwndMainFrame);

//exit(0);

   CoFreeUnusedLibraries();

   CoUninitialize();

   Sleep(1000);

#if 0
   for ( long k = 0; 1; k++ ) {

      if ( -1L == oldDoWarn[k] )
         break;

      HKEY hKeySoftware,hKeyAdobe,hKeyReader;

      RegOpenKeyEx(HKEY_CURRENT_USER,"Software",0,KEY_QUERY_VALUE,&hKeySoftware);
      RegOpenKeyEx(hKeySoftware,"Adobe",0,KEY_READ,&hKeyAdobe);
      RegCloseKey(hKeySoftware);
      RegOpenKeyEx(hKeyAdobe,"Acrobat Reader",0,KEY_READ,&hKeyReader);
      RegCloseKey(hKeyAdobe);

      HKEY hKeyVersion;
      RegOpenKeyEx(hKeyReader,szAcrobatVersion[k],0,KEY_READ,&hKeyVersion);
      RegCloseKey(hKeyReader);

      if ( -2L == oldDoWarn[k] ) {
         long rc = SHDeleteKey(hKeyVersion,"AVAlert");
         RegCloseKey(hKeyVersion);
         continue;
      }

      HKEY hKey;
      RegOpenKeyEx(hKeyVersion,"AVAlert",0,KEY_READ,&hKey);
      RegOpenKeyEx(hKey,"cCheckbox",0,KEY_READ,&hKey);
      RegOpenKeyEx(hKey,"cAcrobat",0,KEY_READ,&hKey);

      DWORD cb = sizeof(DWORD);
      RegSetValueEx(hKey,"iWarnScriptPrintAll",0L,REG_DWORD,(BYTE *)&oldDoWarn[k],cb);
      RegCloseKey(hKey);

   }
#endif

   return (int)qmessage.wParam;
   }


   int initWindows(HINSTANCE hInstance) {

   WNDCLASS gClass;
   
   memset(&gClass,0,sizeof(WNDCLASS));
   gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
   gClass.lpfnWndProc = handler;
   gClass.cbClsExtra = 32;
   gClass.cbWndExtra = 32;
   gClass.hInstance = hInstance;
   gClass.hIcon = NULL;
   gClass.hCursor = NULL;
   gClass.hbrBackground = 0;
   gClass.lpszMenuName = NULL;
   gClass.lpszClassName = "PrintDocument";
  
   RegisterClass(&gClass);

   gClass.lpszClassName = "PrintDocumentDialog";
   gClass.lpfnWndProc = handler;

   RegisterClass(&gClass);

   hwndMainFrame = CreateWindowEx(WS_EX_TOOLWINDOW,"PrintDocument","CursiVision",WS_VISIBLE,rcMainFrame.left,rcMainFrame.top,rcMainFrame.right - rcMainFrame.left,rcMainFrame.bottom - rcMainFrame.top,NULL,NULL,NULL,NULL);

   hwndDialog = CreateWindowEx(0L,"PrintDocumentDialog","",0L,-100,-100,100,100,hwndMainFrame,NULL,NULL,NULL);

   hwndCancel = CreateWindowEx(0L,"BUTTON","Cancel",WS_CHILD | WS_VISIBLE,(rcMainFrame.right - rcMainFrame.left)/2 - 24,(rcMainFrame.bottom - rcMainFrame.top)/3,48,32,hwndMainFrame,(HMENU)IDCANCEL,NULL,NULL);

   hwndStatusBar = CreateWindowEx(0L,STATUSCLASSNAME,"",WS_CHILD | WS_VISIBLE | SBT_TOOLTIPS,0,0,0,0,hwndMainFrame,NULL,NULL,NULL);

   HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

   SendMessage(hwndCancel,WM_SETFONT,(WPARAM)hGUIFont,(LPARAM)TRUE);

   char szTemp[MAX_PATH];
   sprintf(szTemp,"CursiVision is printing: %s",szArguments[IDX_FILENAME_ROOT]);
   SetWindowText(hwndMainFrame,szTemp);

   return 1;
   }
