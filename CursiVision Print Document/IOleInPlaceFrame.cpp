
#include "Print Document.h"

   long __stdcall _IOleInPlaceFrame::QueryInterface(REFIID riid,void **ppv) {

   if ( IID_IUnknown == riid ) 
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( IID_IOleInPlaceFrame == riid ) 
      *ppv = static_cast<IOleInPlaceFrame*>(this);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }
   unsigned long __stdcall _IOleInPlaceFrame::AddRef() {
   return ++refCount;
   }
   unsigned long __stdcall _IOleInPlaceFrame::Release() {
   return --refCount;
   }


   HRESULT _IOleInPlaceFrame::GetWindow(HWND *gwh) {
   *gwh = hwndHost;
   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::ContextSensitiveHelp(BOOL) {
   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::GetBorder(RECT *) {
   return INPLACE_E_NOTOOLSPACE;
   }


   HRESULT _IOleInPlaceFrame::RequestBorderSpace(LPCBORDERWIDTHS) {
   return INPLACE_E_NOTOOLSPACE;
   }


   HRESULT _IOleInPlaceFrame::SetBorderSpace(LPCBORDERWIDTHS pBorderWidths) {
   return S_OK ;
   }


   HRESULT _IOleInPlaceFrame::SetActiveObject(IOleInPlaceActiveObject *pObj,LPCOLESTR) {

   if ( pIOleInPlaceActiveObject_Reader )
      pIOleInPlaceActiveObject_Reader -> Release();

   pIOleInPlaceActiveObject_Reader = NULL;

   pIOleInPlaceActiveObject_Reader = pObj;

   if ( pObj )
      pIOleInPlaceActiveObject_Reader -> AddRef();

   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::InsertMenus(HMENU,LPOLEMENUGROUPWIDTHS) {
   return E_UNEXPECTED;
   }


   HRESULT _IOleInPlaceFrame::RemoveMenus(HMENU) {
   return E_UNEXPECTED;
   }


   HRESULT _IOleInPlaceFrame::SetMenu(HMENU,HOLEMENU,HWND) {
   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::SetStatusText(LPCOLESTR) {
   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::EnableModeless(BOOL) {
   return S_OK;
   }


   HRESULT _IOleInPlaceFrame::TranslateAccelerator(MSG *,WORD) {
   return S_FALSE;
   }