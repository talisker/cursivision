
#include "Print Document.h"

#include <process.h>

   unsigned int __stdcall startPrinter(void *);
   unsigned int __stdcall monitorPrinter(void *);

   static long printedCopies = 0L;
   static BYTE *bSettings = NULL;
   static long monitorThreadWaiting = 1L;

   LRESULT CALLBACK handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_SYSCOMMAND: {
      if ( wParam == SC_CLOSE ) {
         PostQuitMessage(0L);
         return (LRESULT)0;
      }
      }
      break;

   case WM_PRINT_ONLY:
      startPrinter(NULL);
      break;

   case WM_PRINT_CLEANUP: {

      if ( pIOleObject_PDFiumControl ) {

         pIOleObject_PDFiumControl -> DoVerb(OLEIVERB_HIDE,NULL,NULL,0,NULL,NULL);

         pIOleObject_PDFiumControl -> Close(OLECLOSE_NOSAVE);

         pIOleObject_PDFiumControl -> SetClientSite(NULL);

         pIPDFiumControl -> Cleanup();

         long k = pIOleObject_PDFiumControl -> Release();

         if ( pIOleInPlaceActiveObject_Reader )
            k = pIOleInPlaceActiveObject_Reader -> Release();

         if ( pIOleInPlaceObject_Reader )
            k = pIOleInPlaceObject_Reader -> Release();

         if ( bSettings )
            delete [] bSettings;

      }

      PostQuitMessage(0);

      }
      break;

   case WM_PAINT: {

      PAINTSTRUCT psPaint;
      RECT rcText;
      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
      BeginPaint(hwnd,&psPaint);
      GetClientRect(hwnd,&rcText);
      long cx = rcText.right - rcText.left;
      long cy = rcText.bottom - rcText.top;
      FillRect(psPaint.hdc,&rcText,(HBRUSH)(COLOR_WINDOW + 1));
      rcText.top = rcText.top - 12 + cy/3;
      HGDIOBJ oldFont = SelectObject(psPaint.hdc,hGUIFont);
      char szText[128];
      sprintf(szText," The document is being printed (copy %ld of %s)",printedCopies + 1,szArguments[IDX_COPIES]);
      DrawText(psPaint.hdc,szText,-1,&rcText,DT_CENTER | DT_VCENTER);
      SelectObject(psPaint.hdc,oldFont);
      EndPaint(hwnd,&psPaint);
      SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)0,(LPARAM)szArguments[IDX_FILENAME_ROOT]);
      return (LRESULT)TRUE;
      }
      break;

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDCANCEL:
         PostMessage(hwnd,WM_PRINT_CLEANUP,0L,0L);
         break;

      }
      }
      break;

   case WM_SIZE: {
      long cx = LOWORD(lParam);
      long cy = HIWORD(lParam);
      SetWindowPos(hwndCancel,HWND_TOP,cx/2 - 16,cy/3 + 16,0,0,SWP_NOSIZE);
      SetWindowPos(hwndStatusBar,HWND_TOP,0,HIWORD(lParam) - STATUS_BAR_HEIGHT,LOWORD(lParam),STATUS_BAR_HEIGHT,0L);
      }
      break;

   case WM_DESTROY:
      break;

   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }


   unsigned int __stdcall startPrinter(void *) {

   CoCreateInstance(CLSID_PDFiumControl,NULL,CLSCTX_ALL,IID_IPDFiumControl,reinterpret_cast<void **>(&pIPDFiumControl));

   pIOleInPlaceFrame_Reader = new _IOleInPlaceFrame(hwndDialog);
   pIOleInPlaceSite_Reader = new _IOleInPlaceSite(hwndDialog,pIOleInPlaceFrame_Reader);
   pIOleClientSite_Reader = new _IOleClientSite(pIOleInPlaceSite_Reader,pIOleInPlaceFrame_Reader);

   pIPDFiumControl -> QueryInterface(IID_IOleObject,reinterpret_cast<void **>(&pIOleObject_PDFiumControl));

   pIPDFiumControl -> QueryInterface(IID_IOleInPlaceObject,reinterpret_cast<void **>(&pIOleInPlaceObject_Reader));

   pIOleObject_PDFiumControl -> SetClientSite(pIOleClientSite_Reader);

   DWORD dwSize = MAX_PATH;

   GetDefaultPrinter(szDefaultPrinter,&dwSize);

   BOOL rc = TRUE;

   if ( strcmp(szArguments[IDX_PRINT_TO],"<default>") )
      rc = SetDefaultPrinter(szArguments[IDX_PRINT_TO]);

   if ( ! rc ) {
      long k = GetLastError();
      k = k;
   }

   if ( szArguments[IDX_SETTINGS_FILENAME][0] ) {

      FILE *fX = fopen(szArguments[IDX_SETTINGS_FILENAME],"rb");
      fseek(fX,0,SEEK_END);
      long n = ftell(fX);

      bSettings = new BYTE[n + 1];
      memset(bSettings,0,(n + 1) * sizeof(BYTE));
      fseek(fX,0,SEEK_SET);
      fread(bSettings,n,1,fX);
      fclose(fX);      

      DEVMODE *pDevMode = (DEVMODE *)bSettings;

      HANDLE hPrinter;

      OpenPrinter((char *)pDevMode -> dmDeviceName,&hPrinter,NULL);

      DWORD cb = 32768;

      long rc = GetPrinter(hPrinter,2,savedPrinterInfo,0,&cb);

      cb = min(cb,32768);

      memset(savedPrinterInfo,0,32768 * sizeof(BYTE));

      rc = GetPrinter(hPrinter,2,savedPrinterInfo,cb,&cb);

      BYTE *newPrinterInfo = new BYTE[cb + 1];

      memset(newPrinterInfo,0,(cb + 1) * sizeof(BYTE));

      memcpy(newPrinterInfo,savedPrinterInfo,cb);

      PRINTER_INFO_2 *pPrinterInfo = (PRINTER_INFO_2 *)newPrinterInfo;

      pPrinterInfo -> pDevMode = (DEVMODE *)pDevMode;

      rc = SetPrinter(hPrinter,2,(BYTE *)pPrinterInfo,0);

      delete [] newPrinterInfo;

   } else 

      savedPrinterInfo[0] = '\0';

   BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);

   memset(bstrFileName,0,(MAX_PATH - 8) * sizeof(OLECHAR));

   MultiByteToWideChar(CP_ACP,0,szArguments[IDX_FILENAME],-1,bstrFileName,MAX_PATH);

   //pIOleObject_PDFiumControl -> DoVerb(OLEIVERB_INPLACEACTIVATE,NULL,pIOleClientSite_Reader,0,hwndDialog,&rcMainFrame);

   monitorThreadWaiting = 1L;

   unsigned int threadAddr;

   HANDLE hNotification = (HANDLE)_beginthreadex(NULL,4096,monitorPrinter,(void *)NULL,CREATE_SUSPENDED,&threadAddr);

   ResumeThread(hNotification);

   while ( 0 == monitorThreadWaiting )
      Sleep(100);

   pIPDFiumControl -> PrintDocument(bstrFileName,FALSE);

   return 0;
   }


   unsigned int __stdcall monitorPrinter(void *) {

   HANDLE hPrinter = NULL;

   if ( strcmp(szArguments[IDX_PRINT_TO],"<default>") )
      OpenPrinter(szArguments[IDX_PRINT_TO],&hPrinter,NULL);
   else
      OpenPrinter(szDefaultPrinter,&hPrinter,NULL);

   HANDLE firstPrinterChangeNotification = FindFirstPrinterChangeNotification(hPrinter,PRINTER_CHANGE_ADD_JOB,0L,NULL);

   monitorThreadWaiting = 1L;

   while ( WAIT_TIMEOUT == WaitForSingleObject(firstPrinterChangeNotification,1000) ) 
      Sleep(100);

   FindNextPrinterChangeNotification(firstPrinterChangeNotification,NULL,NULL,NULL);

   FindClosePrinterChangeNotification(firstPrinterChangeNotification);

   firstPrinterChangeNotification = FindFirstPrinterChangeNotification(hPrinter,PRINTER_CHANGE_DELETE_JOB,0L,NULL);

   while ( WAIT_TIMEOUT == WaitForSingleObject(firstPrinterChangeNotification,1000) ) 
      Sleep(100);

   FindClosePrinterChangeNotification(firstPrinterChangeNotification);

   ClosePrinter(hPrinter);

   printedCopies++;

   if ( atol(szArguments[IDX_COPIES]) > printedCopies ) {
      PostMessage(hwndMainFrame,WM_PRINT_ONLY,0L,0L);
      InvalidateRect(hwndMainFrame,NULL,TRUE);
      return 0;
   }

   if ( savedPrinterInfo[0] ) 
      long rc = SetPrinter(hPrinter,2,savedPrinterInfo,0);

   ShowWindow(hwndMainFrame,SW_HIDE);

   if ( strcmp(szArguments[IDX_PRINT_TO],"<default>") )
      SetDefaultPrinter(szDefaultPrinter);

Sleep(1000);
   PostMessage(hwndMainFrame,WM_PRINT_CLEANUP,0L,0L);

   return 0;
   }
