
#include "Print Document.h"

   long __stdcall _IOleClientSite::QueryInterface(REFIID riid,void **ppv) {

   if ( IID_IUnknown == riid ) 
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( IID_IOleClientSite == riid ) 
      *ppv = static_cast<IOleClientSite*>(this);
   else

   if ( IID_IOleInPlaceSite == riid ) 
      return pIOleInPlaceSite -> QueryInterface(riid,ppv);
   else

   if ( IID_IOleInPlaceSiteEx == riid ) 
      return pIOleInPlaceSite -> QueryInterface(riid,ppv);
   else

   if ( IID_IOleInPlaceFrame == riid ) 
      return pIOleInPlaceFrame -> QueryInterface(riid,ppv);
   else

      return E_NOINTERFACE;

   AddRef();

   return S_OK;
   }

   unsigned long __stdcall _IOleClientSite::AddRef() {
   return ++refCount;
   }

   unsigned long __stdcall _IOleClientSite::Release() {
   return --refCount;
   }


   HRESULT _IOleClientSite::SaveObject() {
   return S_OK;
   }

   
   HRESULT _IOleClientSite::GetMoniker(DWORD,DWORD,IMoniker**) {
   return S_OK;
   }


   HRESULT _IOleClientSite::GetContainer(IOleContainer **) {
   return S_OK;
   }


   HRESULT _IOleClientSite::ShowObject() {
   return S_OK;
   }


   HRESULT _IOleClientSite::OnShowWindow(BOOL) {
   return S_OK;
   }


   HRESULT _IOleClientSite::RequestNewObjectLayout() {
   return S_OK;
   }