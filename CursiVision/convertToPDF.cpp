// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

void convertToPDF(char *pszInputFileName) {

   char szInstallationDirectory[1024];
   char szDocument[1024];

   strcpy(szInstallationDirectory,szApplicationName);

   char *p = strrchr(szInstallationDirectory,'\\');

   if ( ! p )
      p = strrchr(szInstallationDirectory,'/');
   
   *p = '\0';

   sprintf(szInstallationDirectory + strlen(szInstallationDirectory),"\\convertToPDF.exe");

   sprintf(szDocument,"\"%s\"",pszInputFileName);

   _spawnl(_P_WAIT,szInstallationDirectory,szDocument,NULL);

   return;
   }
