// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   static resultDisposition *processDispositionForContinuation = NULL;
   long pageNumberForContinuation = -1L;

   LRESULT CALLBACK wmUserHandler(UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_SET_ACTIVE:
#ifdef _DEBUG
#else
      if ( pCursiVision -> isPrintJob ) {
         SetWindowPos(hwndMainFrame,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
         if ( pCursiVision -> pIPrintingSupport ) {
            HWND hwndProfileProperties;
            pCursiVision -> pIPrintingSupport -> GetPropertiesWindow(&hwndProfileProperties);
            if ( hwndProfileProperties )
               SetWindowPos(hwndProfileProperties,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
         }
      }
#endif
      SetActiveWindow(hwndMainFrame);
      break;

   case WM_FORCE_SHUTDOWN: 
Sleep(100);
      _exit(0);
      return (LRESULT)FALSE;

   case WM_DOCUMENT_SET_URL: {

      pCursiVision -> pauseTracking();

      pCursiVision -> pIPDFiumControl -> CloseDocument();

      BSTR bstrURL = SysAllocStringLen(NULL,1024);

      MultiByteToWideChar(CP_ACP,0,(char *)lParam,-1,bstrURL,1024);

      if ( ! pCursiVision -> pIPdfPage ) {
         pCursiVision -> pIPDFiumControl -> DisplayDocument(0,0,0,bstrURL,0);
         SysFreeString(bstrURL);
         break;
      }

      LOGBRUSH logBrush;

      GetObject(GetSysColorBrush(COLOR_WINDOW + 1),sizeof(LOGBRUSH),&logBrush);

#if 1
      HDC hdc = GetDC(hwndMainFrame);
      UINT xPixelsPerInch = GetDeviceCaps(hdc,LOGPIXELSX);
      UINT yPixelsPerInch = GetDeviceCaps(hdc,LOGPIXELSY);
      ReleaseDC(hwndMainFrame,hdc);
#else
      UINT xPixelsPerInch = 0;
      UINT yPixelsPerInch = 0;

      GetDpiForMonitor(MonitorFromWindow(hwndMainFrame,MONITOR_DEFAULTTONEAREST),MDT_EFFECTIVE_DPI,&xPixelsPerInch,&yPixelsPerInch);
#endif

      RECT rcPage;
      pCursiVision -> pIPdfPage -> PageSize(&rcPage);

#if 0

      long cx = (long)((double)(rcPage.right - rcPage.left) * (double)xPixelsPerInch / 72.0);
      long cy = (long)((double)(rcPage.bottom - rcPage.top ) * (double)yPixelsPerInch / 72.0);

#else
      double aspectRatio = (double)(rcPage.right - rcPage.left) / (double)(rcPage.bottom - rcPage.top);

      GetWindowRect(hwndHTMLHost,&rcPage);

      long cx = rcPage.right - rcPage.left - 64;
      long cy = (long)((double)cx / aspectRatio);

      if ( cy > ( rcPage.bottom - rcPage.top ) ) {
         cy = rcPage.bottom - rcPage.left - 64;
         cx = (long)((double)cy * aspectRatio);
      }

#endif

      pCursiVision -> pIPDFiumControl -> DisplayDocument(logBrush.lbColor,cx,cy,bstrURL,(long)wParam);

      SysFreeString(bstrURL);
      }
      break;
   
   case WM_DOCUMENT_IS_OPENED: {
      BOOL isPDF = FALSE;
      pCursiVision -> pIPDFiumControl -> get_IsPDF(&isPDF);
      if ( ! isPDF ) {
         pCursiVision -> pauseTracking();
         break;
      }
      pCursiVision -> resumeTracking();
      pCursiVision -> notifyDocumentIsTracked();
      char szTemp[64];
      GetWindowText(hwndMainFrame,szTemp,64);
      if ( NULL == strstr(szTemp,"Profile:") )
         SetWindowText(hwndMainFrame,pCursiVision -> szActiveDocumentRootName);
      pCursiVision -> isDocumentOpenInProgress = false;
      }
      break;

   case WM_DOCUMENT_IS_TRACKED: {

      if ( pCursiVision -> pSignatureProcess ) {
         EnterCriticalSection(&pCursiVision -> trackPageNumberAccess);
         pCursiVision -> lastPadToPDFScale = pCursiVision -> padToPDFScale;
         LeaveCriticalSection(&pCursiVision -> trackPageNumberAccess);
         SetTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE_PART2,START_DOODLE_PART_2_DELAY,0L);
         break;
      }

      TBBUTTONINFO buttonInfo = {0};

      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;

      if ( pCursiVision -> pDoodleOptions -> SignedLocationCount() ) 
         buttonInfo.fsState = TBSTATE_ENABLED;
      else 
         buttonInfo.fsState = 0;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_FORGET,(LPARAM)&buttonInfo);

      buttonInfo.fsState = TBSTATE_ENABLED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)IDC_SAVE_AS,(LPARAM)&buttonInfo);
      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_OPTIONS,(LPARAM)&buttonInfo);

      buttonInfo.fsState = pCursiVision -> isWritingEnabled ? TBSTATE_ENABLED : 0;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

      buttonInfo.fsState = 0;
      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

      pCursiVision -> isDocumentOpen = true;
      pCursiVision -> isDocumentOpenInProgress = false;

      }
      break;

   case WM_DOCUMENT_TRACK_CYCLE_FINISHED:
      PostMessage(hwndMainFrame,WM_DOCUMENT_IS_TRACKED,0L,0L);
      break;
	  
   case WM_POTENTIAL_QUIT: {

      if ( ! pCursiVision -> documentIsOpen() || ! pCursiVision -> documentIsModified() || pCursiVision -> handledByBackEnd ) {
         if ( pCursiVision -> pSignatureProcess )
            delete pCursiVision -> pSignatureProcess;
         pCursiVision -> pSignatureProcess = NULL;
         PostQuitMessage(0);
         break;
      }

      long optionNumber = pCursiVision -> handleUnsavedDocument(true);

      if ( IDDI_OPTIONS_OPTION_1 == optionNumber ) {
         pCursiVision -> quitAfterSaving = true;
         PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_SAVE_AS,0),0L);
         return (LRESULT)0;
      }

      if ( IDDI_OPTIONS_OPTION_2 == optionNumber ) {
         PostQuitMessage(0);
         return (LRESULT)0;
      }

      }
      break;

   case WM_POST_PAINT: 
      pCursiVision -> expectingPostPaint = false;
      break;

   case WM_REGISTER_PAD:
      pCursiVision -> registerSignaturePad((char *)wParam);
      break;

   case WM_DISPLAY_SIGNATURE_BOX: {
      signatureBox *pSignatureBox = (signatureBox *)lParam;
      if ( pSignatureBox )
         pSignatureBox -> display();
      break;
      }

   case WM_REFRESH_SIGNATURE_BOX: {

      if ( hdcPDFSource )
         DeleteDC(hdcPDFSource);

      HDC hdcSource = GetDC(hwndHTMLHost);

      hdcPDFSource = CreateCompatibleDC(hdcSource);

      if ( hBitmapPDFPage )
         DeleteObject(hBitmapPDFPage);

      RECT rcPDFPage;

      GetWindowRect(hwndHTMLHost,&rcPDFPage);

      hBitmapPDFPage = CreateBitmap(rcPDFPage.right - rcPDFPage.left,rcPDFPage.bottom - rcPDFPage.top,1,GetDeviceCaps(hdcPDFSource,BITSPIXEL),NULL);

      SelectObject(hdcPDFSource,hBitmapPDFPage);

      BitBlt(hdcPDFSource,0,0,cxHTMLHost,cyHTMLHost,hdcSource,0,0,SRCCOPY);

      ReleaseDC(hwndHTMLHost,hdcSource);

      pCursiVision -> pSignatureProcess -> SignatureBox() -> ResendPadImage();

      InvalidateRect(pCursiVision -> pSignatureProcess -> SignatureBox() -> GetHWND(),NULL,TRUE);

      RedrawWindow(pCursiVision -> pSignatureProcess -> SignatureBox() -> GetHWND(),NULL,NULL,RDW_UPDATENOW);

      }
      break;

   case WM_FIRE_DOCUMENT_OPENED: {

      TBBUTTONINFO buttonInfo = {0};
      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;
      buttonInfo.fsState = TBSTATE_ENABLED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);      

      break;
      }

   case WM_PRINT_ONLY: 
      SetTimer(hwndMainFrame,TIMER_EVENT_PRINT_ONLY,FIND_PAGE_TIMER_DURATION,NULL);
      break;

   case WM_START_ADHOC:
      if ( ! pCursiVision -> pSignatureProcess )
         break;
      pCursiVision -> pSignatureProcess -> Start();
      break;

   case WM_IMMEDIATE_DOODLE:
      pCursiVision -> isImmediateDoodle = true;

      // Intentional Fall-thru

   case WM_OPEN_PDF_FILE:
      memset(pCursiVision -> szLastActiveDocument,0,sizeof(pCursiVision -> szLastActiveDocument));
      pCursiVision -> openDocument((char *)wParam,1,true);
      if ( pCursiVision -> isImmediateDoodle )
         SetTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE,DOODLE_START_DELAY,NULL);
      break;

   case WM_APPLY_SIGNATURE:
      if ( pCursiVision -> pSignatureProcess )
         pCursiVision -> pSignatureProcess -> ApplySignature();
      PostMessage(hwndMainFrame,WM_PLAY_NEXT_PAGE,wParam,0L);
      break;

   case WM_PLAY_NEXT_PAGE: 
      if ( ! pCursiVision -> pSignatureProcess ) 
         break;
      if ( OPTION_ID_CLEAR == wParam ) 
         pCursiVision -> pSignatureProcess -> ReplayCurrentPage();
      else
         pCursiVision -> pSignatureProcess -> PlayNextPage((long)wParam);
      break;

   case WM_FINISH_SIGNATURE_PAGE: {
      if ( ! pCursiVision -> pSignatureProcess )
         break;
      pageNumberForContinuation = (long)lParam;
      processDispositionForContinuation = pCursiVision -> currentProcessDisposition();
      pCursiVision -> FinalizeSignatureProcess(true);
      PostMessage(hwndMainFrame,WM_FINISH_SIGNATURE_PAGE_2,wParam,lParam);

      }
      break;

  case WM_PREPARE_SIGNATURE_REPLAY: {
      if ( ! pCursiVision -> pSignatureProcess )
         break;
      pageNumberForContinuation = -1L;
      processDispositionForContinuation = pCursiVision -> currentProcessDisposition();
      }
      break;

//NTC: 12-3-2012
//
// The following directives WERE active before today, but I commented them out.
// The Topaz pad was doing the flash thing, and this may have been why (the
// signature process was not getting deleted and rebuilt).
//
//#ifdef DO_PROCESSES
   case WM_FINISH_SIGNATURE_PAGE_2: {
      if ( pCursiVision -> pSignatureProcess )
         delete pCursiVision -> pSignatureProcess;
      pCursiVision -> pSignatureProcess = NULL;
      SetTimer(hwndMainFrame,TIMER_EVENT_CONTINUE_PLAY,100,NULL);
      }
      break;
//#endif

   case WM_FINISH_SIGNATURE_PLAY: {

      bool doFinalize = true;

      if ( hwndSigningControl )
         ShowWindow(hwndSigningControl,SW_HIDE);

      if ( pCursiVision -> currentProcessDisposition() && pCursiVision -> currentProcessDisposition() -> doContinuousDoodle ) {

         doFinalize = false;

         pCursiVision -> doodleCount++;

         writingLocation writingLocation;
         long pageNumber;
         long adobePageNumber;
//
// Combine these two ifs based on pDoodleOptions redirecting to the printing support profile ???
//
         if ( pCursiVision -> PrintingProfile() && ! pCursiVision -> isPrintProfileTraining ) {
            if ( E_FAIL == pCursiVision -> PrintingProfile() -> GetTarget(pCursiVision -> doodleCount,&writingLocation.documentRect,&pageNumber,&adobePageNumber) ) 
               doFinalize = true;
         }

         if ( ! pCursiVision -> isDoodleProfileTraining && ! pCursiVision -> isPrintProfileTraining ) {
            if ( E_FAIL == pCursiVision -> pDoodleOptions -> GetLocation(pCursiVision -> doodleCount,&writingLocation.documentRect,&pageNumber,&adobePageNumber,false) ) // ???? <-- what is the last parameter (doRemember) used in the TabletPC version ?
               doFinalize = true;
         }

         if ( ! doFinalize )
            SetTimer(hwndMainFrame,TIMER_EVENT_REDOODLE,RE_DOODLE_DELAY,0L);

      } 

      if ( doFinalize ) {

         pCursiVision -> FinalizeSignatureProcess(false);

         TBBUTTONINFO buttonInfo = {0};
         buttonInfo.cbSize = sizeof(TBBUTTONINFO);
         buttonInfo.dwMask = TBIF_STATE;
         buttonInfo.fsState = 0;
         SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

         SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

      }

      SetWindowText(hwndStatusBar,"");

      if ( pCursiVision -> currentProcessDisposition() && ! pCursiVision -> hasExitDeferral )
         pCursiVision -> hasExitDeferral = ! pCursiVision -> currentProcessDisposition() -> doExit;

      if ( doFinalize && pCursiVision -> isImmediateDoodle && ! pCursiVision -> hasExitDeferral )
         PostMessage(hwndMainFrame,WM_POTENTIAL_QUIT,0L,0L);

      pCursiVision -> hasExitDeferral = false;

      }

      return (LRESULT)0;

   case WM_START_PRINT_PROCESSING: {
      pCursiVision -> openDocument((char *)wParam);
      pCursiVision -> processPrinting();
      }
      return (LRESULT)0;

   case WM_TRACK_PAGE_NUMBER: {
      pCursiVision -> trackPageNumberUIThread();
      }
      return (LRESULT)0;

   default:
      break;
   }

   return DefWindowProc(hwndMainFrame,msg,wParam,lParam);
   }