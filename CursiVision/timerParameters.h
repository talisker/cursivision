// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define TIMER_EVENT_MIN_ID             1

#define TIMER_EVENT_BRING_TO_TOP       1
#define TIMER_EVENT_START_DOODLE       2
#define TIMER_EVENT_END_SIGNATURE      3
#define TIMER_EVENT_REDOODLE           4
#define TIMER_EVENT_CONTINUE_PLAY      5
#define TIMER_EVENT_PRINT_ONLY         7
#define TIMER_EVENT_START_DOODLE_PART2 8
#define TIMER_EVENT_DISPLAY_WAITING    9

#define TIMER_EVENT_MAX_ID             9

#define FIND_PAGE_TIMER_DURATION       50
#define ESTABLISH_PENHOST_DURATION     100

#define DOODLE_START_DELAY             500

#define RE_DOODLE_DELAY                500

#define START_DOODLE_PART_2_DELAY      50

#define PDF_OPENED_DELAY               400
