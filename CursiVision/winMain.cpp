// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define DEFINE_DATA

#include <olectl.h>
#include <commctrl.h>

#include "CursiVision.h"

#include "htmlhelp.h"

#include "PdfEnabler_i.c"
#include "Properties_i.c"
#include "SignaturePad_i.c"
#include "PrintingSupport_i.c"
#include "CursiVision_i.c"
#include "PDFiumControl_i.c"

   extern "C" int registerServer();
   extern "C" int unRegisterServer();
   extern "C" int isPrinterInstalled(char *szLicense);
   extern "C" int registerAll(char *pszDirectory);
   extern "C" int unRegisterAll(char *pszDirectory);
   extern "C" long installPrinter(char *pszInstallationDirectory);
   extern "C" int GrantFullAccess(char *pszDirectory);
   extern "C" int fixupLinks(char *pszDirectory,char *pszVersionSpec);

   int __stdcall WinMain(HINSTANCE hInst,HINSTANCE hInstancePrevious,LPSTR lpCmdLine,int nCmdShow) {

   hModule = hInst;

   isAdministrator = false;
   SID_IDENTIFIER_AUTHORITY ntAuthority{SECURITY_NT_AUTHORITY};
   PSID administratorsGroup; 

   isAdministrator = AllocateAndInitializeSid(&ntAuthority,2,SECURITY_BUILTIN_DOMAIN_RID,DOMAIN_ALIAS_RID_ADMINS,0, 0, 0, 0, 0, 0,&administratorsGroup) ? true : false; 

   if ( isAdministrator ) {
      BOOL v = TRUE;
      if ( ! CheckTokenMembership(NULL,administratorsGroup,&v) ) 
         isAdministrator = false;
      else
         isAdministrator = v ? true : false;
      FreeSid(administratorsGroup); 
   }

   HKEY hKeySettings = NULL;

   RegOpenKeyEx(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate\\CursiVision",0L,KEY_READ,&hKeySettings);
   
   if ( ! ( NULL == hKeySettings ) ) {

      DWORD cb = MAX_PATH;
      DWORD dwType = REG_SZ;
      RegQueryValueEx(hKeySettings,"Global Settings Store",NULL,&dwType,(BYTE *)&szGlobalDataStore,&cb);

      RegCloseKey(hKeySettings);

   }

   if ( isAdministrator && szGlobalDataStore[0] ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"%s\\Printing Profiles",szGlobalDataStore);
      CreateDirectory(szTemp,NULL);
      sprintf(szTemp,"%s\\Settings",szGlobalDataStore);
      CreateDirectory(szTemp,NULL);
   }

   char *pCommandLine = GetCommandLine();
   char **argv;
   int argc = 0;

   long n = (DWORD)strlen(pCommandLine) + 1;
   BSTR bstrCommandLine = SysAllocStringLen(NULL,n);
   MultiByteToWideChar(CP_ACP,0,pCommandLine,-1,bstrCommandLine,n);
   BSTR *pArgs = CommandLineToArgvW(bstrCommandLine,&argc);
   argv = new char *[argc];

   for ( long k = 0; k < argc; k++ ) {
      argv[k] = new char[wcslen(pArgs[k]) + 1];
      memset(argv[k],0,(wcslen(pArgs[k]) + 1) * sizeof(char));
      WideCharToMultiByte(CP_ACP,0,pArgs[k],-1,argv[k],(DWORD)wcslen(pArgs[k]),0,0);
   }

   memset(szApplicationDataDirectory,0,sizeof(szApplicationDataDirectory));

   GetModuleFileName(NULL,szApplicationName,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szApplicationName,-1,wstrModuleName,MAX_PATH);

   strcpy(szProgramDirectory,szApplicationName);

   char *p = strrchr(szProgramDirectory,'\\');
   if ( ! p )
      p = strrchr(szProgramDirectory,'/');
   if ( p )
      *p = '\0';

   CoInitialize(NULL);

   char szTemp[MAX_PATH];

   long argumentMatchCount = 0;

   memset(szArguments,0,sizeof(szArguments));

   long argUsed[32];
   memset(argUsed,0,32 * sizeof(long));

   char switches[][32] = {"/settings ","/open ","/play ","/print ","/file ","/saveas ","/regserver ","/unregserver ",         // 0-7
                              "/forward ","/printto ","/doodle ","/passthru ","/backend ","/viewonly","/regcomponents","/unregcomponents", // 08-15
                              "/installprinter","/grantfullaccess","/fixuplinks",""};   // 16-18
   char switchesAlternate[][32] = {"-settings ","-open ","-play ","-print ","-file ","-saveas ","-regserver ","-unregserver ",
                              "-forward ","-printto ","-doodle ","-passthru ","-backend ","-viewonly","-regcomponents","-unregcomponents",
                              "-installprinter","-grantfullaccess","-fixuplinks",""};

   for ( long argIndex = 0; argIndex < argc; argIndex++ ) {

      if ( argUsed[argIndex] )
         continue;

      char *p;

      char *pLowerCase = new char[strlen(argv[argIndex]) + 2];

      memset(pLowerCase,0,(strlen(argv[argIndex]) + 2)* sizeof(char));

      strcpy(pLowerCase,argv[argIndex]);

      p = pLowerCase;

      while ( *p ) {
         *p = tolower(*p);
         p++;
      }
   
      *p = ' ';

      for ( long k = 0; 1; k++ ) {

         if ( ! switches[k][0] )
            break;

         if ( ( p = strstr(pLowerCase,switches[k]) ) || ( p = strstr(pLowerCase,switchesAlternate[k]) ) ) {
   
            if ( IDX_REGISTER == k ) {
               registerServer();
               CoUninitialize();
               exit(0);
            }

            if ( IDX_UNREGISTER == k ) {
               unRegisterServer();
               CoUninitialize();
               exit(0);
            }

            if ( IDX_INSTALL_PRINTER == k ) {
               long rc = installPrinter(argv[2]);
               exit(rc);
            }

            if ( IDX_GRANT_FULL_ACCESS == k ) {
               long rc = GrantFullAccess(argv[2]);
               exit(rc);
            }

            if ( IDX_FIXUP_LINKS == k ) {
               long rc = fixupLinks(argv[2],argv[3]);
               exit(rc);
            }

            if ( IDX_REGISTER_COMPONENTS == k ) {
               registerAll(szProgramDirectory);
               CoUninitialize();
               exit(0);
            }

            if ( IDX_UNREGISTER_COMPONENTS == k ) {
               unRegisterAll(szProgramDirectory);
               CoUninitialize();
               exit(0);
            }

            if ( IDX_DOODLE == k ) {
               strcpy(szArguments[IDX_DOODLE],"Yes");
               p += strlen(switches[k]);
               break;
            }

            if ( IDX_VIEW_ONLY == k ) {
               strcpy(szArguments[IDX_VIEW_ONLY],"Yes");
               p += strlen(switches[k]);
               break;
            }

            p += strlen(switches[k]);
   
            while ( *p && ' ' == *p )
               p++;
   
            if ( ! *p ) {

               if ( argIndex < argc - 1 ) {
                  p = argv[argIndex + 1];
                  argUsed[argIndex + 1] = 1;
               }

            }

            char *pStart = p;

            if ( '\"' == *pStart ) {
   
               pStart++;
               if ( '\"' == *pStart )
                  pStart++;
               char *pEnd = pStart;
               while ( *pEnd && '\"' != *pEnd )
                  pEnd++;
               if ( ! *pEnd ) {
                  MessageBox(NULL,"There is an unterminated \" in the passed arguments.","Error!",MB_OK | MB_ICONEXCLAMATION);
                  exit(0);
               }
   
               *pEnd = '\0';

            }
   
            strcpy(szArguments[k],pStart);

            argumentMatchCount++;

         }

      }

   }

   pCursiVision = new CursiVision();

   if ( ! pCursiVision )
      exit(0);

   pCursiVision -> essentialInitialization();

   if ( szArguments[IDX_SETTINGS][0] ) {
      char *p = strchr(szArguments[IDX_SETTINGS],'\\');
      if ( ! p ) {
         char szTemp[MAX_PATH];
         sprintf(szTemp,"%s\\%s",szUserDirectory,szArguments[IDX_SETTINGS]);
         pCursiVision -> SetPropertiesFile(szTemp);
      } else
         pCursiVision -> SetPropertiesFile(szArguments[IDX_SETTINGS]);
   }

   if ( szArguments[IDX_USE_BACK_END_NAME][0] ) {

      char szBackEndKeep[MAX_PATH];

      sprintf(szBackEndKeep,szArguments[IDX_USE_BACK_END_NAME]);

      ICatInformation *pICatInformation = NULL;

      HRESULT rc = CoCreateInstance(CLSID_StdComponentCategoriesMgr,NULL,CLSCTX_ALL,IID_ICatInformation,reinterpret_cast<void **>(&pICatInformation));

      if ( S_OK != rc || ! pICatInformation ) {
         MessageBox(NULL,"Unable to create the StdComponentCategoriesMgr object","Error",MB_OK);
         exit(0);
      }

      IEnumCLSID *pIEnumCLSID = NULL;

      CATID catIds[1];

      catIds[0] = IID_ICursiVisionBackEnd;

      pICatInformation -> EnumClassesOfCategories(1,catIds,0,NULL,&pIEnumCLSID);

      CATEGORYINFO catInfo;

      memset(&catInfo,0,sizeof(CATEGORYINFO));

      unsigned long countFound;
      long k = 0;
      CLSID clsid;
      long countRows = 0;
      bool found = false;

      while ( S_OK == pIEnumCLSID -> Next(1,&clsid,&countFound) ) {

         BSTR bstrDescription;

         StringFromCLSID(clsid,&bstrDescription);

         char szBackEnd[MAX_PATH];

         WideCharToMultiByte(CP_ACP,0,bstrDescription,-1,szBackEnd,MAX_PATH,0,0);

         if ( 0 == _stricmp(szBackEnd,szArguments[IDX_USE_BACK_END_NAME]) ) {
            strcpy(szArguments[IDX_USE_BACK_END_NAME],szBackEnd);
            found = true;
            break;
         }

         HKEY hKey;
         DWORD dwType = REG_SZ;
         DWORD cb = MAX_PATH;
         char szKey[MAX_PATH];

         sprintf(szKey,"CLSID\\%s",szBackEnd);

         RegOpenKeyEx(HKEY_CLASSES_ROOT,szKey,0,KEY_READ,&hKey);

         RegQueryValueEx(hKey,NULL,0L,&dwType,(BYTE *)szBackEnd,&cb);

         RegCloseKey(hKey);

         if ( 0 == cb ) 
            continue;

         if ( 0 == _stricmp(szBackEnd,szArguments[IDX_USE_BACK_END_NAME]) ) {
            WideCharToMultiByte(CP_ACP,0,bstrDescription,-1,szArguments[IDX_USE_BACK_END_NAME],MAX_PATH,0,0);
            found = true;
            break;
         }

      }

      if ( ! found ) {
         char szMessage[1024];
         sprintf(szMessage,"The processing backend specified by:\n\n\t%s\n\nWas not found or is not registered on this machine.\n\nPlease make sure the back end processor is installed.",szArguments[IDX_USE_BACK_END_NAME]);
         MessageBox(NULL,szMessage,"Error!",MB_ICONEXCLAMATION);
         exit(0);
      }

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      CLSID clsId;
      OLECHAR oleString[MAX_PATH];
      MultiByteToWideChar(CP_ACP,0,szArguments[IDX_USE_BACK_END_NAME],-1,oleString,MAX_PATH);

      CLSIDFromString(oleString,&clsId);

      rc = CoCreateInstance(clsId,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( S_OK != rc ) {
         char szMessage[1024];
         sprintf(szMessage,"The CursiVision Back End processor \n\n\t%s\n\nIs not registered correctly or is non-functional",szBackEndKeep);
         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);
         exit(0);        
      }

      pICursiVisionBackEnd -> Release();

   }

   if ( szArguments[IDX_FORWARD_TO_SERVER][0] )
      pCursiVision -> SetForwardServer(szArguments[IDX_FORWARD_TO_SERVER]);

   if ( szArguments[IDX_PRINT][0] ) {

      FILE *fx = fopen(szArguments[IDX_PRINT],"rb");
      if ( ! fx ) {
         char szTemp[1024];
         sprintf(szTemp,"The file %s cannot be found.\n\nThis is an error condition.\n\nPlease try re-installing the print driver.",szArguments[IDX_PRINT]);
         MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
         exit(0);
      }
      fclose(fx);

      pCursiVision -> isPrintJob = true;

   }

   if ( 2 == argc && 0 == argumentMatchCount ) 
      strcpy(szArguments[IDX_FILENAME],argv[1]);

   if ( pCursiVision -> isPrintJob )
      strcpy(szArguments[IDX_FILENAME],szArguments[IDX_PRINT]);

   if ( ! pCursiVision -> initialize(false,'\0' == szArguments[IDX_FILENAME][0] && '\0' == szArguments[IDX_PRINT][0]) )
      exit(0);

   if ( '\0' == szArguments[IDX_FILENAME][0] ) {
      if ( pCursiVision -> processingDisposition.openLastDocument && '\0' != pCursiVision -> szActiveDocument[0] ) {
         FILE *fX = fopen(pCursiVision -> szActiveDocument,"rb");
         if ( fX ) {
            fclose(fX);
            pCursiVision -> openDocument(pCursiVision -> szActiveDocument,1,true,false,false,false);
         } else {
            char szMessage[1024];
            sprintf(szMessage,"The last opened document:\n\n\t%s\n\nDoes not exist.",pCursiVision -> szActiveDocument);
            MessageBox(hwndMainFrame,szMessage,"Error",MB_OK | MB_TOPMOST | MB_ICONEXCLAMATION);
            pCursiVision -> szActiveDocument[0] = '\0';
         }
      }
   }

   if ( 2 == argc && 0 == argumentMatchCount ) {

      char *p = argv[1] + strlen(argv[1]) - 1;
      bool isPDF = false;
      bool isPS = false;
      if ( p && p > argv[1] ) {
         if ( *p == '\"' ) {
            if ( 0 == _stricmp(p - 4,".pdf\"") )
               isPDF = true;
            if ( 0 == _stricmp(p - 3,".ps\"") )
               isPS = true;
         } else {
            if ( 0 == _stricmp(p - 3,".pdf") )
               isPDF = true;
            if ( 0 == _stricmp(p - 2,".ps") )
               isPS = true;
         }
      }

      if ( isPDF ) {

         if ( 1 == deviceCount && '\0' == szArguments[IDX_VIEW_ONLY][0] ) {
            pCursiVision -> isImmediateDoodle = true;
            PostMessage(hwndMainFrame,WM_IMMEDIATE_DOODLE,(WPARAM)argv[1],0L);
         } else
            PostMessage(hwndMainFrame,WM_OPEN_PDF_FILE,(WPARAM)argv[1],0L);

      } else {

         if ( isPS ) {

            strcpy(szArguments[IDX_FILENAME],argv[1]);

            PostMessage(hwndMainFrame,WM_OPEN_PDF_FILE,(WPARAM)argv[1],0L);

         } else {

            sprintf(szTemp,"CursiVision does not recognize the file type of %s. CursiVision opens PDF files by default",argv[1]);

            MessageBox(NULL,szTemp,"Note!",MB_OK | MB_ICONEXCLAMATION);
            exit(0);

         }

      }

   }

   if ( szArguments[IDX_DOODLE][0] ) {

      if ( ! szArguments[IDX_FILENAME][0] ) {
         char szTemp[1024];
         sprintf(szTemp,"Doodle was specified but no file name was provided.");
         MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
         exit(0);
      }

      pCursiVision -> isImmediateDoodle = true;

      PostMessage(hwndMainFrame,WM_IMMEDIATE_DOODLE,(WPARAM)szArguments[IDX_FILENAME],0L);

   }

   if ( szArguments[IDX_PRINT][0] ) {

      ShowWindow(hwndMainFrame,SW_SHOW);

      pCursiVision -> isPrintJob = true;

      PostMessage(hwndMainFrame,WM_START_PRINT_PROCESSING,(WPARAM)szArguments[IDX_PRINT],0L);

   } else if ( szArguments[IDX_FILENAME][0] && ! szArguments[IDX_DOODLE][0] ) {

      FILE *fx = fopen(szArguments[IDX_FILENAME],"rb");
      if ( ! fx ) {
         char szTemp[1024];
         sprintf(szTemp,"The file \"%s\"\n\ncannot be found.\n\nPlease specify the name of an existing PDF File",szArguments[IDX_FILENAME]);
         MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
         exit(0);
      }
      fclose(fx);

      if ( 2 != argc || argumentMatchCount ) 
         pCursiVision -> openDocument(szArguments[IDX_FILENAME],1,true);

      if ( ! szArguments[IDX_PRINT_TO][0] )
         ShowWindow(hwndMainFrame,SW_SHOW);
   
   } else {

      if ( ! szArguments[IDX_PRINT_TO][0] )
         ShowWindow(hwndMainFrame,SW_SHOW);

      if ( pCursiVision -> processingDisposition.showStartupProperties && ! pCursiVision -> isImmediateDoodle ) 
         PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(ID_SETUP,0L),0L);

   }

   if ( ! szArguments[IDX_PRINT_TO][0] ) {
      pCursiVision -> resized();
      PostMessage(hwndMainFrame,WM_SET_ACTIVE,0L,0L);
   } else
      PostMessage(hwndMainFrame,WM_PRINT_ONLY,0L,0L);

   pCursiVision -> AddRef();

   if ( ! pCursiVision -> isPrintJob )
      pCursiVision -> createPrintingSupportThreaded();

   MSG qMessage;

   while ( GetMessage(&qMessage,(HWND)NULL,0L,0L) ) { 

      if ( ! IsDialogMessage(hwndHTMLHost,&qMessage) ) {

         if ( hwndSigningControl ) {
            if ( WM_KEYUP == qMessage.message && VK_ESCAPE == qMessage.wParam ) {
               SendMessage(hwndMainFrame,qMessage.message,qMessage.wParam,qMessage.lParam);
               continue;
            }
         }

         if ( pCursiVision -> expectingPostPaint ) {
            DWORD countPaintMessages = GetQueueStatus(QS_PAINT);
            if ( 0 == HIWORD(countPaintMessages) && 0 == (QS_PAINT & LOWORD(countPaintMessages)) ) 
               PostMessage(hwndMainFrame,WM_POST_PAINT,0L,0L);
         }

         TranslateMessage(&qMessage);
         DispatchMessage(&qMessage);

      }

   }

   pCursiVision -> quitTracking();

   signatureBox::ReleaseSignaturePad();

   if ( hwndDisplayWaiting )
      DestroyWindow(hwndDisplayWaiting);

   DestroyWindow(hwndHTMLHost);

   pCursiVision -> pIGProperties -> Save();

   delete pCursiVision;

   DestroyWindow(hwndMainFrame);

   CoFreeUnusedLibraries();

   CoUninitialize();

   return (int)qMessage.wParam;
   }

   extern "C" int __cdecl DoHelp(HWND hwnd,char *szHelpFile) {
   HWND hwndHelp = HtmlHelpA(hwnd,szHelpFile,HH_DISPLAY_TOPIC,NULL);
   return 0;
   }


