// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"


   void CursiVision::processPrinting() {

   if ( ! pIPrintingSupport ) 
      createPrintingSupport();

   boolean createGlobalPrintingProfile = false;

   char szTemp[MAX_PATH];

   sprintf(szTemp,"%s\\createGlobalPrintingProfile",szApplicationDataDirectory);

   FILE *fX = fopen(szTemp,"rb");

   if ( fX ) {
      createGlobalPrintingProfile = true;
      fclose(fX);
      DeleteFile(szTemp);
   }

   if ( E_FAIL == pIPrintingSupport -> TakeDocumentInfo(szActiveDocument,reinterpret_cast<void *>(&processingDisposition),reinterpret_cast<void *>(pIPdfDocument),createGlobalPrintingProfile) )
      return;

   if ( PrintingProfile() && ! PrintingProfile() -> DoSignatureCapture() ) {
      saveDocument();
      return;
   }

   isImmediateDoodle = true;

   SetTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE,500,NULL);

   return;
   }

