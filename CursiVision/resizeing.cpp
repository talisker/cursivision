// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <math.h>

   void CursiVision::resized() {

   if ( expectingPostPaint )
      return;

   if ( IsIconic(hwndMainFrame) )
      return;

   if ( ! pIPdfPage || ! pIOleInPlaceObject_HTML ) 
      return;

   if ( ( ! documentIsOpen() || !  documentIsModified() ) && 0 == doodleCount ) 
      SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)1,(LPARAM)&"not modified");
   else
      SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)1,(LPARAM)&"modified");

   RECT rcPage;
   pCursiVision -> pIPdfPage -> PageSize(&rcPage);

   double aspectRatio = (double)(rcPage.right - rcPage.left) / (double)(rcPage.bottom - rcPage.top);

   GetWindowRect(hwndHTMLHost,&rcPage);

   long cx = rcPage.right - rcPage.left - 64;
   long cy = (long)((double)cx / aspectRatio);

   if ( cy > ( rcPage.bottom - rcPage.top ) ) {
      cy = rcPage.bottom - rcPage.left - 64;
      cx = (long)((double)cy * aspectRatio);
   }

   pCursiVision -> pIPDFiumControl -> ResizePDFDocument(cx,cy);

   return;
   }
