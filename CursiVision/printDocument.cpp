// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include "PrintingBackEnd_i.h"
#include "PrintingBackEnd_i.c"

   long CursiVision::printDocument() {

   IPrintingBackEndAdditional *pIPrintingBackEndAdditional = NULL;

   long rc = CoCreateInstance(CLSID_CursiVisionPrintingBackEndAdditional,NULL,CLSCTX_INPROC_SERVER,
                                 IID_IPrintingBackEndAdditional,reinterpret_cast<void **>(&pIPrintingBackEndAdditional));

   pIPrintingBackEndAdditional -> TakeMainWindow(hwndMainFrame);
   pIPrintingBackEndAdditional -> PrintDocument(szActiveDocument);

   pIPrintingBackEndAdditional -> Release();

   return 0L;
   }

