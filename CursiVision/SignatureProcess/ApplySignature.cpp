// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#define DRAW_PAD_OUTLINE

   bool SignatureProcess::ApplySignature() {

   RECT sourceRect;

   pSignatureBox -> GetRect(&sourceRect);

   pSignatureBox -> Hide();

   memcpy(&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect,&sourceRect,sizeof(RECT));

   pCursiVision -> toDocumentCoordinates(&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect);

   pCursiVision -> pDoodleOptions -> ReplaceLocation(pCursiVision -> DoodleCount(),&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect,pCursiVision -> pdfPageNumber,pCursiVision -> pdfAdobePageNumber);

   long totalPoints = pCursiVision -> RealTimeSignatureGraphic() -> totalPoints;

   if ( ! totalPoints ) {
      long needToImplementAbilityToStoreAndEraseSignatureByIndex = -1L;
      pCursiVision -> updateDocument(needToImplementAbilityToStoreAndEraseSignatureByIndex,true);
      return false;
   }

   signatureGraphic *pGraphic = new signatureGraphic();

   pGraphic -> pdfPageNumber = pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].zzpdfPageNumber;

   long maxPointIndex = totalPoints;

#ifdef DRAW_PAD_OUTLINE
      totalPoints += 6;
#endif

   pGraphic -> totalPoints = totalPoints;

   pGraphic -> pSignatureDataX = new long[totalPoints];
   pGraphic -> pSignatureDataY = new long[totalPoints];
   pGraphic -> pSignatureDataPage = new long[totalPoints];
   pGraphic -> pInkWeight = new float[totalPoints];

   long *padDataX = pCursiVision -> RealTimeSignatureGraphic() -> pSignatureDataX;
   long *padDataY = pCursiVision -> RealTimeSignatureGraphic() -> pSignatureDataY; 
   long *padPage = pCursiVision -> RealTimeSignatureGraphic() -> pSignatureDataPage;

   pGraphic -> offsetX = -pCursiVision -> pdfDocumentUpperLeft.x;
   pGraphic -> offsetY = -pCursiVision -> pdfDocumentUpperLeft.y;

   double scaleX = 1.0;
   double scaleY = 1.0;

   if ( pCursiVision -> SignaturePad() -> HasScaling() ) {
      scaleX = pCursiVision -> SignaturePad() -> PadToLCDScaleX();
      scaleY = pCursiVision -> SignaturePad() -> PadToLCDScaleY();
   }

   pGraphic -> windowsWidth = sourceRect.right - sourceRect.left;
   pGraphic -> windowsHeight = sourceRect.bottom - sourceRect.top;

   pGraphic -> interWindowScaleX = (double)pGraphic -> windowsWidth / (double)pCursiVision -> SignaturePad() -> LCDWidth();
   pGraphic -> interWindowScaleY = (double)pGraphic -> windowsHeight / (double)pCursiVision -> SignaturePad() -> LCDHeight();

   scaleX *= pGraphic -> interWindowScaleX;
   scaleY *= pGraphic -> interWindowScaleY;

   pGraphic -> scaleX = scaleX;
   pGraphic -> scaleY = scaleY;

   pGraphic -> windowsOrigin.x = sourceRect.left;
   pGraphic -> windowsOrigin.y = sourceRect.top;

   for ( long k = 0; k < maxPointIndex; k++ ) {
      pGraphic -> pSignatureDataPage[k] = padPage[k];
      if ( (0 == padDataX[k] && 0 == padDataY[k]) ) {
         pGraphic -> pSignatureDataX[k] = 0;
         pGraphic -> pSignatureDataY[k] = 0;
         continue;
      }
      pGraphic -> pSignatureDataX[k] = (long)((double)(padDataX[k]) * scaleX + (double)pGraphic -> windowsOrigin.x);
      pGraphic -> pSignatureDataY[k] = (long)((double)(padDataY[k]) * scaleY + (double)pGraphic -> windowsOrigin.y);
   }

   memcpy(pGraphic -> pInkWeight,pCursiVision -> RealTimeSignatureGraphic() -> pInkWeight,totalPoints * sizeof(float));

#ifdef DRAW_PAD_OUTLINE
   pGraphic -> pSignatureDataX[maxPointIndex] = 0;
   pGraphic -> pSignatureDataY[maxPointIndex] = 0;
   pGraphic -> pSignatureDataX[maxPointIndex + 1] = sourceRect.left;
   pGraphic -> pSignatureDataY[maxPointIndex + 1] = sourceRect.top;
   pGraphic -> pSignatureDataX[maxPointIndex + 2] = sourceRect.right;
   pGraphic -> pSignatureDataY[maxPointIndex + 2] = sourceRect.top;
   pGraphic -> pSignatureDataX[maxPointIndex + 3] = sourceRect.right;
   pGraphic -> pSignatureDataY[maxPointIndex + 3] = sourceRect.bottom;
   pGraphic -> pSignatureDataX[maxPointIndex + 4] = sourceRect.left;
   pGraphic -> pSignatureDataY[maxPointIndex + 4] = sourceRect.bottom;
   pGraphic -> pSignatureDataX[maxPointIndex + 5] = sourceRect.left;
   pGraphic -> pSignatureDataY[maxPointIndex + 5] = sourceRect.top;
   pGraphic -> pSignatureDataPage[maxPointIndex] = padPage[0];
   pGraphic -> pSignatureDataPage[maxPointIndex + 1] = padPage[0];
   pGraphic -> pSignatureDataPage[maxPointIndex + 2] = padPage[0];
   pGraphic -> pSignatureDataPage[maxPointIndex + 3] = padPage[0];
   pGraphic -> pSignatureDataPage[maxPointIndex + 4] = padPage[0];
   pGraphic -> pSignatureDataPage[maxPointIndex + 5] = padPage[0];
#endif

   EnterCriticalSection(&pCursiVision -> graphicListAccess);
   (*pCursiVision -> SignatureGraphics())[(DWORD)(*pCursiVision -> SignatureGraphics()).size() + 1] = pGraphic;
   LeaveCriticalSection(&pCursiVision -> graphicListAccess);

   pCursiVision -> SignaturePad() -> DeleteSignatureData();

   long needToImplementAbilityToStoreAndEraseSignatureByIndex = -1L;

   pCursiVision -> updateDocument(needToImplementAbilityToStoreAndEraseSignatureByIndex,true);

   return true;
   }

