
#include "InsertPDF.h"

#ifdef DO_PROCESSES

   LRESULT CALLBACK SignatureProcess::frameHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   SignatureProcess *p = reinterpret_cast<SignatureProcess *>(GetWindowLong(hwnd,GWL_USERDATA));

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      p = reinterpret_cast<SignatureProcess *>(pc -> lpCreateParams);
      SetWindowLong(hwnd,GWL_USERDATA,reinterpret_cast<long>(p));
      }
      break;

   case WM_PAINT: {
      PAINTSTRUCT ps;
      BeginPaint(hwnd,&ps);
      FillRect(ps.hdc,&ps.rcPaint,(HBRUSH)(COLOR_BTNFACE + 1));
      EndPaint(hwnd,&ps);
      }
      break;

   case WM_BRING_TO_TOP: {
      HWND hwndTarget = (HWND)wParam;
      if ( p && ! hwndTarget ) {
         SetWindowPos(p -> hwndFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
         RedrawWindow(p -> hwndFrame,NULL,NULL,RDW_UPDATENOW | RDW_INVALIDATE | RDW_ALLCHILDREN);
      } else if ( p ) {
         SetWindowPos(hwndTarget,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
         RedrawWindow(hwndTarget,NULL,NULL,RDW_UPDATENOW | RDW_INVALIDATE | RDW_ALLCHILDREN);
      }
      }
      break;

   case WM_MOVE:
      InvalidateRect(hwndPDFDocument,NULL,TRUE);
      UpdateWindow(hwndPDFDocument);
      break;

   case WM_SIZE: {

      if ( p -> playMode || p -> adHocMode ) {
         ShowWindow(p -> hwndFrame,SW_HIDE);
         break;
      }

      GetWindowRect(hwnd,&p -> rectFrame);

      if ( ! pInsertPDF -> pISignaturePad )
         return (LRESULT)0L;

      RECT rectPad;

      pInsertPDF -> pISignaturePad -> GetRect(&rectPad);

      rectPad.right = rectPad.left + (long)((double)(rectPad.right - rectPad.left));
      rectPad.bottom = rectPad.top + (long)((double)(rectPad.bottom - rectPad.top));

      long cx = p -> rectFrame.right - p -> rectFrame.left;
      long cy = p -> rectFrame.bottom - p -> rectFrame.top;

      RECT rectTabControl;

      rectTabControl.left = 8;
      rectTabControl.top = 8;

      rectTabControl.right = rectTabControl.left + max(rectPad.right - rectPad.left,MIN_TAB_CONTROL_WIDTH) + 48;
      rectTabControl.bottom = rectTabControl.top + max(rectPad.bottom - rectPad.top,MIN_TAB_CONTROL_HEIGHT) + 48;

      SetWindowPos(p -> hwndTabControl,HWND_TOP,rectTabControl.left,rectTabControl.top,
                              rectTabControl.right - rectTabControl.left,rectTabControl.bottom - rectTabControl.top + TAB_CONTROL_BUFFER_BOTTOM,0L);

      SetWindowPos(p -> hwndSelectRegion,HWND_TOP,
                                 rectTabControl.left + 8,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectRegionInstructions,HWND_TOP,
                                 rectTabControl.left + 168,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectPieces,HWND_TOP,
                                 rectTabControl.left + 96,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectPieceTargets,HWND_TOP,
                                 rectTabControl.left + 8 + 64 + 16 + 8 + 64 + 16 + 8,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectTarget,HWND_TOP,
                                 rectTabControl.left + 8,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectFont,HWND_TOP,
                                 rectTabControl.left + 8 + 64 + 16 + 8,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndSelectTargetInstructions,HWND_TOP,
                                 rectTabControl.left + 8 + 64 + 16 + 8 + 48 + 8,
                                 rectTabControl.bottom + TAB_CONTROL_BUFFER_BOTTOM - BUTTON_HEIGHT - 8,0,0,SWP_NOSIZE);

      rectTabControl.bottom += TAB_CONTROL_BUFFER_BOTTOM;

      SetWindowPos(p -> hwndPadControlHost,HWND_TOP,
                     (rectTabControl.right + rectTabControl.left)/2 - (rectPad.right - rectPad.left)/2 - rectTabControl.left,
                     (rectTabControl.bottom + rectTabControl.top)/2 - (rectPad.bottom - rectPad.top)/2 - rectTabControl.top,
                     rectPad.right - rectPad.left,rectPad.bottom - rectPad.top,0L);

      p -> resizePageControls(&p -> thePages[p -> currentPageNumber]);

      SetWindowPos(p -> hwndCancel,HWND_TOP,8,cy - BOTTOM_BAND,0,0,SWP_NOSIZE);

      SetWindowPos(p -> hwndInstructionText2,HWND_TOP,0,0,cx - 32 - 8,cy - 2 * BOTTOM_BAND - 8,SWP_NOMOVE);

      SetWindowPos(p -> hwndNext,HWND_TOP,cx - 8 - 64 - 12,cy - BOTTOM_BAND,0,0,SWP_NOSIZE);
      SetWindowPos(p -> hwndFinished,HWND_TOP,cx - 8 - 64 - 8 - 64 - 12,cy - BOTTOM_BAND,0,0,SWP_NOSIZE);
      SetWindowPos(p -> hwndPrevious,HWND_TOP,cx - 8 - 64 - 8 - 64 - 8 - 64 - 12,cy - BOTTOM_BAND,0,0,SWP_NOSIZE);

      return (LRESULT)0L;
      }
      break;

   case WM_NOTIFY: {

      NMHDR *pNMHeader = reinterpret_cast<NMHDR *>(lParam);

      imagePage *pPage = &p -> thePages[p -> currentPageNumber];

      long action = -1L;

      if ( TCN_SELCHANGING == pNMHeader -> code ) 
         action = SW_HIDE;
      else 
         if ( TCN_SELCHANGE == pNMHeader -> code )
            action = SW_SHOW;

      if ( -1L == action && UDN_DELTAPOS == pNMHeader -> code ) {

         NM_UPDOWN *pUD = reinterpret_cast<NM_UPDOWN *>(lParam);
         char szTemp[16];
         GetWindowText(pPage -> hwndChoiceCount,szTemp,16);
         long currentValue = min(2,max(0,atol(szTemp) + pUD -> iDelta));
         sprintf(szTemp,"%ld",currentValue);
         SetWindowText(pPage -> hwndChoiceCount,szTemp);
         break;

      } else if ( -1L == action )
         break;

      long index = SendMessage(pNMHeader -> hwndFrom,TCM_GETCURSEL,0L,0L);

      switch ( index ) {

      case 0:

         if ( SW_SHOW == action ) {

            ShowWindow(pPage -> hwndText,SW_HIDE);
            ShowWindow(pPage -> hwndCombined,SW_HIDE);
            ShowWindow(pPage -> hwndInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectRegion,SW_SHOW);
            ShowWindow(p -> hwndSelectPieces,! pInsertPDF -> pISignaturePad -> IsLCD() || pInsertPDF -> pISignaturePad -> IsFullPage() ? SW_HIDE : SW_SHOW);
            ShowWindow(p -> hwndSelectPieceTargets,! pInsertPDF -> pISignaturePad -> IsLCD() || pInsertPDF -> pISignaturePad -> IsFullPage() ? SW_HIDE : SW_SHOW);
            ShowWindow(p -> hwndSelectTarget,SW_HIDE);
            ShowWindow(p -> hwndSelectTargetInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectFont,SW_HIDE);

            for ( long j = 0; j < pPage -> innerRectCount; j++ ) {
               pInsertPDF -> toWindowCoordinates(&pPage -> innerRects[j].padRectDocCoordinates,&pPage -> innerRects[j].padRect);
               pInsertPDF -> toWindowCoordinates(&pPage -> innerRects[j].localRectDocCoordinates,&pPage -> innerRects[j].localRect);
            }

            for ( long j = 0; j < pPage -> targetRectCount; j++ ) {
               pInsertPDF -> toWindowCoordinates(&pPage -> targetRects[j].padRectDocCoordinates,&pPage -> targetRects[j].padRect);
               pInsertPDF -> toWindowCoordinates(&pPage -> targetRects[j].localRectDocCoordinates,&pPage -> targetRects[j].localRect);
            }

         } else {

            for ( long j = 0; j < pPage -> innerRectCount; j++ ) {
               pInsertPDF -> toDocumentCoordinates(&pPage -> innerRects[j].padRect,&pPage -> innerRects[j].padRectDocCoordinates);
               pInsertPDF -> toDocumentCoordinates(&pPage -> innerRects[j].localRect,&pPage -> innerRects[j].localRectDocCoordinates);
            }

            for ( long j = 0; j < pPage -> targetRectCount; j++ ) {
               pInsertPDF -> toDocumentCoordinates(&pPage -> targetRects[j].padRect,&pPage -> targetRects[j].padRectDocCoordinates);
               pInsertPDF -> toDocumentCoordinates(&pPage -> targetRects[j].localRect,&pPage -> targetRects[j].localRectDocCoordinates);
            }
         }

         ShowWindow(p -> hwndPadControlHost,action);

         break;

      case 1:

         if ( SW_SHOW == action && ! pInsertPDF -> pISignaturePad -> IsLCD() ) {

            ShowWindow(p -> hwndPadControlHost,SW_HIDE);
            ShowWindow(pPage -> hwndText,SW_HIDE);
            ShowWindow(pPage -> hwndCombined,SW_HIDE);
            ShowWindow(p -> hwndSelectRegion,SW_HIDE);
            ShowWindow(p -> hwndSelectPieces,SW_HIDE);
            ShowWindow(p -> hwndSelectPieceTargets,SW_HIDE);
            ShowWindow(p -> hwndSelectTarget,SW_HIDE);
            ShowWindow(p -> hwndSelectTargetInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectFont,SW_HIDE);

            EDITSTREAM editStream;
            memset(&editStream,0,sizeof(EDITSTREAM));
            editStream.pfnCallback = loadInstructions;
            editStream.dwCookie = IDS_PROCESS_NONLCD_INSTRUCTIONS;
            SendMessage(pPage -> hwndInstructions,WM_SETTEXT,0L,(LPARAM)"");
            SendMessage(pPage -> hwndInstructions,EM_STREAMIN,(WPARAM)(SF_RTF | SFF_SELECTION),(LPARAM)&editStream);
            ShowWindow(pPage -> hwndInstructions,action);

            break;

         }

         if ( SW_SHOW == action ) {

            ShowWindow(p -> hwndPadControlHost,SW_HIDE);
            ShowWindow(pPage -> hwndCombined,SW_HIDE);
            ShowWindow(pPage -> hwndInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectRegion,SW_HIDE);
            ShowWindow(p -> hwndSelectPieces,SW_HIDE);
            ShowWindow(p -> hwndSelectPieceTargets,SW_HIDE);
            ShowWindow(p -> hwndSelectTarget,SW_SHOW);
            EnableWindow(p -> hwndSelectTarget,pPage -> useText ? TRUE : FALSE);
            ShowWindow(p -> hwndSelectTargetInstructions,pPage -> useText ? SW_SHOW : SW_HIDE);
            ShowWindow(p -> hwndSelectFont,SW_SHOW);
            EnableWindow(p -> hwndSelectFont,TRUE);

            LOGFONT logicalFont = {0};
            strcpy(logicalFont.lfFaceName,pPage -> szFontFace);
            HDC hdc = GetDC(pPage -> hwndText);
            logicalFont.lfHeight = -MulDiv(pPage -> fontSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
            ReleaseDC(pPage -> hwndText,hdc);
            logicalFont.lfWeight = pPage -> fontWeight;
            switch ( pPage -> fontType ) {
            case REGULAR_FONTTYPE: 
               break;
            case ITALIC_FONTTYPE:
               logicalFont.lfItalic = 1L;
               break;
            case BOLD_FONTTYPE:
               logicalFont.lfWeight = FW_BOLD;
               break;
            }
            logicalFont.lfQuality = PROOF_QUALITY;
            logicalFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
            HFONT hFont = CreateFontIndirect(&logicalFont);
            SendMessage(pPage -> hwndText,WM_SETFONT,(WPARAM)hFont,MAKELPARAM(TRUE,0));
            InvalidateRect(pPage -> hwndText,NULL,TRUE);
            DeleteObject(hFont);

         }

         ShowWindow(pPage -> hwndText,action);

         break;

      case 2:

         if ( SW_SHOW == action ) {
            ShowWindow(p -> hwndPadControlHost,SW_HIDE);
            ShowWindow(pPage -> hwndText,SW_HIDE);
            ShowWindow(pPage -> hwndInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectRegion,SW_HIDE);
            ShowWindow(p -> hwndSelectPieces,SW_HIDE);
            ShowWindow(p -> hwndSelectPieceTargets,SW_HIDE);
            ShowWindow(p -> hwndSelectTarget,SW_HIDE);
            ShowWindow(p -> hwndSelectTargetInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectFont,SW_HIDE);
         }

         ShowWindow(pPage -> hwndCombined,action);

         break;

      case 3:

         if ( SW_SHOW == action ) {

            ShowWindow(p -> hwndPadControlHost,SW_HIDE);
            ShowWindow(pPage -> hwndText,SW_HIDE);
            ShowWindow(pPage -> hwndCombined,SW_HIDE);
            ShowWindow(p -> hwndSelectRegion,SW_HIDE);
            ShowWindow(p -> hwndSelectPieces,SW_HIDE);
            ShowWindow(p -> hwndSelectPieceTargets,SW_HIDE);
            ShowWindow(p -> hwndSelectTarget,SW_HIDE);
            ShowWindow(p -> hwndSelectTargetInstructions,SW_HIDE);
            ShowWindow(p -> hwndSelectFont,SW_HIDE);

            EDITSTREAM editStream;
            memset(&editStream,0,sizeof(EDITSTREAM));

            editStream.pfnCallback = loadInstructions;

            if ( pPage -> useImage ) 
               editStream.dwCookie = IDS_PROCESS_IMAGE_INSTRUCTIONS;

            else if ( pPage -> useText )
               editStream.dwCookie = IDS_PROCESS_TEXT_INSTRUCTIONS;

            else if ( pPage -> useBoth )
               editStream.dwCookie = IDS_PROCESS_BOTH_INSTRUCTIONS;

            SendMessage(pPage -> hwndInstructions,WM_SETTEXT,0L,(LPARAM)"");
            SendMessage(pPage -> hwndInstructions,EM_STREAMIN,(WPARAM)(SF_RTF | SFF_SELECTION),(LPARAM)&editStream);

         }

         ShowWindow(pPage -> hwndInstructions,action);

         break;

      default:
         break;
      }
      }
      break;

   case WM_COMMAND: {

      if ( REGION_CAPTURE_STAGE_DONE != p -> RegionCaptureStage() )
         p -> RegionCaptureStage(REGION_CAPTURE_STAGE_CANCEL);

      p -> piecesConstructionMode = false;
      p -> regionConstructionMode = false;

      if ( IDCANCEL == LOWORD(wParam) ) {
         if ( p && REGION_CAPTURE_STAGE_DONE != p -> RegionCaptureStage() ) {
            p -> RegionCaptureStage(REGION_CAPTURE_STAGE_DONE);
            return SendMessage(hwndPDFDocument,msg,wParam,lParam);
         }
         break;
      }

      imagePage *pPage = &p -> thePages[p -> currentPageNumber];

      switch ( LOWORD(wParam) ) {

         case ID_SETUP: {
            IUnknown *pIUnknown;
            p -> processingDisposition.isProcessDisposition = true;
            p -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
            p -> pIGProperties -> ShowProperties((long)hwnd,pIUnknown);
            pIUnknown -> Release();
            }
            break;

         case IDC_USE_IMAGE:
         case IDC_USE_TEXT:
         case IDC_USE_BOTH: {
            long controlId = LOWORD(wParam);
            NMHDR nmhdr;
            memset(&nmhdr,0,sizeof(NMHDR));
            nmhdr.hwndFrom = p -> hwndTabControl;
            nmhdr.code = TCN_SELCHANGING;
            SendMessage(p -> hwndFrame,WM_NOTIFY,(WPARAM)0,(LPARAM)&nmhdr);
            SendMessage(p -> hwndTabControl,TCM_SETCURSEL,(WPARAM)IDC_USE_IMAGE == controlId ? 0 : IDC_USE_TEXT == controlId ? 1 : 2,0L);
            nmhdr.code = TCN_SELCHANGE;
            SendMessage(p -> hwndFrame,WM_NOTIFY,(WPARAM)0,(LPARAM)&nmhdr);
            pPage -> useImage = (BST_CHECKED == SendMessage(pPage -> hwndUseImage,BM_GETCHECK,0L,0L));
            pPage -> useText = (BST_CHECKED == SendMessage(pPage -> hwndUseText,BM_GETCHECK,0L,0L));
            pPage -> useBoth = (BST_CHECKED == SendMessage(pPage -> hwndUseBoth,BM_GETCHECK,0L,0L));
            if ( IDC_USE_TEXT == controlId ) {
               ShowWindow(p -> hwndSelectTarget,pPage -> useText ? SW_SHOW : SW_HIDE);
               ShowWindow(p -> hwndSelectFont,pPage -> useText ? SW_SHOW : SW_HIDE);
               ShowWindow(p -> hwndSelectTargetInstructions,pPage -> useText ? SW_SHOW : SW_HIDE);
               EnableWindow(p -> hwndSelectTarget,pPage -> useText ? TRUE : FALSE);
               EnableWindow(p -> hwndSelectFont,pPage -> useText ? TRUE : FALSE);
            }
            }
            break;

         case IDC_FORCE_FIT: 
            pPage -> fitToSize = (BST_CHECKED == SendMessage(pPage -> hwndForceFit,BM_GETCHECK,0L,0L));
            break;

         case IDC_NEXT:
            p -> showNextPage();
            return (LRESULT)0;
      
         case IDC_PREVIOUS:
            p -> showPreviousPage();
            return (LRESULT)0;
      
         case IDC_DELETE:
            p -> deletePage(p -> currentPageNumber);
            UpdateWindow(p -> hwndFrame);
            break;

         case IDC_CANCEL:
            ShowWindow(hwnd,SW_HIDE);
            SendMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_PROCESS_CANCELED,0L),0L);
            break;

         case IDC_PROCESS_FINISHED:
            if ( p -> Save() ) {
               ShowWindow(hwnd,SW_HIDE);
               SendMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_PROCESS_FINISHED,0L),0L);
            }
            break;

         case IDC_PROCESS_NAME:
            GetWindowText(p -> hwndProcessName,p -> szProcessName,MAX_PATH);
            EnableWindow(p -> hwndNext,p -> szProcessName[0] ? ( p -> szSignatureDeviceName[0] ? TRUE : FALSE ): FALSE);
            break;

         case IDC_SELECT_REGION:
            SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
            pPage -> fitToSize = true;
            memset(p -> thePages[p -> currentPageNumber].innerRects,0,sizeof(p -> thePages[p -> currentPageNumber].innerRects));
            memset(p -> thePages[p -> currentPageNumber].targetRects,0,sizeof(p -> thePages[p -> currentPageNumber].targetRects));
            p -> thePages[p -> currentPageNumber].innerRectCount = 0;
            p -> thePages[p -> currentPageNumber].targetRectCount = 0;
            memset(&p -> thePages[p -> currentPageNumber].outerRect,0,sizeof(RECT));
            if ( pInsertPDF -> pISignaturePad -> IsFullPage() ) {
               p -> RegionCaptureStage(REGION_CAPTURE_STAGE_ONE);
               InsertPDF::trackPageNumber(NULL,0,NULL,0);
               p -> OuterRect(NULL);
               p -> RegionCaptureStage(REGION_CAPTURE_STAGE_DONE);
               p -> capturePadImage(p -> currentPageNumber);
               SetWindowPos(p -> hwndFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
               InvalidateRect(p -> hwndPadControlHost,NULL,TRUE);
            } else {
               SetWindowText(hwndStatusBar,"Move the mouse over the document. The outline shows the size of the pad's display. Click when done.");
               p -> regionConstructionMode = true;
               InvalidateRect(p -> hwndPadControlHost,NULL,TRUE);
               RedrawWindow(p -> hwndPadControlHost,NULL,NULL,RDW_UPDATENOW);
               p -> RegionCaptureStage(REGION_CAPTURE_STAGE_ONE);
            }
            break;

         case IDC_SELECT_PIECES:
            SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
            SetWindowText(hwndStatusBar,"Click and drag regions on the document. For each region, move it to where you would like it to appear on the pad.");
            memset(&p -> thePages[p -> currentPageNumber].outerRect,0,sizeof(RECT));
            p -> BeginPiecesCapture();
            break;

         case IDC_SELECT_PIECE_TARGETS:
            SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
            SetWindowText(hwndStatusBar,"Click and drag regions on the document. For each region, move it to where you would like it to appear on the pad.");
            memset(&p -> thePages[p -> currentPageNumber].outerRect,0,sizeof(RECT));
            p -> BeginPieceTargetsCapture();
            break;

         case IDC_SELECT_TARGET:
            p -> isTargetCapture = 1L;
            SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
            SetWindowText(hwndStatusBar,"Move the mouse over the document. The outline shows the place where the user's writing will be placed. Click when done.");
            p -> RegionCaptureStage(REGION_CAPTURE_STAGE_ONE);
            SendMessage(p -> hwndSelectTarget,BM_SETSTATE,(WPARAM)BST_PUSHED,0L);
            break;

         case IDC_SELECT_FONT: {
            imagePage *pPage = &p -> thePages[p -> currentPageNumber];
            CHOOSEFONT chooseFont = {0};
            LOGFONT logicalFont = {0};
            strcpy(logicalFont.lfFaceName,pPage -> szFontFace);
            HDC hdc = GetDC(pPage -> hwndText);
            logicalFont.lfHeight = -MulDiv(pPage -> fontSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
            ReleaseDC(pPage -> hwndText,hdc);
            logicalFont.lfWeight = pPage -> fontWeight;
            switch ( pPage -> fontType ) {
            case REGULAR_FONTTYPE: 
               break;
            case ITALIC_FONTTYPE:
               logicalFont.lfItalic = 1L;
               break;
            case BOLD_FONTTYPE:
               logicalFont.lfWeight = FW_BOLD;
               break;
            }

            chooseFont.lStructSize = sizeof(CHOOSEFONT);
            chooseFont.hwndOwner = p -> hwndFrame;
            chooseFont.Flags = CF_SCREENFONTS | CF_INITTOLOGFONTSTRUCT;
            chooseFont.lpLogFont = &logicalFont;
            chooseFont.nFontType = (WORD)pPage -> fontType;

            if ( ! ChooseFont(&chooseFont) )
               break;

            strcpy(pPage -> szFontFace,chooseFont.lpLogFont -> lfFaceName);
            pPage -> fontSize = chooseFont.iPointSize / 10;
            pPage -> fontWeight = chooseFont.lpLogFont -> lfWeight;
            pPage -> fontType = chooseFont.nFontType;

            HFONT hFont = CreateFontIndirect(chooseFont.lpLogFont);

            SendMessage(pPage -> hwndText,WM_SETFONT,(WPARAM)hFont,(LPARAM)TRUE);

            DeleteObject(hFont);

            }
            break;
            
         case IDC_BRANCH_COUNT: {

            if ( ! p )
               break;

            if ( EN_UPDATE != HIWORD(wParam) )
               break;

            char szTemp[32];
            GetWindowText(pPage -> hwndChoiceCount,szTemp,32);
            if ( ! szTemp[0] )
               break;

            long choiceCount = atol(szTemp);
            if ( 1 != choiceCount && 2 != choiceCount ) {
               MessageBeep(MB_ICONASTERISK);
               if ( 0 == choiceCount )
                  SetWindowText((HWND)lParam,"1");
               if ( 3 == choiceCount )
                  SetWindowText((HWND)lParam,"2");
               return (LRESULT)0;
            }

            EnableWindow(pPage -> hwndChoice[1],2 <= choiceCount);
            EnableWindow(pPage -> hwndChoiceTarget[1],2 <= choiceCount);

            pPage -> choiceCount = choiceCount;

            if ( ! pInsertPDF -> pISignaturePad )
               break;

            RECT rectPad;

            pInsertPDF -> pISignaturePad -> GetRect(&rectPad);

            rectPad.right = rectPad.left + (long)((double)(rectPad.right - rectPad.left));
            rectPad.bottom = rectPad.top + (long)((double)(rectPad.bottom - rectPad.top));

            if ( pPage -> choiceCount > 1 )
               rectPad.bottom += MULTIPLE_HOTSPOT_ADDITIONAL_SPACE;

            SetWindowPos(p -> hwndFrame,HWND_TOP,
                           0,0,
                           max(rectPad.right - rectPad.left,MIN_FRAME_WIDTH) + PAD_RIGHT_BUFFER,
                           max(rectPad.bottom - rectPad.top,MIN_FRAME_HEIGHT) + PAD_BOTTOM_BUFFER,SWP_NOMOVE);

            p -> resizePageControls(pPage);

            }
            break;

         case IDC_CHOICE_1:
         case IDC_CHOICE_2: {
            if ( EN_UPDATE != HIWORD(wParam) )
               break;
            GetWindowText((HWND)lParam,pPage -> szChoice[LOWORD(wParam) - IDC_CHOICE_1],16);
            }
            break;

         case IDC_TARGET_1:
         case IDC_TARGET_2: {
            if ( EN_UPDATE != HIWORD(wParam) )
               break;
            char szTemp[32];
            GetWindowText((HWND)lParam,szTemp,32);
            if ( ! szTemp[0] )
               break;
            pPage -> choiceTarget[LOWORD(wParam) - IDC_TARGET_1] = atol(szTemp);
            }
            break;

         default:
            break;

         }
         break;

      }

      break;

   case WM_DESTROY:
      break;

   case WM_INITMENUPOPUP: {

      if ( (HMENU)wParam != hMenuDeviceList )
         break;

      MENUITEMINFO menuItemInfo;
      long deviceNameSize = 128;
      char szDeviceName[128];
      
      memset(&menuItemInfo,0,sizeof(MENUITEMINFO));
   
      menuItemInfo.cbSize = sizeof(MENUITEMINFO);
      menuItemInfo.fMask = MIIM_STRING;
   
      for ( long j = 0; 1; j++ ) {

         menuItemInfo.cch = deviceNameSize;
         menuItemInfo.dwTypeData = szDeviceName;

         if ( ! GetMenuItemInfo(hMenuDeviceList,j,TRUE,&menuItemInfo) ) 
            break;

         if ( 0 == _stricmp(p -> szSignatureDeviceName,(char *)menuItemInfo.dwTypeData) )
            menuItemInfo.fState = MFS_CHECKED | MFS_DISABLED;
         else
            menuItemInfo.fState = MFS_UNCHECKED | (p -> maxPageNumber && p -> szSignatureDeviceName[0]) ? MFS_DISABLED : 0 ;

         menuItemInfo.fMask = MIIM_STATE;

         SetMenuItemInfo(hMenuDeviceList,j,TRUE,&menuItemInfo);

         menuItemInfo.fMask = MIIM_STRING;

      }
      }
      break;

   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }



   LRESULT CALLBACK SignatureProcess::pageHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   SignatureProcess *p = reinterpret_cast<SignatureProcess *>(GetWindowLong(hwnd,GWL_USERDATA));

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      p = reinterpret_cast<SignatureProcess *>(pc -> lpCreateParams);
      SetWindowLong(hwnd,GWL_USERDATA,reinterpret_cast<long>(p));
      }
      break;

   case WM_COMMAND:
      return frameHandler(hwnd,msg,wParam,lParam);

   case WM_NOTIFY:
      return frameHandler(hwnd,msg,wParam,lParam);

   case WM_PAINT: {
      PAINTSTRUCT ps;
      BeginPaint(hwnd,&ps);
      EndPaint(hwnd,&ps);
      char szTemp[128];
      sprintf(szTemp,"Page %ld of %ld",p -> thePages[p -> currentPageNumber].pageNumber,maxPageNumber);
      SetWindowText(p -> thePages[p -> currentPageNumber].hwndStatus,szTemp);
      return (LRESULT)0;
      }
      break;

   case WM_SIZE: {
      return (LRESULT)0;
      }

   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }

#endif