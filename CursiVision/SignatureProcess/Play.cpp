// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"
#include "signatureBox.h"
#include <process.h>

   static RECT continuationRect = {0,0,0,0};
   static char continuationPageLabel[64];
   static long continuationPageNumber = -1L;

   void SignatureProcess::Start() {
   PlayCurrentPage();
   return;
   }

   void SignatureProcess::PlayCurrentPage() {

   pCursiVision -> lowerMouseVellum();

   writingLocation *pWritingLocation = &pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()];

   pCursiVision -> SetPageNumber(pWritingLocation -> zzpdfPageNumber);

   if ( hwndSigningControl )
      ShowWindow(hwndSigningControl,SW_HIDE);

   RECT rcFullPage = {0};

   if ( pCursiVision -> pISignaturePad_PropertiesOnly -> IsFullPage() ) {

      pWritingLocation -> documentRect.left = pCursiVision -> pdfDocumentUpperLeft.x;
      pWritingLocation -> documentRect.top = pCursiVision -> pdfDocumentUpperLeft.y;
      pWritingLocation -> documentRect.right = pCursiVision -> pdfDocumentUpperRight.x;
      pWritingLocation -> documentRect.bottom = pCursiVision -> pdfDocumentLowerRight.y;

      memcpy(&rcFullPage,&pWritingLocation -> documentRect,sizeof(RECT));

      pCursiVision -> toDocumentCoordinates(&pWritingLocation -> documentRect);

   }

   if ( hdcPDFSource )
      DeleteDC(hdcPDFSource);

   HDC hdcSource = GetDC(hwndHTMLHost);

   hdcPDFSource = CreateCompatibleDC(hdcSource);

   if ( hBitmapPDFPage )
      DeleteObject(hBitmapPDFPage);

   RECT rcPDFPage;

   GetWindowRect(hwndHTMLHost,&rcPDFPage);

   hBitmapPDFPage = CreateBitmap(rcPDFPage.right - rcPDFPage.left,rcPDFPage.bottom - rcPDFPage.top,1,GetDeviceCaps(hdcPDFSource,BITSPIXEL),NULL);

   SelectObject(hdcPDFSource,hBitmapPDFPage);

   BitBlt(hdcPDFSource,0,0,cxHTMLHost,cyHTMLHost,hdcSource,0,0,SRCCOPY);

   ReleaseDC(hwndHTMLHost,hdcSource);

   RECT rectSingleImage = {0};
   RECT rectDocument = {0};

   if ( 0 == pWritingLocation -> documentRect.top && 0 == pWritingLocation -> documentRect.bottom ) {

      rectSingleImage.right = pCursiVision -> padWidthPixels();
      rectSingleImage.bottom = pCursiVision -> padHeightPixels();

   } else {

      if ( pCursiVision -> pISignaturePad_PropertiesOnly -> IsFullPage() ) {
         memcpy(&rectSingleImage,&rcFullPage,sizeof(RECT));
      } else {
         memcpy(&rectSingleImage,&pWritingLocation -> documentRect,sizeof(RECT));
         pCursiVision -> toWindowCoordinates(&rectSingleImage);
      }

   }

   SetWindowText(GetDlgItem(hwndSigningControl,IDDI_DOODLE_OPTIONS_RESETBUTTON),"Clear");
   ShowWindow(GetDlgItem(hwndSigningControl,IDDI_DOODLE_OPTIONS_LEFTBUTTON),SW_HIDE);
   SetWindowText(GetDlgItem(hwndSigningControl,IDDI_DOODLE_OPTIONS_RIGHTBUTTON),"OK");

   if ( pSignatureBox )
      delete pSignatureBox;

   pSignatureBox = new signatureBox(&rectSingleImage);

   SetWindowText(hwndStatusBar,"The Document is ready to sign");

   if ( hwndSigningControl )
      ShowWindow(hwndSigningControl,SHOW_SIGNING_CONTROL_PANEL);

   return;
   }

   void SignatureProcess::ReplayCurrentPage() {
   pCursiVision -> ClearSignatureGraphics();
   if ( pCursiVision -> SignaturePad() -> IsLCD() ) {
      if ( E_NOTIMPL == pCursiVision -> SignaturePad() -> ClearInk() ) {
         pSignatureBox -> ResendPadImage();
      }
   }
   pSignatureBox -> Refresh();
   return;
   }

   void SignatureProcess::PlayNextPage(long optionNumber) {
   SendMessage(hwndMainFrame,WM_FINISH_SIGNATURE_PLAY,0L,0L);
   return;
   }

   void SignatureProcess::ContinueNextPage(long pn) {

#if 0
   currentPageNumber = pn;
   if ( -1L == currentPageNumber )
      currentPageNumber = 1;
#endif

   if ( 0 == pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect.left && 
                  0 == pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect.right ) {
      memcpy(&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect,&continuationRect,sizeof(RECT));
      pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].zzpdfPageNumber = continuationPageNumber;
   }

   PlayCurrentPage();

   return;
   }
