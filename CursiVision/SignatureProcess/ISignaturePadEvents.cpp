// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma warning(disable:4786)

#include "CursiVision.h"

   // IUnknown

   long __stdcall signatureBox::_ISignaturePadEvents::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( riid == IID_ISignaturePadEvents )
      *ppv = static_cast<_ISignaturePadEvents *>(this);
   else

      return pCursiVision -> QueryInterface(riid,ppv);

   AddRef();
   return S_OK;
   }
   unsigned long __stdcall signatureBox::_ISignaturePadEvents::AddRef() {
   refCount++;
   return refCount;
   }
   unsigned long __stdcall signatureBox::_ISignaturePadEvents::Release() { 
   refCount--;
   return refCount;
   }
 

   STDMETHODIMP signatureBox::_ISignaturePadEvents::PenDown() {

   pCursiVision -> signatureEndTimerSet = false;
   KillTimer(hwndMainFrame,TIMER_EVENT_END_SIGNATURE);

   return S_OK;
   }


   STDMETHODIMP signatureBox::_ISignaturePadEvents::PenUp() {

   if ( ! pCursiVision -> signatureEndTimerSet ) {
      if ( 0.0 != pParent -> pISignaturePad -> PadTimeout() ) {
         SetTimer(hwndMainFrame,TIMER_EVENT_END_SIGNATURE,(long)(1000.0 * pParent -> pISignaturePad -> PadTimeout()),NULL);
         pCursiVision -> signatureEndTimerSet = true;
      }
   }

   return S_OK;
   }


   STDMETHODIMP signatureBox::_ISignaturePadEvents::PenPoint(long x,long y,float inkWeight) {

   if ( ignoreEvents )
      return S_OK;

   if ( ! pParent -> pGDIPlusGraphic ) 
      return S_OK;

#if APPLY_INK_WEIGHT
#else
   inkWeight = 0.0;
#endif   

   pCursiVision -> RealTimeSignatureGraphic() -> addPoint(x,y,pCursiVision -> pdfPageNumber,inkWeight);

   if ( 0L == pParent -> lastSignatureX ) {
      pParent -> lastSignatureX = x;
      pParent -> lastSignatureY = y;
   }

   if ( ! ( 0L == x ) ) {
      if ( ! ( 0.0 == inkWeight ) ) 
         pParent -> pGDIPlusPen -> SetWidth(inkWeight);
      pParent -> pGDIPlusGraphic -> DrawLine(pParent -> pGDIPlusPen,pParent -> lastSignatureX,pParent -> lastSignatureY,x,y);
   }

   pParent -> lastSignatureX = x;
   pParent -> lastSignatureY = y;

   return S_OK;
   }


   STDMETHODIMP signatureBox::_ISignaturePadEvents::OptionSelected(long optionNumber ) {

   if ( ignoreEvents )
      return S_OK;

   if ( OPTION_ID_OK == optionNumber )
      PostMessage(hwndMainFrame,WM_APPLY_SIGNATURE,(WPARAM)optionNumber,0L);
   else
      PostMessage(hwndMainFrame,WM_PLAY_NEXT_PAGE,(WPARAM)optionNumber,0L);

   return S_OK;
   }

   STDMETHODIMP signatureBox::_ISignaturePadEvents::ItemSelected(long optionNumber,BSTR itemText) {
   return S_OK;
   }