// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   static POINTL hostMotion = {0L,0L};
   static HBITMAP hBitmapSignaturePad = NULL;

   ISignaturePad *signatureBox::pISignaturePad = NULL;
   signatureBox::_ISignaturePadEvents *signatureBox::pISignaturePadEvents = NULL;

   signatureBox::signatureBox(RECT *prc) :

      hwndSignatureBox(NULL),
      hwndStatusBar(NULL),

      pGDIPlusGraphic(NULL),
      pGDIPlusPen(NULL),

      gdiplusStartupInput(0),
      gdiplusToken(NULL),
      lastSignatureX(0L),
      lastSignatureY(0L),

      isPositioned(false),

      pIConnectionPointContainer(NULL),
      pIConnectionPoint(NULL),
      dwConnectionCookie(0L),

      cxPad(0L),
      cyPad(0L)

   {

   memset(&rcGreenBox,0,sizeof(RECT));
   memset(&rcCurrentLocation,0,sizeof(RECT));
   memset(&rcPDFBackground,0,sizeof(RECT));

   memcpy(&rcPDFBackground,prc,sizeof(RECT));
   memcpy(&rcOriginalLocation,prc,sizeof(RECT));

   if ( ! pISignaturePad ) {

      if ( CursiVision::pIClassFactory_SignaturePads )
         CursiVision::pIClassFactory_SignaturePads -> CreateInstance(NULL,IID_ISignaturePad,reinterpret_cast<void **>(&pISignaturePad));
      else
         CoCreateInstance(CLSID_CursiVisionSignaturePad,NULL,CLSCTX_INPROC_SERVER,IID_ISignaturePad,reinterpret_cast<void **>(&pISignaturePad));

      if ( ! ( S_OK == pISignaturePad -> Load(NULL,hwndHTMLHost,pCursiVision -> pICursiVisionServices) ) ) {
         PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(ID_DOODLE_CANCEL,0),0L);
         return;
      }

      pISignaturePadEvents = new _ISignaturePadEvents();

   }

   pISignaturePadEvents -> pParent = this;

   IUnknown *pIUnknown = NULL;

   pISignaturePadEvents -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

   pISignaturePad -> QueryInterface(IID_IConnectionPointContainer,reinterpret_cast<void**>(&pIConnectionPointContainer));

   if ( pIConnectionPointContainer ) {
      HRESULT hr = pIConnectionPointContainer -> FindConnectionPoint(IID_ISignaturePadEvents,&pIConnectionPoint);
      if ( pIConnectionPoint )
         hr = pIConnectionPoint -> Advise(pIUnknown,&dwConnectionCookie);
   }

   pIUnknown -> Release();

   cxPad = pISignaturePad -> LCDWidth();
   cyPad = pISignaturePad -> LCDHeight();

   PostMessage(hwndMainFrame,WM_DISPLAY_SIGNATURE_BOX,0L,(LPARAM)this);

   return;
   }


   signatureBox::~signatureBox() {

   unInitializeGDIPlus();

   if ( pISignaturePadEvents ) {

      if ( pIConnectionPointContainer ) {

         if ( pIConnectionPoint ) {
            pIConnectionPoint -> Unadvise(dwConnectionCookie);
            pIConnectionPoint -> Release();
         }

         long k = pIConnectionPointContainer -> Release();

      }

      pIConnectionPointContainer = NULL;
      pIConnectionPoint = NULL;

      dwConnectionCookie = 0;

      pISignaturePadEvents -> pParent = NULL;

   }

   if ( pISignaturePad ) {
      pISignaturePad -> Stop();
      pISignaturePad -> ClearTablet();
   }

   DestroyWindow(hwndSignatureBox);

   return;
   }


   void signatureBox::ReleaseSignaturePad() {

   if ( ! pISignaturePad )
      return;

   if ( pISignaturePadEvents && pISignaturePadEvents -> pParent ) {

      if ( pISignaturePadEvents -> pParent -> pIConnectionPointContainer ) {

         if ( pISignaturePadEvents -> pParent -> pIConnectionPoint ) {
            pISignaturePadEvents -> pParent -> pIConnectionPoint -> Unadvise(pISignaturePadEvents -> pParent -> dwConnectionCookie);
            pISignaturePadEvents -> pParent -> pIConnectionPoint -> Release();
         }

         long k = pISignaturePadEvents -> pParent -> pIConnectionPointContainer -> Release();

      }

      pISignaturePadEvents -> pParent -> pIConnectionPointContainer = NULL;
      pISignaturePadEvents -> pParent -> pIConnectionPoint = NULL;

      pISignaturePadEvents -> pParent -> dwConnectionCookie = 0;

   }

   pISignaturePad -> Stop();
   pISignaturePad -> Unload();
   pISignaturePad -> Release();
   pISignaturePad = NULL;
   CoFreeUnusedLibraries();

   return;
   }


   void signatureBox::delayedShow() {
   unInitializeGDIPlus();
   DestroyWindow(hwndSignatureBox);
   display();
   return;
   }


   void signatureBox::Move(long deltaX,long deltaY) {
   if ( pCursiVision -> SignaturePad() -> IsFullPage() )
      return;
   RECT rcCurrent;
   GetWindowRect(hwndSignatureBox,&rcCurrent);
   hostMotion.x = deltaX;
   hostMotion.y = deltaY;
   SetWindowPos(hwndSignatureBox,HWND_TOP,rcCurrent.left + deltaX,rcCurrent.top + deltaY,0,0,SWP_NOSIZE | SWP_NOREDRAW);
   hostMotion.x = 0L;
   hostMotion.y = 0L;
   return;
   }


   void signatureBox::display() {

   if ( ! hBitmapPDFPage ) {
      PostMessage(hwndMainFrame,WM_DISPLAY_SIGNATURE_BOX,0L,(LPARAM)this);
      return;
   }

   static long isRegistered = 0L;

   if ( ! isRegistered ) {

      isRegistered = 1L;

      WNDCLASS gClass;
      
      memset(&gClass,0,sizeof(WNDCLASS));

      gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
      gClass.lpfnWndProc = signatureBox::handler;
      gClass.cbClsExtra = 32;
      gClass.cbWndExtra = 32;
      gClass.hInstance = hModule;
      gClass.hIcon = NULL;
      gClass.hCursor = NULL;
      gClass.hbrBackground = 0;
      gClass.lpszMenuName = NULL;
      gClass.lpszClassName = "SignatureBox";

      RegisterClass(&gClass);
   
   }

   RECT rcParent,rcStatusBar = {0};

   GetWindowRect(hwndHTMLHost,&rcParent);

   long cxBox = rcOriginalLocation.right - rcOriginalLocation.left;
   long cyBox = rcOriginalLocation.bottom - rcOriginalLocation.top;

   rcGreenBox.left = SIGNATURE_BOX_MARGIN;
   rcGreenBox.top = SIGNATURE_BOX_MARGIN;
   rcGreenBox.right = rcGreenBox.left + cxBox;
   rcGreenBox.bottom = rcGreenBox.top + cyBox;

   memcpy(&rcCurrentLocation,&rcOriginalLocation,sizeof(RECT));

   memcpy(&rcPDFBackground,&rcOriginalLocation,sizeof(RECT));

   if ( pISignaturePad -> IsFullPage() ) {

      hwndSignatureBox = CreateWindowEx(0L,"SignatureBox","",WS_CHILD,
                                              rcCurrentLocation.left,rcCurrentLocation.top,rcCurrentLocation.right - rcCurrentLocation.left,rcCurrentLocation.bottom - rcCurrentLocation.top,
                                                hwndHTMLHost,NULL,hModule,(LPARAM *)this);

      pCursiVision -> SignaturePad() -> Load(NULL,hwndSignatureBox,reinterpret_cast<void *>(pCursiVision -> pICursiVisionServices));

   } else {

      bool canChangeSignatureArea = ( pCursiVision -> PrintingProfile() ? pCursiVision -> pIPrintingSupport -> NonAdminCanChange() : true) ? true : false;

      if ( canChangeSignatureArea ) {
         hwndSignatureBox = CreateWindowEx(WS_EX_CLIENTEDGE | WS_EX_TOOLWINDOW,"SignatureBox","The document is ready to sign",WS_SIZEBOX,0,0,0,0,hwndHTMLHost,NULL,hModule,(LPARAM *)this);
         hwndStatusBar = CreateWindowEx(WS_EX_CLIENTEDGE,STATUSCLASSNAME,"Move and/or size this window to change the signature area",WS_CHILD | WS_VISIBLE | SBS_SIZEGRIP,0,0,0,CW_USEDEFAULT,hwndSignatureBox,NULL,hModule,(LPARAM *)this);
      } else {
         hwndSignatureBox = CreateWindowEx(WS_EX_CLIENTEDGE,"SignatureBox","The document is ready to sign",0L,0,0,0,0,hwndHTMLHost,NULL,hModule,(LPARAM *)this);
         hwndStatusBar = CreateWindowEx(WS_EX_CLIENTEDGE,STATUSCLASSNAME,"Admin privileges are required to change the signature area.",WS_CHILD | WS_VISIBLE | SBS_SIZEGRIP,0,0,0,CW_USEDEFAULT,hwndSignatureBox,NULL,hModule,(LPARAM *)this);
         EnableWindow(hwndSignatureBox,FALSE);
      }

      GetWindowRect(hwndStatusBar,&rcStatusBar);

      rcCurrentLocation.left += rcParent.left - SIGNATURE_BOX_MARGIN;
      rcCurrentLocation.top += rcParent.top - SIGNATURE_BOX_MARGIN;
      rcCurrentLocation.right += rcParent.left + SIGNATURE_BOX_MARGIN;
      rcCurrentLocation.bottom += rcParent.top + (SIGNATURE_BOX_MARGIN + rcStatusBar.bottom - rcStatusBar.top);

      AdjustWindowRectEx(&rcCurrentLocation,(DWORD)GetWindowLongPtr(hwndSignatureBox,GWL_STYLE),FALSE,(DWORD)GetWindowLongPtr(hwndSignatureBox,GWL_EXSTYLE));

      RECT rcAdjust = {0};

      AdjustWindowRectEx(&rcAdjust,(DWORD)GetWindowLongPtr(hwndSignatureBox,GWL_STYLE),FALSE,(DWORD)GetWindowLongPtr(hwndSignatureBox,GWL_EXSTYLE));

      rcPDFBackground.left -= SIGNATURE_BOX_MARGIN;
      rcPDFBackground.top -= SIGNATURE_BOX_MARGIN;

      rcPDFBackground.left += rcAdjust.left;
      rcPDFBackground.top += rcAdjust.top;

      rcPDFBackground.right = rcPDFBackground.left + cxBox + 2 * SIGNATURE_BOX_MARGIN;
      rcPDFBackground.bottom = rcPDFBackground.top + cyBox + 2 * SIGNATURE_BOX_MARGIN;

   }

   SetWindowPos(hwndSignatureBox,HWND_TOP,rcCurrentLocation.left,rcCurrentLocation.top,rcCurrentLocation.right - rcCurrentLocation.left,rcCurrentLocation.bottom - rcCurrentLocation.top,SWP_SHOWWINDOW);

   initializeGDIPlus();

   setupGDIPlus();

   SendMessage(hwndSignatureBox,WM_EXITSIZEMOVE,0L,0L);

   pISignaturePad -> Start();

   return;
   }


   void signatureBox::GetRect(RECT *prc) {

   RECT rcParent = {0};

   if ( ! pISignaturePad -> IsFullPage() ) 
      GetWindowRect(hwndHTMLHost,&rcParent);

   prc -> left = rcCurrentLocation.left - rcParent.left + SIGNATURE_BOX_MARGIN;
   prc -> top = rcCurrentLocation.top - rcParent.top + SIGNATURE_BOX_MARGIN;

   prc -> right = prc -> left + rcGreenBox.right - rcGreenBox.left;
   prc -> bottom = prc -> top + rcGreenBox.bottom - rcGreenBox.top;

   return;
   }


   long signatureBox::GetPDFPageNumber() {

   POINTL ptlGreenBox;

   memcpy(&ptlGreenBox,(POINTL *)&rcPDFBackground,sizeof(POINTL));

   ptlGreenBox.x += rcGreenBox.left;
   ptlGreenBox.y += rcGreenBox.top;

   long v = 0L;

   pCursiVision -> pIPDFiumControl -> get_PDFPageNumberAtY(ptlGreenBox.y,rcGreenBox.bottom - rcGreenBox.top,&v);

   return v;
   }


   long signatureBox::SetupGDIPlus() {
   setupGDIPlus();
   return 0L;
   }

   void signatureBox::initializeGDIPlus() {
   if ( NULL == pGDIPlusGraphic )
      GdiplusStartup(&gdiplusToken,&gdiplusStartupInput,NULL);
   if ( NULL == hdcGDIPlus )
      ReleaseDC(hwndSignatureBox,hdcGDIPlus);
   return;
   }


   void signatureBox::setupGDIPlus() {

   EnterCriticalSection(&pCursiVision -> graphicListAccess);

   pCursiVision -> ResetRealTimeSignatureGraphic();

   lastSignatureX = 0L;
   lastSignatureY = 0L;

   hdcGDIPlus = GetDC(hwndSignatureBox);

   if ( pGDIPlusGraphic )
      delete pGDIPlusGraphic;

   pGDIPlusGraphic = NULL;

   if ( pGDIPlusPen )
      delete pGDIPlusPen;

   pGDIPlusPen = NULL;

   if ( pCursiVision -> SignaturePad() ) {
      COLORREF color = pISignaturePad -> InkColor();
      pGDIPlusPen = new Pen(Color(GetRValue(color),GetGValue(color),GetBValue(color)),(float)pISignaturePad -> InkWeight());
   } else
      pGDIPlusPen = new Pen(Color(0,0,0),1.0F);

   pGDIPlusPen -> SetLineCap(LineCapSquare,LineCapSquare,DashCapFlat);

   setTransform();

   LeaveCriticalSection(&pCursiVision -> graphicListAccess);

   return;
   }


   void signatureBox::setTransform() {

   if ( pGDIPlusGraphic )
      delete pGDIPlusGraphic;

   pGDIPlusGraphic = new Graphics(hdcGDIPlus);

   pGDIPlusGraphic -> SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeHighQuality);

   double scaleX = 1.0;
   double scaleY = 1.0;

   if ( pCursiVision -> SignaturePad() -> HasScaling() ) {
      scaleX = pCursiVision -> SignaturePad() -> PadToLCDScaleX();
      scaleY = pCursiVision -> SignaturePad() -> PadToLCDScaleY();
   }

   scaleX *= (double)(rcGreenBox.right - rcGreenBox.left) / (double)cxPad;
   scaleY *= (double)(rcGreenBox.bottom - rcGreenBox.top) / (double)cyPad;

   pGDIPlusGraphic -> ResetTransform();

   pGDIPlusGraphic -> ScaleTransform((float)scaleX,(float)scaleY);

   pGDIPlusGraphic -> TranslateTransform((float)SIGNATURE_BOX_MARGIN / (float)scaleX,(float)SIGNATURE_BOX_MARGIN / (float)scaleY);

   return;
   }


   void signatureBox::unInitializeGDIPlus() {

   if ( pGDIPlusPen )
      delete pGDIPlusPen;

   if ( pGDIPlusGraphic )
      delete pGDIPlusGraphic;

   pGDIPlusGraphic = NULL;
   pGDIPlusPen = NULL;

   if ( hdcGDIPlus )
      ReleaseDC(hwndSignatureBox,hdcGDIPlus);

   hdcGDIPlus = NULL;

   GdiplusShutdown(gdiplusToken);

   return;

   }


   void signatureBox::resetGDIPlus() {
   pCursiVision -> ResetRealTimeSignatureGraphic();
   RedrawWindow(hwndSignatureBox,NULL,NULL,RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
   }


   LRESULT CALLBACK signatureBox::handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   if ( msg >= WM_KEYFIRST && msg <= WM_KEYLAST )
      return SendMessage(hwndMainFrame,msg,wParam,lParam);

   signatureBox *p = (signatureBox *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {
   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      p = reinterpret_cast<signatureBox *>(pc -> lpCreateParams);
      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);
      }
      break;

   case WM_WINDOWPOSCHANGED: {

      WINDOWPOS *pPosition = (WINDOWPOS *)lParam;

      RECT rcAdjust = {0};

      AdjustWindowRectEx(&rcAdjust,(DWORD)GetWindowLongPtr(hwnd,GWL_STYLE),FALSE,(DWORD)GetWindowLongPtr(hwnd,GWL_EXSTYLE));

      pPosition -> x -= rcAdjust.left;
      pPosition -> y -= rcAdjust.top;
      pPosition -> cx += (rcAdjust.right - rcAdjust.left);
      pPosition -> cy += (rcAdjust.bottom - rcAdjust.top);

      long deltaCX = pPosition -> cx - (p -> rcCurrentLocation.right - p -> rcCurrentLocation.left);
      long deltaCY = pPosition -> cy - (p -> rcCurrentLocation.bottom - p -> rcCurrentLocation.top);
      
      long deltaX = p -> rcCurrentLocation.left - pPosition -> x;
      long deltaY = p -> rcCurrentLocation.top - pPosition -> y;

      p -> rcPDFBackground.left -= deltaX;
      p -> rcPDFBackground.top -= deltaY;
      p -> rcPDFBackground.right -= deltaX;
      p -> rcPDFBackground.bottom -= deltaY;

      long cxClient = pPosition -> cx - 2 * (rcAdjust.right - rcAdjust.left);
      long cyClient = pPosition -> cy - 2 * (rcAdjust.bottom - rcAdjust.top);

      p -> rcPDFBackground.right = p -> rcPDFBackground.left + cxClient;
      p -> rcPDFBackground.bottom = p -> rcPDFBackground.top + cyClient;

      p -> rcCurrentLocation.left = pPosition -> x;
      p -> rcCurrentLocation.top = pPosition -> y;
      p -> rcCurrentLocation.right = p -> rcCurrentLocation.left + pPosition -> cx;
      p -> rcCurrentLocation.bottom = p -> rcCurrentLocation.top + pPosition -> cy;

      p -> rcGreenBox.right = cxClient - SIGNATURE_BOX_MARGIN_WNDPROC;
      
      double cxPad = ((double)pCursiVision -> SignaturePad() -> Width() * pCursiVision -> lastPadToPDFScale);
      double cyPad = ((double)pCursiVision -> SignaturePad() -> Height() * pCursiVision -> lastPadToPDFScale);

      if ( pCursiVision -> SignaturePad() -> IsFullPage() ) {
         cxPad = (double)cxClient;
         cyPad = (double)cyClient;
      }

      double aspectRatio = cxPad / cyPad;

      p -> rcGreenBox.bottom = p -> rcGreenBox.top + (long)((double)(p -> rcGreenBox.right - p -> rcGreenBox.left) / aspectRatio);

      if ( p -> rcGreenBox.bottom > (cyClient - SIGNATURE_BOX_MARGIN_WNDPROC) ) {
         p -> rcGreenBox.bottom = cyClient - SIGNATURE_BOX_MARGIN_WNDPROC;
         p -> rcGreenBox.right = p -> rcGreenBox.left + (long)((double)(p -> rcGreenBox.bottom - p -> rcGreenBox.top) * aspectRatio);
      }

      if ( p -> pGDIPlusGraphic )
         p -> setTransform();

      if ( ! p -> pISignaturePad -> IsFullPage() ) {
         p -> rcPDFBackground.left -= hostMotion.x;
         p -> rcPDFBackground.top -= hostMotion.y;
         p -> rcPDFBackground.right -= hostMotion.x;
         p -> rcPDFBackground.bottom -= hostMotion.y;
      }

      if ( p -> hwndStatusBar ) {
         RECT rcStatusBar;
         GetWindowRect(p -> hwndStatusBar,&rcStatusBar);
         SetWindowPos(p -> hwndStatusBar,HWND_TOP,0,pPosition -> cy - (rcStatusBar.bottom - rcStatusBar.top),pPosition -> cx,rcStatusBar.bottom - rcStatusBar.top,0L);
      }

      if ( 0 == hostMotion.x && 0 == hostMotion.y )
         RedrawWindow(hwnd,NULL,NULL,RDW_INVALIDATE | RDW_ERASE | RDW_UPDATENOW | RDW_ERASENOW);

      pCursiVision -> pdfPageNumber = p -> GetPDFPageNumber();

      }
      return (LRESULT)0;

   case WM_EXITSIZEMOVE: {

      if ( ! p -> pISignaturePad -> IsLCD() )
         break;

      if ( p -> pISignaturePad -> IsFullPage() )
         break;

      if ( NULL == hBitmapPDFPage )
         break;

      p -> resendPadImage();

      }
      break;

   case WM_PAINT: {

      PAINTSTRUCT ps;
      BeginPaint(hwnd,&ps);

      FillRect(ps.hdc,&ps.rcPaint,(HBRUSH)(COLOR_APPWORKSPACE + 1));

      BitBlt(ps.hdc,0,0,p -> rcPDFBackground.right - p -> rcPDFBackground.left,p -> rcPDFBackground.bottom - p -> rcPDFBackground.top,
                                       hdcPDFSource,p -> rcPDFBackground.left,p -> rcPDFBackground.top,SRCCOPY);

      HPEN hPen = CreatePen(PS_SOLID,2,RGB(0,255,0));
      HPEN oldPen = (HPEN)SelectObject(ps.hdc,hPen);

      MoveToEx(ps.hdc,p -> rcGreenBox.left + 1,p -> rcGreenBox.top + 1,NULL);

      LineTo(ps.hdc,p -> rcGreenBox.right - 1,p -> rcGreenBox.top + 1);
      LineTo(ps.hdc,p -> rcGreenBox.right - 1,p -> rcGreenBox.bottom - 1);
      LineTo(ps.hdc,p -> rcGreenBox.left + 1,p -> rcGreenBox.bottom - 1);
      LineTo(ps.hdc,p -> rcGreenBox.left + 1,p -> rcGreenBox.top + 1);

      DeleteObject(SelectObject(ps.hdc,oldPen));

      EnterCriticalSection(&pCursiVision -> graphicListAccess);

      long lastX,lastY;

      lastX = 0L;
      lastY = 0L;

      signatureGraphic *pSG = pCursiVision -> RealTimeSignatureGraphic();

      for ( long k = 0; k < pSG -> totalPoints; k++ ) {

         if ( 0.0 == pSG -> pSignatureDataX[k] && 0.0 == pSG -> pSignatureDataY[k] ) {
            lastX = 0L;
            lastY = 0L;
            continue;
         }

         if ( ! ( pCursiVision -> pdfPageNumber == pSG -> pSignatureDataPage[k] ) ) {
            lastX = 0L;
            lastY = 0L;
            continue;
         }

         long x = (long)pSG -> pSignatureDataX[k];
         long y = (long)pSG -> pSignatureDataY[k];

         if ( 0 != lastX && 0 != lastY ) {
            if ( ! ( 0.0 == pSG -> pInkWeight[k] ) ) 
               p -> pGDIPlusPen -> SetWidth(pSG -> pInkWeight[k]);
            p -> pGDIPlusGraphic -> DrawLine(p -> pGDIPlusPen,lastX,lastY,x,y);
         }

         lastX = x;
         lastY = y;

      }

      LeaveCriticalSection(&pCursiVision -> graphicListAccess);

      EndPaint(hwnd,&ps);
      }
      break;
   
   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }


   void signatureBox::resendPadImage() {

   if ( 0 > cxPad || 0 > cyPad || 0 >= (cxPad * cyPad) )
      return;

   HDC hdcTarget = CreateCompatibleDC(NULL);
   HBITMAP hBitmapPad = CreateBitmap(cxPad,cyPad,1,GetDeviceCaps(hdcPDFSource,BITSPIXEL),NULL);
   HGDIOBJ hOldBitmap = SelectObject(hdcTarget,hBitmapPad);

   StretchBlt(hdcTarget,0,0,cxPad,cyPad,hdcPDFSource,rcPDFBackground.left + rcGreenBox.left,rcPDFBackground.top + rcGreenBox.top,rcGreenBox.right - rcGreenBox.left,rcGreenBox.bottom - rcGreenBox.top,SRCCOPY);

   SelectObject(hdcTarget,hOldBitmap);

   pCursiVision -> SignaturePad() -> DisplaySignatureBitmapHandle((UINT_PTR)hBitmapPad,hdcTarget,0,0,cxPad,cyPad);
   pCursiVision -> SignaturePad() -> DisplayOk(OPTION_ID_OK);
   pCursiVision -> SignaturePad() -> DisplayClear(OPTION_ID_CLEAR);

   DeleteObject(hBitmapPad);

   DeleteDC(hdcTarget);

   return;
   }