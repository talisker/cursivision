
#include "InsertPDF.h"

#ifdef DO_PROCESSES

   struct {
      long x;
      long y;
   } mouseStart, mouseEnd;

   innerRect *pActiveInnerRect = NULL;

   RECT rcPieceParent = {0,0,0,0};

   LRESULT CALLBACK SignatureProcess::piecesHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   SignatureProcess *p = reinterpret_cast<SignatureProcess *>(GetWindowLong(hwnd,GWL_USERDATA));

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      p = reinterpret_cast<SignatureProcess *>(pc -> lpCreateParams);
      SetWindowLong(hwnd,GWL_USERDATA,reinterpret_cast<long>(p));
      }
      break;

   case WM_PAINT: {

      if ( ! p )
         break;

      if ( p -> IsPlayMode() || p -> IsAdHocMode() )
         break;

      PAINTSTRUCT ps;
 
      HBRUSH white,softGreen;

      bool constructionMode = ( 0 == wParam );

      if ( ! constructionMode && 0 == p -> thePages[p -> currentPageNumber].innerRectCount )
         break;

      if ( constructionMode ) {
         BeginPaint(hwnd,&ps);
         white = CreateSolidBrush(RGB(240,240,240));
         softGreen = CreateSolidBrush(RGB(228,255,228));
      } else {
         ps.hdc = (HDC)wParam;
         GetClientRect(hwnd,&ps.rcPaint);
         white = CreateSolidBrush(RGB(255,255,255));
         softGreen = CreateSolidBrush(RGB(255,255,255));
      }

      FillRect(ps.hdc,&ps.rcPaint,white);

      if ( constructionMode || p -> piecesConstructionMode ) {

         char szTemp[2048];

         if ( ps.rcPaint.right - ps.rcPaint.left < 256 )
            sprintf(szTemp,"Use the options to create\rthe pad image");
         else if ( pInsertPDF -> pISignaturePad -> IsFullPage() ) 
            sprintf(szTemp,"You are using a full page writing device.\r\rNavigate the document to the page desired "
                           "and click Select Page");
         else
            sprintf(szTemp,"To define regions from the document that display on the pad, click Select Pieces.\r\r"
                  "Click and drag out regions on the document to add them to this window.\r\r"
                  "Then, click and drag the pieces in this window to position them where you want them to appear on the pad.\r\r"
                  "Also, Select Targets to define areas on the document where signature results will be placed.\r\r"
                  "You should also drag those regions to where you want them to appear on the pad to capture signature data");

         SetTextColor(ps.hdc,RGB(128,128,128));
         SetBkMode(ps.hdc,TRANSPARENT);
         RECT rectCenter;
         GetClientRect(hwnd,&rectCenter);
         rectCenter.left += 8;
         rectCenter.right -= 16;
         rectCenter.top += 8;
         rectCenter.bottom -= 16;
         DrawTextEx(ps.hdc,szTemp,strlen(szTemp),&rectCenter,DT_WORDBREAK,NULL);

      } else if ( p -> regionConstructionMode ) {

         char szTemp[2048];
//TODO: double check that instruction text still appears when working with a 4x5 or larger pad.
         if ( ps.rcPaint.right - ps.rcPaint.left < 256 )
            sprintf(szTemp,"Use the options to create\rthe pad image");
         else if ( pInsertPDF -> pISignaturePad -> IsFullPage() )
            sprintf(szTemp,"You are using a full page writing device.\r\rNavigate the document to the page desired "
                           "and click Select Page");
         else
            sprintf(szTemp,"To define the image that will appear on the pad, Select Region.\r\r"
                  "Move the mouse in the document until the desired region is within box drawn, then, click the left mouse button.\r\r"
                  "This entire region will appear on the pad. It is not necessary to choose a target for signature data.");

         SetTextColor(ps.hdc,RGB(128,128,128));
         SetBkMode(ps.hdc,TRANSPARENT);
         RECT rectCenter;
         GetClientRect(hwnd,&rectCenter);
         rectCenter.left += 8;
         rectCenter.right -= 16;
         rectCenter.top += 8;
         rectCenter.bottom -= 16;
         DrawTextEx(ps.hdc,szTemp,strlen(szTemp),&rectCenter,DT_WORDBREAK,NULL);

      }

      RECT rectPad;

      pInsertPDF -> pISignaturePad -> GetRect(&rectPad);

      rectPad.right = rectPad.left + (rectPad.right - rectPad.left);
      rectPad.bottom = rectPad.top + (rectPad.bottom - rectPad.top);

      HBITMAP hBitmapResults = CreateCompatibleBitmap(ps.hdc,rectPad.right - rectPad.left,rectPad.bottom - rectPad.top);
      HGDIOBJ hOldBitmapResults = SelectObject(ps.hdc,hBitmapResults);

      if ( ! p -> piecesConstructionMode && 0 == p -> thePages[p -> currentPageNumber].innerRectCount ) {

         char szTemp[MAX_PATH];
         sprintf(szTemp,"%s\\%s_Page_%ld.bmp",p -> GetBitmapDirectory(),p -> szProcessName,p -> currentPageNumber);

         FILE *fBitmap = fopen(szTemp,"rb");
         if ( ! fBitmap )
            goto doTargets;

         BITMAPFILEHEADER bitmapFileHeader;

         if ( ! fread(&bitmapFileHeader,sizeof(BITMAPFILEHEADER),1,fBitmap) ) {
            fclose(fBitmap);
            goto doTargets;
         }

         long colorTableSize = bitmapFileHeader.bfOffBits - sizeof(BITMAPFILEHEADER);

         BYTE *buffer = new BYTE[colorTableSize];

         if ( ! fread(buffer,colorTableSize,1,fBitmap) ) {
            delete [] buffer;
            fclose(fBitmap);
            goto doTargets;
         }

         BITMAPINFO *pBitmapInfo = (BITMAPINFO *)buffer;

         BYTE *pBits = new BYTE[pBitmapInfo -> bmiHeader.biSizeImage];
         long rc = fread(pBits,pBitmapInfo -> bmiHeader.biSizeImage,1,fBitmap);
         fclose(fBitmap);

         long cx = pBitmapInfo -> bmiHeader.biWidth;
         long cy = pBitmapInfo -> bmiHeader.biHeight;
         long cxPad = rectPad.right - rectPad.left;

         rc = StretchDIBits(ps.hdc,0,0,rectPad.right - rectPad.left,rectPad.bottom - rectPad.top,0,0,cx,cy,pBits,pBitmapInfo,DIB_RGB_COLORS,SRCCOPY);

         delete [] buffer;
         delete [] pBits;

      } else {

         for ( long k = 0; k < p -> thePages[p -> currentPageNumber].innerRectCount; k++ ) {
   
            innerRect *pIR = &p -> thePages[p -> currentPageNumber].innerRects[k];

            char szTemp[1024];
            sprintf(szTemp,"%s\\%s_Piece_%ld_Page_%ld.bmp",p -> GetBitmapDirectory(),p -> szProcessName,k + 1,p -> currentPageNumber);
   
            FILE *fBitmap = fopen(szTemp,"rb");
            if ( ! fBitmap )
               continue;
   
            BITMAPFILEHEADER bitmapFileHeader;
   
            if ( ! fread(&bitmapFileHeader,sizeof(BITMAPFILEHEADER),1,fBitmap) ) {
               fclose(fBitmap);
               continue;
            }
   
            long colorTableSize = bitmapFileHeader.bfOffBits - sizeof(BITMAPFILEHEADER);

            BYTE *buffer = new BYTE[colorTableSize];
   
            if ( ! fread(buffer,colorTableSize,1,fBitmap) ) {
               delete [] buffer;
               fclose(fBitmap);
               continue;
            }
   
            BITMAPINFO *pBitmapInfo = (BITMAPINFO *)buffer;

            RECT *pRectLocal = &p -> thePages[p -> currentPageNumber].innerRects[k].localRect;
   
            BYTE *pBits = new BYTE[pBitmapInfo -> bmiHeader.biSizeImage];
            long rc = fread(pBits,pBitmapInfo -> bmiHeader.biSizeImage,1,fBitmap);
            fclose(fBitmap);
   
            long cx = pBitmapInfo -> bmiHeader.biWidth;
            long cy = pBitmapInfo -> bmiHeader.biHeight;

            long cxDest = pRectLocal -> right - pRectLocal -> left;
            long cyDest = pRectLocal -> bottom - pRectLocal -> top;

            rc = StretchDIBits(ps.hdc,pRectLocal -> left,pRectLocal -> top,cxDest,cyDest,0,0,cx,cy,pBits,pBitmapInfo,DIB_RGB_COLORS,SRCCOPY);

            delete [] buffer;

            delete[] pBits;
   
         }

doTargets:
   
         for ( long k = 0; k < p -> thePages[p -> currentPageNumber].targetRectCount; k++ ) {
   
            innerRect *pIR = &p -> thePages[p -> currentPageNumber].targetRects[k];
   
            RECT *pLocal = &pIR -> localRect;
   
            long cx = pLocal -> right - pLocal -> left;
            long cy = pLocal -> bottom - pLocal -> top;

            FillRect(ps.hdc,pLocal,softGreen);
   
            HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
            HGDIOBJ oldPen = SelectObject(ps.hdc,hPen);
   
            SetROP2(ps.hdc,R2_XORPEN);
   
            MoveToEx(ps.hdc,pLocal -> left,pLocal -> top,NULL);
            LineTo(ps.hdc,pLocal -> left + cx,pLocal -> top);
            LineTo(ps.hdc,pLocal -> left + cx,pLocal -> top + cy);
            LineTo(ps.hdc,pLocal -> left,pLocal -> top + cy);
            LineTo(ps.hdc,pLocal -> left,pLocal -> top);

            DeleteObject(SelectObject(ps.hdc,oldPen));

#if 0

            HDC hdcDocument = GetDC(hwndPDFDocument);
            HPEN hPenDocument = CreatePen(PS_SOLID,1,RGB(128,128,128));
            oldPen = SelectObject(hdcDocument,hPenDocument);
   
            SetROP2(hdcDocument,R2_XORPEN);

            if ( 0 != rcPieceParent.left && 0 != rcPieceParent.top ) {
               MoveToEx(hdcDocument,rcPieceParent.left,rcPieceParent.top,NULL);
               LineTo(hdcDocument,rcPieceParent.right,rcPieceParent.top);
               LineTo(hdcDocument,rcPieceParent.right,rcPieceParent.bottom);
               LineTo(hdcDocument,rcPieceParent.left,rcPieceParent.bottom);
               LineTo(hdcDocument,rcPieceParent.left,rcPieceParent.top);
            }

            memcpy(&rcPieceParent,&pIR -> padRectWindowsCoordinates,sizeof(RECT));

            MoveToEx(hdcDocument,rcPieceParent.left,rcPieceParent.top,NULL);
            LineTo(hdcDocument,rcPieceParent.right,rcPieceParent.top);
            LineTo(hdcDocument,rcPieceParent.right,rcPieceParent.bottom);
            LineTo(hdcDocument,rcPieceParent.left,rcPieceParent.bottom);
            LineTo(hdcDocument,rcPieceParent.left,rcPieceParent.top);
   
            DeleteObject(SelectObject(hdcDocument,oldPen));
            ReleaseDC(hwndPDFDocument,hdcDocument);
#endif
   
         }

      }

      SelectObject(ps.hdc,hOldBitmapResults);

      DeleteObject(hBitmapResults);
      DeleteObject(white);
      DeleteObject(softGreen);

      EndPaint(hwnd,&ps);

      if ( constructionMode || p -> piecesConstructionMode || p -> regionConstructionMode )
         return (LRESULT)0;

      return (LRESULT)0;

      }
      break;


   case WM_LBUTTONUP: 
      pActiveInnerRect = NULL;
      break;


   case WM_LBUTTONDOWN: {

      mouseStart.x = LOWORD(lParam);
      mouseStart.y = HIWORD(lParam);

      pActiveInnerRect = NULL;

      for ( long k = 0; k < p -> thePages[p -> currentPageNumber].innerRectCount; k++ ) {
         innerRect *pIR = &p -> thePages[p -> currentPageNumber].innerRects[k];
         RECT *pLocal = &pIR -> localRect;
         long cx = pLocal -> right - pLocal -> left;
         long cy = pLocal -> bottom - pLocal -> top;
         if ( pLocal -> left < mouseStart.x && pLocal -> left + cx > mouseStart.x && pLocal -> top < mouseStart.y && pLocal -> top + cy > mouseStart.y ) {
            pActiveInnerRect = pIR;
            break;
         }
      }

      if ( ! pActiveInnerRect ) for ( long k = 0; k < p -> thePages[p -> currentPageNumber].targetRectCount; k++ ) {
         innerRect *pIR = &p -> thePages[p -> currentPageNumber].targetRects[k];
         RECT *pLocal = &pIR -> localRect;
         long cx = pLocal -> right - pLocal -> left;
         long cy = pLocal -> bottom - pLocal -> top;
         if ( pLocal -> left < mouseStart.x && pLocal -> left + cx > mouseStart.x && pLocal -> top < mouseStart.y && pLocal -> top + cy > mouseStart.y ) {
            pActiveInnerRect = pIR;
            memset(&rcPieceParent,0,sizeof(RECT));
            break;
         }
      }

      }
      break;

   case WM_MOUSEMOVE: {

      if ( ! pActiveInnerRect )
         break;

      if ( MK_LBUTTON != wParam )
         break;

      RECT *pLocal = &pActiveInnerRect -> localRect;

      mouseEnd.x = LOWORD(lParam);
      mouseEnd.y = HIWORD(lParam);

      long deltaX = mouseEnd.x - mouseStart.x;
      long deltaY = mouseEnd.y - mouseStart.y;

      mouseStart.x = mouseEnd.x;
      mouseStart.y = mouseEnd.y;

      pLocal -> left += deltaX;
      pLocal -> top += deltaY;
      pLocal -> right += deltaX;
      pLocal -> bottom += deltaY;

      RECT *pPseudoPad = &pActiveInnerRect -> padRect;

      pPseudoPad -> left -= deltaX;
      pPseudoPad -> right -= deltaX;

      pPseudoPad -> top -= deltaY;
      pPseudoPad -> bottom -= deltaX;
      
      InvalidateRect(hwnd,NULL,TRUE);

      }
      break;

   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }

#endif