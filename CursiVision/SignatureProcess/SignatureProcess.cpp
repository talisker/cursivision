// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <math.h>

   SignatureProcess::SignatureProcess() :

      regionCaptureStage(0),
      pdfZoom(100.0),
      pSignatureBox(NULL),
      refCount(0)

   {

   pCursiVision -> ClearSignatureGraphics();

   long currentStyle = (long)GetWindowLongPtr(hwndMainFrame,GWL_STYLE);
   currentStyle &= ~(WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
   currentStyle &= ~(WS_SIZEBOX);

   SetWindowLongPtr(hwndMainFrame,GWL_STYLE,currentStyle);

   EnableWindow(hwndStatusBar,FALSE);

   pCursiVision -> expectingPostPaint = true;

   if ( 0 == pCursiVision -> DoodleCount() ) {

      SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);

      TBBUTTONINFO buttonInfo = {0};
      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;
      buttonInfo.fsState = TBSTATE_PRESSED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

      buttonInfo.fsState = TBSTATE_ENABLED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

   }

   SetCursor(LoadCursor(NULL,IDC_WAIT));

   return;
   }


   SignatureProcess::~SignatureProcess() {

   pCursiVision -> lowerMouseVellum();

   if ( pSignatureBox )
      delete pSignatureBox;

   pSignatureBox = NULL;
 
   long currentStyle = (long)GetWindowLongPtr(hwndMainFrame,GWL_STYLE);
   currentStyle |= (WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
   currentStyle |= WS_SIZEBOX;
   SetWindowLongPtr(hwndMainFrame,GWL_STYLE,currentStyle);
   EnableWindow(hwndStatusBar,TRUE);

   pCursiVision -> expectingPostPaint = true;

   SetWindowPos(hwndMainFrame,HWND_TOP,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);

   if ( hwndDisplayWaiting ) {
      ShowWindow(hwndDisplayWaiting,SW_HIDE);
      DestroyWindow(hwndDisplayWaiting);
      hwndDisplayWaiting = NULL;
   }

   KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);

   TBBUTTONINFO buttonInfo = {0};

   buttonInfo.cbSize = sizeof(TBBUTTONINFO);
   buttonInfo.dwMask = TBIF_STATE;
   buttonInfo.fsState = TBSTATE_ENABLED;

   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

   pCursiVision -> resumeTracking();

   return;
   }


   long SignatureProcess::ZoomedPadWidth() {
   return (long)(PadWidth() * pCursiVision -> lastPadToPDFScale); 
   }


   long SignatureProcess::PadWidth() {
//
//NTC: 11-10-2013: WTF ??? This is returning DOMAIN Width (?!) Should be Pixel Width I think (?!)
//
#if 0
   return pCursiVision -> SignaturePad() -> Width();
#else
   return pCursiVision -> SignaturePad() -> LCDWidth();
#endif
   }  


   long SignatureProcess::ZoomedPadHeight() {
   return (long)(PadHeight() * pCursiVision -> lastPadToPDFScale);
   }

   long SignatureProcess::PadHeight() {
//
//NTC: 11-10-2013: WTF ??? This is returning DOMAIN Width (?!) Should be Pixel Width I think (?!)
//
#if 0
   return pCursiVision SignaturePad() -> Height();
#else
   return pCursiVision -> SignaturePad() -> LCDHeight();
#endif
   }


   long SignatureProcess::RegionCaptureStage(long setStage) {

   if ( -1L == setStage ) 
      return regionCaptureStage; 

   if ( REGION_CAPTURE_STAGE_DONE == setStage ) {
      regionCaptureStage = setStage; 
      KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);
      if ( hwndDisplayWaiting ) {
         ShowWindow(hwndDisplayWaiting,SW_HIDE);
         DestroyWindow(hwndDisplayWaiting);
         hwndDisplayWaiting = NULL;
      }
      PostMessage(hwndMainFrame,WM_START_ADHOC,(WPARAM)0L,0L);
      return regionCaptureStage;
   }

   if ( REGION_CAPTURE_STAGE_ONE == setStage )
      SetTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING,DISPLAY_WAITING_TIMEOUT,NULL);

   if ( REGION_CAPTURE_STAGE_DONE != regionCaptureStage && REGION_CAPTURE_STAGE_CANCEL == setStage ) {
      regionCaptureStage = REGION_CAPTURE_STAGE_DONE;
//      memset(&pdfDocumentUpperLeft,0xFF,sizeof(POINTL));

Beep(2000,100);
Beep(2000,100);
Beep(2000,100);
Beep(2000,100);
Beep(2000,100);
Beep(2000,100);

//      SendMessage(hwndPDFDocument,WM_COMMAND,MAKELPARAM(IDCANCEL,0),0L);
      if ( hwndDisplayWaiting ) {
         ShowWindow(hwndDisplayWaiting,SW_HIDE);
         DestroyWindow(hwndDisplayWaiting);
         hwndDisplayWaiting = NULL;
      }
      KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);
      return regionCaptureStage;
   }

   regionCaptureStage = setStage;

   return setStage; 
   }


   void SignatureProcess::OuterRect(RECT *pRect) { 

   if ( ! pCursiVision -> pISignaturePad_PropertiesOnly -> IsFullPage() ) 
      memcpy(&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect,pRect,sizeof(RECT)); 

   pCursiVision -> toDocumentCoordinates(&pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].documentRect);

   if ( -1L == pCursiVision -> pdfPageNumberInAbbayence ) 
      pCursiVision -> pdfPageNumberInAbbayence = pCursiVision -> pdfPageNumber;

   if ( -1L == pCursiVision -> pdfPageNumberInAbbayence )
      pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].zzpdfPageNumber = pCursiVision -> pdfPageNumber;
   else
      pCursiVision -> pDoodleOptions -> pDoodleOptionProperties -> theLocations[pCursiVision -> DoodleCount()].zzpdfPageNumber = pCursiVision -> pdfPageNumberInAbbayence;

   return;
   }