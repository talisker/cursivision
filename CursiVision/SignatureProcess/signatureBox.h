// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#define SIGNATURE_BOX_MARGIN  ( ! pISignaturePad -> IsFullPage() ? 16 : 0 )

#define SIGNATURE_BOX_MARGIN_WNDPROC  ( ! p -> pISignaturePad -> IsFullPage() ? 16 : 0 )

using namespace Gdiplus;

   class signatureBox {

   public: 

      signatureBox(RECT *prcOriginal);
      ~signatureBox();

      long SetupGDIPlus();

      void display();
      void delayedShow();

      bool IsDisplaying() { return ! ( NULL == hwndSignatureBox ); };

      void Move(long deltaX,long deltaY);

      void Hide() { ShowWindow(hwndSignatureBox,SW_HIDE); };
      void Show() { ShowWindow(hwndSignatureBox,SW_SHOW); };

      void GetRect(RECT *prc);
      long GetPDFPageNumber();

      HWND GetHWND() { return hwndSignatureBox; };

      void Refresh() { 
         if ( hwndSignatureBox ) 
            RedrawWindow(hwndSignatureBox,NULL,NULL,RDW_INVALIDATE | RDW_ERASE | RDW_UPDATENOW | RDW_ERASENOW);
      };

      void ResendPadImage() { resendPadImage(); };

      ISignaturePad *SignaturePad() { return pISignaturePad; };

      static void ReleaseSignaturePad();

   private:

      class _ISignaturePadEvents : ISignaturePadEvents {

      public:

         _ISignaturePadEvents() : refCount(0), pParent(NULL), ignoreEvents(false) {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         STDMETHOD(PenDown)();
         STDMETHOD(PenUp)();
         STDMETHOD(PenPoint)(long x,long y,float inkWeight);
         STDMETHOD(OptionSelected)(long optionNumber);
         STDMETHOD(ItemSelected)(long controlId,BSTR item);

         signatureBox *pParent;

         bool ignoreEvents;

         long refCount;

      };

      void initializeGDIPlus();
      void setupGDIPlus();
      void resetGDIPlus();
      void unInitializeGDIPlus();
      void setTransform();
      void resendPadImage();

      IConnectionPointContainer* pIConnectionPointContainer;
      IConnectionPoint *pIConnectionPoint;
      DWORD dwConnectionCookie;

      HWND hwndSignatureBox,hwndStatusBar;
      RECT rcOriginalLocation,rcCurrentLocation,rcGreenBox;
      RECT rcPDFBackground;

      Graphics *pGDIPlusGraphic;
      Pen *pGDIPlusPen;
      GdiplusStartupInput gdiplusStartupInput;
      ULONG_PTR gdiplusToken;
      long lastSignatureX,lastSignatureY;

      bool isPositioned;

      long cxPad,cyPad;

      static ISignaturePad *pISignaturePad;
      static _ISignaturePadEvents *pISignaturePadEvents;

      static LRESULT CALLBACK handler(HWND,UINT,WPARAM,LPARAM);

   };