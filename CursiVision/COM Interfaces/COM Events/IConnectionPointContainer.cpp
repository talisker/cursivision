// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"


   CursiVision::_IConnectionPointContainer::_IConnectionPointContainer(CursiVision *pp) : 
     pParent(pp)
   { 
   return;
   }

   CursiVision::_IConnectionPointContainer::~_IConnectionPointContainer() {
   return;
   }


   HRESULT CursiVision::_IConnectionPointContainer::QueryInterface(REFIID riid,void **ppv) {
   return pParent -> QueryInterface(riid,ppv);
   }


   STDMETHODIMP_(ULONG) CursiVision::_IConnectionPointContainer::AddRef() {
   return pParent -> AddRef();
   }

   STDMETHODIMP_(ULONG) CursiVision::_IConnectionPointContainer::Release() {
   return pParent -> Release();
   }


   STDMETHODIMP CursiVision::_IConnectionPointContainer::EnumConnectionPoints(IEnumConnectionPoints **ppEnum) {

   _IConnectionPoint *connectionPoints[1];

   *ppEnum = NULL;
 
   if ( pParent -> enumConnectionPoints ) 
      delete pParent -> enumConnectionPoints;
 
   connectionPoints[0] = &pParent -> connectionPoint;
   pParent -> enumConnectionPoints = new _IEnumConnectionPoints(pParent,connectionPoints,1);
 
   return pParent -> enumConnectionPoints -> QueryInterface(IID_IEnumConnectionPoints,(void **)ppEnum);
   }
 
 
   STDMETHODIMP CursiVision::_IConnectionPointContainer::FindConnectionPoint(REFIID riid,IConnectionPoint **ppCP) {
   *ppCP = NULL;
   if ( IID_IPropertyNotifySink == riid ) {
      return pParent -> connectionPoint.QueryInterface(IID_IConnectionPoint,(void **)ppCP);
   }
   return CONNECT_E_NOCONNECTION;
   }


   void CursiVision::_IConnectionPointContainer::fire_PropertyChanged() {
   IEnumConnections* pIEnum;
   CONNECTDATA connectData;
   pParent -> connectionPoint.EnumConnections(&pIEnum);
   if ( ! pIEnum ) return;
   while ( 1 ) {
      if ( pIEnum -> Next(1, &connectData, NULL) ) break;
      IPropertyNotifySink *pClient = reinterpret_cast<IPropertyNotifySink *>(connectData.pUnk);
      pClient -> OnChanged(DISPID_UNKNOWN);
   }
   static_cast<IUnknown*>(pIEnum) -> Release();
   return;
   }
