// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   // IUnknown

   long __stdcall CursiVision::_IPDFiumControlEvents::QueryInterface(REFIID riid,void **ppv) {

   if ( riid == IID_IPDFiumControlEvents )
      *ppv = static_cast<IPDFiumControlEvents *>(this);
   else
      return pParent -> QueryInterface(riid,ppv);

   AddRef();

   return S_OK;
   }
   unsigned long __stdcall CursiVision::_IPDFiumControlEvents::AddRef() {
   return pParent -> AddRef();
   }
   unsigned long __stdcall CursiVision::_IPDFiumControlEvents::Release() { 
   return pParent -> Release();
   }


   HRESULT __stdcall CursiVision::_IPDFiumControlEvents::MouseMessage(UINT msg,WPARAM wParam,LPARAM lParam) {
   return S_OK;
   }


   HRESULT __stdcall CursiVision::_IPDFiumControlEvents::Size(SIZE *pSize) {
   return S_OK;
   }

   
   HRESULT __stdcall CursiVision::_IPDFiumControlEvents::Paint(HDC hdc,RECT *prcUpdate) {

   if ( NULL == pParent -> pSignatureProcess || NULL == pParent -> pSignatureProcess -> SignatureBox() ) 
      return S_OK;

   PostMessage(hwndMainFrame,WM_REFRESH_SIGNATURE_BOX,0L,0L);

   return S_OK;
   }