// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <commctrl.h>

   long monitorIndex = -1L;
   HMONITOR hFoundMonitor = NULL;

   BOOL CALLBACK enumMonitors(HMONITOR hm,HDC,RECT *,LPARAM lp) {
   monitorIndex++;
   if ( (LPARAM)hm == lp )
      return FALSE;
   return TRUE;
   }

   BOOL CALLBACK enumFindMonitor(HMONITOR hm,HDC,RECT *,LPARAM lp) {
   monitorIndex++;
   if ( -1L == lp ) {
      MONITORINFO mi = {0};
      mi.cbSize = sizeof(MONITORINFO);
      GetMonitorInfo(hm,&mi);
      if ( mi.dwFlags & MONITORINFOF_PRIMARY ) {
         hFoundMonitor = hm;
         return FALSE;
      }
   }
   if ( monitorIndex == lp ) {
      hFoundMonitor = hm;
      return FALSE;
   }
   return TRUE;
   }


   HRESULT CursiVision::SavePrep() {

   if ( hwndMainFrame && ! IsIconic(hwndMainFrame) )
      GetWindowRect(hwndMainFrame,&rectFrame);

   WINDOWPLACEMENT windowPlacement = {0};

   windowPlacement.length = sizeof(WINDOWPLACEMENT);

   GetWindowPlacement(hwndMainFrame,&windowPlacement);

   wasMaximized = ! ( 0L == ( windowPlacement.flags & 0x0002 ) );

   memcpy(&rcMaximizedRestore,&windowPlacement.rcNormalPosition,sizeof(RECT));

   HMONITOR hMonitor = MonitorFromRect(&windowPlacement.rcNormalPosition,MONITOR_DEFAULTTOPRIMARY);

   monitorIndex = -1L;

   EnumDisplayMonitors(NULL,NULL,enumMonitors,(LPARAM)hMonitor);
   
   activeMonitor = monitorIndex;

   monitorCount = GetSystemMetrics(SM_CMONITORS);

   return S_OK;
   }


   HRESULT CursiVision::InitNew() {

   rectFrame.top = 50;
   rectFrame.left = 50;
   rectFrame.bottom = (long)(0.75 * (double)GetSystemMetrics(SM_CYSCREEN));
   rectFrame.right = (long)(8.5 * (double)rectFrame.bottom / 11.0);

   rectDoodleOptions.left = 128;
   rectDoodleOptions.top = 64;

   activeMonitor = -1L;

   return Loaded();
   }


   HRESULT CursiVision::Loaded() {

   hFoundMonitor = NULL;

   monitorIndex = -1L;

   EnumDisplayMonitors(NULL,NULL,enumFindMonitor,(LPARAM)activeMonitor);

   RECT rcScreen = {0};

   if ( hFoundMonitor ) {
      MONITORINFO monitorInfo = {0};
      monitorInfo.cbSize = sizeof(MONITORINFO);
      GetMonitorInfo(hFoundMonitor,&monitorInfo);
      rcScreen = monitorInfo.rcWork;
      activeMonitor = monitorIndex;
   } else
      SystemParametersInfo(SPI_GETWORKAREA,0,&rcScreen,0);

   long cxScreen = rcScreen.right - rcScreen.left;
   long cyScreen = rcScreen.bottom - rcScreen.top;
   double aspectRatio = (double)cxScreen / (double)cyScreen;
   long cx = rectFrame.right - rectFrame.left;
   long cy = rectFrame.bottom - rectFrame.top;

   if ( rectFrame.top < rcScreen.top )
      rectFrame.top = rcScreen.top + 8;

   if ( rectFrame.left < rcScreen.left )
      rectFrame.left = rcScreen.left + 8;

   double cxChange = (double)cx;
   while ( (rectFrame.top - rcScreen.top) + cy > cyScreen || (rectFrame.left - rcScreen.left) + cx > cxScreen ) {
      cy -= 1;
      cx = (long)((double)cx - aspectRatio);
   }

   cx = (long)cxChange;

   rectFrame.right = rectFrame.left + cx;
   rectFrame.bottom = rectFrame.top + cy;

   if ( wasMaximized ) {

      WINDOWPLACEMENT windowPlacement = {0};
      windowPlacement.length = sizeof(WINDOWPLACEMENT);
      windowPlacement.flags = WPF_RESTORETOMAXIMIZED;
      windowPlacement.showCmd = SW_SHOWMAXIMIZED;
      windowPlacement.ptMinPosition.x = -1;
      windowPlacement.ptMinPosition.y = -1;
      windowPlacement.ptMaxPosition.x = -1;
      windowPlacement.ptMaxPosition.y = -1;
      memcpy(&windowPlacement.rcNormalPosition,&rcMaximizedRestore,sizeof(RECT));
      SetWindowPlacement(hwndMainFrame,&windowPlacement);

   } else
   
      SetWindowPos(hwndMainFrame,HWND_TOP,rectFrame.left,rectFrame.top,rectFrame.right - rectFrame.left,rectFrame.bottom - rectFrame.top,SWP_SHOWWINDOW);

   if ( szActiveDocument[0] && ! isImmediateDoodle && processingDisposition.openLastDocument && isOkayToAutoOpen )
      if ( -1L == pCursiVision -> openDocument(szActiveDocument,1,true,false,false,false) )
         szActiveDocument[0] = '\0';

   isRestored = true;

   return S_OK;
   }

   HRESULT CursiVision::IsDirty() {
   return S_OK;
   }

   HRESULT CursiVision::GetClassID(BYTE *pCLSID) {
   memcpy(pCLSID,&CLSID_CursiVision,sizeof(GUID));
   return S_OK;
   }
