// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include "pdfEnablerReceiveText.h"

#include "PrintingProfileDefines.h"

   char szProfileInput[PROFILE_BUFFER_SIZE];
   BYTE profileText[PROFILE_BUFFER_SIZE];
   BYTE eol[] = {0x0A};

   long CursiVision::_ICursiVisionServices::generateOutlines(char *pszDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage) {

   long countEntries = 0L;

   setupPDFEnabler(pszDocument);

   long maxPageNumber = 1;

   pIPdfDocument -> get_PageCount(&maxPageNumber);

   if ( -1L != maxPDFPage )
      maxPageNumber = maxPDFPage;

   maxPageNumber = max(maxPageNumber,1);

   if ( 0 == startPage )
      startPage = 1;

   for ( long k = startPage - 1; k < maxPageNumber; k++ )
      countEntries += retrieveOrFindPDFFields(pszCurrentOutlinesFile,k + 1);

   closePDFEnabler();

   return countEntries;
   }


   bool CursiVision::_ICursiVisionServices::readOutlines(RECT **ppPDFEntries,long **ppPDFPages,
                                                         char *pszOriginalDocument,char *pszOutlinesFile,
                                                         char **ppszTextValues,long *pCountEntries,long *pCXPage,long *pCYPage) {

   FILE *fProfile = NULL;
   char szCurrentOutlinesFile[MAX_PATH];
   char szPrintedDocument[MAX_PATH];

   strcpy(szCurrentOutlinesFile,pszOutlinesFile);

   strcpy(szPrintedDocument,pszOriginalDocument);

   if ( pCountEntries )
      *pCountEntries = 0L;

   fProfile = fopen(szCurrentOutlinesFile,"rb");

   if ( ! fProfile )
      return false;

   char bIgnore[5];
   long pageNumber = -1L,pageEntries = 0L,nextPageOffset,cxPage,cyPage;

   char szPageInfo[OUTLINES_PAGE_RECORD_SIZE + 1];

   fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);

   if ( pCXPage )
      *pCXPage = cxPage;

   if ( pCYPage )
      *pCYPage = cyPage;

   long countEntries = 0L;
   long rc = 0L;

   do {

      countEntries += pageEntries;

      fseek(fProfile,nextPageOffset,SEEK_SET);

      rc = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);
      else
         break;

   } while ( 6 == rc && 0 != nextPageOffset );

   fclose(fProfile);

   *ppPDFEntries = new RECT[(countEntries + 1)];
   *ppPDFPages = new long[(countEntries + 1)];

   memset(*ppPDFEntries,0,(countEntries + 1) * sizeof(RECT));
   memset(*ppPDFPages,0,(countEntries + 1) * sizeof(long));

   char *pText = NULL;

   if ( ppszTextValues ) {
      *ppszTextValues = new char[(countEntries + 1) * 33];
      memset(*ppszTextValues,0,33 * (countEntries + 1) * sizeof(char));
      pText = *ppszTextValues;
   }

   if ( pCountEntries )
      *pCountEntries = countEntries;

   RECT *pPDF = *ppPDFEntries;
   long *pPage = *ppPDFPages;
   long countBytes;

   fProfile = fopen(szCurrentOutlinesFile,"rb");

   fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);

   do {

      for ( long k = 0; k < pageEntries; k++, pPDF++, pPage++ ) {

         fread(szProfileInput,1,OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE,fProfile);

         sscanf(szProfileInput,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,&pPDF -> left,&pPDF -> bottom,&pPDF -> right,&pPDF -> top,&countBytes);

         fread(szProfileInput,1,countBytes + 1,fProfile);

         if ( pText ) {
            strncpy(pText,szProfileInput,min(countBytes,32));
            pText += 33;
         }

         *pPage = pageNumber;

      }
   
      fseek(fProfile,nextPageOffset,SEEK_SET);

      rc = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);
      else
         break;

   } while ( 6 == rc && 0 != nextPageOffset );

   fclose(fProfile);

   return true;
   }


   long CursiVision::_ICursiVisionServices::convertPixelsToPDF(char *pszDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage) {

   setupPDFEnabler(pszDocument);

   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(startPage,NULL,&pIPdfPage);

   if ( ! pIPdfPage ) {
      closePDFEnabler();
      return 0L;
   }

   if ( -1L == maxPDFPage )
      pIPdfDocument -> get_PageCount(&maxPDFPage);

   RECT rcPDFPage = {0};
   RECT rcTwips = {0};

   pIPdfPage -> DisplayedPageSize(&rcPDFPage);

   double cxPDFPage = (double)(rcPDFPage.right - rcPDFPage.left);
   double cyPDFPage = (double)(rcPDFPage.bottom - rcPDFPage.top);

   char szTempFile[MAX_PATH];

   strcpy(szTempFile,_tempnam(NULL,NULL));

   FILE *fOutput = fopen(szTempFile,"w+b");

   FILE *fProfile = fopen(pszCurrentOutlinesFile,"rb");

   long pageNumber = -1L,pageEntries = 0L,nextPageOffset,cxPage,cyPage;
   char bIgnore[5];
   long currentOutputPageOffset,countBytes;

   char szPageInfo[OUTLINES_PAGE_RECORD_SIZE + 1];

   long rcx = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   long rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);

   do {

      double scaleX = cxPDFPage / (double)cxPage;
      double scaleY = cyPDFPage / (double)cyPage;

      fprintf(fOutput,"AUTO %04ld:%04ld-%04ld%06ld-",pageNumber,(long)cxPDFPage,(long)cyPDFPage,pageEntries);

      currentOutputPageOffset = ftell(fOutput);

      fprintf(fOutput,"%08ld\n",0);

      for ( long k = 0; k < pageEntries; k++ ) {

         fread(szProfileInput,1,OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE,fProfile);

         sscanf(szProfileInput,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,&rcTwips.left,&rcTwips.top,&rcTwips.right,&rcTwips.bottom,&countBytes);

         rcPDFPage.left = (long)((double)rcTwips.left * scaleX);
         rcPDFPage.right = (long)((double)rcTwips.right * scaleX);

         rcPDFPage.top = (long)(cyPDFPage - (double)rcTwips.top * scaleY);
         rcPDFPage.bottom = (long)(cyPDFPage - (double)rcTwips.bottom * scaleY);

         fread(profileText,countBytes + 1,1,fProfile);

         long cb = sprintf(szProfileInput,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,rcPDFPage.left,rcPDFPage.bottom,rcPDFPage.right,rcPDFPage.top,countBytes);

         fwrite(szProfileInput,cb,1,fOutput);

         fwrite(profileText,countBytes + 1,1,fOutput);

      }
   
      long keepOutputPageOffset = ftell(fOutput);

      fseek(fOutput,currentOutputPageOffset,SEEK_SET);

      fprintf(fOutput,"%08ld",keepOutputPageOffset);

      fseek(fOutput,keepOutputPageOffset,SEEK_SET);

      rc = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&pageEntries,&nextPageOffset);
      else
         break;

   } while ( 6 == rc && 0 != nextPageOffset );

   fclose(fOutput);

   fclose(fProfile);

   if ( ! DeleteFile(pszCurrentOutlinesFile) && ! ( 2 == GetLastError() ) ) {
      char szMessage[1024];
      sprintf(szMessage,"There was an error attempting to delete the file:\n\t%s\n\nPlease check your access rights to this directory.\n\nWindows reports the error code: %ld",pszCurrentOutlinesFile,GetLastError());
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
   }

   if ( ! CopyFile(szTempFile,pszCurrentOutlinesFile,TRUE) ) {
      char szMessage[1024];
      sprintf(szMessage,"There was an error attempting to copy to the file:\n\t%s\n\nPlease check your access rights to this directory.\n\nWindows reports the error code: %ld",pszCurrentOutlinesFile,GetLastError());
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION | MB_TOPMOST);
   }

   DeleteFile(szTempFile);

   closePDFEnabler();

   return 0L;
   }


   void CursiVision::_ICursiVisionServices::setupPDFEnabler(char *pszDocument) {

   closePDFEnabler();

   char szPDFEnablerDLL[MAX_PATH];

   GetModuleFileName(NULL,szPDFEnablerDLL,MAX_PATH);

   char *p = strrchr(szPDFEnablerDLL,'\\');
   if ( ! p )
      p = strrchr(szPDFEnablerDLL,'/');

   *p = '\0';

   sprintf(szPDFEnablerDLL + strlen(szPDFEnablerDLL),"\\PdfEnabler.dll");

   hModule_PDFEnabler = LoadLibrary(szPDFEnablerDLL);

   IClassFactory *pIClassFactory = NULL;
   long (__stdcall *cf)(REFCLSID rclsid, REFIID riid, void **ppObject) = NULL;
   cf = (long (__stdcall *)(REFCLSID rclsid, REFIID riid, void **ppObject))GetProcAddress(hModule_PDFEnabler,"DllGetClassObject");
   cf(CLSID_PdfEnabler,IID_IClassFactory,reinterpret_cast<void **>(&pIClassFactory)); 
   pIClassFactory -> CreateInstance(NULL,IID_IPdfEnabler,reinterpret_cast<void **>(&pIPdfEnabler));

   pIPdfEnabler -> Document(&pIPdfDocument);

   BSTR bstrDocument = SysAllocStringLen(NULL,(UINT)strlen(pszDocument));
   MultiByteToWideChar(CP_ACP,0,pszDocument,-1,bstrDocument,(int)strlen(pszDocument));

   pIPdfDocument -> Open(bstrDocument,NULL,NULL);

   SysFreeString(bstrDocument);

   return;
   }



   void CursiVision::_ICursiVisionServices::closePDFEnabler() {

   if ( hModule_PDFEnabler )
      FreeLibrary(hModule_PDFEnabler);

   hModule_PDFEnabler = NULL;

   pIPdfEnabler = NULL;
   pIPdfDocument = NULL;

   return;
   }



   long CursiVision::_ICursiVisionServices::retrieveOrFindPDFFields(char *pszTargetOutlinesFile,long currentPageNumber) {

#if 1

   return findPDFFields(pszTargetOutlinesFile,currentPageNumber);

#else

   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   if ( NULL == pIPdfPage ) 
      return 0L;

   BSTR bstrOutlines = SysAllocStringLen(NULL,MAX_PATH);
         
   wcscpy(bstrOutlines,_wtempnam(NULL,NULL));

   if ( S_OK != pIPdfPage -> GetBinaryObjectToFile(L"cvOutlines",bstrOutlines) ) {
      DeleteFileW(bstrOutlines);
      SysFreeString(bstrOutlines);
      return findPDFFields(pszTargetOutlinesFile,currentPageNumber);
   }

   char szTemp[MAX_PATH];

   WideCharToMultiByte(CP_ACP,0,bstrOutlines,-1,szTemp,MAX_PATH,0,0);

   if ( stricmp(pszTargetOutlinesFile,szTemp) ) {
      DeleteFile(pszTargetOutlinesFile);
      CopyFile(szTemp,pszTargetOutlinesFile,FALSE);
      DeleteFileW(bstrOutlines);
   }

   SysFreeString(bstrOutlines);

   FILE *fProfile = fopen(pszTargetOutlinesFile,"rb");

   long pageNumber,nextPageOffset,countEntries,cxPage,cyPage;
   BYTE bIgnore[5];

   char szPageInfo[OUTLINES_PAGE_RECORD_SIZE + 1];

   long rcx = fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   while ( ! ( pageNumber == currentPageNumber ) ) {
      fseek(fProfile,nextPageOffset,SEEK_SET);
      long rc = fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);
      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);
      else
         rc = 0;
      if ( rc < 6 ) {
         fclose(fProfile);
         return 0L;
      }
   }

   fclose(fProfile);

   return countEntries;
#endif
   }


   long CursiVision::_ICursiVisionServices::findPDFFields(char *pszTargetOutlinesFile,long currentPageNumber) {

   RECT rcPDFPage;

   IPdfPage *pIPdfPage = NULL;

   pIPdfDocument -> Page(currentPageNumber,NULL,&pIPdfPage);

   if ( NULL == pIPdfPage ) 
      return 0L;

   pIPdfPage -> DisplayedPageSize(&rcPDFPage);

   long cxPage = rcPDFPage.right - rcPDFPage.left;
   long cyPage = rcPDFPage.bottom - rcPDFPage.top;

   struct recieveText *pGetText = new recieveText(pszTargetOutlinesFile,currentPageNumber,cxPage,cyPage);

   HDC hdc = CreateCompatibleDC(NULL);

   HRESULT rc = pIPdfDocument -> ParseText(currentPageNumber,hdc,&rcPDFPage,reinterpret_cast<void *>(pGetText));

   DeleteDC(hdc);

   long countEntries = pGetText -> countFound;

   pGetText -> close();

   delete pGetText;

   if ( ! ( S_OK == rc ) ) {
      char *pszError;
      pIPdfDocument -> GetLastError(&pszError);
      MessageBox(NULL,pszError,"Error!",MB_ICONEXCLAMATION | MB_TOPMOST);
      countEntries = 0L;
   }

   return countEntries;
   }