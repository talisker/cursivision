// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   // IUnknown

   long __stdcall CursiVision::QueryInterface(REFIID riid,void **ppv) {

   if ( ! ppv )
      return E_POINTER;

   *ppv = NULL;

   if ( IID_IUnknown == riid )
      *ppv = static_cast<IUnknown *>(this);
   else

   if ( IID_IConnectionPointContainer == riid ) 
      *ppv = static_cast<IConnectionPointContainer *>(&connectionPointContainer);
   else

   if ( IID_IConnectionPoint == riid ) 
      *ppv = static_cast<IConnectionPoint *>(&connectionPoint);
   else

   if ( IID_IGPropertyPageClient == riid )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else

   if ( IID_IGPropertiesClient == riid )
      *ppv = static_cast<IGPropertiesClient *>(this);
   else

   if ( riid == DIID_DWebBrowserEvents2 )
      return pDWebBrowserEvents_HTML_Host -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IPDFiumControlEvents )
      return pIPDFiumControlEvents -> QueryInterface(riid,ppv);
   else

#if 0
   if ( IID_IElementBehaviorFactory == riid )
      return pIElementBehaviorFactory -> QueryInterface(riid,ppv);
   else

   if ( IID_IElementBehavior == riid )
      return pIElementBehavior -> QueryInterface(riid,ppv);
   else

   if ( IID_IHTMLPainter == riid ) 
      return pIHTMLPainter -> QueryInterface(riid,ppv);
   else
#endif

   if ( IID_IOleInPlaceFrame == riid ) 
      *ppv = static_cast<IOleInPlaceFrame *>(pIOleInPlaceFrame_HTML_Host);
   else

   if ( IID_IOleInPlaceSite == riid ) 
      *ppv = static_cast<IOleInPlaceSite *>(pIOleInPlaceSite_HTML_Host);
   else

   if ( IID_IOleInPlaceSiteEx == riid ) 
      *ppv = static_cast<IOleInPlaceSiteEx *>(pIOleInPlaceSite_HTML_Host);
   else

   if ( IID_IOleDocumentSite == riid ) 
      *ppv = static_cast<IOleDocumentSite *>(pIOleDocumentSite_HTML_Host);
   else

      return E_NOINTERFACE;

   AddRef();
   return S_OK;
   }
   unsigned long __stdcall CursiVision::AddRef() {
   return ++refCount;
   }
   unsigned long __stdcall CursiVision::Release() { 
   return --refCount;
   }