// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"


   long __stdcall CursiVision::_IGPropertyPageClient::QueryInterface(REFIID riid,void **ppv) {
   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown*>(this); 
   else

   if ( riid == IID_IDispatch )
      *ppv = this;
   else

   if ( riid == IID_IGPropertyPageClient )
      *ppv = static_cast<IGPropertyPageClient*>(this);
   else
 
      return pParent -> QueryInterface(riid,ppv);
 
   static_cast<IUnknown*>(*ppv) -> AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall CursiVision::_IGPropertyPageClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall CursiVision::_IGPropertyPageClient::Release() {
   return 1;
   }


   HRESULT CursiVision::_IGPropertyPageClient::BeforeAllPropertyPages() {
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::GetPropertyPagesInfo(long *pCntPages,SAFEARRAY** thePageNames,SAFEARRAY** theHelpDirs,SAFEARRAY** pSize) {
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::CreatePropertyPage(long pageNumber,HWND hwndParent,RECT* pRect,BOOL fModal,HWND *pHwnd) {
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::Apply() {
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::IsPageDirty(long pageNumber,BOOL* isDirty) {
   pParent -> pIGProperties -> Compare((VARIANT_BOOL*)isDirty);
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::Help(BSTR bstrHelpDir) {
   return  S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::TranslateAccelerator(long,long* pResult) {
   *pResult = S_FALSE;
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::AfterAllPropertyPages(BOOL userCanceled) {
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::DestroyPropertyPage(long index) {
   return S_OK;
   }



   HRESULT CursiVision::_IGPropertyPageClient::GetPropertySheetHeader(void *pv) {

   if ( ! pv )
      return E_POINTER;

   PROPSHEETHEADER *pHeader = reinterpret_cast<PROPSHEETHEADER *>(pv);

   pHeader -> dwFlags = PSH_PROPSHEETPAGE | PSH_NOCONTEXTHELP;
   pHeader -> hInstance = hModule;
   pHeader -> pszIcon = 0L;
   pHeader -> pszCaption = "CursiVision Setup";
   pHeader -> pfnCallback = NULL;

   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::get_PropertyPageCount(long *pCount) {
   if ( ! pCount )
      return E_POINTER;
   if ( isAdministrator )
      *pCount = 3L;
   else
      *pCount = 2L;
   return S_OK;
   }


   HRESULT CursiVision::_IGPropertyPageClient::GetPropertySheets(void *pPages) {

   if ( ! pPages )
      return E_POINTER;

   pParent -> currentProcessDisposition() -> pParent = pParent;

   PROPSHEETPAGE *pPropSheetPages = reinterpret_cast<PROPSHEETPAGE *>(pPages);

   pPropSheetPages[0].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[0].dwFlags = PSP_USETITLE;
   pPropSheetPages[0].hInstance = hModule;
   pPropSheetPages[0].pszTemplate = MAKEINTRESOURCE(IDD_DISPOSITION_PROPERTIES);
   pPropSheetPages[0].pfnDlgProc = (DLGPROC)CursiVision::dispositionSettingsHandler;
   pPropSheetPages[0].pszTitle = DISPOSITION_TITLE;
   pPropSheetPages[0].lParam = (LONG_PTR)pParent -> currentProcessDisposition();
   pPropSheetPages[0].pfnCallback = NULL;

   pPropSheetPages[1].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[1].dwFlags = PSP_USETITLE;
   pPropSheetPages[1].hInstance = hModule;
   pPropSheetPages[1].pszTemplate = MAKEINTRESOURCE(IDD_BACKENDS);
   pPropSheetPages[1].pfnDlgProc = (DLGPROC)CursiVision::additionalBackEndsHandler;
   pPropSheetPages[1].pszTitle = "Tool Box";
   pPropSheetPages[1].lParam = (LONG_PTR)pParent -> currentProcessDisposition();
   pPropSheetPages[1].pfnCallback = NULL;

   if ( isAdministrator ) {
      pPropSheetPages[2].dwSize = sizeof(PROPSHEETPAGE);
      pPropSheetPages[2].dwFlags = PSP_USETITLE;
      pPropSheetPages[2].hInstance = hModule;
      pPropSheetPages[2].pszTemplate = MAKEINTRESOURCE(IDD_GLOBAL_REPOSITORY);
      pPropSheetPages[2].pfnDlgProc = (DLGPROC)CursiVision::globalSettingsStoreHandler;
      pPropSheetPages[2].pszTitle = "Global Repository";
      pPropSheetPages[2].lParam = (LONG_PTR)pParent;
      pPropSheetPages[2].pfnCallback = NULL;
   }

   return S_OK;
   }