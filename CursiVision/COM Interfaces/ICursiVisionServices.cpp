// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include "PrintingProfileDefines.h"

   static char szProfileInput[2048];

   CursiVision::_ICursiVisionServices::_ICursiVisionServices(CursiVision *pp) : 
      pParent(pp),
      pIPdfEnabler(NULL),
      pIPdfDocument(NULL),
      hModule_PDFEnabler(NULL) {
   return;
   };

   HRESULT __stdcall CursiVision::_ICursiVisionServices::get_SignaturePad(ISignaturePad **ppResult) {
   if ( ! ppResult )
      return E_POINTER;
   if ( ! pParent -> pSignatureProcess )
      *ppResult = NULL;
   else
      *ppResult = pParent -> pSignatureProcess -> SignaturePad();
   return S_OK;
   }

   HRESULT __stdcall CursiVision::_ICursiVisionServices::get_ParentWindow(HWND *pHwnd) {
   if ( ! pHwnd )
      return E_POINTER;
   *pHwnd = hwndMainFrame;
   return S_OK;
   }

   HRESULT __stdcall CursiVision::_ICursiVisionServices::put_PrintingSupportProfile(IPrintingSupportProfile *psp) {
   if ( NULL == psp )
      pParent -> PrintingProfile((IPrintingSupportProfile *)0x00000001);
   else
      pParent -> PrintingProfile(psp);
   return S_OK;
   }

   HRESULT __stdcall CursiVision::_ICursiVisionServices::get_PrintingSupportProfile(IPrintingSupportProfile **ppsp) {
   if ( ! ppsp )
      return E_POINTER;
   *ppsp = pParent -> PrintingProfile();
   return S_OK;
   }


   HRESULT __stdcall CursiVision::_ICursiVisionServices::RegisterSignaturePad(BSTR bstrOCXName) {
#ifdef WM_REGISTER_PAD
   SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE_CANCEL,0),0L);
   static char szPad[MAX_PATH];
   WideCharToMultiByte(CP_ACP,0,bstrOCXName,-1,szPad,MAX_PATH,0,0);
   PostMessage(hwndMainFrame,WM_REGISTER_PAD,(WPARAM)szPad,0L);
   return S_OK;
#else
   return E_NOTIMPL;
#endif
   }


   long __stdcall CursiVision::_ICursiVisionServices::SavedSignatureGraphicCount() {

   char *pszFileName = NULL;

   if ( pParent -> PrintingProfile() )
      pszFileName = pParent -> PrintingProfile() -> SignatureGraphicFileName();
   else {
      char szSignatureGraphicsFile[MAX_PATH];
      strcpy(szSignatureGraphicsFile,pParent -> currentSettingsFileName());
      char *pszFileName = strrchr(szSignatureGraphicsFile,'.');
      strcpy(pszFileName,".signatures");
      pszFileName = szSignatureGraphicsFile;
   }

   if ( ! pszFileName )
      return 0L;

   long signatureCount = 0L;

   FILE *fSignatures = NULL;

   for ( long k = 0; k < 3; k++ ) {

      fSignatures = fopen(pszFileName,"rb");

      if ( ! fSignatures ) {
         Sleep(100);
         continue;
      }

      fread(&signatureCount,sizeof(long),1,fSignatures);

      fclose(fSignatures);

      return signatureCount;

   }

   return 0;
   }


   HRESULT __stdcall CursiVision::_ICursiVisionServices::SaveSignatureGraphic(void *pvSignatureData) {

   char *pszFileName = NULL;

   if ( pParent -> PrintingProfile() )
      pszFileName = pParent -> PrintingProfile() -> SignatureGraphicFileName();
   else {
      static char szSignatureGraphicsFile[MAX_PATH];
      strcpy(szSignatureGraphicsFile,pParent -> currentSettingsFileName());
      pszFileName = strrchr(szSignatureGraphicsFile,'.');
      if ( pszFileName )
         strcpy(pszFileName,".signatures");
      else
         sprintf(szSignatureGraphicsFile + strlen(szSignatureGraphicsFile),".signatures");
      pszFileName = szSignatureGraphicsFile;
   }

   if ( ! pszFileName )
      return E_UNEXPECTED;

   signatureGraphic *pSG = reinterpret_cast<signatureGraphic *>(pvSignatureData);

   FILE *fSignatures = fopen(pszFileName,"r+b");

   long signatureCount = 0L;

   if ( fSignatures ) {
      fread(&signatureCount,sizeof(long),1,fSignatures);
      fseek(fSignatures,0,SEEK_END);
   } else
      fSignatures = fopen(pszFileName,"w+b");

   signatureCount++;

   long currentPosition = ftell(fSignatures);

   fseek(fSignatures,0,SEEK_SET);

   fwrite(&signatureCount,sizeof(long),1,fSignatures);

   if ( currentPosition )
      fseek(fSignatures,currentPosition,SEEK_SET);
   else
      currentPosition = sizeof(long);

   long nextOffset = 0L;

   fwrite(&nextOffset,sizeof(long),1,fSignatures);

   double oldScaleX = pSG -> scaleToPDFX;
   double oldScaleY = pSG -> scaleToPDFY;
   long oldWindowsOriginX = pSG -> windowsOrigin.x;
   long oldWindowsOriginY = pSG -> windowsOrigin.y;
   long oldWindowsWidth = pSG -> windowsWidth;
   long oldWindowsHeight = pSG -> windowsHeight;
   long oldOffsetX = pSG -> offsetX;
   long oldOffsetY = pSG -> offsetY;

   pSG -> scaleToPDFX = 1.0;
   pSG -> scaleToPDFY = 1.0;
   pSG -> windowsOrigin.x = (long)((double)pSG -> windowsOrigin.x * oldScaleX);
   pSG -> windowsOrigin.y = (long)((double)pSG -> windowsOrigin.y * oldScaleY);
   pSG -> windowsWidth = (long)((double)pSG -> windowsWidth * oldScaleX);
   pSG -> windowsHeight = (long)((double)pSG -> windowsHeight * oldScaleY);
   pSG -> offsetX = (long)((double)pSG -> offsetX * oldScaleX);
   pSG -> offsetY = (long)((double)pSG -> offsetY * oldScaleY);

   fwrite(pSG,sizeof(signatureGraphic),1,fSignatures);

   for ( long k = 0; k < pSG -> totalPoints; k++ ) {
      fwrite(&pSG -> pSignatureDataPage[k],sizeof(long),1,fSignatures);
      double x = pSG -> pSignatureDataX[k];
      if ( 0.0 != x ) {
         x *= oldScaleX;
         x += pSG -> offsetX;
      }
      fwrite(&x,sizeof(double),1,fSignatures);
      double y = pSG -> pSignatureDataY[k];
      if ( 0.0 != y ) {
         y *= oldScaleY;
         y += pSG -> offsetY;
         y = pParent -> pdfPageHeight - y;
      }
      fwrite(&y,sizeof(double),1,fSignatures);
   }

   pSG -> scaleToPDFX = oldScaleX;
   pSG -> scaleToPDFY = oldScaleY;
   pSG -> windowsOrigin.x = oldWindowsOriginX;
   pSG -> windowsOrigin.y = oldWindowsOriginY;
   pSG -> windowsWidth = oldWindowsWidth;
   pSG -> windowsHeight = oldWindowsHeight;
   pSG -> offsetX = oldOffsetX;
   pSG -> offsetY = oldOffsetY;

   nextOffset = ftell(fSignatures);
   fseek(fSignatures,currentPosition,SEEK_SET);
   fwrite(&nextOffset,sizeof(long),1,fSignatures);

   fclose(fSignatures);

   return S_OK;

   }


   void CursiVision::_ICursiVisionServices::FreeSignatureGraphic(void *pvSignatureGraphic) {
   if ( ! pvSignatureGraphic ) 
      return; 
   delete reinterpret_cast<signatureGraphic *>(pvSignatureGraphic); 
   return;
   }


   void *CursiVision::_ICursiVisionServices::GetSignatureGraphic(long index) {

   char *pszFileName = NULL;

   if ( pParent -> PrintingProfile() )
      pszFileName = pParent -> PrintingProfile() -> SignatureGraphicFileName();
   else {
      char szSignatureGraphicsFile[MAX_PATH];
      strcpy(szSignatureGraphicsFile,pParent -> currentSettingsFileName());
      char *pszFileName = strrchr(szSignatureGraphicsFile,'.');
      strcpy(pszFileName,".signatures");
      pszFileName = szSignatureGraphicsFile;
   }

   if ( ! pszFileName )
      return NULL;

   FILE *fSignatures = fopen(pszFileName,"rb");

   if ( ! fSignatures )
      return NULL;

   long countSignatures = 0L;
   long nextOffset = 0L;

   fread(&countSignatures,sizeof(long),1,fSignatures);
   fread(&nextOffset,sizeof(long),1,fSignatures);

   signatureGraphic *pSG = new signatureGraphic();

   for ( long k = 0; k < index; k++ ) {
      fseek(fSignatures,nextOffset,SEEK_SET);
      fread(&nextOffset,sizeof(long),1,fSignatures);
   }

   fread(pSG,sizeof(signatureGraphic),1,fSignatures);

   pSG -> pSignatureDataX = new long[pSG -> totalPoints];
   pSG -> pSignatureDataY = new long[pSG -> totalPoints];
   pSG -> pSignatureDataPage = new long[pSG -> totalPoints];

   for ( long k = 0; k < pSG -> totalPoints; k++ ) {
      fread(&pSG -> pSignatureDataPage[k],sizeof(long),1,fSignatures);
      fread(&pSG -> pSignatureDataX[k],sizeof(double),1,fSignatures);
      fread(&pSG -> pSignatureDataY[k],sizeof(double),1,fSignatures);
   }

   fclose(fSignatures);

   return reinterpret_cast<void *>(pSG);
   }


   void CursiVision::_ICursiVisionServices::ResetTextOutlines() {
   char szOutlinesFileName[MAX_PATH];
   strcpy(szOutlinesFileName,pParent -> currentSettingsFileName());
   char *pc = strrchr(szOutlinesFileName,'.');
   strcpy(pc,".profile");
   DeleteFile(szOutlinesFileName);
   return;
   }


   void *CursiVision::_ICursiVisionServices::GetTextOutlines(long pageNumber,long *pReportedPageWidth,long *pReportedPageHeight) {

   if ( pParent -> PrintingProfile() )
      return pParent -> PrintingProfile() -> GetTextOutlines(pageNumber,pReportedPageWidth,pReportedPageHeight);

   char szDocument[MAX_PATH];
   char szOutlinesFileName[MAX_PATH];

   strcpy(szDocument,pParent -> szActiveDocument);
   strcpy(szOutlinesFileName,pParent -> currentSettingsFileName());

   char *pc = strrchr(szOutlinesFileName,'.');

   strcpy(pc,".profile");

   FILE *fProfile = fopen(szOutlinesFileName,"rb");

   if ( ! fProfile ) {

      if ( 0 == generateOutlines(szDocument,szOutlinesFileName,pageNumber,pageNumber) )
         return NULL;

      fProfile = fopen(szOutlinesFileName,"rb");

   }

   long pn,cxPage,cyPage,countEntries,nextPageOffset;
   char bIgnore[5];
   char szPageInfo[OUTLINES_PAGE_RECORD_SIZE + 1];

   long rc = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pn,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   while ( ! ( pn == pageNumber ) ) {

      fseek(fProfile,nextPageOffset,SEEK_SET);

      rc = (long)fread(szPageInfo,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      if ( OUTLINES_PAGE_RECORD_SIZE < rc )
         rc = sscanf(szPageInfo,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pn,&cxPage,&cyPage,&countEntries,&nextPageOffset);
      else
         rc = 0;

      if ( rc < 6 ) {
         fclose(fProfile);
         DeleteFile(szOutlinesFileName);
         return GetTextOutlines(pageNumber,pReportedPageWidth,pReportedPageHeight);
      }
      if ( 0 == nextPageOffset )
         break;
   }
   
   *pReportedPageWidth = cxPage;
   *pReportedPageHeight = cyPage;

   RECT *pRectangles = new RECT[countEntries + 1];
   
   memset(pRectangles,0,(countEntries + 1) * sizeof(RECT));

   RECT *p = pRectangles;

   char szTemp[2048];
   long countBytes;

   for ( long k = 0; k < countEntries; k++ ) {
      fgets(szTemp,2048,fProfile);
      sscanf(szTemp,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,&p -> left,&p -> bottom,&p -> right,&p -> top,&countBytes);
      p++;
   }

   fclose(fProfile);

   return reinterpret_cast<void *>(pRectangles);
   }


   void CursiVision::_ICursiVisionServices::FreeTextOutlines(void *pvRectangles) {
   if ( pParent -> PrintingProfile() ) {
      pParent -> PrintingProfile() -> FreeTextOutlines(pvRectangles);
      return;
   }
   RECT *pRects = reinterpret_cast<RECT *>(pvRectangles);
   delete [] pRects;
   return;
   }


   char *CursiVision::_ICursiVisionServices::DocumentName() {
   if ( pParent -> PrintingProfile() )
      return pParent -> PrintingProfile() -> DocumentName();
   return pParent -> szActiveDocument;
   }


   HRESULT CursiVision::_ICursiVisionServices::GenerateOutlines(char *pszDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage,long *pCount) {
   if ( ! pCount )
      return E_POINTER;
   *pCount = generateOutlines(pszDocument,pszCurrentOutlinesFile,startPage,maxPDFPage);
   if ( *pCount )
      return S_OK;
   return E_FAIL;
   }


   HRESULT CursiVision::_ICursiVisionServices::ConvertPixelsToPDF(char *pszDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage) {
   convertPixelsToPDF(pszDocument,pszCurrentOutlinesFile,startPage,maxPDFPage);
   return S_OK;
   }


   long __stdcall CursiVision::_ICursiVisionServices::FieldCount() {
   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();
   if ( ! pDoodleOptions ) 
      return 0L;
   long rc = 0L; 
   for ( long k = 0; k < MAX_TEXT_RECT_COUNT; k++ ) {
      if ( ! pDoodleOptions -> dataFieldLabels[k][0] )
         break;
      rc++;
   }
   return rc;
   }


   char * __stdcall CursiVision::_ICursiVisionServices::FieldLabels() { 
   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();
   if ( ! pDoodleOptions ) 
      return NULL;
   return &pDoodleOptions -> dataFieldLabels[0][0];
   }


   char * __stdcall CursiVision::_ICursiVisionServices::FieldValue(long index) {

   if ( index > MAX_TEXT_RECT_COUNT )
      return NULL;

   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();

   if ( ! pDoodleOptions ) 
      return NULL;

   if ( 0 == pDoodleOptions -> dataFields[index].left && 0 == pDoodleOptions -> dataFields[index].right )
      return NULL;

   char szOutlinesFileName[MAX_PATH];

   long countEntries = 0L;

   if ( pDoodleOptions -> isPrintingProfileDoodleOptions )

      strcpy(szOutlinesFileName,pParent -> PrintingProfile() -> OutlinesFileName());

   else {

      strcpy(szOutlinesFileName,_tempnam(NULL,NULL));

      long maxPageNumber = -1;

      for ( long k = 0; k < pDoodleOptions -> countDataFields; k++ ) 
         maxPageNumber = max(maxPageNumber,pDoodleOptions -> dataFieldPage[k]);

      countEntries = generateOutlines(pParent -> szActiveDocument,szOutlinesFileName,1,maxPageNumber);

      if ( 0 == countEntries ) {
         DeleteFile(szOutlinesFileName);
         return NULL;
      }

   }

   FILE *fProfile = fopen(szOutlinesFileName,"rb");

   if ( ! fProfile ) {
      if ( ! pDoodleOptions -> isPrintingProfileDoodleOptions )
         DeleteFile(szOutlinesFileName);
      return NULL;
   }

   long pageNumber,nextPageOffset,cxPage,cyPage;
   char bIgnore[5];
   char szProfile[OUTLINES_PAGE_RECORD_SIZE + 2];

   fread(szProfile,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

   long fieldsRead = sscanf(szProfile,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   static char szResult[2048];
   char szCompositeText[2048];

   memset(szResult,0,sizeof(szResult));
   memset(szCompositeText,0,sizeof(szCompositeText));

   RECT *expectedFields = pDoodleOptions -> dataFields;

   long *expectedFieldRequired = pDoodleOptions -> dataFieldRequired;

   bool found = false;

   long slop[] = {4, 6, 8, 0, 0};

   long slopIndex = 0;

   long countBytes = 0L;

   while ( slop[slopIndex] ) {

      do {

         if ( pageNumber == pDoodleOptions -> dataFieldPage[index] ) {

            for ( long k = 0; k < countEntries; k++ ) {

               fgets(szProfileInput,PROFILE_BUFFER_SIZE,fProfile);
         
               RECT rcEntry;
         
               sscanf(szProfileInput,OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT,&rcEntry.left,&rcEntry.bottom,&rcEntry.right,&rcEntry.top,&countBytes);
         
               ASCIIHexDecodeInPlace(&szProfileInput[OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE]);

               if ( rcEntry.left >= expectedFields[index].left && rcEntry.right <= expectedFields[index].right &&
                        rcEntry.top <= expectedFields[index].top && rcEntry.bottom >= expectedFields[index].bottom ) {

                  long n = max(0,2047 - (long)strlen(szCompositeText));

                  if ( n > 0 ) {
                     strncpy(szCompositeText + (long)strlen(szCompositeText),&szProfileInput[OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE],n);
                     szCompositeText[strlen(szCompositeText)] = ' ';
                  }

                  n = (long)strlen(szCompositeText);
                  if ( n ) {
                     char *p = &szCompositeText[n - 1];
                     n = 0;
                     while ( p > szCompositeText && ( ' ' == *p || 0x0A == *p || 0x0D == *p ) ) {
                        *p = '\0';
                        n++;
                        p--;
                     }
                     if ( n )
                        *(p + 1) = ' ';
                  }

                  found = true;

                  continue;
         
               } else {
         
                  if ( abs(rcEntry.left - expectedFields[index].left) > PDF_RECT_LEFT_SLOP )
                     continue;
         
                  if ( abs(rcEntry.top - expectedFields[index].top) > slop[slopIndex] )
                     continue;
         
                  if ( abs(rcEntry.bottom - expectedFields[index].bottom) > slop[slopIndex] )
                     continue;
         
               }
         
               strcpy(szResult + strlen(szResult),&szProfileInput[OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE]);
         
               szResult[strlen(szResult)] = ' ';
         
               found = true;
         
            }
         
         }
            
         if ( found )
            break;
         
         if ( 0 == nextPageOffset )
            break;
         
         fseek(fProfile,nextPageOffset,SEEK_SET);
         
         long rc = (long)fread(szProfile,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);
         
         if ( OUTLINES_PAGE_RECORD_SIZE < rc )
            fieldsRead = sscanf(szProfile,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);
         else
            break;
         
      } while ( 6 == fieldsRead );
         
      if ( found ) 
         break;

      slopIndex++;
         
      fseek(fProfile,0,SEEK_SET);

      fread(szProfile,1,OUTLINES_PAGE_RECORD_SIZE + 1,fProfile);

      sscanf(szProfile,OUTLINES_PAGE_RECORD_FORMAT,bIgnore,&pageNumber,&cxPage,&cyPage,&countEntries,&nextPageOffset);

   }

   fclose(fProfile);

   if ( ! found && expectedFieldRequired[index] ) {
      char szMessage[1024];
      sprintf(szMessage,"CursiVision did not find the required field: %s in the document.\n\nPlease double check the fields. Use drag-select to make the field bigger or specify the field as \"not required\".",pDoodleOptions -> dataFieldLabels[index]);
      MessageBox(NULL,szMessage,"Error",MB_OK | MB_TOPMOST);
      if ( ! pDoodleOptions -> isPrintingProfileDoodleOptions )
         DeleteFile(szOutlinesFileName);
      return NULL;
   }

   if ( szCompositeText[0] )
      strcpy(szResult,szCompositeText);

   char *p = szResult + strlen(szResult) - 1;
   while ( p > szResult && ' ' == *p ) {
      *p = '\0';
      p--;
   }

   p = szResult;

   while ( * p ) {
      if ( 0x0A == *p || 0x0D == *p ) 
         *p = ' ';
      p++;
   }

   if ( ! pDoodleOptions -> isPrintingProfileDoodleOptions )
      DeleteFile(szOutlinesFileName);

   return szResult;
   }


   char * __stdcall CursiVision::_ICursiVisionServices::FieldValueFromLabel(char *pszLabel) {
   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();
   if ( ! pDoodleOptions ) 
      return NULL;
   for ( long k = 0; k < MAX_TEXT_RECT_COUNT; k++ ) {
      if ( ! pDoodleOptions -> dataFieldLabels[k][0] ) 
         return NULL;
      if ( 0 == _stricmp(pDoodleOptions -> dataFieldLabels[k],pszLabel) ) 
         return FieldValue(k);
   }
   return NULL;
   }


   long __stdcall CursiVision::_ICursiVisionServices::SignatureLocationCount() {
   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();
   if ( ! pDoodleOptions ) 
      return 0L;
   long rc = 0L; 
   for ( long k = 0; k < MAX_DOODLE_RECT_COUNT; k++ ) {
      if ( pDoodleOptions -> theLocations[k].documentRect.left == pDoodleOptions -> theLocations[k].documentRect.right )
         break;
      rc++;
   }
   return rc;
   }

   oleWritingLocation * __stdcall CursiVision::_ICursiVisionServices::SignatureLocations() {
   doodleOptionProperties *pDoodleOptions = pParent -> currentDoodleOptionProperties();
   if ( ! pDoodleOptions ) 
      return NULL;
   return reinterpret_cast<oleWritingLocation *>(pDoodleOptions -> theLocations);
   }