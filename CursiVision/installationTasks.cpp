#include "CursiVision.h"

   extern "C" int registerServer() {

   ICatRegister *pICatRegister;

   HRESULT rc = CoCreateInstance(CLSID_StdComponentCategoriesMgr,NULL,CLSCTX_ALL,IID_ICatRegister,reinterpret_cast<void **>(&pICatRegister));

   CATEGORYINFO catInfo;

   catInfo.catid = IID_ICursiVisionBackEnd;
   catInfo.lcid = 0x0409;
   wcscpy(catInfo.szDescription,L"Objects that implement the CursiVision Processing Tool interface");

   pICatRegister -> RegisterCategories(1,&catInfo);

   pICatRegister -> Release();

   char *OBJECT_NAME;
   char *OBJECT_NAME_V;
   char *OBJECT_VERSION;
   GUID OBJECT_CLSID;
   GUID OBJECT_LIBID;
   char *OBJECT_DESCRIPTION;

   OBJECT_NAME = "InnoVisioNate.CursiVision";
   OBJECT_NAME_V = "InnoVisioNate.CursiVision.1";
   OBJECT_VERSION = "1.0";
   memcpy(&OBJECT_CLSID,&CLSID_CursiVision,sizeof(GUID));
   memcpy(&OBJECT_LIBID,&LIBID_CursiVision,sizeof(GUID));
   OBJECT_DESCRIPTION = "InnoVisioNate CursiVision Processor";

   ITypeLib *ptLib;
   HKEY keyHandle,clsidHandle;
   DWORD disposition;
   char szTemp[256],szCLSID[256];
   LPOLESTR oleString;
  
   StringFromCLSID(OBJECT_CLSID,&oleString);
   WideCharToMultiByte(CP_ACP,0,oleString,-1,szCLSID,256,0,0);
  
   if ( S_OK != LoadTypeLib(wstrModuleName,&ptLib) )
      rc = ResultFromScode(SELFREG_E_TYPELIB);
   else
      if ( S_OK != RegisterTypeLib(ptLib,wstrModuleName,NULL) )
         rc = ResultFromScode(SELFREG_E_TYPELIB);

   RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_CREATE_SUB_KEY,&keyHandle);
  
      RegCreateKeyEx(keyHandle,szCLSID,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&clsidHandle,&disposition);
      sprintf(szTemp,OBJECT_DESCRIPTION);
      RegSetValueEx(clsidHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"Control");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"ProgID");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"InprocServer");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szApplicationName,(DWORD)strlen(szApplicationName));
  
      sprintf(szTemp,"InprocServer32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szApplicationName,(DWORD)strlen(szApplicationName));
//      RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Free",5);
      RegSetValueEx(keyHandle,"ThreadingModel",0,REG_SZ,(BYTE *)"Apartment",9);
  
      sprintf(szTemp,"LocalServer");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szApplicationName,(DWORD)strlen(szApplicationName));
    
      sprintf(szTemp,"TypeLib");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
    
      StringFromCLSID(OBJECT_LIBID,&oleString);
      WideCharToMultiByte(CP_ACP,0,oleString,-1,szTemp,256,0,0);//W2A(oleString,szTemp,256);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
        
      sprintf(szTemp,"ToolboxBitmap32");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
//      sprintf(szTemp,"%s, 1",szModuleName);
//      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,strlen(szModuleName));
  
      sprintf(szTemp,"Version");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_VERSION);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"MiscStatus");
      RegCreateKeyEx(clsidHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"0");
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
      sprintf(szTemp,"1");
      RegCreateKeyEx(keyHandle,szTemp,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,"%ld",
                 OLEMISC_ALWAYSRUN |
                 OLEMISC_ACTIVATEWHENVISIBLE | 
                 OLEMISC_RECOMPOSEONRESIZE | 
                 OLEMISC_INSIDEOUT |
                 OLEMISC_SETCLIENTSITEFIRST |
                 OLEMISC_CANTLINKINSIDE );
//sprintf(szTemp,"%ld",131473L);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,"CurVer",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      sprintf(szTemp,OBJECT_NAME_V);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szTemp,(DWORD)strlen(szTemp));
  
   RegCreateKeyEx(HKEY_CLASSES_ROOT,OBJECT_NAME_V,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegCreateKeyEx(keyHandle,"CLSID",0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&keyHandle,&disposition);
      RegSetValueEx(keyHandle,NULL,0,REG_SZ,(BYTE *)szCLSID,(DWORD)strlen(szCLSID));
  
   return 0;
   }


   extern "C" int unRegisterServer() {
   return 0;
   }


   extern "C" int registerAll(char *pszDirectory) {

   char szTemp[MAX_PATH];
   char szOldDirectory[MAX_PATH];

   GetCurrentDirectory(MAX_PATH,szOldDirectory);

   SetCurrentDirectory(pszDirectory);

   WIN32_FIND_DATA wfd;
   
   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   sprintf(szTemp,"%s\\*.ocx",pszDirectory);

   HANDLE hFiles = FindFirstFile(szTemp,&wfd);
   
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {

      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);

      HMODULE hModule = LoadLibrary(szTemp);

      if ( ! hModule ) 
         continue;

      long (__stdcall *r)() = (long (__stdcall *)())GetProcAddress(hModule,"DllRegisterServer");

      if ( ! r )
         continue;

      (r)();

      FreeLibrary(hModule);

   } while ( FindNextFile(hFiles,&wfd) );

   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   sprintf(szTemp,"%s\\*.dll",pszDirectory);

   hFiles = FindFirstFile(szTemp,&wfd);
   
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {

      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);

      HMODULE hModule = LoadLibrary(szTemp);

      if ( ! hModule ) 
         continue;

      long (__stdcall *r)() = (long (__stdcall *)())GetProcAddress(hModule,"DllRegisterServer");

      if ( ! r )
         continue;

      (r)();

      FreeLibrary(hModule);

   } while ( FindNextFile(hFiles,&wfd) );

   SetCurrentDirectory(szOldDirectory);

   return 1L;
   }


   extern "C" int unRegisterAll(char *pszDirectory) {

   char szTemp[MAX_PATH];

   char szOldDirectory[MAX_PATH];

   GetCurrentDirectory(MAX_PATH,szOldDirectory);

   SetCurrentDirectory(pszDirectory);

   WIN32_FIND_DATA wfd;
   
   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   sprintf(szTemp,"%s\\*.ocx",pszDirectory);

   HANDLE hFiles = FindFirstFile(szTemp,&wfd);
   
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {

      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);

      HMODULE hModule = LoadLibrary(szTemp);

      if ( ! hModule ) 
         continue;

      long (__stdcall *r)() = (long (__stdcall *)())GetProcAddress(hModule,"DllUnregisterServer");

      if ( r ) 
         (r)();

      FreeLibrary(hModule);

   } while ( FindNextFile(hFiles,&wfd) );

   sprintf(szTemp,"%s\\*.dll",pszDirectory);

   hFiles = FindFirstFile(szTemp,&wfd);
   
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {

      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);

      HMODULE hModule = LoadLibrary(szTemp);

      if ( ! hModule ) 
         continue;

      long (__stdcall *r)() = (long (__stdcall *)())GetProcAddress(hModule,"DllUnregisterServer");

      if ( r )
         (r)();

      FreeLibrary(hModule);

   } while ( FindNextFile(hFiles,&wfd) );

   sprintf(szTemp,"%s\\*.ocx",pszDirectory);
   hFiles = FindFirstFile(szTemp,&wfd);
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {
      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);
      BOOL b = DeleteFile(szTemp);
   } while ( FindNextFile(hFiles,&wfd) );


   sprintf(szTemp,"%s\\*.dll",pszDirectory);
   hFiles = FindFirstFile(szTemp,&wfd);
   if ( ! ( INVALID_HANDLE_VALUE == hFiles ) ) do {
      sprintf(szTemp,"%s\\%s",pszDirectory,wfd.cFileName);
      BOOL b = DeleteFile(szTemp);
   } while ( FindNextFile(hFiles,&wfd) );

   HANDLE hPrinter = NULL;

   PRINTER_DEFAULTS pDefaults = { NULL, NULL, PRINTER_ALL_ACCESS };

   BOOL b = OpenPrinter("CursiVision Printer",&hPrinter,&pDefaults);

   if ( hPrinter )
      b = DeletePrinter(hPrinter);

   b = DeletePrinterDriverEx(NULL,NULL,"CursiVision Printer",DPD_DELETE_UNUSED_FILES,0);

   ClosePrinter(hPrinter);

   return 1L;
   }

   extern "C" long installPrinter(char *pszInstallationDirectory) {

   char szInstallDirectory[MAX_PATH];
   char szPrintDriverDirectory[MAX_PATH];

   DWORD cb = MAX_PATH;

   GetPrinterDriverDirectory(NULL,NULL,1,(BYTE *)szPrintDriverDirectory,cb,&cb);

   sprintf(szInstallDirectory,"%s\\Print Driver",pszInstallationDirectory);

   char szArch[32];

#ifdef _WIN64
   strcpy(szArch,"amd64");
#else
   strcpy(szArch,"i386");
#endif

   char szDependencies[] = "CursiVisionPrinter.dll\0CursiVisionPrinter.ini\0PSCRIPT.NTF\0PS_SCHM.GDL\0\0";

   char szTarget[MAX_PATH];
   char szSource[MAX_PATH];

   char szChild[] = {"\\"};

   sprintf(szSource,"%s\\CursiVision.ppd",szInstallDirectory);
   sprintf(szTarget,"%s\\%sCursiVision.ppd",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\%s\\CursiVisionPrinter.dll",szInstallDirectory,szArch);
   sprintf(szTarget,"%s\\%sCursiVisionPrinter.dll",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\CursiVisionPrinter.ini",szInstallDirectory);
   sprintf(szTarget,"%s\\%sCursiVisionPrinter.ini",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\%s\\PSCRIPT5.DLL",szInstallDirectory,szArch);
   sprintf(szTarget,"%s\\%sPSCRIPT5.DLL",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\%s\\PS5UI.DLL",szInstallDirectory,szArch);
   sprintf(szTarget,"%s\\%sPS5UI.DLL",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\PS_SCHM.GDL",szInstallDirectory);
   sprintf(szTarget,"%s\\%sPS_SCHM.GDL",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\PSCRIPT.HLP",szInstallDirectory);
   sprintf(szTarget,"%s\\%sPSCRIPT.HLP",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   sprintf(szSource,"%s\\PSCRIPT.NTF",szInstallDirectory);
   sprintf(szTarget,"%s\\%sPSCRIPT.NTF",szPrintDriverDirectory,szChild);
   CopyFile(szSource,szTarget,FALSE);

   DRIVER_INFO_6 driverInfo = {0};

   driverInfo.cVersion = 3;
   driverInfo.pName = "CursiVision Printer";
   driverInfo.pEnvironment = NULL;
   driverInfo.pDriverPath = "PSCRIPT5.DLL";
   driverInfo.pDataFile = "CursiVision.ppd";
   driverInfo.pConfigFile = "PS5UI.DLL";
   driverInfo.pHelpFile = "PSCRIPT.HLP";
   driverInfo.pDependentFiles = szDependencies;
   driverInfo.pMonitorName = NULL;
   driverInfo.pDefaultDataType = NULL;
   driverInfo.pszMfgName = "CursiVision";
   driverInfo.pszHardwareID = "cursivision_pdf";
   driverInfo.pszProvider = "InnoVisioNate Inc.";
   
   BOOL wasSuccessful = AddPrinterDriverEx(NULL,6,(BYTE *)&driverInfo,APD_COPY_ALL_FILES);

   if ( ! wasSuccessful ) {
#if 0
      char szMessage[1024];
      sprintf(szMessage,"The CursiVision installer was not able to setup the CursiVision\r"
"print driver because of an error.\r\rThe error code provided is: %ld\r\r"
"A document is on your desktop that provides instructions for manually installing the CursiVision printer.\r\r",GetLastError());
      MessageBox(NULL,szMessage,"Note",MB_ICONEXCLAMATION); 
#endif
      return GetLastError();
   }

   PRINTER_DEFAULTS def = {0};
   def.DesiredAccess = SERVER_ACCESS_ADMINISTER;
   
   DWORD dwStatus,dwNeeded;
   char xcvResults[256];

   cb = (DWORD)wcslen(L"CursiVision");

   HANDLE hPrinter = NULL;

   wasSuccessful = OpenPrinter(",XcvMonitor Local Port",&hPrinter,&def);
   if ( ! wasSuccessful )
      return GetLastError();

   wasSuccessful = XcvData(hPrinter,L"AddPort",(BYTE *)L"CursiVision",sizeof(L"CursiVision"),(BYTE *)xcvResults,256,&dwNeeded,&dwStatus);

   if ( ! wasSuccessful )
     return GetLastError();

   ClosePrinter(hPrinter);

   PRINTER_INFO_2 printerInfo = {0};

   printerInfo.pServerName = NULL;
   printerInfo.pPrinterName = "CursiVision Printer";
   printerInfo.pShareName = NULL;
   printerInfo.pPortName = "CursiVision";
   printerInfo.pDriverName = "CursiVision Printer";
   printerInfo.pPrintProcessor = "WinPrint";

   hPrinter = AddPrinter(NULL,2,(BYTE *)&printerInfo);

   if ( hPrinter ) {
      ClosePrinter(hPrinter);
      return 0L;
   }

   if ( 1802 == GetLastError() )
      return 0L;

	return GetLastError();
   }


#include "GrantFullAccess.cpp"


   extern "C" int fixupLinks(char *pszDirectory,char *pszVersionSpec) {

   char szTemp[MAX_PATH];

   char szOldDirectory[MAX_PATH];

   GetCurrentDirectory(MAX_PATH,szOldDirectory);

   SetCurrentDirectory(pszDirectory);

   char ppszTargetStrings[2][64];
   char ppszReplacementStrings[2][64];

   strcpy(ppszTargetStrings[0],"<data directory>");
   strcpy(ppszTargetStrings[1],"<version>");

   strcpy(ppszReplacementStrings[0],pszDirectory);
   strcpy(ppszReplacementStrings[1],pszVersionSpec);

   WIN32_FIND_DATA wfd;
   
   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   sprintf(szTemp,"%s\\*.htm",pszDirectory);

   HANDLE hFiles = FindFirstFile(szTemp,&wfd);
   
   if ( INVALID_HANDLE_VALUE != hFiles ) do {

      for ( long k = 0; k < 2; k++ ) {

         FILE *fHTML = fopen(wfd.cFileName,"rb");
         
         fseek(fHTML,0,SEEK_END);
      
         long nBytes = ftell(fHTML);

         fseek(fHTML,0,SEEK_SET);

         char *pData = new char[nBytes];

         fread(pData,1,nBytes,fHTML);

         fclose(fHTML);

         fHTML = fopen(wfd.cFileName,"wb");

         char *pStart = pData;

         char *p = pStart;

         while ( p = strstr(p,ppszTargetStrings[k]) ) {
            fwrite(pData,1,p - (char *)pData,fHTML);
            fwrite(ppszReplacementStrings[k],1,strlen(ppszReplacementStrings[k]),fHTML);
            p += strlen(ppszTargetStrings[k]);
            pData = p;
         }
               
         fwrite(pData,1,nBytes - (pData - pStart),fHTML);

         fclose(fHTML);

         delete [] pStart;

      }

   } while ( FindNextFile(hFiles,&wfd) );

   SetCurrentDirectory(szOldDirectory);

   return 0;
   }