// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <winVer.h>

   LRESULT CALLBACK CursiVision::optionsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   CursiVision *p = (CursiVision *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      SetWindowLongPtr(hwnd,GWLP_USERDATA,lParam);

      p = (CursiVision *)lParam;

      RECT rcParent,rcThis;

      GetWindowRect(GetParent(hwnd),&rcParent);

      GetWindowRect(hwnd,&rcThis);

      SetWindowPos(hwnd,HWND_TOP,rcParent.left + (rcParent.right - rcParent.left) / 2 - (rcThis.right - rcThis.left) / 2,
                                 rcParent.top + (rcParent.bottom - rcParent.top) / 2 - (rcThis.bottom - rcThis.top) / 2,0,0,SWP_NOSIZE);

      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

      SendMessage(GetDlgItem(hwnd,IDDI_OPTIONS_TEXT),WM_SETFONT,(WPARAM)hGUIFont,(LPARAM)TRUE);

      SetDlgItemText(hwnd,IDDI_OPTIONS_TEXT,p -> szOptionText);
      char *po = p -> szOptions;
      long k = IDDI_OPTIONS_OPTION_1;
      RECT rcButton;
      GetWindowRect(GetDlgItem(hwnd,IDDI_OPTIONS_OPTION_1),&rcButton);
      while ( *po ) {
         SetDlgItemText(hwnd,k,po);
         po += strlen(po);
         po++;
         SetWindowPos(GetDlgItem(hwnd,k),HWND_TOP,0,0,p -> optionWidths[k - IDDI_OPTIONS_OPTION_1],p -> optionHeight,SWP_NOMOVE);
         SendMessage(GetDlgItem(hwnd,k),WM_SETFONT,(WPARAM)hGUIFont,(LPARAM)TRUE);
         k++;
      }

      SetWindowPos(hwnd,HWND_TOP,0,0,p -> optionPaneWidth,p -> optionPaneHeight,SWP_NOMOVE);

      SetFocus(GetDlgItem(hwnd,p -> defaultOption));

      }
      return (LRESULT)FALSE;

   case WM_SIZE: {
      RECT rcThis,rcButton;
      GetWindowRect(GetDlgItem(hwnd,IDDI_OPTIONS_OPTION_1),&rcButton);
      GetWindowRect(hwnd,&rcThis);
      long cx = LOWORD(lParam);
      SetWindowPos(GetDlgItem(hwnd,IDDI_OPTIONS_OPTION_1),HWND_TOP,cx / 4 - p -> optionWidths[0] / 2,rcThis.bottom - rcThis.top - 3 * p -> optionHeight,0,0,SWP_NOSIZE);
      SetWindowPos(GetDlgItem(hwnd,IDDI_OPTIONS_OPTION_2),HWND_TOP,cx / 2 - p -> optionWidths[1] / 2,rcThis.bottom - rcThis.top - 3 * p -> optionHeight,0,0,SWP_NOSIZE);
      SetWindowPos(GetDlgItem(hwnd,IDDI_OPTIONS_OPTION_3),HWND_TOP,3 * cx / 4 - p -> optionWidths[2] / 2,rcThis.bottom - rcThis.top - 3 * p -> optionHeight,0,0,SWP_NOSIZE);
      SetWindowPos(GetDlgItem(hwnd,IDDI_OPTIONS_TEXT),HWND_TOP,32,32,rcThis.right - rcThis.left - 32,rcThis.bottom - rcThis.top - 30,0L);
      }
      break;

   case WM_COMMAND: {

      if ( ! p )
         break;

      switch ( LOWORD(wParam) ) {

      case IDDI_OPTIONS_OPTION_1:
      case IDDI_OPTIONS_OPTION_2:
      case IDDI_OPTIONS_OPTION_3:
         p -> optionChoice = LOWORD(wParam);
         EndDialog(hwnd,LOWORD(wParam));
         break;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }


