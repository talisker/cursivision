// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <winVer.h>

   LRESULT CALLBACK CursiVision::displayWaitingHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_INITDIALOG: {

      RECT rcDoodleOptions;
      GetWindowRect(hwndSigningControl,&rcDoodleOptions);
      SetWindowPos(hwnd,HWND_TOP,rcDoodleOptions.left,rcDoodleOptions.top,0,0,SWP_NOSIZE);

      HDC hdc = GetDC(hwndHTMLHost);
      HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
      HPEN oldPen = (HPEN)SelectObject(hdc,hPen);

      SetROP2(hdc,R2_XORPEN);

      MoveToEx(hdc,mouseStart.x,mouseStart.y,NULL);
      LineTo(hdc,mouseStart.x,mouseEnd.y);
      LineTo(hdc,mouseEnd.x,mouseEnd.y);
      LineTo(hdc,mouseEnd.x,mouseStart.y);
      LineTo(hdc,mouseStart.x,mouseStart.y);

      DeleteObject(SelectObject(hdc,oldPen));

      ReleaseDC(hwndHTMLHost,hdc);

      SetFocus(hwndMainFrame);

      }
      return (LRESULT)FALSE;

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_DISPLAY_WAITING_SHOW:
         Sleep(500);
         pCursiVision -> adviseWaiting = false;
         pCursiVision -> SaveProperties();
         EndDialog(hwnd,LOWORD(wParam));
         break;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }