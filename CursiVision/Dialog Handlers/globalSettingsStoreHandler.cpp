// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <shlobj.h>

   LRESULT CALLBACK CursiVision::globalSettingsStoreHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_INITDIALOG: {
      SetDlgItemText(hwnd,IDDI_GLOBAL_REPOSITORY,szGlobalDataStore);
      if ( ! isAdministrator ) {
         EnableWindow(hwnd,FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_GLOBAL_REPOSITORY),FALSE);
         EnableWindow(GetDlgItem(hwnd,IDDI_GET_GLOBAL_REPOSITORY),FALSE);
      }
      }
      break;


   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_GET_GLOBAL_REPOSITORY: {

         BROWSEINFO bInfo = {0};

         bInfo.hwndOwner = hwnd;
         
         char file[MAX_PATH];
         memset(file,0,MAX_PATH);

         bInfo.hwndOwner = hwnd;
         bInfo.pszDisplayName = file;
         bInfo.lpszTitle = "Specify the location of your data store. This location must be visible to all CursiVision clients using the data store";
         bInfo.ulFlags = BIF_NEWDIALOGSTYLE;
         ITEMIDLIST *pPIDL = (ITEMIDLIST *)SHBrowseForFolder(&bInfo);

         if ( file[0] ) {
            SHGetPathFromIDList(pPIDL,file);
            SetDlgItemText(hwnd,IDDI_GLOBAL_REPOSITORY,file);
         }

         }
         break;

      default:
         break;

      }

      }
      break;

   case WM_NOTIFY: {

      NMHDR *pNotifyHeader = (NMHDR *)lParam;

      switch ( pNotifyHeader -> code ) {

      case PSN_APPLY: {

         PSHNOTIFY *pNotify = (PSHNOTIFY *)lParam;

         if ( pNotify -> lParam ) {

            char szTempGlobalStore[MAX_PATH];

            GetDlgItemText(hwnd,IDDI_GLOBAL_REPOSITORY,szTempGlobalStore,MAX_PATH);

            char szTemp[1024];

            long errorCode = GetLastError();

            sprintf(szTemp,"%s\\Settings",szTempGlobalStore);

            BOOL rc = CreateDirectory(szTemp,NULL);
            BOOL doChange = true;

            if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
               sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szTempGlobalStore,GetLastError());
               MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
               doChange = false;
            } else {

               sprintf(szTemp,"%s\\Printing Profiles",szTempGlobalStore);
               rc = CreateDirectory(szTemp,NULL);

               if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
                  sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szTempGlobalStore,GetLastError());
                  MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
                  doChange = false;
               } else {

                  sprintf(szTemp,"%s\\Printed Documents",szTempGlobalStore);
                  rc = CreateDirectory(szTemp,NULL);

                  if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
                     sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szTempGlobalStore,GetLastError());
                     MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
                     doChange = false;
                  } else {
                     sprintf(szTemp,"%s\\Signed Printed Documents",szTempGlobalStore);
                     rc = CreateDirectory(szTemp,NULL);
                     if ( ! rc && ! ( ERROR_ALREADY_EXISTS == GetLastError() ) ) {
                        sprintf(szTemp,"There was an error creating a sub-directory in the location specified as:\n\n\t%s\n\nCursiVision needs to have permission to write to this location.\n\nThe error code is: %ld",szTempGlobalStore,GetLastError());
                        MessageBox(NULL,szTemp,"Error!",MB_OK | MB_ICONEXCLAMATION);
                        doChange = false;
                     }

                  }

               }

            }

            if ( doChange ) {
               strcpy(szGlobalDataStore,szTempGlobalStore);
               HKEY hKeySettings = NULL;
               RegOpenKeyEx(HKEY_LOCAL_MACHINE,"Software\\InnoVisioNate\\CursiVision",0L,KEY_ALL_ACCESS,&hKeySettings);
               DWORD cb = (DWORD)strlen(szGlobalDataStore);
               DWORD dwType = REG_SZ;
               RegSetValueEx(hKeySettings,"Global Settings Store",NULL,dwType,(BYTE *)&szGlobalDataStore,cb);
               RegCloseKey(hKeySettings);
            }

         }

         SetWindowLongPtr(hwnd,DWLP_MSGRESULT,PSNRET_NOERROR);
         }
         return (LRESULT)TRUE;

      }

      }
      break;

   default:
      break;
   
   }
   
   return 0L;
   }
