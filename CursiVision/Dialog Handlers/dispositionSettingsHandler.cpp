// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <shellapi.h>
#include <shTypes.h>
#include <shlobj.h>

#include <pshpack1.h>


#include <poppack.h>

#define OBJECT_WITH_PROPERTIES CursiVision

#include "dispositionSettingsDefines.h"

#define LOAD_ADDITIONAL \
PUT_BOOL(pCursiVision -> adviseWaiting,IDDI_DISPLAY_WAITING_SHOW)

#define UNLOAD_ADDITIONAL \
GET_BOOL(pCursiVision -> adviseWaiting,IDDI_DISPLAY_WAITING_SHOW)

#define ADDITIONAL_COMMAND             \
   case IDDI_DISPOSITION_REMEMBER:     \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_CONTINUOUS_DOODLE_ON),SendDlgItemMessage(hwnd,IDDI_DISPOSITION_REMEMBER,BM_GETCHECK,0L,0L) == BST_CHECKED ? TRUE : FALSE); \
      EnableWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_CONTINUOUS_DOODLE_OFF),SendDlgItemMessage(hwnd,IDDI_DISPOSITION_REMEMBER,BM_GETCHECK,0L,0L) == BST_CHECKED ? TRUE : FALSE); \
      break;

#define ADDITIONAL_INITIALIZATION            \
   ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

   LRESULT CALLBACK CursiVision::dispositionSettingsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

#include "dispositionSettingsBody.cpp"

   return (LRESULT)FALSE;
   }


   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doHide(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doShow(HWND hwndTest,LPARAM lParam);
   BOOL CALLBACK doMoveUp(HWND hwndTest,LPARAM lParam);

   extern "C" void disableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doDisable,(LPARAM)pExceptions);
   return;
   }

   extern "C" void enableAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doEnable,(LPARAM)pExceptions);
   return;
   }

   extern "C" void moveUpAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doMoveUp,(LPARAM)pExceptions);
   return;
   }

   extern "C" void hideAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doHide,(LPARAM)pExceptions);
   return;
   }

   extern "C" void showAll(HWND hwnd,long *pExceptions) {
   EnumChildWindows(hwnd,doShow,(LPARAM)pExceptions);
   return;
   }


   BOOL CALLBACK doDisable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,TRUE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,FALSE);
   return TRUE;
   }


   BOOL CALLBACK doEnable(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         EnableWindow(hwndTest,FALSE);
         return TRUE;
      }
   }
   EnableWindow(hwndTest,TRUE);
   return TRUE;
   }


   BOOL CALLBACK doMoveUp(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   LONG_PTR id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   RECT rcParent,rcCurrent;
   GetWindowRect(GetParent(hwndTest),&rcParent);
   GetWindowRect(hwndTest,&rcCurrent);
   SetWindowPos(hwndTest,NULL,rcCurrent.left - rcParent.left,rcCurrent.top - 32 - rcParent.top,0,0,SWP_NOZORDER | SWP_NOSIZE);
   return TRUE;
   }


   BOOL CALLBACK doHide(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   LONG_PTR id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   ShowWindow(hwndTest,SW_HIDE);
   return TRUE;
   }

   BOOL CALLBACK doShow(HWND hwndTest,LPARAM lParam) {
   long *pExceptions = (long *)lParam;
   long id = (long)GetWindowLongPtr(hwndTest,GWL_ID);
   for ( long k = 0; 1; k++ ) {
      if ( ! pExceptions[k] )
         break;
      if ( id == pExceptions[k] ) {
         return TRUE;
      }
   }
   ShowWindow(hwndTest,SW_SHOW);
   return TRUE;
   }