// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <winVer.h>

   LRESULT CALLBACK CursiVision::aboutHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   CursiVision *p = (CursiVision *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      SetWindowLongPtr(hwnd,GWLP_USERDATA,lParam);

      char szTemp[1024],szAbout[1024];

      p = (CursiVision *)lParam;

      RECT rcParent,rcThis;
      GetWindowRect(GetParent(hwnd),&rcParent);
      GetWindowRect(hwnd,&rcThis);
      SetWindowPos(hwnd,HWND_TOP,rcParent.left + (rcParent.right - rcParent.left) / 2 - (rcThis.right - rcThis.left) / 2,
                                 rcParent.top + (rcParent.bottom - rcParent.top) / 2 - (rcThis.bottom - rcThis.top) / 2,0,0,SWP_NOSIZE);

      DWORD viSize;
      unsigned int cbVersionInfo;
      struct LANGANDCODEPAGE {
         WORD wLanguage;
         WORD wCodePage;
      } *pTranslate;

      viSize = GetFileVersionInfoSize(szApplicationName,0);
      BYTE *pvInfo = new BYTE[viSize];

      GetFileVersionInfo(szApplicationName,0L,viSize,pvInfo);

      VerQueryValue(pvInfo,"\\VarFileInfo\\Translation",reinterpret_cast<void **>(&pTranslate),&cbVersionInfo);

      sprintf(szTemp,"\\StringFileInfo\\%04x%04x\\ProductVersion",pTranslate -> wLanguage,pTranslate -> wCodePage);

      BYTE *pvPiece;
      VerQueryValue(pvInfo,szTemp,reinterpret_cast<void **>(&pvPiece),&cbVersionInfo);

      LoadString(NULL,IDS_ABOUT_0,szTemp,sizeof(szTemp));
      SetDlgItemText(hwnd,IDDI_ABOUT_TEXT_0,szTemp);

      HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

      LOGFONT systemFont;

      GetObject(hGUIFont,sizeof(LOGFONT),&systemFont);

      systemFont.lfHeight = (long)((double)systemFont.lfHeight * 1.5);
      systemFont.lfWeight *= 2;

      HFONT hfontMain = CreateFontIndirect(&systemFont);

      SendDlgItemMessage(hwnd,IDDI_ABOUT_TEXT_0,WM_SETFONT,(WPARAM)hfontMain,(LPARAM)TRUE);

      LoadString(NULL,IDS_ABOUT_1,szTemp,sizeof(szTemp));
      sprintf(szAbout,szTemp,pvPiece,__DATE__);
      SetDlgItemText(hwnd,IDDI_ABOUT_TEXT_1,szAbout);

      LoadString(NULL,IDS_ABOUT_2,szTemp,sizeof(szTemp));
      memset(szAbout,0,sizeof(szAbout));

      if ( pCursiVision -> pISignaturePad_PropertiesOnly )
         strcpy(szAbout,pCursiVision -> pISignaturePad_PropertiesOnly -> DeviceProductName());
      else
         strcpy(szAbout,"There is no signature pad connected");

      if ( szAbout[0] )
         SetDlgItemText(hwnd,IDDI_ABOUT_TEXT_2,szAbout);

      LoadString(NULL,IDS_ABOUT_3,szTemp,sizeof(szTemp));
      SetDlgItemText(hwnd,IDDI_ABOUT_TEXT_3,szTemp);

      delete [] pvInfo;

      HBITMAP hBitmapLogo = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_LOGO),IMAGE_BITMAP,835/4,366/4,LR_LOADMAP3DCOLORS);

      SendDlgItemMessage(hwnd,IDDI_ABOUT_ICON,BM_SETIMAGE,(WPARAM)IMAGE_BITMAP,(LPARAM)hBitmapLogo);

      }
      return LRESULT(FALSE);

   case WM_COMMAND: {

      switch ( LOWORD(wParam) ) {

      case IDDI_ABOUT_OK:
         EndDialog(hwnd,0);
         break;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }


