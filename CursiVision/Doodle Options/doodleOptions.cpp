// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   char doodleOptions::szParentDoodleOptionsFileName[];
   char doodleOptions::szParentDoodleOptionsDocumentName[];

   doodleOptions::doodleOptions(char *pszDocument,resultDisposition *pParentDisposition) :
      pIGProperties(NULL),
      pIPdfEnabler(NULL),
      pIPdfDocument(NULL),
      pResultDisposition(NULL),
      hwndDispositionSettings(NULL),
      hwndAdditionalBackEnds(NULL),
      initialSignedLocationCount(0L),
      pSavedSigningLocations(NULL),
      pTemplateDocument(NULL),
      explicitDeleteRequested(false)
   {

   memset(&theDoodleOptionProperties,0,sizeof(struct doodleOptionProperties));

   theDoodleOptionProperties.isPrintingProfileDoodleOptions = false;

   pDoodleOptionProperties = &theDoodleOptionProperties;

   strcpy(pDoodleOptionProperties -> szDocumentName,pszDocument); 

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   pIGProperties -> Add(L"doodle settings",NULL);

   pIGProperties -> DirectAccess(L"doodle settings",TYPE_BINARY,&theDoodleOptionProperties,sizeof(struct doodleOptionProperties));

   pCursiVision -> cookSettingsFileName(pDoodleOptionProperties -> szDocumentName,pDoodleOptionProperties -> szSettingsFileName);

   BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,pDoodleOptionProperties -> szSettingsFileName,-1,bstrFileName,MAX_PATH);

   pIGProperties -> put_FileName(bstrFileName);

   pIGProperties -> LoadFile(&pDoodleOptionProperties -> loadSuccess);

   if ( ! pDoodleOptionProperties -> loadSuccess ) {
      pDoodleOptionProperties -> useGlobalDisposition = false;
      memcpy(&pDoodleOptionProperties -> processingDisposition,pParentDisposition,sizeof(resultDisposition));
   }

   SysFreeString(bstrFileName);

   pIGPropertyPageClient = new _IGPropertyPageClient(this);

   pIGProperties -> AdvisePropertyPageClient(pIGPropertyPageClient);

   pResultDisposition = &pDoodleOptionProperties -> processingDisposition;

   pDoodleOptionProperties -> processingDisposition.isGlobalDisposition = false;

   initialSignedLocationCount = SignedLocationCount();

   return;
   }


   doodleOptions::~doodleOptions() {

   if ( pSavedSigningLocations )
      delete [] pSavedSigningLocations;

   pSavedSigningLocations = NULL;

   if ( ! explicitDeleteRequested )
      SaveProperties();

   if ( pIGPropertyPageClient )
      delete pIGPropertyPageClient;

   pIGPropertyPageClient = NULL;

   if ( pIGProperties )
      pIGProperties -> Release();

   pIGProperties = NULL;

   if ( pIPdfDocument )
      pIPdfDocument -> Release();

   pIPdfDocument = NULL;

   if ( pIPdfEnabler )
      pIPdfEnabler -> Release();

   pIPdfEnabler = NULL;

   if ( pTemplateDocument ) {
      delete pTemplateDocument;
      pTemplateDocument = NULL;
   }

   return;
   }


   void doodleOptions::CreateControlWindow() {
   DLGTEMPLATE *dt = (DLGTEMPLATE *)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_DOODLE_OPTIONS),RT_DIALOG));
   hwndSigningControl = CreateDialogIndirectParam(hModule,dt,(HWND)hwndMainFrame,(DLGPROC)doodleOptions::doodleControlHandler,(ULONG_PTR)(void *)this);
   return;
   }


   struct doodleOptions::doodleOptionProperties *doodleOptions::DoodleOptionProperties() { 
   if ( pCursiVision -> pIPrintingSupportProfile ) {
      doodleOptionProperties *pPrintedDisposition = NULL;
      if ( S_OK == pCursiVision -> pIPrintingSupportProfile -> GetDoodleProperties(reinterpret_cast<void **>(&pPrintedDisposition)) ) {
         if ( pPrintedDisposition )
            return pPrintedDisposition;
      }
   }
   return pDoodleOptionProperties; 
   }

   resultDisposition *doodleOptions::ProcessingDisposition() {

   if ( pCursiVision -> pIPrintingSupportProfile ) {
      static resultDisposition *pPrintingSupportResultDisposition = NULL;
      pCursiVision -> pIPrintingSupportProfile -> GetResultDisposition(reinterpret_cast<void **>(&pPrintingSupportResultDisposition));
      return pPrintingSupportResultDisposition;
   }

   return pResultDisposition;
   }

   long doodleOptions::SignedLocationCount() { 
   if ( pCursiVision -> pIPrintingSupportProfile ) 
      return pCursiVision -> pIPrintingSupportProfile -> SigningRectangleCount(); 
   if ( ! pDoodleOptionProperties -> processingDisposition.doRemember ) 
      return 0L;
   return pDoodleOptionProperties -> countRects; 
   }

   char * doodleOptions::GetDispositionSettingsFileName(bool saveDisposition) {

   if ( pCursiVision -> pIPrintingSupportProfile )
      return pCursiVision -> pIPrintingSupportProfile -> GetDispositionSettingsFileName();

   static char szSettingsName[MAX_PATH];

   BSTR bstrFileName;

   pIGProperties -> get_FileName(&bstrFileName);

   WideCharToMultiByte(CP_ACP,0,bstrFileName,-1,szSettingsName,MAX_PATH,0,0);

   if ( ! saveDisposition )
      return szSettingsName;

   char *p = strrchr(szSettingsName,'.');

   if ( p )
      *p = '\0';

   p = strrchr(szSettingsName,'\\');
   if ( ! p )
      p = strrchr(szSettingsName,'/');
   if ( p )
      *p = '\0';

   sprintf(szSettingsName + strlen(szSettingsName),"\\%s.disposition",NULL == p ? "" : p + 1);

   IGProperties *pProperties;
   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pProperties));
   pProperties -> Add(L"result disposition",NULL);
   pProperties -> DirectAccess(L"result disposition",TYPE_BINARY,&pDoodleOptionProperties -> processingDisposition,sizeof(resultDisposition));
   BSTR bstrNewFileName = SysAllocStringLen(NULL,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,szSettingsName,-1,bstrNewFileName,MAX_PATH);
   pProperties -> put_FileName(bstrNewFileName);
   pProperties -> Save();
   pProperties -> Release();
   SysFreeString(bstrNewFileName);

   return szSettingsName;
   }


   HRESULT __stdcall doodleOptions::GetLocation(long targetIndex,RECT *pTarget,long *pPage,long *pAdobePageNumber,bool ignoreDoRemember) {

   if ( pCursiVision -> pIPrintingSupportProfile )
      return pCursiVision -> pIPrintingSupportProfile -> GetTarget(targetIndex,pTarget,pPage,pAdobePageNumber);

   if ( ! pResultDisposition -> doRemember && ! ignoreDoRemember )
      return E_FAIL;

   if ( MAX_DOODLE_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   if ( targetIndex > ( SignedLocationCount() - 1 ) && ! ignoreDoRemember )
      return E_FAIL;

   if ( pDoodleOptionProperties -> theLocations[targetIndex].documentRect.left == pDoodleOptionProperties -> theLocations[targetIndex].documentRect.right )
      return E_FAIL;

   if ( pTarget )
      memcpy(pTarget,&pDoodleOptionProperties -> theLocations[targetIndex].documentRect,sizeof(RECT));

   if ( pPage ) {
      *pPage = pDoodleOptionProperties -> theLocations[targetIndex].zzpdfPageNumber;
      if ( 1 > *pPage )
         *pPage = 1L;
   }

   if ( pAdobePageNumber ) {
      *pAdobePageNumber = pDoodleOptionProperties -> theLocations[targetIndex].pdfAdobePageNumber;
      if ( 1 > *pAdobePageNumber )
         *pAdobePageNumber = 1L;
   }

   return S_OK;
   }


   HRESULT __stdcall doodleOptions::SaveLocation(long targetIndex,RECT *pTarget,long page,long adobePageNumber) {

   if ( pCursiVision -> pIPrintingSupportProfile ) 
      return pCursiVision -> pIPrintingSupportProfile -> SaveTarget(targetIndex,pTarget,page,adobePageNumber);

   if ( ! pResultDisposition -> doRemember )
      return S_OK;

   if ( ! pTarget )
      return E_POINTER;

   if ( MAX_DOODLE_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   if ( pDoodleOptionProperties -> theLocations[targetIndex].documentRect.left == pDoodleOptionProperties -> theLocations[targetIndex].documentRect.right )
      memcpy(&pDoodleOptionProperties -> theLocations[targetIndex].documentRect,pTarget,sizeof(RECT));
   else
      return E_FAIL;

   pDoodleOptionProperties -> theLocations[targetIndex].zzpdfPageNumber = page;

   pDoodleOptionProperties -> theLocations[targetIndex].pdfAdobePageNumber = adobePageNumber;

   pDoodleOptionProperties -> countRects = max(SignedLocationCount(),targetIndex + 1);

   return S_OK;
   }

   HRESULT __stdcall doodleOptions::ReplaceLocation(long targetIndex,RECT *pTarget,long page,long adobePageNumber) {

   if ( pCursiVision -> pIPrintingSupportProfile ) 
      return pCursiVision -> pIPrintingSupportProfile -> ReplaceTarget(targetIndex,pTarget,page,adobePageNumber);

   if ( ! pResultDisposition -> doRemember )
      return S_OK;

   if ( ! pTarget )
      return E_POINTER;

   if ( MAX_DOODLE_RECT_COUNT < targetIndex - 1 || -1L > targetIndex )
      return E_INVALIDARG;

   memcpy(&pDoodleOptionProperties -> theLocations[targetIndex].documentRect,pTarget,sizeof(RECT));

   pDoodleOptionProperties -> theLocations[targetIndex].zzpdfPageNumber = page;

   pDoodleOptionProperties -> theLocations[targetIndex].pdfAdobePageNumber = adobePageNumber;

   pDoodleOptionProperties -> countRects = max(SignedLocationCount(),targetIndex + 1);

   return S_OK;
   }

   HRESULT doodleOptions::PushProperties() {
   if ( pCursiVision -> pIPrintingSupportProfile ) {
      IGProperties *pIGPropertiesTemp = NULL;
      pCursiVision -> pIPrintingSupportProfile -> QueryInterface(IID_IGProperties,reinterpret_cast<void **>(&pIGPropertiesTemp));
      HRESULT rc = pIGPropertiesTemp -> Push();
      pIGPropertiesTemp -> Release();
      return rc;
   }
   return pIGProperties -> Push();
   }


   HRESULT doodleOptions::PopProperties() {
   if ( pCursiVision -> pIPrintingSupportProfile ) {
      IGProperties *pIGPropertiesTemp = NULL;
      pCursiVision -> pIPrintingSupportProfile -> QueryInterface(IID_IGProperties,reinterpret_cast<void **>(&pIGPropertiesTemp));
      HRESULT rc = pIGPropertiesTemp -> Pop();
      pIGPropertiesTemp -> Release();
      return rc;
   }
   return pIGProperties -> Pop();
   }

   HRESULT doodleOptions::DiscardProperties() {
   if ( pCursiVision -> pIPrintingSupportProfile ) {
      IGProperties *pIGPropertiesTemp = NULL;
      pCursiVision -> pIPrintingSupportProfile -> QueryInterface(IID_IGProperties,reinterpret_cast<void **>(&pIGPropertiesTemp));
      HRESULT rc = pIGPropertiesTemp -> Discard();
      pIGPropertiesTemp -> Release();
      return rc;
   }
   return pIGProperties -> Discard();
   }

   void doodleOptions::SaveSigningLocations() {

   if ( pSavedSigningLocations )
      delete [] pSavedSigningLocations;

   long n = sizeof(theDoodleOptionProperties.theLocations) + sizeof(long);

   pSavedSigningLocations = new BYTE[n];

   memcpy(pSavedSigningLocations,pDoodleOptionProperties -> theLocations,sizeof(theDoodleOptionProperties.theLocations));

   long cb = sizeof(theDoodleOptionProperties.theLocations);

   memcpy(pSavedSigningLocations + cb,&pDoodleOptionProperties -> countRects,sizeof(long));

   return;
   }

   void doodleOptions::RestoreSigningLocations() {

   if ( ! pSavedSigningLocations )
      return;

   memcpy(pDoodleOptionProperties -> theLocations,pSavedSigningLocations,sizeof(theDoodleOptionProperties.theLocations));
 
   long cb = sizeof(theDoodleOptionProperties.theLocations);
 
   memcpy(&pDoodleOptionProperties -> countRects,pSavedSigningLocations + cb,sizeof(long));

   delete [] pSavedSigningLocations;
   pSavedSigningLocations = NULL;

   return;
   }

   HRESULT doodleOptions::SaveProperties(char *pszForDocument) {

   if ( NULL == pszForDocument ) {

      if ( pCursiVision -> pIPrintingSupportProfile ) 
         return pCursiVision -> pIPrintingSupportProfile -> SaveProperties();

      if ( ! pDoodleOptionProperties -> szSettingsFileName[0] )
         return S_OK;

      if ( NULL == pIGProperties )
         return S_OK;

      if ( pDoodleOptionProperties ) {
         if ( pDoodleOptionProperties -> countRects > 1 )
            pDoodleOptionProperties -> processingDisposition.doContinuousDoodle = true;
      } else {
         if ( theDoodleOptionProperties.countRects > 1 )
            theDoodleOptionProperties.processingDisposition.doContinuousDoodle = true;
      }

      return pIGProperties -> Save();
   }

   if ( ! pszForDocument[0] )
      return S_OK;

   if ( NULL == pIGProperties )
      return S_OK;

   char szFutureSettingsFile[MAX_PATH];

   pCursiVision -> cookSettingsFileName(pszForDocument,szFutureSettingsFile);

   FILE *fX = fopen(szFutureSettingsFile,"rb");
   if ( fX ) {
      fclose(fX);
      return S_OK;
   }

   BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);
   BSTR bstrOldFile = NULL;
   
   pIGProperties -> get_FileName(&bstrOldFile);

   MultiByteToWideChar(CP_ACP,0,szFutureSettingsFile,-1,bstrFileName,MAX_PATH);

   pIGProperties -> put_FileName(bstrFileName);

   SaveProperties();

   pIGProperties -> put_FileName(bstrOldFile);

   SysFreeString(bstrFileName);

   return S_OK;
   }


   HRESULT doodleOptions::ShowProperties() {
   IUnknown *pIUnknown = NULL;
   if ( pCursiVision -> pIPrintingSupportProfile ) {
      pCursiVision -> pIPrintingSupportProfile -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
      pCursiVision -> pIPrintingSupportProfile -> ShowProperties(hwndMainFrame,pIUnknown);
      pIUnknown -> Release();
      return S_OK;
   }
   QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
   pIGProperties -> ShowProperties(hwndMainFrame,pIUnknown);
   pIUnknown -> Release();
   return S_OK;
   }


   void doodleOptions::ClearSigningLocations(bool doSave) {

   if ( pCursiVision -> pIPrintingSupportProfile ) {
      pCursiVision -> pIPrintingSupportProfile -> ClearSigningRects();
      SaveProperties();
      return;
   }

   memset(pDoodleOptionProperties -> theLocations,0,sizeof(pDoodleOptionProperties -> theLocations));

   pDoodleOptionProperties -> countRects = 0L;

   if ( doSave )
      SaveProperties();

   return;
   }   


   void doodleOptions::ClearSignaturesFile() {
   char *p = GetDispositionSettingsFileName(false);
   if ( ! p )
      return;
   char szSignaturesFileName[MAX_PATH];
   strcpy(szSignaturesFileName,p);
   p = strrchr(szSignaturesFileName,'.');
   strcpy(p,".signatures");
   DeleteFile(szSignaturesFileName);
   return;
   }

   void doodleOptions::PrepInheritSettings() {
   memset(&szParentDoodleOptionsFileName,0,sizeof(szParentDoodleOptionsFileName)); 
   memset(&szParentDoodleOptionsDocumentName,0,sizeof(szParentDoodleOptionsDocumentName)); 
   strcpy(szParentDoodleOptionsFileName,pDoodleOptionProperties -> szSettingsFileName); 
   strcpy(szParentDoodleOptionsDocumentName,pDoodleOptionProperties -> szDocumentName); 
   return;
   }

   void doodleOptions::InheritSettings() {

   if ( ! szParentDoodleOptionsFileName[0] )
      return;

   FILE *fX = fopen(szParentDoodleOptionsFileName,"rb");
   if ( ! fX )
      return;
   fclose(fX);

   char szRestoreDocumentName[MAX_PATH];
   char szRestoreSettingsFileName[MAX_PATH];

   strcpy(szRestoreDocumentName,pDoodleOptionProperties -> szDocumentName);

   strcpy(szRestoreSettingsFileName,pDoodleOptionProperties -> szSettingsFileName);

   BSTR bstrOldFileName = NULL;

   pIGProperties -> get_FileName(&bstrOldFileName);

   BSTR bstrTempFileName = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szParentDoodleOptionsFileName,-1,bstrTempFileName,MAX_PATH);

   pIGProperties -> put_FileName(bstrTempFileName);

   VARIANT_BOOL loadSuccess = VARIANT_FALSE;

   pIGProperties -> LoadFile(&loadSuccess);

   if ( loadSuccess ) {
      strcpy(pDoodleOptionProperties -> szInheritingOptionsFileName,szRestoreSettingsFileName);
      strcpy(pDoodleOptionProperties -> szInheritingOptionsDocumentName,szRestoreDocumentName);
      pIGProperties -> Save();
      memset(pDoodleOptionProperties -> szInheritingOptionsFileName,0,sizeof(pDoodleOptionProperties -> szInheritingOptionsFileName));
      memset(pDoodleOptionProperties -> szInheritingOptionsDocumentName,0,sizeof(pDoodleOptionProperties -> szInheritingOptionsDocumentName));
      strcpy(pDoodleOptionProperties -> szParentDocumentName,pDoodleOptionProperties -> szDocumentName);
      strcpy(pDoodleOptionProperties -> szParentSettingsFileName,pDoodleOptionProperties -> szSettingsFileName);
   }

   pIGProperties -> put_FileName(bstrOldFileName);

   SysFreeString(bstrOldFileName);

   SysFreeString(bstrTempFileName);

   strcpy(pDoodleOptionProperties -> szDocumentName,szRestoreDocumentName);

   strcpy(pDoodleOptionProperties -> szSettingsFileName,szRestoreSettingsFileName);

   pIGProperties -> Save();

   memset(szParentDoodleOptionsFileName,0,sizeof(szParentDoodleOptionsFileName));
   memset(szParentDoodleOptionsDocumentName,0,sizeof(szParentDoodleOptionsDocumentName));

   return;
   }


   void doodleOptions::DeleteSettingsFiles(char *pszRootDocument) {

   char szSettingsFile[MAX_PATH];

   pCursiVision -> cookSettingsFileName(pszRootDocument,szSettingsFile);

   char *p = strrchr(szSettingsFile,'\\');
   if ( ! p )
      p = strrchr(szSettingsFile,'/');
   if ( ! p )
      return;

   char szCurrentDirectory[MAX_PATH];

   GetCurrentDirectory(MAX_PATH,szCurrentDirectory);

   *p = '\0';

   SetCurrentDirectory(szSettingsFile);

   *p = '\\';

   p = strrchr(p + 1,'.');
   if ( ! p ) {
      SetCurrentDirectory(szCurrentDirectory);
      return;
   }

   char *pExtension = p + 1;

   BSTR bstrPropertiesFile = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szSettingsFile,-1,bstrPropertiesFile,MAX_PATH);

   IGProperties *pIGPropertiesTemp = NULL;

   CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGPropertiesTemp));

   doodleOptionProperties *pTempProperties = new doodleOptionProperties();

   pIGPropertiesTemp -> Add(L"doodle settings",NULL);

   pIGPropertiesTemp -> DirectAccess(L"doodle settings",TYPE_BINARY,pTempProperties,sizeof(struct doodleOptionProperties));

   VARIANT_BOOL loadSuccess;

   pIGPropertiesTemp -> put_FileName(bstrPropertiesFile);

   pIGPropertiesTemp -> LoadFile(&loadSuccess);

   if ( loadSuccess && pTempProperties -> szInheritingOptionsDocumentName[0] )
      DeleteSettingsFiles(pTempProperties -> szInheritingOptionsDocumentName);

   pIGPropertiesTemp -> Release();

   delete pTempProperties;

   SysFreeString(bstrPropertiesFile);

   *pExtension = '*';
   *(pExtension + 1) = '\0';


   WIN32_FIND_DATA wfd;

   memset(&wfd,0,sizeof(WIN32_FIND_DATA));

   HANDLE hFiles = FindFirstFile(szSettingsFile,&wfd);

   if ( INVALID_HANDLE_VALUE != hFiles ) {
      do {
         DeleteFile(wfd.cFileName);
      } while ( FindNextFile(hFiles,&wfd) );
      FindClose(hFiles);
   }

   for ( long k = 0; k < MAX_BACK_END_PROCESSORS; k++ ) {

      if ( ! pResultDisposition -> backEndSettingsFiles[k][0] )
         break;

      DeleteFile(pResultDisposition -> backEndSettingsFiles[k]);
   
   }

   SetCurrentDirectory(szCurrentDirectory);

   explicitDeleteRequested = true;

   return;
   }


   LRESULT CALLBACK doodleOptions::inheritedDocumentHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   doodleOptions *pObject = NULL;

   if ( p ) 
      pObject = (doodleOptions *)p -> pParent;

   switch ( msg ) {

   case WM_INITDIALOG: {

      PROPSHEETPAGE *pPage = reinterpret_cast<PROPSHEETPAGE *>(lParam);
      p = (resultDisposition *)pPage -> lParam;
      pObject = (doodleOptions *)p -> pParent;

      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);

      SetWindowText(GetDlgItem(hwnd,IDDI_INHERITED_DOCUMENT_TEXT), pObject -> pDoodleOptionProperties -> szParentDocumentName);

      SetWindowPos(GetDlgItem(hwnd,IDDI_INHERITED_DOCUMENT_PROPERTIES),HWND_TOP,0,0,BAR_BUTTON_WIDTH,BAR_BUTTON_HEIGHT,SWP_NOMOVE);

      }
      return LRESULT(FALSE);

   case WM_COMMAND: {

//
//NTC: 04-25-2014: I attempted to launch properties from properties.
// There are a whole lot of issues doing this, not the least of which is the templateDocument::tdUI object in all the
// editors is a static object that get's trounced when you try to launch a new instance of the property pages.
// It would be nice to do this in the future (for example, to look at a parent document's properties)
//
#if 0
      switch ( LOWORD(wParam) ) {

      case IDDI_INHERITED_DOCUMENT_PROPERTIES: {

         FILE *fX = fopen(pObject -> pDoodleOptionProperties -> szParentSettingsFileName,"rb");
         if ( ! fX ) {
            char szMessage[1024];
            sprintf(szMessage,"The system cannot find the settings file (%s) for the document %s.",
                                       pObject -> pDoodleOptionProperties -> szParentSettingsFileName,pObject -> pDoodleOptionProperties -> szParentDocumentName);
            MessageBox(NULL,szMessage,"Note",MB_OK | MB_ICONEXCLAMATION);
            break;
         }

         fclose(fX);

         doodleOptions *pParentDoodleOptions = new doodleOptions(pObject -> pDoodleOptionProperties -> szParentSettingsFileName);

         pParentDoodleOptions -> ShowProperties();
         pParentDoodleOptions -> SaveProperties();
         delete pParentDoodleOptions;

         }
         break;

      default:
         break;
      }
#endif

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }
