// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   long __stdcall doodleOptions::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      *ppv = static_cast<IUnknown *>(this); 
   else

   if ( riid == IID_IGPropertyPageClient )
      return pIGPropertyPageClient -> QueryInterface(riid,ppv);
   else
 
      return E_NOINTERFACE;
 
   AddRef();
  
   return S_OK;
   }


   unsigned long __stdcall doodleOptions::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall doodleOptions::Release() {
   return 1;
   }


   long __stdcall doodleOptions::_IGPropertyPageClient::QueryInterface(REFIID riid,void **ppv) {

   *ppv = NULL; 
 
   if ( riid == IID_IUnknown )
      return pParent -> QueryInterface(riid,ppv);
   else

   if ( riid == IID_IGPropertyPageClient )
      *ppv = static_cast<IGPropertyPageClient*>(this);
   else
 
      return E_NOINTERFACE;
 
   AddRef();
  
   return S_OK; 
   }
 
   unsigned long __stdcall doodleOptions::_IGPropertyPageClient::AddRef() {
   return 1;
   }
 
   unsigned long __stdcall doodleOptions::_IGPropertyPageClient::Release() {
   return 1;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::BeforeAllPropertyPages() {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::GetPropertyPagesInfo(long* pCntPages,SAFEARRAY** thePageNames,SAFEARRAY** theHelpDirs,SAFEARRAY** pSize) {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::CreatePropertyPage(long pageNumber,HWND hwndParent,RECT* pRect,BOOL fModal,HWND *pHwnd) {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::Apply() {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::IsPageDirty(long pageNumber,BOOL* isDirty) {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::Help(BSTR bstrHelpDir) {
   return  S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::TranslateAccelerator(long,long* pResult) {
   *pResult = S_FALSE;
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::AfterAllPropertyPages(BOOL userCanceled) {
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::DestroyPropertyPage(long index) {
   return S_OK;
   }



   HRESULT doodleOptions::_IGPropertyPageClient::GetPropertySheetHeader(void *pv) {

   if ( ! pv )
      return E_POINTER;

   static char szCaption[512];

   PROPSHEETHEADER *pHeader = reinterpret_cast<PROPSHEETHEADER *>(pv);

   sprintf(szCaption,"Settings for: %s",pParent -> pDoodleOptionProperties -> szDocumentName);

   pHeader -> dwFlags = PSH_PROPSHEETPAGE | PSH_NOCONTEXTHELP | 0*PSH_HASHELP;
   pHeader -> hInstance = hModule;
   pHeader -> pszIcon = 0L;
   pHeader -> pszCaption = szCaption;
   pHeader -> pfnCallback = NULL;

   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::get_PropertyPageCount(long *pCount) {
   if ( ! pCount )
      return E_POINTER;
   *pCount = 4;
   if ( pParent -> pDoodleOptionProperties -> szParentDocumentName[0] )
      *pCount = 5;
   return S_OK;
   }


   HRESULT doodleOptions::_IGPropertyPageClient::GetPropertySheets(void *pPages) {

   if ( ! pPages )
      return E_POINTER;

   pCursiVision -> currentProcessDisposition() -> pParent = pParent;

   if ( pParent -> pTemplateDocument ) {
      delete pParent -> pTemplateDocument;
      pParent -> pTemplateDocument = NULL;
   }

   pParent -> pTemplateDocument = new templateDocument(pCursiVision -> pICursiVisionServices,NULL,pParent -> DoodleOptionProperties() -> szDocumentName,NULL,NULL);

   pParent -> pTemplateDocument -> openDocument();

   SIZEL sizelDisplay{0,0};

   pParent -> pTemplateDocument -> GetSinglePagePDFDisplaySize(&sizelDisplay);

   PROPSHEETPAGE *pPropSheetPages = reinterpret_cast<PROPSHEETPAGE *>(pPages);

   pPropSheetPages[0].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[0].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[0].hInstance = hModule;
   pPropSheetPages[0].pszTemplate = MAKEINTRESOURCE(IDD_DISPOSITION_PROPERTIES);
   pPropSheetPages[0].pfnDlgProc = (DLGPROC)doodleOptions::dispositionSettingsHandler;
   pPropSheetPages[0].pszTitle = DISPOSITION_TITLE;
   pPropSheetPages[0].lParam = (ULONG_PTR)pCursiVision -> currentProcessDisposition();
   pPropSheetPages[0].pfnCallback = NULL;

   pPropSheetPages[1].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[1].dwFlags = PSP_USEICONID | PSP_USETITLE | PSP_DLGINDIRECT;
   pPropSheetPages[1].hInstance = hModule;

   //
   //NTC: 12-22-2017: I found the reason why the following (allocating global memory for the dialog resource) was necessary.
   // The linker switch: "/SECTION:.rsrc,rw" over comes this. Presumably because you can't modify memory in the resource
   // section.
   // This is also why comctl32.dll would trap when creating property pages.
   //
   // I have switched back away from the fix on 09-02-2017 by placing "1" in the following #if directive.
   //
#if 1
   pPropSheetPages[1].pResource = (PROPSHEETPAGE_RESOURCE)LockResource(LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_SIGNING_LOCATIONS),RT_DIALOG)));
#else
   //
   //NTC: 09-02-2017: I am aware that the copy of the dialog resource allocated here leaks.
   //
   // For some reason, perhaps by compiling with Visual Studio 2015 (compiler switches ?), changing memory loaded via
   // LoadResource throws an access violation, so the dialog template needs to be copied to allocated memory.
   //
   // However, I am not sure where or when the opportunity would come to delete the copy, perhaps somewhere
   // in the properties component's flow, however that would not know whether the caller just called LoadResource
   // which I don't think should be deleted.
   //
   HRSRC hDialog = FindResource(hModule,MAKEINTRESOURCE(IDD_SIGNING_LOCATIONS),RT_DIALOG);
   HGLOBAL hResource = LoadResource(hModule,hDialog);
   void *pResource = LockResource(hResource);
   BYTE *pDialog = new BYTE[SizeofResource(hModule,hDialog)];
   memcpy(pDialog,pResource,SizeofResource(hModule,hDialog));
   pPropSheetPages[1].pResource = (PROPSHEETPAGE_RESOURCE)pDialog;
#endif

   pPropSheetPages[1].pfnDlgProc = (DLGPROC)doodleOptions::signingLocationsHandler;
   pPropSheetPages[1].pszTitle = "Signing Locations";
   pPropSheetPages[1].lParam = (ULONG_PTR)pCursiVision -> currentProcessDisposition();
   pPropSheetPages[1].pfnCallback = NULL;

   adjustPropertiesDialogSize(&sizelDisplay,(DLGTEMPLATEEX *)pPropSheetPages[1].pResource,128);

   pPropSheetPages[2].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[2].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[2].hInstance = hModule;
   pPropSheetPages[2].pszTemplate = MAKEINTRESOURCE(IDD_BACKENDS);
   pPropSheetPages[2].pfnDlgProc = (DLGPROC)doodleOptions::additionalBackEndsHandler;
   pPropSheetPages[2].pszTitle = "Tool Box";
   pPropSheetPages[2].lParam = (ULONG_PTR)pCursiVision -> currentProcessDisposition();
   pPropSheetPages[2].pfnCallback = NULL;

   pPropSheetPages[3].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[3].dwFlags = PSP_USEICONID | PSP_USETITLE | PSP_DLGINDIRECT;
   pPropSheetPages[3].hInstance = hModule;

#if 1
   pPropSheetPages[3].pResource = (PROPSHEETPAGE_RESOURCE)LockResource(LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_DATA_FIELDS),RT_DIALOG)));
#else
   hDialog = FindResource(hModule,MAKEINTRESOURCE(IDD_DATA_FIELDS),RT_DIALOG);
   hResource = LoadResource(hModule,hDialog);
   pResource = LockResource(hResource);
   pDialog = new BYTE[SizeofResource(hModule,hDialog)];
   memcpy(pDialog,pResource,SizeofResource(hModule,hDialog));
   pPropSheetPages[3].pResource = (PROPSHEETPAGE_RESOURCE)pDialog;
#endif

   pPropSheetPages[3].pfnDlgProc = (DLGPROC)doodleOptions::fieldsHandler;
   pPropSheetPages[3].pszTitle = "Data Fields";
   pPropSheetPages[3].lParam = (ULONG_PTR)pCursiVision -> currentProcessDisposition();
   pPropSheetPages[3].pfnCallback = NULL;

   adjustPropertiesDialogSize(&sizelDisplay,(DLGTEMPLATEEX *)pPropSheetPages[3].pResource,64);

   if ( ! pParent -> pDoodleOptionProperties -> szParentDocumentName[0]  )
      return S_OK;

   pPropSheetPages[4].dwSize = sizeof(PROPSHEETPAGE);
   pPropSheetPages[4].dwFlags = PSP_USEICONID | PSP_USETITLE;
   pPropSheetPages[4].hInstance = hModule;
   pPropSheetPages[4].pszTemplate = MAKEINTRESOURCE(IDD_INHERITED_DOCUMENT);
   pPropSheetPages[4].pfnDlgProc = (DLGPROC)doodleOptions::inheritedDocumentHandler;
   pPropSheetPages[4].pszTitle = "Parent Document";
   pPropSheetPages[4].lParam = (ULONG_PTR)pCursiVision -> currentProcessDisposition();
   pPropSheetPages[4].pfnCallback = NULL;

   return S_OK;
   }

