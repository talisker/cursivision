// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include "dispositionSettingsDefines.h"

#define OBJECT_WITH_PROPERTIES doodleOptions

static doodleOptions::doodleOptionProperties *pDoodleOptionProps = NULL;

#define DOODLE_PROPERTIES_PTR pDoodleOptionProps = pObject -> pDoodleOptionProperties;

#define ADDITIONAL_INITIALIZATION            \
   ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

#include "fieldsHandlerDefines.h"

   LRESULT CALLBACK doodleOptions::fieldsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

#include "fieldsHandlerBody.cpp"

   return LRESULT(FALSE);
   }

#include "fieldsHandlerSupport.cpp"