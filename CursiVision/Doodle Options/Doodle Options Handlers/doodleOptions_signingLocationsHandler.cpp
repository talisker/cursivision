// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#define OBJECT_WITH_PROPERTIES doodleOptions

#include "signingLocationsDefines.h"

doodleOptions::doodleOptionProperties *pDoodleOptionProps = NULL;

#define DOODLE_PROPERTIES_PTR pDoodleOptionProps = pObject -> pDoodleOptionProperties;

#define MODULE_HANDLE hModule

#define ADDITIONAL_INITIALIZATION            \
   ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

   LRESULT CALLBACK doodleOptions::signingLocationsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

#include "signingLocationsBody.cpp"

   return LRESULT(FALSE);
   }

#include "signingLocationsSupport.cpp"
