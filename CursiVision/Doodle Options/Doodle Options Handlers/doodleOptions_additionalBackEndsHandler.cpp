// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

extern "C" void disableAll(HWND hwnd,long *pExceptions);
extern "C" void enableAll(HWND hwnd,long *pExceptions);

#define OBJECT_WITH_PROPERTIES doodleOptions

#define CURSIVISION_SERVICES_INTERFACE pCursiVision -> pICursiVisionServices

#define ALL_BACKENDS_LIST pCursiVision -> allBackEnds

#define PARENT_OBJECT_PREFERRED_SETTINGS_FILE_NAME pObject -> pDoodleOptionProperties -> szSettingsFileName

#define ADDITIONAL_INITIALIZATION            \
   ShowWindow(GetDlgItem(hwnd,IDDI_DISPOSITION_NEED_ADMIN_PRIVILEGES),SW_HIDE);

   struct buttonPair {
      HWND hwndList,hwndUse,hwndProperties,hwndOrder;
      GUID objectId;
      char szSettingsFileName[MAX_PATH];
      GUID instanceId;
      IUnknown *pIUnknown_Object;
   };
   static buttonPair buttonPairs[32];

   static HWND hwndTopList = NULL;
   static HWND hwndBottomList = NULL;

   static long nativeTopListHeight = 0;
   static long nativeTopListWidth = 0;
   static long nativeWidth = 0;
   static long nativeHeight = 0;
   static long countAvailableBackEnds = 0;

   LRESULT CALLBACK doodleOptions::additionalBackEndsHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   resultDisposition *p = (resultDisposition *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

#include "additionalBackEndsBody.cpp"

   return LRESULT(FALSE);
   }

#include "additionalBackEnds_ListViewHandler.cpp"
