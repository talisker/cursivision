// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   LRESULT CALLBACK doodleOptions::doodleControlHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   doodleOptions *p = (doodleOptions *)GetWindowLongPtr(hwnd,GWLP_USERDATA);

   switch ( msg ) {

   case WM_INITDIALOG: {

      SetWindowLongPtr(hwnd,GWLP_USERDATA,lParam);

      p = (doodleOptions *)lParam;

      if ( pCursiVision -> SignaturePad() && ! pCursiVision -> SignaturePad() -> IsLCD() ) {
         SetWindowText(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL5),"Use Clear to start over, or Ok to commit the writing.");
         SetWindowText(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL4),"");
      }

      SetWindowPos(hwnd,HWND_TOP,pCursiVision -> rectDoodleOptions.left,pCursiVision -> rectDoodleOptions.top,0,0,SWP_NOSIZE);

      }

      return (LRESULT)FALSE;

   case WM_PAINT: {
      if ( ! p )
         break;
      if ( ! p -> RememberSigningLocations() ) {
         if ( p -> SignedLocationCount() ) {
            SetDlgItemText(hwnd,IDDI_DOODLE_OPTIONS_LABEL1,"CursiVision has remembered signing locations for this document.");
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL2),TRUE);
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_FORGET),TRUE);
         } else {
            SetDlgItemText(hwnd,IDDI_DOODLE_OPTIONS_LABEL1,"CursiVision is not remembering signing locations for this document.");
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL2),FALSE);
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_FORGET),FALSE);
         }
      } else {
         if ( p -> SignedLocationCount() ) {
            SetDlgItemText(hwnd,IDDI_DOODLE_OPTIONS_LABEL1,"CursiVision has remembered signing locations for this document.");
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL2),TRUE);
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_FORGET),TRUE);
         } else {
            SetDlgItemText(hwnd,IDDI_DOODLE_OPTIONS_LABEL1,"CursiVision is remembering signing locations for this document:");
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_LABEL2),FALSE);
            EnableWindow(GetDlgItem(hwnd,IDDI_DOODLE_OPTIONS_FORGET),FALSE);
         }
      }
      break;
      }

   case WM_MOVE:
      GetWindowRect(hwnd,&pCursiVision -> rectDoodleOptions);
      GetWindowRect(hwnd,&p -> ProcessingDisposition() -> rcOptions);
      break;

   case WM_COMMAND: {

      if ( ! p )
         break;

      switch ( LOWORD(wParam) ) {

      case IDDI_DOODLE_OPTIONS_FORGET:
         p -> ClearSigningLocations(true);
         SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE_CANCEL,0),1L);
         SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE,0),0L);
         break;

      case IDDI_DOODLE_OPTIONS_DISPOSITION:
         p -> ShowProperties();
         InvalidateRect(hwnd,NULL,TRUE);
         break;

      case IDDI_DOODLE_OPTIONS_RESETBUTTON:
         if ( pCursiVision -> SignaturePad() )
            pCursiVision -> SignaturePad() -> FireOption(0);
         break;

      case IDDI_DOODLE_OPTIONS_LEFTBUTTON:
         if ( pCursiVision -> SignaturePad() )
            pCursiVision -> SignaturePad() -> FireOption(2);
         break;

      case IDDI_DOODLE_OPTIONS_RIGHTBUTTON:
         ShowWindow(hwnd,SW_HIDE);
         if ( pCursiVision -> SignaturePad() )
            pCursiVision -> SignaturePad() -> FireOption(1);
         break;

      case IDDI_DOODLE_OPTIONS_ERASEBUTTON: {
         pCursiVision -> undoUpdate();
         if ( pCursiVision -> SignaturePad() )
            pCursiVision -> SignaturePad() -> FireOption(0);
         }
         break;

      default:
         break;
      }

      }
      break;

   default:
      break;
   }

   return LRESULT(FALSE);
   }


