
#include "InsertPDF.h"

   HRESULT registerComponent(long resourceId,HMODULE hModule) {

   HRESULT rc = E_FAIL;

   char szDLLName[MAX_PATH];
   char szFileAndPath[MAX_PATH];

   LoadString(NULL,resourceId,szDLLName,MAX_PATH);

   HRSRC hrsrc = FindResourceA(hModule,MAKEINTRESOURCE(resourceId),"#256");

   if ( hrsrc ) {

      HGLOBAL hResource = LoadResource(hModule,hrsrc);

      if ( hResource ) {

         sprintf(szFileAndPath,"%s\\%s",szProgramDirectory,szDLLName);
         FILE *fDLL = fopen(szFileAndPath,"wb");

         SIZE_T sizeData = SizeofResource(hModule,hrsrc);
         void *pData = LockResource(hResource);
         fwrite(pData,sizeData,1,fDLL);
         fclose(fDLL);

         HMODULE h = LoadLibrary(szFileAndPath);
         if ( h ) {
            long (__stdcall *r)();
            r = reinterpret_cast<long (__stdcall *)()>(GetProcAddress(h,"DllRegisterServer"));
            if ( r ) 
               rc = (r)();
         }

      }
   }

   return rc;
   }