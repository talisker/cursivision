// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#define WM_USER_MIN                 (WM_USER + 101)
#define WM_FINISH_SIGNATURE_PLAY    (WM_USER + 101)
#define WM_IMMEDIATE_DOODLE         (WM_USER + 107)
#define WM_OPEN_PDF_FILE            (WM_USER + 108)
#define WM_START_PRINT_PROCESSING   (WM_USER + 111)
#define WM_START_ADHOC              (WM_USER + 112)
#define WM_SET_ACTIVE               (WM_USER + 113)
#define WM_POTENTIAL_QUIT           (WM_USER + 114)
#define WM_FINISH_SIGNATURE_PAGE    (WM_USER + 115)
#define WM_FINISH_SIGNATURE_PAGE_2  (WM_USER + 116)
#define WM_PRINT_ONLY               (WM_USER + 117)
#define WM_CONTINUE_PLAY            (WM_USER + 118)
#define WM_PREPARE_SIGNATURE_REPLAY (WM_USER + 119)
#define WM_PLAY_NEXT_PAGE           (WM_USER + 120)
#define WM_CAN_PAD_GET_SIG_DATA     (WM_USER + 121)
#define WM_POST_PAINT               (WM_USER + 122)
#define WM_REGISTER_PAD             (WM_USER + 123)
#define WM_DISPLAY_SIGNATURE_BOX    (WM_USER + 124)
#define WM_APPLY_SIGNATURE          (WM_USER + 125)
#define WM_FIRE_DOCUMENT_OPENED     (WM_USER + 126)
#define WM_FIRE_DOCUMENT_CLOSED     (WM_USER + 127)
#define WM_FORCE_SHUTDOWN           (WM_USER + 128)
#define WM_DOCUMENT_IS_OPENED       (WM_USER + 129)
#define WM_DOCUMENT_IS_TRACKED      (WM_USER + 130)
#define WM_DOCUMENT_SET_URL         (WM_USER + 131)
#define WM_TRACK_PAGE_NUMBER        (WM_USER + 132)
#define WM_DOCUMENT_TRACK_CYCLE_FINISHED  (WM_USER + 133)
#define WM_REFRESH_SIGNATURE_BOX    (WM_USER + 134)

#define WM_USER_MAX                 WM_REFRESH_SIGNATURE_BOX
