// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <windows.h>
#include <wingdi.h>
#include <list>
#include <stdio.h>
#include <objbase.h>
#include <gdiPlus.h>

#pragma warning(disable:4786)

#include "SignatureProcess\signatureBox.h"

#include "resultDisposition.h"

#include "Properties_i.h"

#include "resource.h"

#define REGION_CAPTURE_STAGE_CANCEL         32
#define REGION_CAPTURE_STAGE_DONE            0
#define REGION_CAPTURE_STAGE_ONE             1
#define USE_PLAY_THREAD

#define MAX_PAGE_COUNT  32

class SignatureProcess  {

   public:

      SignatureProcess();
      ~SignatureProcess();

      ISignaturePad *SignaturePad() { if ( ! SignatureBox() ) return NULL; return SignatureBox() -> SignaturePad(); };

      void Start();

      long RegionCaptureStage(long setStage = -1L);

      bool ApplySignature();
      void OuterRect(RECT *pRect);

      long ZoomedPadWidth();
      long ZoomedPadHeight();

      long PadWidth();
      long PadHeight();

      void PlayCurrentPage();
      void PlayNextPage(long hotSpotId);
      void ReplayCurrentPage();
      void ContinueNextPage(long pageNumber);

      signatureBox *SignatureBox() { return pSignatureBox; };

   private:

      long regionCaptureStage;

      long refCount;

      double pdfZoom;

      signatureBox *pSignatureBox;

      static LRESULT CALLBACK dispositionSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalBackEndsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK listViewHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalSaveOptionsHandler(HWND,UINT,WPARAM,LPARAM);

};
