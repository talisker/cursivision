// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   void convertToPDF(char *pszInputFileName);

   long CursiVision::openDocument(BSTR documentName,long pageNumber,bool resetActive,bool alreadyInBrowser,bool inheritSettings,bool resetPrintProfile) {
   char szTemp[MAX_PATH];
   WideCharToMultiByte(CP_ACP,0,documentName,-1,szTemp,MAX_PATH,0,0);
   return openDocument(szTemp,pageNumber,resetActive,alreadyInBrowser,inheritSettings,resetPrintProfile);
   }


   long CursiVision::openDocument(char *pszFileName,long pageNumber,bool resetActive,bool alreadyInBrowser,bool inheritSettings,bool resetPrintProfile) {

   isDocumentOpen = false;
   isDocumentOpenInProgress = true;
   isInterimDocument = true;

   memset(szPageNumberText,0,sizeof(szPageNumberText));

   if ( resetActive ) {
      isDocumentModified = false;
      isInterimDocument = false;
   }

   pauseTracking();

   char szWorkingCopy[MAX_PATH];

   char *pDocumentName = NULL;

   if ( resetActive && pszFileName != szActiveDocument ) {
      strcpy(szActiveDocument,pszFileName);
      strcpy(szWorkingCopy,szActiveDocument);
   } else
      strcpy(szWorkingCopy,pszFileName);

   pDocumentName = szWorkingCopy;

   if ( resetActive ) {
      char *p = szActiveDocument;
      if ( '\"' == *p ) {
         p++;
         while ( '\"*' != *p && *p ) 
            p++;
         char szTemp[MAX_PATH];
         memset(szActiveDocument,0,sizeof(szActiveDocument));
         strcpy(szTemp,pszFileName + 1);
         strncpy(szActiveDocument,szTemp,p - szActiveDocument - 2);
      }
   }

   FILE *fTest = fopen(pDocumentName,"rb");

   if ( ! fTest ) {

      char *p = strrchr(pDocumentName,'\\');
      if ( ! p )
         p = strrchr(pDocumentName,'/');
      if ( p ) {
         char szTemp[MAX_PATH];
         sprintf(szTemp,"%s\\%s",szUserDirectory,p + 1);
         fTest = fopen(szTemp,"rb");
         if ( ! fTest ) {
            char szMessage[1024];
            sprintf(szMessage,"The system is unable to open the file %s because it does not exist",pDocumentName);
            MessageBox(NULL,szMessage,"Error!",MB_ICONEXCLAMATION);
            memset(szActiveDocument,0,sizeof(szActiveDocument));
            memset(szActiveDocumentRootName,0,sizeof(szActiveDocumentRootName));
            return -1L;
         }
         strcpy(pDocumentName,szTemp);
      } else {
         char szMessage[1024];
         sprintf(szMessage,"The system is unable to open the file %s because it does not exist",pDocumentName);
         MessageBox(NULL,szMessage,"Error!",MB_ICONEXCLAMATION);
         memset(szActiveDocument,0,sizeof(szActiveDocument));
         memset(szActiveDocumentRootName,0,sizeof(szActiveDocumentRootName));
         isDocumentOpenInProgress = false;
         return -1L;
      }

   }

   BYTE preAmble[32];
   fread(preAmble,32,1,fTest);

   fclose(fTest);

   if ( memcmp(preAmble,"%PDF",4) ) {

      char *p = strrchr(pDocumentName,'.');

      if ( p && ( ( 0 == strncmp(p + 1,"ps",2) || 0 == strncmp(p + 1,"PS",2) ) && 2 == strlen(p + 1) ) ) {
         convertToPDF(pDocumentName);
         sprintf(szActiveDocument,pDocumentName);
         p = strrchr(szActiveDocument,'.');
         sprintf(p,".pdf");
         pszFileName = szActiveDocument;
         return openDocument(szActiveDocument,pageNumber,resetActive,alreadyInBrowser,inheritSettings,resetPrintProfile);
      }

      char szMessage[1024];
      sprintf(szMessage,"The file \n\n   \"%s\"\n\nis not a valid PDF file.\n\nTo convert this file to PDF, print the file from it's application to the CursiVision Print Driver.",pDocumentName);
      MessageBox(NULL,szMessage,"Error!",MB_ICONEXCLAMATION);
      isDocumentOpenInProgress = false;

      return -1L;

   }

   fclose(fTest);

   if ( bstrActiveDocument )
      SysFreeString(bstrActiveDocument);

   bstrActiveDocument = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,pDocumentName,-1,bstrActiveDocument,MAX_PATH);

   if ( resetActive ) {
      char *pName = strrchr(pDocumentName,'\\');
      if ( ! pName ) {
         pName = strrchr(pDocumentName,'/');
         if ( ! pName )
            pName = pDocumentName;
         else
            pName++;
      } else
         pName++;
      strcpy(szActiveDocument,pDocumentName);
      strcpy(szActiveDocumentRootName,pName);
      SetWindowText(hwndMainFrame,pName);
   }

   if ( resetPrintProfile )
      memset(szArguments[IDX_PRINT],0,sizeof(szArguments[IDX_PRINT]));

   if ( pIPdfPage )
      pIPdfPage -> Release();

   pIPdfPage = NULL;

   if ( pIPdfDocument )
      pIPdfDocument -> Release();

   pIPdfDocument = NULL;

   memset(szSignedDocument,0,sizeof(szSignedDocument));

   isDocumentOpen = true;

   pIPdfEnabler -> Document(&pIPdfDocument);

   pIPdfDocument -> Open(bstrActiveDocument,NULL,NULL);

   PostMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)0,(LPARAM)"");
   PostMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)1,(LPARAM)"");
   PostMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)2,(LPARAM)"");

   char szPageLabel[64];

   pIPdfDocument -> LabelFromPage(1,64,szPageLabel);

   pIPdfDocument -> Page(1,szPageLabel,&pIPdfPage);

   RECT rectPage;

   pIPdfPage -> PageSize(&rectPage);

   pdfPageWidth = rectPage.right - rectPage.left;
   pdfPageHeight = rectPage.bottom - rectPage.top;

   pIPdfDocument -> get_PageCount(&pdfPageCount);

   double rotation;

   pIPdfPage -> Rotation(&rotation);

   if ( 90.0 == rotation || 270.0 == rotation ) {
      long t = pdfPageWidth;
      pdfPageWidth = pdfPageHeight;
      pdfPageHeight = t;
   }

   if ( resetActive ) {

      if ( pDoodleOptions && inheritSettings ) 
         pDoodleOptions -> PrepInheritSettings();

      createDoodleOptions();

      if ( inheritSettings )
         pDoodleOptions -> InheritSettings();

      pDoodleOptions -> SaveProperties();

      isDocumentModified = false;

   }
   
   pdfPageNumber = pageNumber;

   TBBUTTONINFO buttonInfo = {0};

   buttonInfo.cbSize = sizeof(TBBUTTONINFO);
   buttonInfo.dwMask = TBIF_STATE;

   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_FORGET,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_OPTIONS,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)IDC_SAVE_AS,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

   ShowWindow(hwndHTMLHost,SW_SHOW);

   if ( ! alreadyInBrowser ) {
      static char szPostName[MAX_PATH];
      strcpy(szPostName,pDocumentName);
      PostMessage(hwndMainFrame,WM_DOCUMENT_SET_URL,(WPARAM)pageNumber,(LPARAM)szPostName);
   }

   return 1;
   }


   long CursiVision::openDocumentFast(char *pszFileName,long pageNumber) {

   ignoreDocumentOpenProcessingSteps = true;

   SendMessage(hwndMainFrame,WM_DOCUMENT_SET_URL,(WPARAM)pageNumber,(LPARAM)pszFileName);

   return 1;
   }

 
   long CursiVision::closeDocument() {

   if ( pSignatureProcess ) {
      delete pSignatureProcess;
      pSignatureProcess = NULL;
   }

   pDoodleOptions -> SaveProperties(NULL);

   delete pDoodleOptions;

   pDoodleOptions = NULL;

   pauseTracking();

   PostMessage(hwndMainFrame,WM_DOCUMENT_SET_URL,0L,(LPARAM)szWelcomeURL);

   isDocumentOpen = false;
   isDocumentOpenInProgress = false;
   isInterimDocument = false;

   SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)2,(LPARAM)"");
   SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)1,(LPARAM)"");
   SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)0,(LPARAM)NO_DOCUMENT_OPEN_STATUS_MESSAGE);

   memset(szActiveDocument,0,sizeof(szActiveDocument));
   memset(szActiveDocumentRootName,0,sizeof(szActiveDocumentRootName));

   SetWindowText(hwndMainFrame,"CursiVision");

   TBBUTTONINFO buttonInfo = {0};

   buttonInfo.cbSize = sizeof(TBBUTTONINFO);
   buttonInfo.dwMask = TBIF_STATE;

   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_FORGET,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_OPTIONS,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)IDC_SAVE_AS,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);
   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

   RedrawWindow(hwndMainFrame,NULL,NULL,RDW_INVALIDATE | RDW_UPDATENOW);

   return 0L;
   }