// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <process.h>

   extern "C" int DoHelp(HWND hwnd,char *);


   LRESULT CALLBACK commandHandler(HWND hwnd,WPARAM wParam,LPARAM lParam ) {

   if ( ID_BACKEND_PROPERTIES_1 <= LOWORD(wParam) && LOWORD(wParam) <= ID_BACKEND_PROPERTIES_MAX) {

//
//TODO: 8-26-2011. An implementation to cook the back-end settings file name from the current document's settings
// needs to be created. It is repeated here, below, and in additionalBackEndsBody.cpp and probably
// other places
//
      long beIndex = 0;
      BSTR useBackend = NULL;
      for ( std::list<BSTR>::iterator it = pCursiVision -> menuBackends.begin(); it != pCursiVision -> menuBackends.end(); it++ ) {
         if ( beIndex == ( LOWORD(wParam) - ID_BACKEND_PROPERTIES_1 ) ) {
            useBackend = (*it);
            break;
         }
         beIndex++;
      }

      if ( NULL == useBackend ) {

         IGProperties *pIGPropertiesAllBackends = NULL;

         HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_INPROC_SERVER,IID_IGProperties,reinterpret_cast<void **>(&pIGPropertiesAllBackends));
         
         ICursiVisionBackEnd *ppCursiVisionBackEnds[32];

         memset(ppCursiVisionBackEnds,0,sizeof(ppCursiVisionBackEnds));

         long countBackends = 0;

         for ( std::list<BSTR>::iterator it = pCursiVision -> menuBackends.begin(); it != pCursiVision -> menuBackends.end(); it++ ) {

            CLSID clsId;
            CLSIDFromString((*it),&clsId);

            if ( ! ( S_OK == CoCreateInstance(clsId,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&ppCursiVisionBackEnds[countBackends])) ) )
               continue;

            IUnknown *pIUnknown;

            ppCursiVisionBackEnds[countBackends] -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

            rc = pIGPropertiesAllBackends -> AddPropertyPage(pIUnknown,true);

            if ( ! ( S_OK == rc ) ) {
               pIUnknown -> Release();
               continue;
            }

            PREP_PROPERTIES_FILE(ppCursiVisionBackEnds[countBackends])

            pIUnknown -> Release();

            countBackends++;

         }

         pIGPropertiesAllBackends -> ShowProperties(hwndMainFrame,NULL);

         for ( long k = 0; k < countBackends; k++ )
            ppCursiVisionBackEnds[k] -> Release();
         
         pIGPropertiesAllBackends -> Release();

         return (LRESULT)0L;
      }

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      CLSID clsId;
      CLSIDFromString(useBackend,&clsId);

      HRESULT rc = CoCreateInstance(clsId,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( S_OK != rc ) {
         char szMessage[1024];
         sprintf(szMessage,"The CursiVision tool is not registered correctly or is non-functional\n\nThe system returned the error code %ld",rc);
         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);
         return (LRESULT)0L;
      }

      PREP_PROPERTIES_FILE(pICursiVisionBackEnd)

      IUnknown *pIUnknown = NULL;
      pICursiVisionBackEnd -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
      pCursiVision -> pIGProperties -> ShowProperties(hwndMainFrame,pIUnknown);
      pIUnknown -> Release();

      pICursiVisionBackEnd -> Release();

      return (LRESULT)0L;
   }

   if ( ID_BACKEND_EXECUTE_1 <= LOWORD(wParam) && LOWORD(wParam) <= ID_BACKEND_EXECUTE_MAX ) {

      long beIndex = 0;
      BSTR useBackend = NULL;
      for ( std::list<BSTR>::iterator it = pCursiVision -> menuBackends.begin(); it != pCursiVision -> menuBackends.end(); it++ ) {
         if ( beIndex == ( LOWORD(wParam) - ID_BACKEND_EXECUTE_1 ) ) {
            useBackend = (*it);
            break;
         }
         beIndex++;
      }

      if ( NULL == useBackend )
         return (LRESULT)0L;

      BSTR bstrResultsFile = SysAllocStringLen(NULL,MAX_PATH);

      MultiByteToWideChar(CP_ACP,0,pCursiVision -> szActiveDocument,-1,bstrResultsFile,MAX_PATH);

      bool reOpenedByBackEnd = false;

      pCursiVision -> processBackends(pCursiVision -> currentProcessDisposition(),&bstrResultsFile,false,NULL,&reOpenedByBackEnd,NULL,NULL,NULL,useBackend);

      if ( reOpenedByBackEnd )
         pCursiVision -> openDocument(bstrResultsFile);

      SysFreeString(bstrResultsFile);

      return (LRESULT)0L;

   }

   switch ( LOWORD(wParam) ) {

   case IDC_PRINT:
      pCursiVision -> printDocument();
      break;

   case IDC_CLOSE_FILE:

   case IDC_GETFILE: {

      if ( pCursiVision -> documentIsModified() ) {

         long optionNumber = pCursiVision -> handleUnsavedDocument();

         if ( IDDI_OPTIONS_OPTION_1 == optionNumber ) {
            if ( IDC_GETFILE == LOWORD(wParam) )
               pCursiVision -> openAfterSaving = true;
            PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_SAVE_AS,0),0L);
            return (LRESULT)0;
         }

         if ( IDDI_OPTIONS_OPTION_3 == optionNumber ) {
            return (LRESULT)0;
         }

      }

      if ( IDC_CLOSE_FILE == LOWORD(wParam) ) {
         pCursiVision -> closeDocument();
         break;
      }

      OPENFILENAME openFileName;
      char szFilter[MAX_PATH],szFile[MAX_PATH];

      memset(szFilter,0,sizeof(szFilter));
      memset(szFile,0,sizeof(szFile));
      memset(&openFileName,0,sizeof(OPENFILENAME));

      sprintf(szFilter,"PDF Documents");
      long k = (DWORD)strlen(szFilter) + (DWORD)sprintf(szFilter + (DWORD)strlen(szFilter) + 1,"*.pdf");
      k = k + (DWORD)sprintf(szFilter + k + 2,"All Files");
      sprintf(szFilter + k + 3,"*.*");

      openFileName.lStructSize = sizeof(OPENFILENAME);
      openFileName.hwndOwner = hwnd;
      openFileName.Flags = OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_PATHMUSTEXIST;
      openFileName.lpstrFilter = szFilter;
      openFileName.lpstrFile = szFile;
      openFileName.lpstrDefExt = "pdf";
      openFileName.nFilterIndex = 1;
      openFileName.nMaxFile = MAX_PATH;
      openFileName.lpstrTitle = "Select the existing pdf file";

      openFileName.lpstrInitialDir = szUserDirectory;

      if ( ! GetOpenFileName(&openFileName) ) 
         break;

      memset(pCursiVision -> szLastActiveDocument,0,sizeof(pCursiVision -> szLastActiveDocument));

      pCursiVision -> openDocument(openFileName.lpstrFile,1,true);

      }
      break;

   case IDC_REOPEN_SOURCE: {

      if ( ! pCursiVision -> isDocumentOpen )
         break;

      if ( pCursiVision -> documentIsModified() ) {

         long optionNumber = pCursiVision -> handleUnsavedDocument();

         if ( IDDI_OPTIONS_OPTION_1 == optionNumber ) {
            if ( IDC_GETFILE == LOWORD(wParam) )
               pCursiVision -> openAfterSaving = true;
            PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_SAVE_AS,0),0L);
            return (LRESULT)0;
         }

         if ( IDDI_OPTIONS_OPTION_3 == optionNumber ) {
            return (LRESULT)0;
         }

      }

      pCursiVision -> openDocument(pCursiVision -> szLastActiveDocument,1,true);

      }
      break;

   case IDC_SAVE_AS: {

      if ( ! pCursiVision -> documentIsOpen() ) {
         MessageBox(hwndMainFrame,"Please open a PDF Document first","Note",MB_OK | MB_ICONEXCLAMATION);
         break;
      }

      OPENFILENAME openFileName;
      char szFilter[MAX_PATH],szFile[MAX_PATH];

      memset(szFilter,0,sizeof(szFilter));
      memset(szFile,0,sizeof(szFile));
      memset(&openFileName,0,sizeof(OPENFILENAME));

      strcpy(szFile,pCursiVision -> szActiveDocument);

      sprintf(szFilter,"PDF Documents");
      long k = (long)strlen(szFilter) + (long)sprintf(szFilter + (DWORD)strlen(szFilter) + 1,"*.pdf");
      k = k + (long)sprintf(szFilter + k + 2,"All Files");
      sprintf(szFilter + k + 3,"*.*");

      openFileName.lStructSize = sizeof(OPENFILENAME);
      openFileName.hwndOwner = hwnd;
      openFileName.Flags = OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR | OFN_PATHMUSTEXIST;
      openFileName.lpstrFilter = szFilter;
      openFileName.lpstrFile = szFile;
      openFileName.lpstrDefExt = "pdf";
      openFileName.nFilterIndex = 1;
      openFileName.nMaxFile = MAX_PATH;
      openFileName.lpstrTitle = "Select the target pdf file";

      openFileName.lpstrInitialDir = szUserDirectory;

      if ( ! GetSaveFileName(&openFileName) ) 
         break;

      BSTR bstrFile = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,openFileName.lpstrFile,-1,bstrFile,MAX_PATH);

      pCursiVision -> pIPdfDocument -> Write(bstrFile);
 
      pCursiVision -> openDocument(openFileName.lpstrFile,1,true);
 
      SysFreeString(bstrFile);

      if ( pCursiVision -> quitAfterSaving )
         PostQuitMessage(0);

      if ( pCursiVision -> openAfterSaving ) {
         if ( pCursiVision -> lParamAfterSaving || pCursiVision -> wParamAfterSaving )
//
//NTC: 04-09-2014. Check that this works when running in administrator mode. Apparantly windows doesn't
// allow drag-drop in adminstrator mode - so the drop mechanism might not work
//
            PostMessage(hwndMainFrame,WM_DROPFILES,pCursiVision -> wParamAfterSaving,pCursiVision -> lParamAfterSaving);
         else
            PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_GETFILE,0),0L);
      }

      pCursiVision -> openAfterSaving = false;
      pCursiVision -> lParamAfterSaving = 0L;
      pCursiVision -> wParamAfterSaving = 0L;

      }
      break;

   case ID_FILE_EXIT:
   case IDC_EXIT:
      PostMessage(hwndMainFrame,WM_POTENTIAL_QUIT,0L,0L);
      break;

   case ID_SETUP: {

      pCursiVision -> isGlobalPropertiesSetup = true;

      char szOldGlobalDataStore[MAX_PATH];

      strcpy(szOldGlobalDataStore,szGlobalDataStore);

      if ( ! pCursiVision -> pIPrintingSupport )
         pCursiVision -> createPrintingSupport();

      bool wasPrintingSupportProfile = ! ( NULL == pCursiVision -> PrintingProfile() );

      pCursiVision -> pIGProperties -> ShowProperties(hwnd,NULL);

      if ( ! wasPrintingSupportProfile ) 
         pCursiVision -> PrintingProfile((IPrintingSupportProfile *)0x00000001);

      pCursiVision -> isGlobalPropertiesSetup = false;

      if ( strcmp(szGlobalDataStore,szOldGlobalDataStore) ) 
         pCursiVision -> reCreatePrintingSupport();

      }

      break;

   case ID_CURSIVISION_ONLINE_HOME:
      ShellExecute(NULL, "open", "http://www.cursivision.com",NULL,NULL,SW_SHOWNORMAL);
      break;

   case ID_CURSIVISION_ONLINE_TRAINING_VIDEOS:
      ShellExecute(NULL, "open", "http://www.cursivision.com/index.php?option=com_content&view=section&layout=blog&id=23&itemId=49",NULL,NULL,SW_SHOWNORMAL);
      break;

   case ID_ABOUT:
      DialogBoxParam(hModule,MAKEINTRESOURCE(IDD_ABOUT),hwnd,(DLGPROC)CursiVision::aboutHandler,(ULONG_PTR)(void *)pCursiVision);
      break;

   case ID_HELP_INSTRUCTIONS: {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"%s\\help\\CursiVision.chm",szProgramDirectory);
      DoHelp(hwndMainFrame,szTemp);
      }
      break;

   case ID_RESET_DOODLE_OPTIONS: 
      char szTemp[1024];
      sprintf(szTemp,"Are you sure you would like to reset all the settings to the defaults for:\n\n\t%s",pCursiVision -> szActiveDocumentRootName);
      if ( IDNO == MessageBox(NULL,szTemp,"Note",MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) )
         break;
      pCursiVision -> pDoodleOptions -> DeleteSettingsFiles(pCursiVision -> szActiveDocument);
      break;

   case ID_DOODLE_FORGET:
   case ID_DOODLE_OPTIONS: {

      long countLocations = pCursiVision -> pDoodleOptions -> SignedLocationCount();

      if ( ID_DOODLE_OPTIONS == LOWORD(wParam) ) 
         pCursiVision -> pDoodleOptions -> ShowProperties();
      else {
         pCursiVision -> pDoodleOptions -> ClearSigningLocations(true);
         countLocations = 0;
      }

      TBBUTTONINFO buttonInfo;
      memset(&buttonInfo,0,sizeof(TBBUTTONINFO));
      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;

      if ( countLocations ) 
         buttonInfo.fsState = TBSTATE_ENABLED;
      else 
         buttonInfo.fsState = 0;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_FORGET,(LPARAM)&buttonInfo);

      if ( pCursiVision -> pSignatureProcess && ID_DOODLE_FORGET == LOWORD(wParam) ) {
         SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE_CANCEL,0),0L);
         SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE,0),0L);
      }

      }
      break;

   case ID_DOODLE: {

      if ( ! pCursiVision ->  documentIsOpen() ) {

         if ( pCursiVision -> isDocumentOpenInProgress ) {
            PostMessage(hwnd,WM_COMMAND,wParam,lParam);
            return (LRESULT)0;
         }

         MessageBox(hwndMainFrame,"Please open a PDF Document first","Note",MB_OK | MB_ICONEXCLAMATION);

         TBBUTTONINFO buttonInfo;
         memset(&buttonInfo,0,sizeof(TBBUTTONINFO));
         buttonInfo.cbSize = sizeof(TBBUTTONINFO);
         buttonInfo.dwMask = TBIF_STATE;
         buttonInfo.fsState = 0;
         SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

         break;

      }

      if ( ! pCursiVision -> isWritingEnabled )
         break;

      if ( pCursiVision -> reRunDoodle ) {
         pCursiVision -> reRunDoodle = false;
         PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(ID_DOODLE,0),0L);
         break;
      }

      pCursiVision -> pauseTracking();

      TBBUTTONINFO buttonInfo = {0};
      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;
      buttonInfo.fsState = TBSTATE_CHECKED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

      buttonInfo.fsState = TBSTATE_ENABLED;

      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

      if ( 0 == HIWORD(wParam) )
         pCursiVision -> doodleCount = 0;

      pCursiVision -> hasExitDeferral = ! pCursiVision -> currentProcessDisposition() -> doExit;

      if ( pCursiVision -> pSignatureProcess )
         delete pCursiVision -> pSignatureProcess;

      pCursiVision -> pSignatureProcess = new SignatureProcess();

      pCursiVision -> notifyDocumentIsTracked();

      }
      break;

   case ID_DOODLE_CANCEL: {

      TBBUTTONINFO buttonInfo = {0};

      buttonInfo.cbSize = sizeof(TBBUTTONINFO);
      buttonInfo.dwMask = TBIF_STATE;

      buttonInfo.fsState = 0;
      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE_CANCEL,(LPARAM)&buttonInfo);

      buttonInfo.fsState = pCursiVision -> isWritingEnabled ? TBSTATE_ENABLED : 0;
      SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

      if ( ! pCursiVision -> pSignatureProcess )
         break;

      SendMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(IDCANCEL,0),0L);

      if ( hwndSigningControl )
         ShowWindow(hwndSigningControl,SW_HIDE);

      if ( pCursiVision -> doodleCount && ( pCursiVision -> pDoodleOptions && 0 == pCursiVision -> pDoodleOptions -> InitialSignedLocationCount() ) ) {
         if ( 1L != lParam ) {
            long needToImplementAbilityToStoreAndEraseSignatureByIndex = -1L;
            pCursiVision -> updateDocument(needToImplementAbilityToStoreAndEraseSignatureByIndex,false);
         }
      } else {
         if ( pCursiVision -> pDoodleOptions && 0 == pCursiVision -> pDoodleOptions -> InitialSignedLocationCount() )
            pCursiVision -> pDoodleOptions -> ClearSigningLocations(false);
      }

      if ( hwndSigningControl ) {
         DestroyWindow(hwndSigningControl);
         hwndSigningControl = NULL;
      }

      pCursiVision -> CancelSignatureProcess();

      if ( pCursiVision -> pSignatureProcess )
         delete pCursiVision -> pSignatureProcess;

      pCursiVision -> pSignatureProcess = NULL;

      pCursiVision -> isImmediateDoodle = false;

      SetWindowPos(hwndMainFrame,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);

      SetWindowText(hwndStatusBar,"Operation cancelled");

      }
      break;

   default:
      break;

   }

   return DefWindowProc(hwnd,WM_COMMAND,wParam,lParam);
   }
