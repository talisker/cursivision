// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   LRESULT CALLBACK paintHandler(HWND hwnd,WPARAM wParam,LPARAM lParam) {

   PAINTSTRUCT psPaint;

   BeginPaint(hwnd,&psPaint);

   if ( ! pCursiVision -> pSignatureProcess || ! pCursiVision -> pSignatureProcess -> SignaturePad() || ! pCursiVision -> isDocumentOpen ) {
      EndPaint(hwnd,&psPaint);
      if ( 1L == wParam )
         return (LRESULT)0;
      return DefWindowProc(hwnd,WM_PAINT,wParam,lParam);
   }

   EndPaint(hwnd,&psPaint);

   return (LRESULT)TRUE;
   }

