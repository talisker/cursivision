/*
                       Copyright (c) 2009,2010,2011 Nathan T. Clark
*/

#include "insertPDF.h"

#define OBJECT_WITH_PROPERTIES doodleOptions

#define CURSIVISION_SERVICES_INTERFACE pInsertPDF -> pICursivisionServices

#ifdef CURSIVISION_CONTROL_BUILD
#define IS_CURSIVISION_CONTROL_HANDLER
#endif

#include "multisignOptionsBody.cpp"

