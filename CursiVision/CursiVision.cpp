// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   IClassFactory *CursiVision::pIClassFactory_SignaturePads = NULL;

   CursiVision::CursiVision(IUnknown *pIUnknownOuter) :

      connectionPointContainer(this),
      connectionPoint(this),
      enumConnectionPoints(0),
      enumConnections(0),

      pIPdfEnabler(NULL),
      pIPdfDocument(NULL),
      pIPdfPage(NULL),
      pDoodleOptions(NULL),

      pIGProperties(NULL),
      pIGPropertyPageClient(NULL),

      pIOleClientSite_HTML_Host(NULL),
      pIOleDocumentSite_HTML_Host(NULL),
      pIOleInPlaceSite_HTML_Host(NULL),
      pIOleInPlaceFrame_HTML_Host(NULL),

      pIOleObject_HTML(NULL),
      pIOleInPlaceObject_HTML(NULL),
      pIOleInPlaceActiveObject_HTML(NULL),

      pIPrintingSupport(NULL),
      pIPrintingSupportProfile(NULL),
      printingSupportInitialized(INVALID_HANDLE_VALUE),

      pICursiVisionServices(NULL),

      isPrintProfileTraining(false),
      isDoodleProfileTraining(false),
      isProcessingSaveAndReopen(false),
      isGlobalPropertiesSetup(false),
      ignoreDocumentOpenProcessingSteps(false),

      pSignatureProcess(NULL),

      pdfPageWidth(0),
      pdfPageHeight(0),
      pdfPageCount(0),

      pdfPageNumber(0),
      
      pdfAdobePageNumber(0),

      xPixelsPerPageUnit(0.0),
      yPixelsPerPageUnit(0.0),

      padToPDFScale(0.0),
      lastPadToPDFScale(0.0),

      cachedPadWidthPixels(0L),
      cachedPadHeightPixels(0L),
      cachedPadWidthInches(0.0),
      
      isImmediateDoodle(false),
      isRestored(false),
      isDocumentOpen(false),
      isDocumentOpenInProgress(false),
      isDocumentModified(false),
      isInterimDocument(false),
      isPrintJob(false),
      hasExitDeferral(false),
      reRunDoodle(false),
      cancelOpenInProgress(false),

      activeMonitor(-1L),
      monitorCount(0L),
      wasMaximized(false),

      signatureEndTimerSet(false),
      autoScrollMode(false),
      handledByBackEnd(false),

      expectingPostPaint(false),
      signActivityCanceled(false),

      adviseWaiting(true),

      isOkayToAutoOpen(true),

      quitAfterSaving(false),
      openAfterSaving(false),
      wParamAfterSaving(0L),
      lParamAfterSaving(0L),

      hwndDispositionSettings(NULL),
      hwndAdditionalBackEnds(NULL),

      hAccelerator(NULL),

      processingDisposition(),

      bstrActiveDocument(NULL),

      doodleCount(0),

      optionChoice(0),
      optionPaneWidth(0),
      optionPaneHeight(0),
      optionHeight(0),
      defaultOption(0),

      pRealTimeSignatureGraphic(NULL),
      pLastSignatureGraphic(NULL),

      pDWebBrowserEvents_HTML_Host(NULL),

      pIConnectionPoint_HTML(NULL),
      connectionCookie_HTML(0L),

      pIConnectionPoint_PDFiumControlEvents(NULL),
      connectionCookie_PDFiumControlEvents(0L),

      pdfPageNumberInAbbayence(-1L),

      pISignaturePad_PropertiesOnly(NULL),
      signaturePadHasBeenPinged(false),

      threadTrackPageNumberAddress(0L),
      hThreadTrackPageNumber(NULL),

      trackPageNumberDone(INVALID_HANDLE_VALUE),

      refCount(0)

   {

   memset(&rectFrame,0,sizeof(RECT));
   memset(&rectDoodleOptions,0,sizeof(RECT));
   memset(&rcHTMLHost,0,sizeof(RECT));

   memset(szActiveDocument,0,sizeof(szActiveDocument));
   memset(szActiveDocumentRootName,0,sizeof(szActiveDocumentRootName));
   memset(szSignedDocument,0,sizeof(szSignedDocument));
   memset(szLastActiveDocument,0,sizeof(szLastActiveDocument));
   memset(szInterimFileName,0,sizeof(szInterimFileName));
   memset(szPropertiesFile,0,sizeof(szPropertiesFile));
   memset(szForwardToServer,0,sizeof(szForwardToServer));
   memset(szWelcomeURL,0,sizeof(szWelcomeURL));

   memset(&pdfDocumentUpperLeft,0,sizeof(POINTL));
   memset(&pdfDocumentUpperRight,0,sizeof(POINTL));
   memset(&pdfDocumentLowerLeft,0,sizeof(POINTL));
   memset(&pdfDocumentLowerRight,0,sizeof(POINTL));
   memset(szOptionText,0,sizeof(szOptionText));
   memset(szOptions,0,sizeof(szOptions));
   memset(optionWidths,0,sizeof(optionWidths));
   memset(pdfPageLabel,0,sizeof(pdfPageLabel));

   memset(&trackPageNumberAccess,0,sizeof(CRITICAL_SECTION));

   memset(&rcMaximizedRestore,0,sizeof(RECT));

   char szTemp[MAX_PATH];

   GetCommonAppDataLocation(NULL,szTemp);

   sprintf(szApplicationDataDirectory,"%s\\CursiVision",szTemp);

   CreateDirectory(szApplicationDataDirectory,NULL);

   GetDocumentsLocation(NULL,szTemp);

   sprintf(szUserDirectory,"%s\\CursiVision Files",szTemp);

   CreateDirectory(szUserDirectory,NULL);

   sprintf(szPropertiesFile,"%s\\Settings\\CursiVision.settings",szApplicationDataDirectory);

   sprintf(szWelcomeURL,"%s\\WelcomeCursiVision.htm",szApplicationDataDirectory);

   pIGPropertyPageClient = new _IGPropertyPageClient(this);

   memset(&graphicListAccess,0,sizeof(CRITICAL_SECTION));

   InitializeCriticalSection(&graphicListAccess);

   pCursiVision = this;

   pICursiVisionServices = new _ICursiVisionServices(this);

   InitializeCriticalSection(&trackPageNumberAccess);

   return;
   }


   CursiVision::~CursiVision() {

   if ( SignaturePad() )
      SignaturePad() -> ClearTablet();

   if ( pISignaturePad_PropertiesOnly )
      pISignaturePad_PropertiesOnly -> Release();

   pISignaturePad_PropertiesOnly = NULL;

   quitTracking();

   if ( pIConnectionPoint_HTML ) {
      pIConnectionPoint_HTML -> Unadvise(connectionCookie_HTML);
      pIConnectionPoint_HTML -> Release();
      pIConnectionPoint_HTML = NULL;
   }

   if ( pIConnectionPoint_PDFiumControlEvents ) {
      pIConnectionPoint_PDFiumControlEvents -> Unadvise(connectionCookie_PDFiumControlEvents);
      pIConnectionPoint_PDFiumControlEvents -> Release();
      pIConnectionPoint_PDFiumControlEvents = NULL;
   }

   if ( pDWebBrowserEvents_HTML_Host ) 
      delete pDWebBrowserEvents_HTML_Host;

   pDWebBrowserEvents_HTML_Host = NULL;

   if ( pIPDFiumControlEvents )
      delete pIPDFiumControlEvents;

   pIPDFiumControlEvents = NULL;

   pIOleObject_HTML -> Close(OLECLOSE_NOSAVE);

   pIOleObject_HTML -> SetClientSite(NULL);

   pIOleInPlaceObject_HTML -> Release();

   if ( pIOleInPlaceActiveObject_HTML )
      pIOleInPlaceActiveObject_HTML -> Release();

   pIOleInPlaceActiveObject_HTML = NULL;

   pIOleObject_HTML -> Release();

   pIOleObject_HTML = NULL;

   //pIPDFiumControl -> Cleanup();

   long k = pIPDFiumControl -> FinalRelease();

   delete pIOleInPlaceFrame_HTML_Host;
   delete pIOleInPlaceSite_HTML_Host;
   delete pIOleClientSite_HTML_Host;
   delete pIOleDocumentSite_HTML_Host;

   DeleteCriticalSection(&graphicListAccess);

   if ( ! isImmediateDoodle )
      pIGProperties -> Save();

   if ( pSignatureProcess ) {
      delete pSignatureProcess;
      pSignatureProcess = NULL;
   }

   if ( pIGProperties )
      pIGProperties -> Release();

   if ( pDoodleOptions )
      delete pDoodleOptions;

   if ( pIPdfPage )
      pIPdfPage -> Release();

   if ( pIPdfDocument )
      pIPdfDocument -> Release();

   if ( pIPdfEnabler )
      pIPdfEnabler -> Release();
//
//NTC: 09-20-2014: I am not sure why deleting the PrintingSupport object is causing a problem, but it should be 
// checked out.
#if 0
   if ( pIPrintingSupport )
      pIPrintingSupport -> Release();
#endif

   if ( bstrActiveDocument )
      SysFreeString(bstrActiveDocument);

   if ( szInterimFileName[0] )
      DeleteFile(szInterimFileName);

   if ( szDefaultPrinter[0] )
      SetDefaultPrinter(szDefaultPrinter);

   for ( std::list<BSTR>::iterator it = menuBackends.begin(); it != menuBackends.end(); it++ )
      SysFreeString((*it));
   
   menuBackends.clear();

   for ( std::list<backEndPackage *>::iterator it = allBackEnds.begin(); it != allBackEnds.end(); it++ )
      delete (*it);
   
   allBackEnds.clear();

   if ( pICursiVisionServices )
      delete pICursiVisionServices;

   if ( hwndHTMLHost )
      DestroyWindow(hwndHTMLHost);

   pCursiVision = NULL;

   return;
   }


   long CursiVision::essentialInitialization() {

   if ( pIGProperties )
      return 0L;

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pIGProperties));

   if ( ! pIGProperties ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"The InnoVisioNate Properties control is not registered. Please try re-installing CursiVision.");
      MessageBox(NULL,szTemp,"Error",MB_OK | MB_ICONEXCLAMATION);
      CoFreeUnusedLibraries();
      CoUninitialize();
      exit(0);
   }

   rc = CoCreateInstance(CLSID_PDFiumControl,NULL,CLSCTX_ALL,IID_IPDFiumControl,reinterpret_cast<void **>(&pIPDFiumControl));

   if ( ! pIPDFiumControl ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"The InnoVisioNate PDF Viewer Control is not registered. Please try re-installing CursiVision.");
      MessageBox(NULL,szTemp,"Error",MB_OK | MB_ICONEXCLAMATION);
      CoFreeUnusedLibraries();
      CoUninitialize();
      exit(0);
   }

   INITCOMMONCONTROLSEX icex;
   icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
   icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES | ICC_TAB_CLASSES | ICC_LISTVIEW_CLASSES | ICC_PROGRESS_CLASS;

   InitCommonControlsEx(&icex);

   return 0;
   }


   long CursiVision::initialize(bool okayToAutoOpen,bool loadWelcomePage) {

   isOkayToAutoOpen = okayToAutoOpen;

   pIGProperties -> Advise(static_cast<IGPropertiesClient *>(this));

   pIGProperties -> Add(L"frame",NULL);
   pIGProperties -> DirectAccess(L"frame",TYPE_BINARY,&rectFrame,sizeof(RECT));

   pIGProperties -> Add(L"ownerFrame",NULL);
   pIGProperties -> DirectAccess(L"ownerFrame",TYPE_BINARY,&pCursiVision -> rectFrame,sizeof(RECT));

   pIGProperties -> Add(L"document",NULL);
   pIGProperties -> DirectAccess(L"document",TYPE_BINARY,szActiveDocument,sizeof(szActiveDocument));

   pIGProperties -> Add(L"processing disposition",NULL);
   pIGProperties -> DirectAccess(L"processing disposition",TYPE_BINARY,&processingDisposition,sizeof(processingDisposition));

   pIGProperties -> Add(L"doodle options",NULL);
   pIGProperties -> DirectAccess(L"doodle options",TYPE_BINARY,&rectDoodleOptions,sizeof(rectDoodleOptions));

   pIGProperties -> Add(L"advise waiting",NULL);
   pIGProperties -> DirectAccess(L"advise waiting",TYPE_BINARY,&adviseWaiting,sizeof(adviseWaiting));

   pIGProperties -> Add(L"active monitor",NULL);
   pIGProperties -> DirectAccess(L"active monitor",TYPE_BINARY,&activeMonitor,sizeof(activeMonitor));

   pIGProperties -> Add(L"monitor count",NULL);
   pIGProperties -> DirectAccess(L"monitor count",TYPE_BINARY,&monitorCount,sizeof(monitorCount));

   pIGProperties -> Add(L"was maximized",NULL);
   pIGProperties -> DirectAccess(L"was maximized",TYPE_BINARY,&wasMaximized,sizeof(wasMaximized));

   pIGProperties -> Add(L"maximized restore",NULL);
   pIGProperties -> DirectAccess(L"maximized restore",TYPE_BINARY,&rcMaximizedRestore,sizeof(RECT));

   pIGProperties -> AdvisePropertyPageClient(pIGPropertyPageClient,true);

   if ( ! hwndMainFrame )
      if ( ! initWindows(loadWelcomePage) )
         return 0;

   short isLoaded = FALSE;

   BSTR bstrFileName = SysAllocStringLen(NULL,MAX_PATH);
   MultiByteToWideChar(CP_ACP,0,szPropertiesFile,-1,bstrFileName,MAX_PATH);
   pIGProperties -> put_FileName(bstrFileName);
   SysFreeString(bstrFileName);
   pIGProperties -> LoadFile(&isLoaded);
   if ( ! isLoaded )
      InitNew();

   processingDisposition.isGlobalDisposition = true;

   HRESULT rc = CoCreateInstance(CLSID_PdfEnabler,NULL,CLSCTX_ALL,IID_IPdfEnabler,reinterpret_cast<void **>(&pIPdfEnabler));

   if ( ! pIPdfEnabler ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"The PDF Enabler is not registered. Please try re-installing CursiVision.");
      long rc = MessageBox(NULL,szTemp,"Error",MB_OK | MB_ICONEXCLAMATION);
      CoFreeUnusedLibraries();
      CoUninitialize();
      exit(0);
   }

   if ( rcHTMLHost.left == rcHTMLHost.right ) 
      GetClientRect(hwndHTMLHost,&rcHTMLHost);

   Loaded();

   if ( ! pISignaturePad_PropertiesOnly ) {

      HRESULT rc = CoCreateInstance(CLSID_CursiVisionSignaturePad,NULL,CLSCTX_ALL,IID_ISignaturePad,reinterpret_cast<void **>(&pISignaturePad_PropertiesOnly));
   
      if ( ! pISignaturePad_PropertiesOnly ) {
         char szTemp[2048];
         LoadString(NULL,IDS_NOT_REGISTERED,szTemp,sizeof(szTemp));
         MessageBox(NULL,szTemp,"Error",MB_OK | MB_ICONEXCLAMATION);
         CoFreeUnusedLibraries();
         CoUninitialize();
         exit(0);
      }

      pingSignaturePad();

      pIGProperties -> AddPropertyPage(pISignaturePad_PropertiesOnly,true);

      strcpy(deviceName,pISignaturePad_PropertiesOnly -> DeviceName());

      cachedPadWidthPixels = pISignaturePad_PropertiesOnly -> Width();
      cachedPadHeightPixels = pISignaturePad_PropertiesOnly -> Height();
      cachedPadWidthInches = pISignaturePad_PropertiesOnly -> WidthInInches();

   }

   return 1;
   }


   long CursiVision::pingSignaturePad() {

   signaturePadHasBeenPinged = true;

   if ( ! ( S_OK == pISignaturePad_PropertiesOnly -> Load(NULL,NULL,pICursiVisionServices) ) ) {

      char szMessage[1024];

      sprintf(szMessage,"The signature pad is not connected to your computer or is not running.\n\n"
                          "CursiVision is using the:\n\n\t%s\n\nsignature pad.\n\nPlease connect or configure the signature pad.\n\n"
                          "To configure the pad, try the Tools Setup menu option, the signature pad settings are available with the \"%s\" option on the left.",
                           pISignaturePad_PropertiesOnly -> DeviceName(),pISignaturePad_PropertiesOnly -> DeviceName());

      MessageBox(NULL,szMessage,"Error!",MB_ICONEXCLAMATION | MB_TOPMOST | MB_OK);

      isWritingEnabled = false;

   } else

      isWritingEnabled = true;

   TBBUTTONINFO buttonInfo;
   memset(&buttonInfo,0,sizeof(TBBUTTONINFO));
   buttonInfo.cbSize = sizeof(TBBUTTONINFO);
   buttonInfo.dwMask = TBIF_STATE;
   buttonInfo.fsState = isWritingEnabled ? TBSTATE_ENABLED : 0L;

   SendMessage(hwndToolBar,TB_SETBUTTONINFO,(WPARAM)ID_DOODLE,(LPARAM)&buttonInfo);

   return 1L;
   }


   long CursiVision::registerSignaturePad(char *pszOCXFileName) {

   if ( ! isAdministrator ) {
      char szMessage[1024];
      sprintf(szMessage,"Changing the Signature pad requires administrative privileges.\n\nPlease run CursiVision \"As Administrator\" to change the signature pad.");
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION);
      return 0L;
   }

   char szOCXFile[MAX_PATH];

   sprintf(szOCXFile,"%s\\%s",szProgramDirectory,pszOCXFileName);
   FILE *fX = fopen(szOCXFile,"rb");
   if ( ! fX ) {
      char szMessage[1024];
      sprintf(szMessage,"The signature pad implementation file:\n\n\t%s\n\nwas not found in the CursiVision Installation directory.\n\n"
                          "Reinstalling CursiVision may help.\n\nThe active signature pad has not been changed.",pszOCXFileName);
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION);
      return 0L;
   }

   fclose(fX);

   CRITICAL_SECTION *pCriticalSection = new CRITICAL_SECTION;

   InitializeCriticalSection(pCriticalSection);

   EnterCriticalSection(pCriticalSection);

   HMODULE hModule = LoadLibrary(szOCXFile);

   if ( ! hModule ) {
      char szMessage[1024];
      sprintf(szMessage,"The signature pad implementation file:\n\n\t%s\n\nwas found, however, it does not appear to be a valid CursiVision signature pad interface file.",pszOCXFileName);
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION);
      LeaveCriticalSection(pCriticalSection);
      DeleteCriticalSection(pCriticalSection);
      delete pCriticalSection;
      return 0L;
   }

   long (__stdcall *regServer)() = (long (__stdcall *)())GetProcAddress(hModule,"DllRegisterServer");

   long (__stdcall *getClassObject)(REFCLSID rclsid, REFIID riid, void **ppObject) = (long (__stdcall *)(REFCLSID rclsid, REFIID riid, void **ppObject))GetProcAddress(hModule,"DllGetClassObject");

   if ( ! regServer || ! getClassObject ) {
      char szMessage[1024];
      sprintf(szMessage,"The signature pad implementation file:\n\n\t%s\n\nwas found, however, CursiVision is unable to register it because either or both of DllRegisterServer and DllGetClassObject is not implemented.",pszOCXFileName);
      MessageBox(hwndMainFrame,szMessage,"Error",MB_ICONEXCLAMATION);
      LeaveCriticalSection(pCriticalSection);
      DeleteCriticalSection(pCriticalSection);
      delete pCriticalSection;
      FreeLibrary(hModule);
      return 0L;   
   }

   if ( pIClassFactory_SignaturePads )
      pIClassFactory_SignaturePads -> Release();

   pIClassFactory_SignaturePads = NULL;

   if ( pISignaturePad_PropertiesOnly ) {
      pIGProperties -> RemovePropertyPage(pISignaturePad_PropertiesOnly,true);
      pISignaturePad_PropertiesOnly -> Release();
   }

   signatureBox::ReleaseSignaturePad();

   regServer();

   pISignaturePad_PropertiesOnly = NULL;

   getClassObject(CLSID_CursiVisionSignaturePad,IID_IClassFactory,reinterpret_cast<void **>(&pIClassFactory_SignaturePads)); 

   pIClassFactory_SignaturePads -> CreateInstance(NULL,IID_ISignaturePad,reinterpret_cast<void **>(&pISignaturePad_PropertiesOnly));

   pIGProperties -> AddPropertyPage(pISignaturePad_PropertiesOnly,true);
   
   strcpy(deviceName,pISignaturePad_PropertiesOnly -> DeviceName());

   cachedPadWidthPixels = pISignaturePad_PropertiesOnly -> Width();
   cachedPadHeightPixels = pISignaturePad_PropertiesOnly -> Height();
   cachedPadWidthInches = pISignaturePad_PropertiesOnly -> WidthInInches();

   CoFreeUnusedLibraries();

   LeaveCriticalSection(pCriticalSection);

   DeleteCriticalSection(pCriticalSection);

   delete pCriticalSection;

   pingSignaturePad();

   return 1;
   }


   double CursiVision::XPixelsPerPageUnit() {
   return xPixelsPerPageUnit;
   }


   double CursiVision::YPixelsPerPageUnit() {
   return yPixelsPerPageUnit;
   }


   long CursiVision::toPixels(double inPageCoordinates) {
   return (long)(inPageCoordinates * yPixelsPerPageUnit);
   }


   void CursiVision::toDocumentCoordinates(RECT *pRect) {

   pRect -> left -= pdfDocumentUpperLeft.x;
   pRect -> right -= pdfDocumentUpperLeft.x;
   pRect -> top -= pdfDocumentUpperLeft.y;
   pRect -> bottom -= pdfDocumentUpperLeft.y;

   pRect -> left = (long)((double)pRect -> left / xPixelsPerPageUnit);
   pRect -> right = (long)((double)pRect -> right / xPixelsPerPageUnit);

   long cyPixels = pRect -> bottom - pRect -> top;

   pRect -> top = pdfPageHeight - (long)((double)pRect -> top / yPixelsPerPageUnit);
   pRect -> bottom = pRect -> top - (long)((double)cyPixels / yPixelsPerPageUnit);

   return;
   }


   void CursiVision::toDocumentCoordinates(RECT *pInput,RECT *pOutput) {
   memcpy(pOutput,pInput,sizeof(RECT));
   toDocumentCoordinates(pOutput);
   return;
   }


   void CursiVision::toDocumentCoordinates(POINTL *pPtl) {

   pPtl -> x -= pdfDocumentUpperLeft.x;
   pPtl -> y -= pdfDocumentUpperLeft.x;

   pPtl -> x = (long)((double)pPtl -> x / xPixelsPerPageUnit);
   pPtl -> y = pdfPageHeight - (long)((double)pPtl -> y / yPixelsPerPageUnit);

   return;
   }

   void CursiVision::toWindowCoordinates(RECT *pRect) {

//
//NTC: 02-19-2012: PDF Coordinates are a normal coordinate system where 
// positive Y is up - and the "bottom" coordinate should be less than the top coordinate.
// However, in other parts, maybe many parts, of the system, PDF coordinates have been
// being used as positive down.
// It is assumed that all rectangles maintained by the system are intended to be with the bottom
// below the top in the view. Therefore, if the PDF coordinates are in the natural PDF coordinate system
// (bottom is LESS than top - where in windows bottom is MORE than top) this transformation will detect 
// this and correct for it.
//
// This will be an issue if saved coordinates are transformed to windows - transformed back, and resaved.
// I do not think this is happening anywhere.
//
   long yTop = pRect -> top;
   long yBottom = pRect -> bottom;

   if ( yBottom < yTop ) {
      yTop = pdfPageHeight - yTop;
      yBottom = yTop + (pRect -> top - pRect -> bottom);
   }

   double cxWindow = (double)(pRect -> right - pRect -> left) * xPixelsPerPageUnit;
   double cyWindow = (double)(yBottom - yTop) * yPixelsPerPageUnit;

   double xWindow = (double)pRect -> left * xPixelsPerPageUnit;
   double yWindow = (double)yTop * yPixelsPerPageUnit;

   pRect -> left = (long)(xWindow + 0.5);
   pRect -> right = (long)(xWindow + cxWindow + 0.5);

   pRect -> top = (long)(yWindow + 0.5);
   pRect -> bottom = (long)(yWindow + cyWindow + 0.5);

   pRect -> left += pdfDocumentUpperLeft.x;
   pRect -> right += pdfDocumentUpperLeft.x;
   pRect -> top += pdfDocumentUpperLeft.y;
   pRect -> bottom += pdfDocumentUpperLeft.y;

   return;
   }

   void CursiVision::toWindowCoordinates(RECT *pInput,RECT *pOutput) {
   memcpy(pOutput,pInput,sizeof(RECT));
   toWindowCoordinates(pOutput);
   return;
   }


   void CursiVision::toWindowCoordinates(POINTL *pPtl) {
   pPtl -> x = (long)((double)pPtl -> x * xPixelsPerPageUnit);
   pPtl -> y = (long)((double)pPtl -> y * yPixelsPerPageUnit);
   pPtl -> x += pdfDocumentUpperLeft.y;
   pPtl -> y += pdfDocumentUpperLeft.y;
   return;
   }


   HRESULT CursiVision::createPrintingSupport() {

   if ( ! ( INVALID_HANDLE_VALUE == printingSupportInitialized ) ) {
      WaitForSingleObject(printingSupportInitialized,INFINITE);
      return S_OK;
   }

   HRESULT rc = CoCreateInstance(CLSID_CursiVisionPrintingSupport,NULL,CLSCTX_ALL,IID_IPrintingSupport,reinterpret_cast<void **>(&pIPrintingSupport));

   if ( ! pIPrintingSupport ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"The Printing Support control is not registered. Please try re-installing CursiVision.");
      MessageBox(NULL,szTemp,"Error",MB_ICONEXCLAMATION);
      return rc;
   }

   pIPrintingSupport -> ServicesAdvise(reinterpret_cast<void *>(pICursiVisionServices));

   pIPrintingSupport -> TakeMainWindow(hwndMainFrame);

   pIGProperties -> AddPropertyPage(pIPrintingSupport,true);

   return rc;
   }


   HRESULT CursiVision::reCreatePrintingSupport() {
   pIGProperties -> RemovePropertyPage(pCursiVision -> pIPrintingSupport,true);
   pIPrintingSupport -> Release();
   CloseHandle(printingSupportInitialized);
   printingSupportInitialized = INVALID_HANDLE_VALUE;
   createPrintingSupportThreaded();
   return S_OK;
   }


   void CursiVision::createPrintingSupportThreaded() {

   printingSupportInitialized = CreateSemaphore(NULL,0,1,NULL);

   HRESULT rc = CoCreateInstance(CLSID_CursiVisionPrintingSupport,NULL,CLSCTX_ALL,IID_IPrintingSupport,reinterpret_cast<void **>(&pIPrintingSupport));

   if ( ! pIPrintingSupport ) {
      char szTemp[MAX_PATH];
      sprintf(szTemp,"The Printing Support control is not registered. Please try re-installing CursiVision.");
      MessageBox(NULL,szTemp,"Error",MB_ICONEXCLAMATION);
      return;
   }

   pIPrintingSupport -> ServicesAdvise(reinterpret_cast<void *>(pICursiVisionServices));

   pIPrintingSupport -> TakeMainWindow(hwndMainFrame);

   pIPrintingSupport -> InitializeThreaded(printSupportInitialized,reinterpret_cast<void *>(this));

   return;
   }


   void CursiVision::printSupportInitialized(void *pv) {

   CursiVision *pThis = reinterpret_cast<CursiVision *>(pv);

   pThis -> pIGProperties -> AddPropertyPage(pThis -> pIPrintingSupport,true);

   ReleaseSemaphore(pThis -> printingSupportInitialized,1,NULL);

   CloseHandle(pThis -> printingSupportInitialized);

   pThis -> printingSupportInitialized = INVALID_HANDLE_VALUE;

   return;
   }


   void CursiVision::createDoodleOptions() {

   if ( pDoodleOptions )
      delete pDoodleOptions;

   pDoodleOptions = new doodleOptions(szActiveDocument,&processingDisposition);

   return;
   }
   

   IPrintingSupportProfile *CursiVision::PrintingProfile(IPrintingSupportProfile *psp) {
   if ( 0x00000001 == (ULONG_PTR)psp )
      pIPrintingSupportProfile = NULL;
   else if ( psp ) {
      pIPrintingSupportProfile = psp; 
      if ( pDoodleOptions )
         pDoodleOptions -> InitialSignedLocationCount(pIPrintingSupportProfile -> SigningRectangleCount());
   }
   return pIPrintingSupportProfile; 
   }


   void CursiVision::SetPageNumber(long pageNumber) {

   if ( pageNumber == pdfPageNumber )
      return;

   pdfPageNumber = pageNumber;

   pIPDFiumControl -> GoToPage(pageNumber);

   return;
   }


   resultDisposition *CursiVision::currentProcessDisposition() {

   if ( isGlobalPropertiesSetup )
      return &processingDisposition;

   if ( pDoodleOptions )
      return pDoodleOptions -> ProcessingDisposition();

   return &processingDisposition;
   }


   CursiVision::doodleOptionProperties *CursiVision::currentDoodleOptionProperties() {

   if ( PrintingProfile() ) {
      CursiVision::doodleOptionProperties *pResult = NULL;
      PrintingProfile() -> GetDoodleProperties(reinterpret_cast<void **>(&pResult));
      return pResult;
   }

   if ( ! pDoodleOptions )
      return NULL;

   return (CursiVision::doodleOptionProperties *)pDoodleOptions -> DoodleOptionProperties();
   }


   char *CursiVision::currentSettingsFileName() {
   return pDoodleOptions -> GetDispositionSettingsFileName();
   }


   void CursiVision::cookSettingsFileName(char *pszDocumentName,char *pszResult,ICursiVisionBackEnd *pICursiVisionBackEnd,char *pszBackendSettingsFile) {

   char szDirectory[MAX_PATH],szRootName[MAX_PATH];

   strcpy(szDirectory,pszDocumentName);
   char *p = strrchr(szDirectory,'\\');
   if ( ! p )
      p = strrchr(szDirectory,'/');
   if ( p ) {
      *p = '\0';
      strcpy(szRootName,p + 1);
   } else
      strcpy(szRootName,pszDocumentName);
   p = strrchr(szRootName,'.');
   if ( p )
      *p = '\0';

   sprintf(pszResult,"%s\\Settings\\Doodle",szApplicationDataDirectory);

   CreateDirectory(pszResult,NULL);

   sprintf(pszResult + strlen(pszResult),"\\%ld-%s.settings",HashCode(szDirectory),szRootName);

   if ( pICursiVisionBackEnd ) {

      char szCodeName[128];
      BSTR codeName;

      pICursiVisionBackEnd -> get_CodeName(&codeName);

      WideCharToMultiByte(CP_ACP,0,codeName,-1,szCodeName,128,0,0);

      SysFreeString(codeName);

      strcpy(pszBackendSettingsFile,pszResult);

      char *p = strrchr(pszBackendSettingsFile,'.');
      if ( p )
         *p = '\0';

      sprintf(pszBackendSettingsFile + strlen(pszBackendSettingsFile),"_%s.settings",szCodeName);

   }

   return;
   }


   char * CursiVision::GetDispositionSettingsFileName() {

   static char szSettingsName[MAX_PATH];

   BSTR bstrFileName;

   pIGProperties -> get_FileName(&bstrFileName);

   WideCharToMultiByte(CP_ACP,0,bstrFileName,-1,szSettingsName,MAX_PATH,0,0);

   char *p = strrchr(szSettingsName,'.');

   if ( p )
      *p = '\0';

   p = strrchr(szSettingsName,'\\');
   if ( ! p )
      p = strrchr(szSettingsName,'/');
   if ( p )
      *p = '\0';

   sprintf(szSettingsName + strlen(szSettingsName),"\\%s.disposition",NULL == p ? "" : p + 1);

   IGProperties *pProperties;

   HRESULT rc = CoCreateInstance(CLSID_InnoVisioNateProperties,NULL,CLSCTX_ALL,IID_IGProperties,reinterpret_cast<void **>(&pProperties));

   pProperties -> Add(L"result disposition",NULL);

   pProperties -> DirectAccess(L"result disposition",TYPE_BINARY,&processingDisposition,sizeof(resultDisposition));
   
   BSTR bstrNewFileName = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szSettingsName,-1,bstrNewFileName,MAX_PATH);

   pProperties -> put_FileName(bstrNewFileName);

   pProperties -> Save();

   pProperties -> Release();

   SysFreeString(bstrNewFileName);

   return szSettingsName;
   }


   BSTR CursiVision::getBackEndDescription(GUID theBackEnd) {

   HKEY hKey;
   DWORD cb,dwType = REG_SZ;
   OLECHAR szKey[MAX_PATH];
   OLECHAR szDescription[MAX_PATH];
   BSTR bstrClsId;

   StringFromCLSID(theBackEnd,&bstrClsId);

   swprintf(szKey,L"CLSID\\%s",bstrClsId);

   CoTaskMemFree(bstrClsId);

   RegOpenKeyExW(HKEY_CLASSES_ROOT,szKey,0,KEY_READ,&hKey);

   cb = MAX_PATH;
   RegQueryValueExW(hKey,NULL,0L,&dwType,(BYTE *)szDescription,&cb);
   RegCloseKey(hKey);
   if ( 0 == cb ) 
      return SysAllocString(L"No information available");

   return SysAllocString(szDescription);
   }


   BOOL CursiVision::IsContinuousDoodle() {
   if ( pSignatureProcess && &processingDisposition == currentProcessDisposition() && processingDisposition.doContinuousDoodle )
      return true;
   return false;
   }


   long CursiVision::handleUnsavedDocument(bool isForExit) {

   DLGTEMPLATE *dt = (DLGTEMPLATE *)LoadResource(hModule,FindResource(hModule,MAKEINTRESOURCE(IDD_OPTIONS),RT_DIALOG));

   memset(szOptions,0,sizeof(szOptions));

   sprintf(szOptionText,"Your document has been signed. Do you want to:\n\n\t\tSave the modified file,\n\t\tDiscard the changes,\n\t\tContinue writing ?");
   long k = 0L;
   if ( isForExit ) {
      k = sprintf(szOptions,"&Save and exit");
      k += sprintf(&szOptions[k + 1],"&Discard and exit");
      optionWidths[0] = 96;
      optionWidths[1] = 96;
      optionWidths[2] = 96;
   } else {
      k = sprintf(szOptions,"&Save");
      k += sprintf(&szOptions[k + 1],"&Discard");
      optionWidths[0] = 54;
      optionWidths[1] = 54;
      optionWidths[2] = 54;
   }
   sprintf(&szOptions[k + 2],"&Continue");
   optionPaneWidth = 256 + 128 + 64;
   optionPaneHeight = 128 + 64;
   optionHeight = 24;
   defaultOption = IDDI_OPTIONS_OPTION_1;

   DialogBoxIndirectParam(hModule,dt,hwndMainFrame,(DLGPROC)CursiVision::optionsHandler,(ULONG_PTR)this);

   return optionChoice;
   }



   void CursiVision::raiseMouseVellum() {
   if ( hwndMouseVellum )
      return;
   hwndMouseVellum = CreateWindowEx(0,"mouseVellum","Mouse Vellum",WS_CHILD | WS_VISIBLE,0,0,0,0,hwndMainFrame,NULL,NULL,NULL);
   SetCursor(LoadCursor(NULL,IDC_ARROW));
   SetWindowPos(hwndMouseVellum,HWND_TOP,0,toolBarHeight,cxHTMLHost - GetSystemMetrics(SM_CXVSCROLL),cyHTMLHost - toolBarHeight,0L);
   return;
   }


   void CursiVision::lowerMouseVellum() {
   DestroyWindow(hwndMouseVellum);
   hwndMouseVellum = NULL;
   return;
   }

   long CursiVision::padWidthPixels() {
   if ( cachedPadWidthPixels )
      return cachedPadWidthPixels;
   if ( ! pISignaturePad_PropertiesOnly )
      return 0;
   cachedPadWidthPixels = pISignaturePad_PropertiesOnly -> Width();
   return cachedPadWidthPixels;
   }

   long CursiVision::padHeightPixels() {
   if ( cachedPadHeightPixels )
      return cachedPadHeightPixels;
   if ( ! pISignaturePad_PropertiesOnly )
      return 0;
   cachedPadHeightPixels = pISignaturePad_PropertiesOnly -> Height();
   return cachedPadHeightPixels;
   }

   double CursiVision::padWidthInches() {
   if ( cachedPadWidthInches )
      return cachedPadWidthInches;
   if ( ! pISignaturePad_PropertiesOnly )
      return 0;
   cachedPadWidthInches = pISignaturePad_PropertiesOnly -> WidthInInches();
   return cachedPadWidthInches;
   }

   HRESULT CursiVision::PushProperties() {
   return pIGProperties -> Push();
   }

   HRESULT CursiVision::PopProperties() {
   return pIGProperties -> Pop();
   }

   HRESULT CursiVision::DiscardProperties() {
   return pIGProperties -> Discard();
   }

   HRESULT CursiVision::SaveProperties() {
   BSTR bstrFileName = NULL;
   pIGProperties -> get_FileName(&bstrFileName);
   if ( ! bstrFileName || 0 == bstrFileName[0] ) 
      return E_UNEXPECTED;
   return pIGProperties -> Save();
   }

