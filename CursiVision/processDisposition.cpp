// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <time.h>

   extern "C" int GetDocumentsLocation(HWND hwnd,char *);

   void CursiVision::processDisposition(resultDisposition *pDisposition,bool deferBackEnds,long reOpenedPageNumber,
                                          char *szResultFile,char *pszGraphicDataFileName,char *pszSettingsFileName) {

   SetWindowPos(hwndMainFrame,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);

   if ( szArguments[IDX_FILESAVEASNAME][0] ) {

      char *p = strrchr(szArguments[IDX_FILESAVEASNAME],'\\');
      if ( ! p )
         p = strrchr(szArguments[IDX_FILESAVEASNAME],'/');
      if ( p ) {
         *p = '\0';
         strcpy(pDisposition -> szFileStorageDirectory,szArguments[IDX_FILESAVEASNAME]);
         *p = '\\';
      }

      pDisposition -> doProperties = false;
      pDisposition -> doSave = true;
      pDisposition -> saveIn = true;
      pDisposition -> saveMyDocuments = false;
      pDisposition -> saveByOriginal = false;
      pDisposition -> doAppend = false;
      pDisposition -> doReplace = true;
      pDisposition -> doAppendDate = false;
      pDisposition -> doAppendTime = false;

   }

   char szLocalMyDocumentsDirectory[MAX_PATH];

   memset(szLocalMyDocumentsDirectory,0,sizeof(szLocalMyDocumentsDirectory));

   GetDocumentsLocation(hwndMainFrame,szLocalMyDocumentsDirectory);

   if ( szLocalMyDocumentsDirectory[0] ) {

      char szTemp[MAX_PATH];

      sprintf(szTemp,"%s\\%s",szLocalMyDocumentsDirectory,"My Signed Documents");

      strcpy(szLocalMyDocumentsDirectory,szTemp);
   
      strcpy(pDisposition -> szMyDocumentsDirectory,szLocalMyDocumentsDirectory);

   }

   bool isFileSaved = false;

   if ( pDisposition -> doProperties ) {

      bool oldValue = pDisposition -> userCanChangeDoProperties;

      pDisposition -> userCanChangeDoProperties = false;

      if ( PrintingProfile() && pSignatureProcess ) {

         IUnknown *pIUnknown = NULL;
         PrintingProfile() -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));
         PrintingProfile() -> ShowProperties(hwndMainFrame,pIUnknown);
         PrintingProfile() -> SaveProperties();
         pIUnknown -> Release();

      } else

         pIGProperties -> ShowProperties(hwndMainFrame,NULL);

      pDisposition -> userCanChangeDoProperties = oldValue;

   }

   BSTR bstrResultsFile = NULL;

   if ( szArguments[IDX_FILESAVEASNAME][0] ) {
   
      strcpy(szResultFile,szArguments[IDX_FILESAVEASNAME]);

      bstrResultsFile = SysAllocStringLen(NULL,MAX_PATH);

      MultiByteToWideChar(CP_ACP,0,szArguments[IDX_FILESAVEASNAME],-1,bstrResultsFile,MAX_PATH);

      pIPdfDocument -> Write(bstrResultsFile);

   } else if ( pDisposition -> doSave ) {

#define SAVE_FILE \
      pIPdfDocument -> Write(bstrResultsFile);

#include "savePDFFile.cpp"

   }

   if ( pDisposition -> doSave && ! isFileSaved )
      return;

   BSTR bstrOriginalResultsFile = NULL;

   SetWindowText(hwndStatusBar,szResultFile);

   if ( ! deferBackEnds && pDisposition -> countBackEnds && szResultFile[0] ) {
      if ( bstrResultsFile )
         SysFreeString(bstrResultsFile);
      bstrResultsFile = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szResultFile,-1,bstrResultsFile,MAX_PATH);
      bstrOriginalResultsFile = SysAllocString(bstrResultsFile);
   }

   bool deleteTheFile = false;
   bool temporaryCopyExists = false;

   if ( NULL == bstrResultsFile ) {
      bstrResultsFile = SysAllocString(_wtempnam(NULL,L"cvtmp"));
      bstrOriginalResultsFile = SysAllocString(bstrResultsFile);
      pIPdfDocument -> Write(bstrResultsFile);
      deleteTheFile = true;
      temporaryCopyExists = true;
   }

   handledByBackEnd = false;

   bool reOpenedByBackEnd = false;

   if ( ! deferBackEnds )
      processBackends(pDisposition,&bstrResultsFile,temporaryCopyExists,&handledByBackEnd,&reOpenedByBackEnd,pszGraphicDataFileName,pszSettingsFileName);

   if ( ! isFileSaved && pDisposition -> doRetain ) {
      char szRetainedActiveDocument[MAX_PATH];
      strcpy(szRetainedActiveDocument,szActiveDocument);
      if ( ! temporaryCopyExists ) {
         BSTR bstrInternal = _wtempnam(NULL,L"cvtmp");
         pIPdfDocument -> Write(bstrInternal);
         WideCharToMultiByte(CP_ACP,0,bstrInternal,-1,szActiveDocument,MAX_PATH,0,0);
         openDocument(szActiveDocument,1,true,false,true,false);
         DeleteFileW(bstrInternal);
         SysFreeString(bstrInternal);
      } else {
         WideCharToMultiByte(CP_ACP,0,bstrResultsFile,-1,szActiveDocument,MAX_PATH,0,0);
         openDocument(szActiveDocument,1,true,false,true,false);
      }
      strcpy(szActiveDocument,szRetainedActiveDocument);
      char *p = strrchr(szRetainedActiveDocument,'\\');
      if ( ! p )
         p = strrchr(szRetainedActiveDocument,'/');
      if ( p )
         SetWindowText(hwndMainFrame,p + 1);
      else
         SetWindowText(hwndMainFrame,szRetainedActiveDocument);
      isDocumentModified = true;
      handledByBackEnd = false;
   }

   if ( szArguments[IDX_FILESAVEASNAME][0] ) {
      BSTR bstrSaveAs = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szArguments[IDX_FILESAVEASNAME],-1,bstrSaveAs,MAX_PATH);
      if ( _wcsicmp(bstrSaveAs,bstrResultsFile) )
         CopyFileW(bstrResultsFile,bstrSaveAs,FALSE);
      SysFreeString(bstrSaveAs);
   }

   if ( isFileSaved && ! reOpenedByBackEnd )
      isDocumentModified = false;

   if ( deleteTheFile )
      DeleteFileW(bstrOriginalResultsFile);

   if ( bstrOriginalResultsFile )
      SysFreeString(bstrOriginalResultsFile);

   if ( pDisposition -> doReopenOriginal ) 

      openDocument(szActiveDocument,-1L == reOpenedPageNumber ? 1 : reOpenedPageNumber,true,false,false,false);

   else {

      if ( pDisposition -> doCloseDocument ) 

         PostMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(IDC_CLOSE_FILE,0L),0L);

      else if ( ! pDisposition -> doExit ) {

         if ( reOpenedByBackEnd ) 
            openDocument(bstrResultsFile,-1L == reOpenedPageNumber ? 1 : reOpenedPageNumber,true,false,true,true);

         else if ( isFileSaved )
            openDocument(bstrResultsFile,-1L == reOpenedPageNumber ? 1 : reOpenedPageNumber,true,false,true,true);
   
      }

   }

   if ( bstrResultsFile )
      SysFreeString(bstrResultsFile);

   return;
   }

