// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   void CursiVision::FinalizeSignatureProcess(bool deferProcessDisposition) {
   RedrawWindow(hwndMainFrame,NULL,NULL,RDW_UPDATENOW | RDW_INVALIDATE | RDW_ALLCHILDREN);
   long needToImplementAbilityToStoreAndEraseSignatureByIndex = -1L;
   updateDocument(needToImplementAbilityToStoreAndEraseSignatureByIndex,deferProcessDisposition);
   return;
   }


   void CursiVision::CancelSignatureProcess() {
   ClearSignatureGraphics();
   if ( SignaturePad() )
      SignaturePad() -> ClearTablet();
   RedrawWindow(hwndMainFrame,NULL,NULL,RDW_UPDATENOW | RDW_INVALIDATE | RDW_ALLCHILDREN);
   return;
   }


   void CursiVision::ClearSignatureGraphics() {

   EnterCriticalSection(&graphicListAccess);

   std::map<long,signatureGraphic *>::iterator it;
   for ( it = SignatureGraphics() -> begin(); it != SignatureGraphics() -> end(); it++ ) {
      signatureGraphic *pGS = (*it).second;
      if ( ! pGS -> isIndependentOfList && pGS != pLastSignatureGraphic )
         delete pGS;
   }

   SignatureGraphics() -> clear();

   LeaveCriticalSection(&graphicListAccess);

   if ( pRealTimeSignatureGraphic )
      delete pRealTimeSignatureGraphic;

   pRealTimeSignatureGraphic = NULL;

   return;
   }