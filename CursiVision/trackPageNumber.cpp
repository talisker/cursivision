// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <math.h>

   bool isDocumentTracked = false;
   bool doPauseTracking = false;
   bool isTrackingPaused = false;
   bool doQuitTracking = false;
   bool wantsTrackCycleFinished = false;
   bool wantsDocumentTracked = false;
   long trackCount = 0L;

#define SUFFICIENT_TRACK_COUNT   3

   void CursiVision::startTracking() {

   if ( ! hThreadTrackPageNumber ) {

      isDocumentTracked = false;
      doPauseTracking = false;
      isTrackingPaused = false;
      doQuitTracking = false;
      wantsTrackCycleFinished = false;
      wantsDocumentTracked = false;
      trackCount = 0L;

      hThreadTrackPageNumber = (HANDLE)_beginthreadex(NULL,0,CursiVision::trackPageNumberMonitor,(void *)pCursiVision,CREATE_SUSPENDED,&pCursiVision -> threadTrackPageNumberAddress);

      ResumeThread(hThreadTrackPageNumber);

      return;

   }

   resumeTracking();

   return;
   }



   void CursiVision::resumeTracking() {

   if ( ! hThreadTrackPageNumber ) {
      startTracking();
      return;
   }

   isDocumentTracked = false;
   doPauseTracking = false;

#if 1

   Sleep(50);
   return;

#else

   bool isPaused = false;

   EnterCriticalSection(&trackPageNumberAccess);
   isPaused = isTrackingPaused;
   LeaveCriticalSection(&trackPageNumberAccess);

   if ( ! isPaused )
      pauseTracking();

   EnterCriticalSection(&trackPageNumberAccess);
   isDocumentTracked = false;
   doPauseTracking = false;
   isTrackingPaused = true;
   LeaveCriticalSection(&trackPageNumberAccess);

   bool doExit = false;
   do {
      EnterCriticalSection(&trackPageNumberAccess);
      doExit = ! isTrackingPaused;
      LeaveCriticalSection(&trackPageNumberAccess);
      Sleep(50);
   } while ( ! doExit );
   return;
#endif
   }


   void CursiVision::pauseTracking() {

   if ( ! hThreadTrackPageNumber )
      return;

#if 1

   doPauseTracking = true;
   isDocumentTracked = false;
   Sleep(50);
   return;

#else
   EnterCriticalSection(&trackPageNumberAccess);
   doPauseTracking = true;
   isTrackingPaused = false;
   LeaveCriticalSection(&trackPageNumberAccess);

   bool doExit = false;
   do {
      EnterCriticalSection(&trackPageNumberAccess);
      doExit = isTrackingPaused;
      LeaveCriticalSection(&trackPageNumberAccess);
      Sleep(50);
   } while ( ! doExit );
   return;
#endif
   }



   void CursiVision::notifyDocumentIsTracked() {

   if ( ! isTrackingPaused )
      pauseTracking();

#if 1
   isDocumentTracked = false;
   wantsDocumentTracked = true;
   trackCount = 0L;
   resumeTracking();
   return;
#else

   EnterCriticalSection(&trackPageNumberAccess);
   isDocumentTracked = false;
   wantsDocumentTracked = true;
   trackCount = 0L;
   LeaveCriticalSection(&trackPageNumberAccess);
   resumeTracking();
   return;
#endif
   }

   void CursiVision::notifyTrackCycleEnded() {
   wantsTrackCycleFinished = true;
   return;
   }

   void CursiVision::quitTracking() {
//
//TODO: There is a hang on exiting CursiVision. It must be in this loop.
// Find and correct the hang.
//
#if 0
   doQuitTracking = true;
   while ( hThreadTrackPageNumber )
      Sleep(50);
#else
   if ( hThreadTrackPageNumber )
      TerminateThread(hThreadTrackPageNumber,0L);
#endif
   return;
   }


   void CursiVision::protectedTrackPageNumber() {
   EnterCriticalSection(&trackPageNumberAccess);
   CursiVision::trackPageNumber();
   LeaveCriticalSection(&trackPageNumberAccess);
   return;
   }


   unsigned int __stdcall CursiVision::trackPageNumberMonitor(void *p) {

   CursiVision *pThis = (CursiVision *)p;

   while ( ! doQuitTracking ) {

      EnterCriticalSection(&pThis -> trackPageNumberAccess);

      if ( doPauseTracking ) {
         isTrackingPaused = true;
         LeaveCriticalSection(&pThis -> trackPageNumberAccess);
         Sleep(100);
         continue;
      }

      isTrackingPaused = false;

      LeaveCriticalSection(&pThis -> trackPageNumberAccess);

      Sleep(FIND_PAGE_TIMER_DURATION);

      EnterCriticalSection(&pThis -> trackPageNumberAccess);

      CursiVision::trackPageNumber();

      if ( isDocumentTracked && wantsDocumentTracked ) {
         if ( ++trackCount > SUFFICIENT_TRACK_COUNT ) {
            PostMessage(hwndMainFrame,WM_DOCUMENT_IS_TRACKED,0L,0L);
            wantsDocumentTracked = false;
            trackCount = 0L;
         }
      }

      if ( isDocumentTracked && wantsTrackCycleFinished ) {
         PostMessage(hwndMainFrame,WM_DOCUMENT_TRACK_CYCLE_FINISHED,0L,0L);
         wantsTrackCycleFinished = false;
      }

      LeaveCriticalSection(&pThis -> trackPageNumberAccess);

   }

   pThis -> hThreadTrackPageNumber = NULL;

   return 0L;
   }

   
   void CALLBACK CursiVision::trackPageNumber() {

   if ( ! pCursiVision -> isDocumentOpen || pCursiVision -> isDocumentOpenInProgress ) 
      return;

   pCursiVision -> trackPageNumberDone = CreateSemaphore(NULL,0,1,NULL);

   PostMessage(hwndMainFrame,WM_TRACK_PAGE_NUMBER,0L,0L);

   WaitForSingleObject(pCursiVision -> trackPageNumberDone,INFINITE);

   CloseHandle(pCursiVision -> trackPageNumberDone);

   pCursiVision -> trackPageNumberDone = INVALID_HANDLE_VALUE;

   return;
   }


   void CursiVision::trackPageNumberUIThread() {
   
   long pageCount = 0L;

   long mostVisiblePage = 0L;

   pCursiVision -> pIPDFiumControl -> get_PageCount(&pageCount);

   pCursiVision -> pIPDFiumControl -> get_PDFPageMostVisible(&mostVisiblePage);

   sprintf_s(szPageNumberText,64,"Page: %ld of %ld",mostVisiblePage,pageCount);

   PostMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)2,(LPARAM)&szPageNumberText);

   pCursiVision -> pIPDFiumControl -> get_PDFPageXPixelsInView(pCursiVision -> pdfPageNumber,&pCursiVision -> pdfDocumentUpperLeft.x);

   pCursiVision -> pIPDFiumControl -> get_PDFPageYPixelsInView(pCursiVision -> pdfPageNumber,&pCursiVision -> pdfDocumentUpperLeft.y);

   if ( -32768 == pCursiVision -> pdfDocumentUpperLeft.y ) {

//Beep(2000,100);

   }

   long cx,cy;

   pCursiVision -> pIPDFiumControl -> get_PDFPageWidthPixels(pCursiVision -> pdfPageNumber,&cx);

   pCursiVision -> pIPDFiumControl -> get_PDFPageHeightPixels(pCursiVision -> pdfPageNumber,&cy);

   pCursiVision -> pdfDocumentUpperRight.x = pCursiVision -> pdfDocumentUpperLeft.x + cx;
   pCursiVision -> pdfDocumentUpperRight.y = pCursiVision -> pdfDocumentUpperLeft.y;

   pCursiVision -> pdfDocumentLowerRight.x = pCursiVision -> pdfDocumentUpperRight.x;
   pCursiVision -> pdfDocumentLowerRight.y = pCursiVision -> pdfDocumentUpperRight.y + cy;

   pCursiVision -> pdfDocumentLowerLeft.x = pCursiVision -> pdfDocumentUpperLeft.x;
   pCursiVision -> pdfDocumentLowerLeft.y = pCursiVision -> pdfDocumentUpperLeft.y + cy;

   pCursiVision -> pIPDFiumControl -> get_PDFWidth(&cx);

   pCursiVision -> xPixelsPerPageUnit = (double)(pCursiVision -> pdfDocumentUpperRight.x - pCursiVision -> pdfDocumentUpperLeft.x) / (double)(pCursiVision -> pdfPageWidth);

   pCursiVision -> yPixelsPerPageUnit = pCursiVision -> xPixelsPerPageUnit;

   isDocumentTracked = true;

   pCursiVision -> padToPDFScale = pCursiVision -> padWidthInches() * (double)(pCursiVision -> pdfDocumentUpperRight.x - pCursiVision -> pdfDocumentUpperLeft.x) *
                                 72.0 / (double)pCursiVision -> pdfPageWidth / (double)pCursiVision -> padWidthPixels();

   ReleaseSemaphore(pCursiVision -> trackPageNumberDone,1,NULL);

   return;
   }
