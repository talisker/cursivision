// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   long CursiVision::initWindows(bool loadWelcomePage) {

   if ( hwndMainFrame )
      return 0L;

   HWND hwndHostingParent = NULL;

   WNDCLASS gClass;
   
   memset(&gClass,0,sizeof(WNDCLASS));
   gClass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
   gClass.lpfnWndProc = handler;
   gClass.cbClsExtra = 32;
   gClass.cbWndExtra = 32;
   gClass.hInstance = hModule;
   gClass.hIcon = NULL;
   gClass.hCursor = NULL;
   gClass.hbrBackground = 0;
   gClass.lpszMenuName = NULL;
   gClass.lpszClassName = "insertPDF";
  
   RegisterClass(&gClass);

   gClass.lpfnWndProc = htmlHostHandler;
   gClass.lpszClassName = "htmlHost";

   RegisterClass(&gClass);

   gClass.lpfnWndProc = mouseVellumHandler;
   gClass.lpszClassName = "mouseVellum";

   RegisterClass(&gClass);

   HMENU hMenu = LoadMenu(NULL,MAKEINTRESOURCE(IDR_MAIN_MENU));

   hwndMainFrame = CreateWindowEx(WS_EX_CLIENTEDGE,"insertPDF","CursiVision",WS_OVERLAPPEDWINDOW,0,0,0,0,NULL,hMenu,NULL,NULL);

   if ( pCursiVision -> isPrintJob ) {
      SetFocus(hwndMainFrame);

#ifdef _DEBUG
#else
      SetWindowPos(hwndMainFrame,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);
#endif
   }

   hwndHTMLHost = CreateWindowEx(WS_EX_TRANSPARENT,"htmlHost","HTML Host",WS_CHILD | WS_VISIBLE,0,0,0,0,hwndMainFrame,NULL,NULL,NULL);

   hwndToolBar = CreateWindowEx(0L,TOOLBARCLASSNAME,"",WS_CHILD | WS_VISIBLE | TBSTYLE_TOOLTIPS,0,0,0,0,hwndMainFrame,NULL,NULL,NULL);

   hwndStatusBar = CreateWindowEx(0L,STATUSCLASSNAME,"",WS_CHILD | WS_VISIBLE | SBT_TOOLTIPS,0,0,0,CW_USEDEFAULT,hwndMainFrame,NULL,NULL,NULL);

   RECT rcStatusBar;

   GetWindowRect(hwndStatusBar,&rcStatusBar);

   statusBarHeight = rcStatusBar.bottom - rcStatusBar.top;

   char szTemp[MAX_PATH];

   MENUITEMINFO menuItemInfo;

   memset(&menuItemInfo,0,sizeof(MENUITEMINFO));

   menuItemInfo.cbSize = sizeof(MENUITEMINFO);

   menuItemInfo.fMask = MIIM_STRING | MIIM_SUBMENU;
   menuItemInfo.fType = MFT_STRING;
   menuItemInfo.dwTypeData = szTemp;
   menuItemInfo.cch = 128;   

   hMenuDoodle = NULL;
   hMenuFile = NULL;
   hMenuBackends = NULL;
   HMENU hMenuHelp = NULL;

   for ( long k = 0; 1; k++ ) {

      menuItemInfo.cch = 128;

      if ( ! GetMenuItemInfo(hMenu,k,TRUE,&menuItemInfo) ) 
         break;

      if ( 0 == _stricmp(szTemp,"&File") ) {
         hMenuFile = menuItemInfo.hSubMenu;
         if ( hMenuDoodle && hMenuHelp && hMenuTools )
            break;
      }

      if ( 0 == _stricmp(szTemp,"&Writing") ) {
         hMenuDoodle = menuItemInfo.hSubMenu;
         if ( hMenuFile && hMenuHelp && hMenuTools )
            break;
      }

      if ( 0 == _stricmp(szTemp,"&Tools") ) {

         hMenuTools = menuItemInfo.hSubMenu;

         MENUITEMINFO subMenuItemInfo = {0};

         subMenuItemInfo.cbSize = sizeof(MENUITEMINFO);

         subMenuItemInfo.fMask = MIIM_SUBMENU | MIIM_STRING;
         subMenuItemInfo.fType = MFT_STRING;
         subMenuItemInfo.dwTypeData = szTemp;
         subMenuItemInfo.cch = MAX_PATH;   

         for ( long j = 0; 1; j++ ) {
            subMenuItemInfo.cch = MAX_PATH;   
            if ( ! GetMenuItemInfo(hMenuTools,j,TRUE,&subMenuItemInfo) )
               break;
            if ( 0 == _stricmp(subMenuItemInfo.dwTypeData,"More Tools...") ) {
               hMenuBackends = subMenuItemInfo.hSubMenu;
               subMenuItemInfo.fMask = MIIM_ID;
               subMenuItemInfo.wID = ID_BACKEND_EXECUTE_1 - 1;
               SetMenuItemInfo(hMenuTools,j,TRUE,&subMenuItemInfo);
               break;
            }
         }

         if ( hMenuFile && hMenuHelp && hMenuTools )
            break;

      }

      if ( 0 == _stricmp(szTemp,"Help") ) {
         hMenuHelp = menuItemInfo.hSubMenu;
         if ( hMenuFile && hMenuDoodle && hMenuTools )
            break;
      }

   }

   HICON hIcon = LoadIcon(hModule,MAKEINTRESOURCE(IDD_ICON));

   SendMessage(hwndMainFrame,WM_SETICON,(WPARAM)ICON_SMALL,(LPARAM)hIcon);
   SendMessage(hwndMainFrame,WM_SETICON,(WPARAM)ICON_BIG,(LPARAM)hIcon);

   DragAcceptFiles(hwndMainFrame,TRUE);
   DragAcceptFiles(hwndHTMLHost,TRUE);

   HWND hwndToolTips = (HWND)SendMessage(hwndToolBar,TB_GETTOOLTIPS,0L,0L);

   SendMessage(hwndToolTips,TTM_ACTIVATE,(WPARAM)TRUE,0L);

   TOOLINFO toolInfo;

   memset(&toolInfo,0,sizeof(TOOLINFO));

   toolInfo.cbSize = sizeof(TOOLINFO);
   toolInfo.uFlags = TTF_CENTERTIP | 0*TTF_SUBCLASS;
   toolInfo.uId = IDC_GETFILE;
   toolInfo.hwnd = hwndToolBar;
   toolInfo.lpszText = "Open a pdf file";

   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   memset(&menuItemInfo,0,sizeof(MENUITEMINFO));

   menuItemInfo.cbSize = sizeof(MENUITEMINFO);
   menuItemInfo.fMask = MIIM_BITMAP;

   hBitmapSetup = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_PROPERTIES),IMAGE_BITMAP,16,16,LR_SHARED | LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT);
   hBitmapCancel = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_CANCEL),IMAGE_BITMAP,16,16,LR_SHARED | LR_LOADMAP3DCOLORS | LR_CREATEDIBSECTION);
   hBitmapHelp = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_HELP),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS);
   hBitmapForget = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_FORGET),IMAGE_BITMAP,16,16,LR_SHARED | LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT);

   menuItemInfo.hbmpItem = hBitmapSetup;
   SetMenuItemInfo(hMenu,ID_SETUP,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_OPEN),IMAGE_BITMAP,16,16,LR_SHARED | LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT);
   SetMenuItemInfo(hMenu,IDC_GETFILE,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_SAVE_AS),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT);
   SetMenuItemInfo(hMenu,IDC_SAVE_AS,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_DOODLE),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT);
   SetMenuItemInfo(hMenu,ID_DOODLE,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = hBitmapCancel;
   SetMenuItemInfo(hMenuDoodle,ID_DOODLE_CANCEL,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = hBitmapHelp;
   SetMenuItemInfo(hMenu,ID_HELP_INSTRUCTIONS,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_WEB),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS);
   SetMenuItemInfo(hMenuHelp,1,TRUE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_WEB_HOMEPAGE),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS);
   SetMenuItemInfo(hMenu,ID_CURSIVISION_ONLINE_HOME,FALSE,&menuItemInfo);

   menuItemInfo.hbmpItem = (HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_WEB_TRAINING),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS);
   SetMenuItemInfo(hMenu,ID_CURSIVISION_ONLINE_TRAINING_VIDEOS,FALSE,&menuItemInfo);

   RECT rcItem;
   HFONT hGUIFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

   HIMAGELIST toolbarImageList = ImageList_Create(16,16,ILC_COLOR8,8,0);
   HIMAGELIST toolbarDisabledImageList = ImageList_Create(16,16,ILC_COLOR8,8,0);

   ImageList_Add(toolbarImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_OPEN),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS),NULL);
   ImageList_Add(toolbarImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_SAVE_AS),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS),NULL);
   ImageList_Add(toolbarImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_DOODLE),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT),NULL);
   ImageList_Add(toolbarImageList,hBitmapCancel,NULL);
   ImageList_Add(toolbarImageList,hBitmapSetup,NULL);
   ImageList_Add(toolbarImageList,hBitmapForget,NULL);

   ImageList_Add(toolbarDisabledImageList,hBitmapCancel,NULL);
   ImageList_Add(toolbarDisabledImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_SAVE_AS_DISABLED),IMAGE_BITMAP,16,16,0*LR_LOADMAP3DCOLORS),NULL);
   ImageList_Add(toolbarDisabledImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_DOODLE_DISABLED),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS | LR_LOADTRANSPARENT),NULL);
   ImageList_Add(toolbarDisabledImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_CANCEL_DISABLED),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS),NULL);
   ImageList_Add(toolbarDisabledImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_PROPERTIES_DISABLED),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS),NULL);
   ImageList_Add(toolbarDisabledImageList,(HBITMAP)LoadImage(hModule,MAKEINTRESOURCE(IDD_ICON_FORGET_DISABLED),IMAGE_BITMAP,16,16,LR_LOADMAP3DCOLORS | LR_CREATEDIBSECTION | LR_LOADTRANSPARENT),NULL);

   SendMessage(hwndToolBar,CCM_SETVERSION,(WPARAM)5,0);
   SendMessage(hwndToolBar,TB_SETBITMAPSIZE,0,(LPARAM)MAKELONG(16,16));
   SendMessage(hwndToolBar,TB_BUTTONSTRUCTSIZE,(WPARAM)sizeof(TBBUTTON),0L);
   SendMessage(hwndToolBar,TB_SETIMAGELIST,0L,(LPARAM)toolbarImageList);
   SendMessage(hwndToolBar,TB_SETDISABLEDIMAGELIST,0L,(LPARAM)toolbarDisabledImageList);

   TBBUTTON buttons[16];

   memset(buttons,0,16 * sizeof(TBBUTTON));

   buttons[0].fsStyle = BTNS_SEP;

   buttons[1].iBitmap = 0;
   buttons[1].idCommand = IDC_GETFILE;
   buttons[1].fsState = TBSTATE_ENABLED;
   buttons[1].fsStyle = TBSTYLE_AUTOSIZE;

   buttons[2].iBitmap = 1;
   buttons[2].idCommand = IDC_SAVE_AS;
   buttons[2].fsState = 0;
   buttons[2].fsStyle = TBSTYLE_AUTOSIZE;

   buttons[3].fsStyle = BTNS_SEP;

   buttons[4].iBitmap = 2;
   buttons[4].idCommand = ID_DOODLE;
   buttons[4].fsState = 0;
   buttons[4].fsStyle = BTNS_CHECK | TBSTYLE_AUTOSIZE;

   buttons[5].iBitmap = 3;
   buttons[5].idCommand = ID_DOODLE_CANCEL;
   buttons[5].fsState = 0;
   buttons[5].fsStyle = TBSTYLE_AUTOSIZE;

   SendMessage(hwndToolBar,TB_ADDBUTTONS,(WPARAM)6,(LPARAM)buttons);

   SendMessage(hwndToolBar,TB_GETITEMRECT,(WPARAM)5,(LPARAM)&rcItem);

   HWND hwndTemp = CreateWindowEx(0L,"STATIC","For this document:",WS_CHILD | WS_VISIBLE | DT_RIGHT,rcItem.right + /*4*/16,rcItem.top + 4,96,rcItem.bottom - rcItem.top - 4,hwndToolBar,NULL,NULL,NULL);
   SendMessage(hwndTemp,WM_SETFONT,(WPARAM)hGUIFont,(LPARAM)TRUE);

   memset(buttons,0,16 * sizeof(TBBUTTON));

   buttons[0].iBitmap = 120;
   buttons[0].fsStyle = BTNS_SEP;

   buttons[1].iBitmap = 4;
   buttons[1].idCommand = ID_DOODLE_OPTIONS;
   buttons[1].fsState = 0;
   buttons[1].fsStyle = TBSTYLE_AUTOSIZE;

   buttons[2].iBitmap = 8;
   buttons[2].fsStyle = BTNS_SEP;

   buttons[3].iBitmap = 5;
   buttons[3].idCommand = ID_DOODLE_FORGET;
   buttons[3].fsState = 0;
   buttons[3].fsStyle = TBSTYLE_AUTOSIZE;

   buttons[4].iBitmap = 8;
   buttons[4].fsStyle = BTNS_SEP;

   SendMessage(hwndToolBar,TB_ADDBUTTONS,(WPARAM)4,(LPARAM)buttons);

   SendMessage(hwndToolBar,TB_GETITEMRECT,(WPARAM)9,(LPARAM)&rcItem);

   hwndTemp = CreateWindowEx(0L,"STATIC","Global settings:",WS_CHILD | WS_VISIBLE | DT_RIGHT,rcItem.right + 16,rcItem.top + 4,80,rcItem.bottom - rcItem.top - 4,hwndToolBar,NULL,NULL,NULL);

   SendMessage(hwndTemp,WM_SETFONT,(WPARAM)hGUIFont,(LPARAM)TRUE);

   buttons[0].iBitmap = 104;
   buttons[0].fsStyle = BTNS_SEP;

   buttons[1].iBitmap = 4;
   buttons[1].idCommand = ID_SETUP;
   buttons[1].fsState = TBSTATE_ENABLED;
   buttons[1].fsStyle = TBSTYLE_AUTOSIZE;

   SendMessage(hwndToolBar,TB_ADDBUTTONS,(WPARAM)2,(LPARAM)buttons);

   memset(&toolInfo,0,sizeof(TOOLINFO));

   toolInfo.cbSize = sizeof(TOOLINFO);
   toolInfo.uFlags = TTF_CENTERTIP;
   toolInfo.hwnd = hwndToolBar;

   toolInfo.uId = IDC_GETFILE;
   toolInfo.lpszText = "Open a pdf file";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = IDC_SAVE_AS;
   toolInfo.lpszText = "Save the file with a specified name";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = ID_DOODLE;
   toolInfo.lpszText = "Write into the document";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = ID_DOODLE_CANCEL;
   toolInfo.lpszText = "Cancel writing in the document";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = ID_DOODLE_OPTIONS;
   toolInfo.lpszText = "Options for this document";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = ID_DOODLE_FORGET;
   toolInfo.lpszText = "Forget the previously remembered signing locations for this document";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   toolInfo.uId = ID_SETUP;
   toolInfo.lpszText = "Change the global settings that apply to documents you have not signed yet";
   SendMessage(hwndToolTips,TTM_UPDATETIPTEXT,(WPARAM)0L,(LPARAM)&toolInfo);

   SendMessage(hwndStatusBar,SB_SETTEXT,(WPARAM)0,(LPARAM)NO_DOCUMENT_OPEN_STATUS_MESSAGE);

   findKnownBackEnds();

   long countRows = 0;

   memset(&menuItemInfo,0,sizeof(MENUITEMINFO));

   menuItemInfo.cbSize = sizeof(MENUITEMINFO);

   menuItemInfo.fMask = MIIM_SUBMENU | MIIM_TYPE | MIIM_ID;
   menuItemInfo.fType = MFT_STRING;
   menuItemInfo.dwTypeData = szTemp;
   menuItemInfo.cch = 0; 

   for ( std::list<backEndPackage *>::iterator it = allBackEnds.begin(); it != allBackEnds.end(); it++ ) {

      backEndPackage *pBackendPackage = (*it);

      if ( ! pBackendPackage -> canRunFromTools )
         continue;

      HMENU hSubMenu = CreateMenu();

      MENUITEMINFO subMenuItem = {0};

      subMenuItem.cbSize = sizeof(MENUITEMINFO);

      sprintf(szTemp,"Properties...");

      subMenuItem.fMask = MIIM_STRING | MIIM_ID;
      subMenuItem.fType = MFT_STRING;
      subMenuItem.dwTypeData = szTemp;
      subMenuItem.cch = (UINT)strlen(szTemp);
      subMenuItem.wID = ID_BACKEND_PROPERTIES_1 + countRows;

      InsertMenuItem(hSubMenu,0,TRUE,&subMenuItem);

      sprintf(szTemp,"Execute");

      subMenuItem.cch = (UINT)strlen(szTemp);
      subMenuItem.wID = ID_BACKEND_EXECUTE_1 + countRows;

      InsertMenuItem(hSubMenu,1,TRUE,&subMenuItem);

      menuItemInfo.hSubMenu = hSubMenu;
      menuItemInfo.dwTypeData = pBackendPackage -> szDescription;
      menuItemInfo.cch = (UINT)strlen(pBackendPackage -> szDescription);
      menuItemInfo.wID = subMenuItem.wID;

      if ( 0 == countRows )
         SetMenuItemInfo(hMenuBackends,countRows,TRUE,&menuItemInfo);
      else
         InsertMenuItem(hMenuBackends,countRows,TRUE,&menuItemInfo);

      BSTR bstrClsid;

      StringFromCLSID(pBackendPackage -> objectId,&bstrClsid);

      menuBackends.insert(menuBackends.end(),SysAllocString(bstrClsid));

      CoTaskMemFree(bstrClsid);

      countRows++;

   }

   memset(&menuItemInfo,0,sizeof(MENUITEMINFO));

   menuItemInfo.cbSize = sizeof(MENUITEMINFO);

   menuItemInfo.fMask = MIIM_TYPE | MIIM_ID;
   menuItemInfo.fType = MFT_STRING;
   menuItemInfo.dwTypeData = "All Properties...";
   menuItemInfo.wID = ID_BACKEND_PROPERTIES_1 + countRows;
   menuItemInfo.cch = 0; 

   InsertMenuItem(hMenuBackends,countRows,TRUE,&menuItemInfo);

   pIOleInPlaceFrame_HTML_Host = new _IOleInPlaceFrame(this,hwndHTMLHost);
   pIOleInPlaceSite_HTML_Host = new _IOleInPlaceSite(this,pIOleInPlaceFrame_HTML_Host);
   pIOleClientSite_HTML_Host = new _IOleClientSite(this,pIOleInPlaceSite_HTML_Host,pIOleInPlaceFrame_HTML_Host);
   pIOleDocumentSite_HTML_Host = new _IOleDocumentSite(this,pIOleClientSite_HTML_Host);

   pIPDFiumControl -> QueryInterface(IID_IOleObject,reinterpret_cast<void **>(&pIOleObject_HTML));

   pIOleObject_HTML -> QueryInterface(IID_IOleInPlaceObject,reinterpret_cast<void **>(&pIOleInPlaceObject_HTML));

   pIOleObject_HTML -> SetClientSite(pIOleClientSite_HTML_Host);

   pDWebBrowserEvents_HTML_Host = new _DWebBrowserEvents2(this);

   IUnknown *pIUnknown = NULL;

   pDWebBrowserEvents_HTML_Host -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

   IConnectionPointContainer *pIConnectionPointContainer = NULL;

   pIPDFiumControl -> QueryInterface(IID_IConnectionPointContainer,reinterpret_cast<void**>(&pIConnectionPointContainer));

   if ( pIConnectionPointContainer ) {

      pIConnectionPointContainer -> FindConnectionPoint(DIID_DWebBrowserEvents2,&pIConnectionPoint_HTML);

      if ( pIConnectionPoint_HTML ) 
         pIConnectionPoint_HTML -> Advise(pIUnknown,&connectionCookie_HTML);

      pIUnknown -> Release();

      pIPDFiumControlEvents = new _IPDFiumControlEvents(this);

      pIPDFiumControlEvents -> QueryInterface(IID_IUnknown,reinterpret_cast<void **>(&pIUnknown));

      pIConnectionPointContainer -> FindConnectionPoint(IID_IPDFiumControlEvents,&pIConnectionPoint_PDFiumControlEvents);

      if ( pIConnectionPoint_PDFiumControlEvents ) 
         pIConnectionPoint_PDFiumControlEvents -> Advise(pIUnknown,&connectionCookie_PDFiumControlEvents);

      pIConnectionPointContainer -> Release();

      pIUnknown -> Release();

   } else 

      pIUnknown -> Release();

   if ( loadWelcomePage )
      PostMessage(hwndMainFrame,WM_DOCUMENT_SET_URL,0L,(LPARAM)szWelcomeURL);

   return 1;
   }

   void CursiVision::findKnownBackEnds() {

   ICatInformation *pICatInformation = NULL;

   CoCreateInstance(CLSID_StdComponentCategoriesMgr,NULL,CLSCTX_ALL,IID_ICatInformation,reinterpret_cast<void **>(&pICatInformation));

   IEnumCLSID *pIEnumCLSID = NULL;

   CATID catIds[] = { IID_ICursiVisionBackEnd };

   pICatInformation -> EnumClassesOfCategories(1,catIds,0,NULL,&pIEnumCLSID);

   CATEGORYINFO catInfo;

   memset(&catInfo,0,sizeof(CATEGORYINFO));

   unsigned long countFound;
   CLSID clsid;

   while ( S_OK == pIEnumCLSID -> Next(1,&clsid,&countFound) ) {

      if ( ! countFound )
         break;

      backEndPackage *pBackendPackage = new backEndPackage();

      BSTR bstrClsid;

      StringFromCLSID(clsid,&bstrClsid);

      char szCLSID[256];

      WideCharToMultiByte(CP_ACP,0,bstrClsid,-1,szCLSID,256,0,0);

      HKEY keyHandle;
      HKEY clsidHandle;
      DWORD dwType = REG_SZ;
      DWORD dwLength = 256;

      RegOpenKeyEx(HKEY_CLASSES_ROOT,"CLSID",0,KEY_QUERY_VALUE,&keyHandle);
      RegOpenKeyEx(keyHandle,szCLSID,0,KEY_QUERY_VALUE,&clsidHandle);
      RegQueryValueEx(clsidHandle,NULL,NULL,&dwType,(BYTE *)pBackendPackage -> szDescription,&dwLength);

      memcpy(&pBackendPackage -> objectId,&clsid,sizeof(GUID));

      allBackEnds.insert(allBackEnds.end(),pBackendPackage);

      //
      //NTC: 05-31-2019: This variable was used in case some of the back end processes could not logically run directly from the Tools menu against
      // the document currently open.
      // When I removed the instantiation of the tools (in this loop) to get the tool description, because that could be potentially slow, I no longer have
      // an object I can call to determine this value.
      // It may not be worthwhile re-implementing this because that would go back to instantiating every tool.
      //

      pBackendPackage -> canRunFromTools = true;

   }

   return;
   }

