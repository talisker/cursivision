// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include "templateDocument.h"

#include "writingLocation.h"

#if 0
#pragma pack(push)

#pragma pack(1)

typedef struct DLGTEMPLATEEX {
   WORD dlgVer;
   WORD signature;
   DWORD helpID;
   DWORD exStyle;
   DWORD style;
   WORD cDlgItems;
   short x;
   short y;
   short cx;
   short cy;
#if 0
   sz_Or_Ord menu;
   sz_Or_Ord windowClass;
   WCHAR title[titleLen];
   WORD pointsize;
   WORD weight;
   BYTE italic;
   BYTE charset;
   WCHAR typeface[stringLen];
#endif
} DLGTEMPLATEEX, *LPDLGTEMPLATEEX;

#pragma pack(pop)
#endif

struct doodleOptionProperties;

class doodleOptions : public IUnknown {

public:

   doodleOptions(char *pszDocument,resultDisposition *pParentDisposition);
   ~doodleOptions();

   bool RememberSigningLocations() { return DoodleOptionProperties() -> processingDisposition.doRemember; };

   resultDisposition *ProcessingDisposition();

   long InitialSignedLocationCount(long v = -LONG_MAX) { if ( ! ( v == -LONG_MAX ) ) initialSignedLocationCount = v; return initialSignedLocationCount; };

   long SignedLocationCount();

   void ClearSigningLocations(bool doSave);

   void ClearSignaturesFile();

   void DeleteSettingsFiles(char *pszRootDocumentName);

   void SaveSigningLocations();
   void RestoreSigningLocations();

   void PrepInheritSettings();

   void InheritSettings();
   char * GetDispositionSettingsFileName(bool saveDisposition = true);

   void CreateControlWindow();

   HRESULT __stdcall SaveLocation(long targetIndex,RECT *,long pageNumber,long adobePageNumber);
   HRESULT __stdcall ReplaceLocation(long targetIndex,RECT *,long pageNumber,long adobePageNumber);
   HRESULT __stdcall GetLocation(long targetIndex,RECT *,long *pPageNumber,long *pAdobePageNumber,bool ignoreDoRemember);

   //   IUnknown

   STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
   STDMETHOD_ (ULONG, AddRef)();
   STDMETHOD_ (ULONG, Release)();

   STDMETHOD(PushProperties)();
   STDMETHOD(PopProperties)();
   STDMETHOD(DiscardProperties)();
   STDMETHOD(SaveProperties)(char *pszForDocument = NULL);

   STDMETHOD(ShowProperties)();

#include "doodleOptionsProperties.h"

   struct doodleOptionProperties theDoodleOptionProperties;

   struct doodleOptionProperties *pDoodleOptionProperties;

   struct doodleOptionProperties *DoodleOptionProperties();

   templateDocument *pTemplateDocument;

private:

   //      IPropertyPageClient

   class _IGPropertyPageClient : public IGPropertyPageClient {
   public:

      _IGPropertyPageClient(doodleOptions *p) : pParent(p),refCount(0) {};
      ~_IGPropertyPageClient() {};

      //   IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

   private:

      STDMETHOD(BeforeAllPropertyPages)();
      STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** stringHelpDirs,SAFEARRAY** pSize);
      STDMETHOD(CreatePropertyPage)(long,HWND,RECT*,BOOL,HWND *pHwndPropertyPage);
      STDMETHOD(Apply)();
      STDMETHOD(IsPageDirty)(long,BOOL*);
      STDMETHOD(Help)(BSTR bstrHelpDir);
      STDMETHOD(TranslateAccelerator)(long,long*);
      STDMETHOD(AfterAllPropertyPages)(BOOL);
      STDMETHOD(DestroyPropertyPage)(long);

      STDMETHOD(GetPropertySheetHeader)(void *pHeader);
      STDMETHOD(get_PropertyPageCount)(long *pCount);
      STDMETHOD(GetPropertySheets)(void *pSheets);

      doodleOptions *pParent;

      //void adjustPropertiesDialogSize(DLGTEMPLATEEX *pDialog,long cyReservedHeader);

      long refCount;

   } *pIGPropertyPageClient;


   IGProperties *pIGProperties;

   IPdfEnabler *pIPdfEnabler;
   IPdfDocument *pIPdfDocument;

   BYTE *pSavedSigningLocations;

   resultDisposition *pResultDisposition;

   long initialSignedLocationCount;

   HWND hwndDispositionSettings;
   HWND hwndAdditionalBackEnds;

   bool explicitDeleteRequested;

   static char szParentDoodleOptionsDocumentName[MAX_PATH];
   static char szParentDoodleOptionsFileName[MAX_PATH];

   static LRESULT CALLBACK doodleControlHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK dispositionSettingsHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK listViewHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK multiSignOptionsHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK additionalSaveOptionsHandler(HWND,UINT,WPARAM,LPARAM);

   static LRESULT CALLBACK signingLocationsHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK fieldsHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK additionalBackEndsHandler(HWND,UINT,WPARAM,LPARAM);
   static LRESULT CALLBACK inheritedDocumentHandler(HWND,UINT,WPARAM,LPARAM);

};

