// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <math.h>

   mousePosition mouseStart,mouseEnd;

   static boolean mouseEntered = false;

   static long oldPageNumber = -1L;
   static long newPageNumber = -1L;

   static long xOrigin = 0L;
   static long yOrigin = 1L;
   static long cxPad = 0L;
   static long cyPad = 0L;

   static long boxVisible = 0L;
   static long firstMouse = 0L;
   static long lastMouseX = -1L, lastMouseY = -1L;

   LRESULT CALLBACK mouseVellumHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   SignatureProcess *pSignatureProcess = pCursiVision -> pSignatureProcess;

   ISignaturePad *pISignaturePad = pCursiVision -> pISignaturePad_PropertiesOnly;

   switch ( msg ) {

   case WM_CREATE: {
      CREATESTRUCT *pc = reinterpret_cast<CREATESTRUCT *>(lParam);
      CursiVision *p = reinterpret_cast<CursiVision *>(pc -> lpCreateParams);
      SetWindowLongPtr(hwnd,GWLP_USERDATA,(ULONG_PTR)p);
      mouseEntered = false;
      firstMouse = 1L;
      oldPageNumber = -1L;
      }
      break;

   case WM_SHOWWINDOW:
      xOrigin = (long)((double)pISignaturePad -> OriginPointX() * pCursiVision -> lastPadToPDFScale);
      yOrigin = (long)((double)pISignaturePad -> OriginPointY() * pCursiVision -> lastPadToPDFScale);
      cxPad = (long)((double)pISignaturePad -> Width() * pCursiVision -> lastPadToPDFScale);
      cyPad = (long)((double)pISignaturePad -> Height() * pCursiVision -> lastPadToPDFScale);
      boxVisible = 0L;
      firstMouse = 1L;
      break;

   case WM_KEYDOWN:
   case WM_KEYUP:
      if ( VK_ESCAPE == wParam )
         PostMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE_CANCEL,0),0L);
      else
         PostMessage(hwndMainFrame,msg,wParam,lParam);
      break;

   case WM_LBUTTONDOWN:
      mouseStart.x = LOWORD(lParam) - xOrigin;
      mouseStart.y = HIWORD(lParam) - yOrigin;
      return (LRESULT)0;

   case WM_LBUTTONUP: {

      mouseEntered = false;

      if ( boxVisible ) {
         HDC hdc = GetDC(hwnd);
         HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
         HPEN oldPen = (HPEN)SelectObject(hdc,hPen);

         SetROP2(hdc,R2_XORPEN);

         MoveToEx(hdc,lastMouseX,lastMouseY,NULL);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY);

         boxVisible = 0L;

         DeleteObject(SelectObject(hdc,oldPen));

         ReleaseDC(hwnd,hdc);
      }

      SetWindowText(hwndStatusBar,"");

      RECT rectResult;

      rectResult.left = mouseStart.x;
      rectResult.top = mouseStart.y;
      rectResult.right = mouseEnd.x;
      rectResult.bottom = mouseEnd.y;

      pSignatureProcess -> OuterRect(&rectResult);

      pSignatureProcess -> RegionCaptureStage(REGION_CAPTURE_STAGE_DONE);

      memset(&mouseStart,0,sizeof(mousePosition));
      memset(&mouseEnd,0,sizeof(mousePosition));

      break;
      }

   case WM_MOUSEMOVE: {

#if 0
      if ( firstMouse ) {
         firstMouse = 0L;
         break;
      }
#else
      if ( firstMouse < 3 ) {
         firstMouse++;
         break;
      }
#endif

      long currentMouseX = LOWORD(lParam) - xOrigin;
      long currentMouseY = HIWORD(lParam) - yOrigin;

      KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);

      SetTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING,DISPLAY_WAITING_TIMEOUT,NULL);

      if ( ! mouseEntered || pCursiVision -> pdfPageNumber != oldPageNumber ) {

         mouseEntered = true;

         memset(&mouseStart,0,sizeof(mousePosition));
         memset(&mouseEnd,0,sizeof(mousePosition));

         TRACKMOUSEEVENT mouseTracking = {0};
         mouseTracking.cbSize = sizeof(TRACKMOUSEEVENT);
         mouseTracking.dwFlags = TME_LEAVE;
         mouseTracking.hwndTrack = hwnd;
         TrackMouseEvent(&mouseTracking);

         oldPageNumber = pCursiVision -> pdfPageNumber;

      }

      HDC hdc = GetDC(hwnd);
      HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
      HPEN oldPen = (HPEN)SelectObject(hdc,hPen);

      SetROP2(hdc,R2_XORPEN);

      if ( boxVisible ) {
         MoveToEx(hdc,lastMouseX,lastMouseY,NULL);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY);
         boxVisible = 0L;
      }

      mouseStart.x = currentMouseX;
      mouseStart.y = currentMouseY;

      mouseEnd.x = mouseStart.x + cxPad;
      mouseEnd.y = mouseStart.y + cyPad;

      MoveToEx(hdc,mouseStart.x,mouseStart.y,NULL);
      LineTo(hdc,mouseStart.x,mouseEnd.y);
      LineTo(hdc,mouseEnd.x,mouseEnd.y);
      LineTo(hdc,mouseEnd.x,mouseStart.y);
      LineTo(hdc,mouseStart.x,mouseStart.y);

      lastMouseX = mouseStart.x;
      lastMouseY = mouseStart.y;

      boxVisible = 1L;

      DeleteObject(SelectObject(hdc,oldPen));

      ReleaseDC(hwnd,hdc);

      return (LRESULT)0;
      }

   case WM_MOUSELEAVE: {

      mouseEntered = false;

      if ( boxVisible ) {

         HDC hdc = GetDC(hwnd);
         HPEN hPen = CreatePen(PS_SOLID,1,RGB(128,128,128));
         HPEN oldPen = (HPEN)SelectObject(hdc,hPen);

         SetROP2(hdc,R2_XORPEN);

         MoveToEx(hdc,lastMouseX,lastMouseY,NULL);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY);
         LineTo(hdc,lastMouseX + cxPad,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY + cyPad);
         LineTo(hdc,lastMouseX,lastMouseY);

         DeleteObject(SelectObject(hdc,oldPen));

         ReleaseDC(hwnd,hdc);
   
         boxVisible = 0L;

      }

      }
      break;

   case WM_PAINT: {

#ifdef DRAW_WINDOW_BORDERS
      {
      PAINTSTRUCT ps;
      BeginPaint(hwnd,&ps);
      RECT rcFMS;
      GetClientRect(hwnd,&rcFMS);
      HPEN fms2 = CreatePen(PS_SOLID,8,RGB(0,0,255));
      SelectObject(ps.hdc,fms2);
      MoveToEx(ps.hdc,rcFMS.left + 4,rcFMS.top + 4,NULL);
      LineTo(ps.hdc,rcFMS.right - 4,rcFMS.top + 4);
      LineTo(ps.hdc,rcFMS.right - 4,rcFMS.bottom - 4);
      LineTo(ps.hdc,rcFMS.left + 4,rcFMS.bottom - 4);
      LineTo(ps.hdc,rcFMS.left + 4,rcFMS.top + 4);
      EndPaint(hwnd,&ps);
      }
#endif

      return (LRESULT)0L;
      }

   default:
      break;

   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }