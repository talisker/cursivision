// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   LRESULT CALLBACK htmlHostHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   switch ( msg ) {

   case WM_PAINT: {
      PAINTSTRUCT ps;
      BeginPaint(hwnd,&ps);
      EndPaint(hwnd,&ps);
      return (LRESULT)TRUE;
      }

   case WM_DROPFILES: 
      return SendMessage(hwndMainFrame,msg,wParam,lParam);

   default:
      break;

   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }
