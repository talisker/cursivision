// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/*

   At some point - I would like to make this source common across all versions. If done, the version for
   CursiVision For TabletPC (NOT THIS ONE) contains the ability to spread the signature across multiple pages, so don't loose
   that capability.

*/


#include "CursiVision.h"

namespace CursiVisionReceptor {
#include "ForwardToReceptor_i.h"
#include "ForwardToReceptor_i.c"
}

#define SET_STATE  "\n q 8 M 1 j 1 J \n"

#define STROKE " S "

#define RESTORE_STATE "\n Q \n"

#define MOVE " %lf %lf m \n"
#define LINE " %lf %lf l \n"

#define LINEWIDTH " %ld w  "

   static char szResultFile[MAX_PATH] = {0};
   static char szGraphicDataFileName[MAX_PATH] = {0};
   static FILE *fGraphicDataFile = NULL;

   long CursiVision::updateDocument(long writingLocationIndex,bool deferProcessDisposition) {

   SetWindowPos(hwndMainFrame,HWND_NOTOPMOST,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE);

   EnterCriticalSection(&graphicListAccess);

   std::map<long,signatureGraphic *> *pGraphics = SignatureGraphics();

   double rotation;

   long oldInkWeight = 0;

   long entryPageNumber = pdfPageNumber;

   pIPdfPage -> Rotation(&rotation);

   char szReset[16384];

   char szTempName[MAX_PATH];

   FILE *fTemp = NULL;

   char currentPageLabel[64];
   long currentPageNumber = 0L;

   double lastX = 0.0;
   double lastY = 0.0;

   for ( std::pair<long,signatureGraphic *> thePair : *pGraphics ) {

      signatureGraphic *pSG = thePair.second;

      pSG -> scaleToPDFX = 1.0 / xPixelsPerPageUnit;
      pSG -> scaleToPDFY = 1.0 / yPixelsPerPageUnit;

      pSG -> padOriginPDFUnits.x = (long)((double)(pSG -> windowsOrigin.x + pSG -> offsetX) * pSG -> scaleToPDFX);
      pSG -> padOriginPDFUnits.y = pdfPageHeight - (long)((double)(pSG -> windowsOrigin.y + pSG -> offsetY) * pSG -> scaleToPDFY);

      pSG -> padWidthPDFUnits = (long)((double)pSG -> windowsWidth * pSG -> scaleToPDFX);
      pSG -> padHeightPDFUnits = (long)((double)pSG -> windowsHeight * pSG -> scaleToPDFY);

      pSG -> padOriginPDFUnits.y -= pSG -> padHeightPDFUnits;

      if ( pICursiVisionServices )
         pICursiVisionServices -> SaveSignatureGraphic(reinterpret_cast<void *>(pSG));

      bool moveNext = true;
      bool emitPage = false;

      for ( long k = 0; k < pSG -> totalPoints; k++ ) {

resetPage:

         if ( 0L == currentPageNumber || ! ( currentPageNumber == pSG -> pSignatureDataPage[k] ) ) {

            if ( fTemp ) {

               fprintf(fTemp,STROKE);      
               fprintf(fTemp,RESTORE_STATE);

               long fileSize = ftell(fTemp);
         
               IPdfPage *pIPdfPage_CurrentPage = NULL;

               pIPdfDocument -> Page(currentPageNumber,currentPageLabel,&pIPdfPage_CurrentPage);

               if ( ! pIPdfPage_CurrentPage ) {

                  char szMessage[1024];

                  sprintf(szMessage,"CursiVision has lost track of the page number of the page on which this handwriting has occurred.\n\n"
                     "We sincerely apologize for the inconvenience but will need to cancel this batch of handwriting.\n\n"
                     "The system was trying to place handwriting on page %ld\n\nPlease try again",currentPageNumber);

                  MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);

                  fclose(fTemp);

                  DeleteFile(szTempName);

                  return 1;
               }

               BYTE *pszX = new BYTE[fileSize];
            
               memset(pszX,0,fileSize);

               fseek(fTemp,0,SEEK_SET);
            
               fread(pszX,1,fileSize,fTemp);
            
               fclose(fTemp);

               fTemp = NULL;

               if ( NULL == fGraphicDataFile ) {
                  sprintf(szGraphicDataFileName,"%s",_tempnam(NULL,NULL));
                  fGraphicDataFile = fopen(szGraphicDataFileName,"wb");
               }

               fwrite(pszX,fileSize,1,fGraphicDataFile);
            
               pIPdfPage_CurrentPage -> AddStream(pszX,fileSize,writingLocationIndex);
            
               delete [] pszX;

               DeleteFile(szTempName);

            }

            strcpy(szTempName,_tempnam(NULL,NULL));
            fTemp = fopen(szTempName,"w+b");
            currentPageNumber = pSG -> pSignatureDataPage[k];
            emitPage = true;

         }

         if ( 0L == currentPageNumber ) {
            currentPageNumber = pSG -> pSignatureDataPage[k];
            emitPage = true;
         }

         if ( emitPage ) {

            emitPage = false;

            double red = 0.0,green = 0.0,blue = 0.0;

            double lineWidth = 1.0;

            if ( SignaturePad() ) {

               DWORD inkColor = SignaturePad() -> InkColor();

               red = (double)GetRValue(inkColor) / 255.0;
               green = (double)GetGValue(inkColor) / 255.0;
               blue = (double)GetBValue(inkColor) / 255.0;
//
//TODO: 9-7-2011. It is not clear why the factor 2 is needed here.
// lineWidth is supposed to be in "user space units", and the scaling factor
// is xPixelsPerPageUnit to convert pixels to user space units. But 
// without the 2, the lines are too wide.
//
               lineWidth = (double)SignaturePad() -> InkWeight() / xPixelsPerPageUnit / 2.0;

            }

            sprintf(szReset,"%s %lf %lf %lf RG %lf w \n",SET_STATE,red,green,blue,lineWidth);

            if ( 90.0 == rotation ) {

               sprintf(szReset + strlen(szReset)," 1 0 0 1 %lf 0 cm\n",(double)pdfPageWidth);
               sprintf(szReset + strlen(szReset)," 0 1 -1 0 0 0 cm\n");
               sprintf(szReset + strlen(szReset)," 1 0 0 1 0 %lf cm \n",(double)(pdfPageWidth - pdfPageHeight));

            } else if ( 270.0 == rotation ) {
//
//NTC: 03-11-2012
// I do not have a 270 degree rotated document to test this with.
//
               sprintf(szReset + strlen(szReset)," 1 0 0 1 %lf 0 cm\n",(double)pdfPageWidth);
               sprintf(szReset + strlen(szReset)," 0 -1 1 0 0 0 cm\n");
               sprintf(szReset + strlen(szReset)," 1 0 0 1 0 %lf cm \n",(double)(pdfPageWidth - pdfPageHeight));

            } else if ( 180.0 == rotation ) {

               sprintf(szReset + strlen(szReset)," 1 0 0 1 0 %lf cm \n",(double)pdfPageHeight);
               sprintf(szReset + strlen(szReset)," 0 -1 1 0 0 0 cm\n");
               sprintf(szReset + strlen(szReset)," 1 0 0 1 0 %lf cm \n",(double)pdfPageWidth);
               sprintf(szReset + strlen(szReset)," 0 -1 1 0 0 0 cm\n");


            }

            fprintf(fTemp,szReset);

         }

         if ( currentPageNumber != pSG -> pSignatureDataPage[k] )
            goto resetPage;

         if ( 0.0 == pSG -> pSignatureDataX[k] && 0.0 == pSG -> pSignatureDataY[k] ) {
            moveNext = true;
            continue;
         }

//#ifdef USE_INKWEIGHT
         long newWeight = (long)(pSG -> pInkWeight[k] / xPixelsPerPageUnit);

         if ( ! ( newWeight == oldInkWeight ) ) {
            fprintf(fTemp," S %ld w ",newWeight);
            oldInkWeight = newWeight;
            fprintf(fTemp,MOVE,lastX,lastY);
         }
//#endif
         lastX = (pSG -> pSignatureDataX[k] + pSG -> offsetX) * pSG -> scaleToPDFX;

         lastY = pSG -> pSignatureDataY[k];

         lastY = (double)pdfPageHeight - (lastY + pSG -> offsetY) * pSG -> scaleToPDFY;

         fprintf(fTemp,moveNext ? MOVE : LINE,lastX,lastY);

         moveNext = false;
   
      }

   }

   if ( fTemp ) {

      fprintf(fTemp,STROKE);      
      fprintf(fTemp,RESTORE_STATE);

      long fileSize = ftell(fTemp);
   
      IPdfPage *pIPdfPage_CurrentPage = NULL;

      pIPdfDocument -> Page(currentPageNumber,currentPageLabel,&pIPdfPage_CurrentPage);

      if ( ! pIPdfPage_CurrentPage ) {

         char szMessage[1024];

         sprintf(szMessage,"CursiVision has lost track of the page number of the page on which this handwriting has occurred.\n\n"
            "We sincerely apologize for the inconvenience but will need to cancel this batch of handwriting.\n\n"
            "The system was trying to place handwriting on page %ld\n\nPlease try again",currentPageNumber);

         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);

         fclose(fTemp);

         DeleteFile(szTempName);

         return 1;
      }

      BYTE *pszX = new BYTE[fileSize];
   
      memset(pszX,0,fileSize);

      fseek(fTemp,0,SEEK_SET);
   
      fread(pszX,1,fileSize,fTemp);
   
      fclose(fTemp);

      fTemp = NULL;

      if ( NULL == fGraphicDataFile ) {
         sprintf(szGraphicDataFileName,"%s",_tempnam(NULL,NULL));
         fGraphicDataFile = fopen(szGraphicDataFileName,"wb");
      }

      fwrite(pszX,fileSize,1,fGraphicDataFile);
   
      pIPdfPage_CurrentPage -> AddStream(pszX,fileSize,writingLocationIndex);

      delete [] pszX;

      DeleteFile(szTempName);

   }

   isDocumentModified = true;

   handledByBackEnd = false;

   for ( std::map<long,signatureGraphic *>::reverse_iterator it = pGraphics -> rbegin(); it != pGraphics -> rend(); it++ ) {
      if ( (*it).second != pLastSignatureGraphic ) {
         delete pLastSignatureGraphic;
         pLastSignatureGraphic = NULL;
      }
      pLastSignatureGraphic = (*it).second;
      break;
   }

   ClearSignatureGraphics();

   LeaveCriticalSection(&graphicListAccess);

   if ( deferProcessDisposition ) {

      if ( szInterimFileName[0] )
         DeleteFile(szInterimFileName);

      strcpy(szInterimFileName,_tempnam(NULL,NULL));

      BSTR bstrTempFile = SysAllocStringLen(NULL,MAX_PATH);

      MultiByteToWideChar(CP_ACP,0,szInterimFileName,-1,bstrTempFile,MAX_PATH);

      pIPdfDocument -> Write(bstrTempFile);

      SysFreeString(bstrTempFile);

      openDocumentFast(szInterimFileName,entryPageNumber);

      return 1;

   }

   if ( fGraphicDataFile )
      fclose(fGraphicDataFile);

   fGraphicDataFile = NULL;

   return saveDocument(entryPageNumber);
   }


   long CursiVision::saveDocument(long reOpenedPageNumber) {

   SetWindowPos(hwndMainFrame,HWND_BOTTOM,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE);

   InvalidateRect(hwndMainFrame,NULL,TRUE);

   UpdateWindow(hwndMainFrame);

   strcpy(szLastActiveDocument,szActiveDocument);

   memset(szResultFile,0,sizeof(szResultFile));

   resultDisposition *pResultDisposition = currentProcessDisposition();

   if ( pSignatureProcess ) {
      delete pSignatureProcess;
      pSignatureProcess = NULL;
   }

   pIPdfDocument -> Seal();

   isProcessingSaveAndReopen = true;

   processDisposition(pResultDisposition,false,reOpenedPageNumber,szResultFile,szGraphicDataFileName,currentSettingsFileName());

   BSTR bstrResultFileName = SysAllocStringLen(NULL,(DWORD)strlen(szResultFile));

   MultiByteToWideChar(CP_ACP,0,szResultFile,-1,bstrResultFileName,(DWORD)strlen(szResultFile));

   strcpy(szSignedDocument,szResultFile);

   if ( pResultDisposition -> doExit ) 
      PostMessage(hwndMainFrame,WM_POTENTIAL_QUIT,0L,0L);

   if ( pResultDisposition -> doCloseDocument || pResultDisposition -> doExit )
      pCursiVision -> cancelOpenInProgress = true;

   SysFreeString(bstrResultFileName);

   DeleteFile(szGraphicDataFileName);

   memset(szGraphicDataFileName,0,sizeof(szGraphicDataFileName));

//   if ( hwndSigningControl ) {
//      doodleOptions *pDoodleOptions = (doodleOptions *)GetWindowLongPtr(hwndDoodleOptions,GWLP_USERDATA);
      pDoodleOptions -> SaveProperties();
      pDoodleOptions -> SaveSigningLocations();
//
//NTC: 12-15-2011: There is STILL a problem here.
// If I clear the signing locations, documents signed through print jobs don't remember their signing locations,
// (even if I restore them)
// If I DONT clear them - the signing locations migrate to the settings for the signed document.
//
//      if ( ! pIPrintingSupportProfile )
//         pDoodleOptions -> ClearSigningLocations(false);
      pDoodleOptions -> SaveProperties(szResultFile);
      pDoodleOptions -> RestoreSigningLocations();
      DestroyWindow(hwndSigningControl);
//      delete pDoodleOptions;
      hwndSigningControl = NULL;
//   }

   if ( PrintingProfile() ) {
      PrintingProfile() -> SaveProperties();
      PrintingProfile((IPrintingSupportProfile *)0x00000001);
   }

//
// CHECK - signature process doesn't have a resultdisposition (?!)
//
//   if ( pSignatureProcess && ! pSignatureProcess -> GetResultDisposition() -> useGlobalProperties && ! wasAdHocMode )
//      isDocumentModified = false;

   isProcessingSaveAndReopen = false;

   return 1;
   }


   long CursiVision::undoUpdate() {

   long signatureCount;

   pIPdfDocument -> get_NamedCount(L"cvSignatures",&signatureCount);

   long entryPageNumber = pdfPageNumber;

   if ( S_OK == pIPdfDocument -> RemoveLastAddedStream() ) {
      signatureCount--;
      pIPdfDocument -> UpdateNamedCount(L"cvSignatures",max(0,signatureCount),currentProcessDisposition() -> maximumSignatures);
   }

   if ( szInterimFileName[0] )
      DeleteFile(szInterimFileName);

   strcpy(szInterimFileName,_tempnam(NULL,NULL));

   BSTR bstrTempFile = SysAllocStringLen(NULL,MAX_PATH);

   MultiByteToWideChar(CP_ACP,0,szInterimFileName,-1,bstrTempFile,MAX_PATH);

   pIPdfDocument -> Write(bstrTempFile);

   SysFreeString(bstrTempFile);

   openDocument(szInterimFileName,entryPageNumber,false);

   isDocumentModified = true;

   handledByBackEnd = false;

   return 1;
   }