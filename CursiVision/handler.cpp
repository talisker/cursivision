// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   LRESULT CALLBACK handler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {

   if ( WM_USER < msg && msg <= WM_USER_MAX )
      return wmUserHandler(msg,wParam,lParam);

   switch ( msg ) {

   case WM_ACTIVATE:
      if ( pCursiVision && ( WA_ACTIVE == LOWORD(wParam) || WA_CLICKACTIVE == LOWORD(wParam) ) && pCursiVision -> pIOleInPlaceActiveObject_HTML ) 
         pCursiVision -> pIOleInPlaceActiveObject_HTML -> OnFrameWindowActivate(TRUE);
      break;
	  
   case WM_KEYDOWN:
   case WM_KEYUP: {
      if ( VK_ESCAPE == wParam && WM_KEYUP == msg ) {
         PostMessage(hwndMainFrame,WM_COMMAND,MAKELPARAM(ID_DOODLE_CANCEL,0),0L);
      } else {
         if ( pCursiVision -> pSignatureProcess )
            return SendMessage(hwndHTMLHost,msg,wParam,lParam);
      }
      }
      break;

   case WM_SYSCOMMAND: {
      if ( wParam == SC_CLOSE ) {
         PostMessage(hwndMainFrame,WM_POTENTIAL_QUIT,0L,0L);
         return (LRESULT)0;
      }
      }
      break;

   case WM_QUERYENDSESSION:
      return (LRESULT)TRUE;

   case WM_ENDSESSION:
      if ( ENDSESSION_CLOSEAPP == lParam && wParam )
         PostMessage(hwnd,WM_FORCE_SHUTDOWN,0L,0L);
      return (LRESULT)0;

   case WM_TIMER:
      return timerHandler(hwnd,wParam,lParam);

   case WM_COMMAND: 
      return commandHandler(hwnd,wParam,lParam);

   case WM_DROPFILES: {

      if ( pCursiVision -> pSignatureProcess ) {
         MessageBox(hwndMainFrame,"CursiVision is busy signing a document.\r\rPlease cancel or finish writing in the document","Note",MB_OK);
         break;
      }

      if ( pCursiVision -> documentIsModified() ) {

         long optionNumber = pCursiVision -> handleUnsavedDocument();

         if ( IDDI_OPTIONS_OPTION_1 == optionNumber ) {
            pCursiVision -> openAfterSaving = true;
            pCursiVision -> wParamAfterSaving = wParam;
            pCursiVision -> lParamAfterSaving = lParam;
            PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(IDC_SAVE_AS,0),0L);
            return (LRESULT)0;
         }

         if ( IDDI_OPTIONS_OPTION_3 == optionNumber )
            return (LRESULT)0;

      }

      char szFiles[MAX_PATH];
      HDROP hDropInfo = reinterpret_cast<HDROP>(wParam);
      long countFiles = DragQueryFile(hDropInfo,0xFFFFFFFF,NULL,0L);
      for ( long k = 0; k < countFiles; k++ ) {
         DragQueryFile(hDropInfo,k,szFiles,MAX_PATH);
         SetDlgItemText(hwndHTMLHost,IDC_FILENAME,szFiles);
         memset(pCursiVision -> szLastActiveDocument,0,sizeof(pCursiVision -> szLastActiveDocument));
         pCursiVision -> openDocument(szFiles,1,true,false,false,true);
         break;
      }

      }
      break;

   case WM_SIZE: 
      return sizeHandler(hwnd,wParam,lParam);

   case WM_MOVE: 

      RECT rcOldPosition;

      if ( ! IsIconic(hwndMainFrame) ) {
         memcpy(&rcOldPosition,&pCursiVision -> rectFrame,sizeof(RECT));
         GetWindowRect(hwndMainFrame,&pCursiVision -> rectFrame);
      } else
         break;

      if ( ! pCursiVision -> pSignatureProcess )
         break;

      if ( ! pCursiVision -> pSignatureProcess -> SignatureBox() )
         break;

      pCursiVision -> pSignatureProcess -> SignatureBox() -> Move(pCursiVision -> rectFrame.left - rcOldPosition.left,pCursiVision -> rectFrame.top - rcOldPosition.top);

      break;

   case WM_DESTROY:
      break;

   case WM_INITMENUPOPUP: {

      HMENU theMenu = (HMENU)wParam;

      MENUITEMINFO menuItemInfo = {0};
   
      menuItemInfo.cbSize = sizeof(MENUITEMINFO);
      menuItemInfo.fMask = MIIM_STATE | MIIM_ID;

      if ( theMenu == hMenuFile ) {

         GetMenuItemInfo(theMenu,IDC_SAVE_AS,FALSE,&menuItemInfo);

         if ( pCursiVision -> documentIsOpen() )
            menuItemInfo.fState = MFS_ENABLED;
         else
            menuItemInfo.fState = MFS_DISABLED;

         menuItemInfo.wID = IDC_SAVE_AS;
         SetMenuItemInfo(theMenu,IDC_SAVE_AS,FALSE,&menuItemInfo);

         menuItemInfo.wID = IDC_CLOSE_FILE;
         SetMenuItemInfo(theMenu,IDC_CLOSE_FILE,FALSE,&menuItemInfo);

         menuItemInfo.wID = IDC_PRINT;
         SetMenuItemInfo(theMenu,IDC_PRINT,FALSE,&menuItemInfo);

         if ( pCursiVision -> szLastActiveDocument[0] && strcmp(pCursiVision -> szLastActiveDocument,pCursiVision -> szActiveDocument) )
            menuItemInfo.fState = MFS_ENABLED;
         else
            menuItemInfo.fState = MFS_DISABLED;

         menuItemInfo.wID = IDC_REOPEN_SOURCE;
         SetMenuItemInfo(theMenu,IDC_REOPEN_SOURCE,FALSE,&menuItemInfo);

         break;

      }

      if ( theMenu == hMenuDoodle ) {

         GetMenuItemInfo(theMenu,ID_DOODLE_CANCEL,FALSE,&menuItemInfo);

         if ( pCursiVision -> documentIsOpen() && pCursiVision -> pSignatureProcess ) 
            menuItemInfo.fState = MFS_ENABLED;
         else
            menuItemInfo.fState = MFS_DISABLED;

         SetMenuItemInfo(theMenu,ID_DOODLE_CANCEL,FALSE,&menuItemInfo);

         long theState = MFS_ENABLED;

         if ( ! pCursiVision -> documentIsOpen() || pCursiVision -> pSignatureProcess || ! pCursiVision -> isWritingEnabled ) 
            theState = MFS_DISABLED;

         long theIds[] = {ID_DOODLE,ID_DOODLE_OPTIONS,0L};

         for ( long k = 0; 1; k++ ) {

            if ( ! theIds[k] )
               break;

            if ( ! GetMenuItemInfo(theMenu,theIds[k],FALSE,&menuItemInfo) )
               continue;

            menuItemInfo.fState = theState;
            SetMenuItemInfo(theMenu,theIds[k],FALSE,&menuItemInfo);

         }

         RemoveMenu(theMenu,ID_DOODLE_OPTIONS,MF_BYCOMMAND);
         RemoveMenu(theMenu,ID_RESET_DOODLE_OPTIONS,MF_BYCOMMAND);
         RemoveMenu(theMenu,ID_DOODLE_FORGET,MF_BYCOMMAND);

         memset(&menuItemInfo,0,sizeof(MENUITEMINFO));
         menuItemInfo.cbSize = sizeof(MENUITEMINFO);

         if ( pCursiVision -> pDoodleOptions ) {

            char szTemp[MAX_PATH];

            sprintf(szTemp,"Settings for %s",pCursiVision -> szActiveDocumentRootName);

            menuItemInfo.fMask = MIIM_STATE | MIIM_ID | MIIM_STRING | MIIM_BITMAP;
            menuItemInfo.fType = MFT_STRING;
            menuItemInfo.fState = MFS_ENABLED;
            menuItemInfo.wID = ID_DOODLE_OPTIONS;
            menuItemInfo.dwTypeData = szTemp;
            menuItemInfo.hbmpItem = hBitmapSetup;

            InsertMenuItem(theMenu,ID_DOODLE_OPTIONS,FALSE,&menuItemInfo);

            sprintf(szTemp,"Reset all settings for %s",pCursiVision -> szActiveDocumentRootName);

            menuItemInfo.wID = ID_RESET_DOODLE_OPTIONS;
            menuItemInfo.hbmpItem = hBitmapCreate;

            InsertMenuItem(theMenu,ID_RESET_DOODLE_OPTIONS,FALSE,&menuItemInfo);

            if ( pCursiVision -> pDoodleOptions -> SignedLocationCount() ) {
               sprintf(szTemp,"Forget locations for %s",pCursiVision -> szActiveDocumentRootName);
               menuItemInfo.wID = ID_DOODLE_FORGET;
               menuItemInfo.hbmpItem = hBitmapForget;
               InsertMenuItem(theMenu,ID_DOODLE_FORGET,FALSE,&menuItemInfo);
            }

         }

      }

      if ( theMenu == hMenuTools ) {

         GetMenuItemInfo(theMenu,ID_BACKEND_EXECUTE_1 - 1,FALSE,&menuItemInfo);

         if ( pCursiVision -> documentIsOpen() && ! pCursiVision -> GetSignatureProcess() ) 
            menuItemInfo.fState = MFS_ENABLED;
         else
            menuItemInfo.fState = MFS_DISABLED;

         SetMenuItemInfo(theMenu,ID_BACKEND_EXECUTE_1 - 1,FALSE,&menuItemInfo);

      }

      }
      break;

   default:
      break;
   }

   return DefWindowProc(hwnd,msg,wParam,lParam);
   }


   unsigned int __stdcall monitorPrinter(void *) {
   HANDLE hPrinter = NULL;
   OpenPrinter(szArguments[IDX_PRINT_TO],&hPrinter,NULL);
   HANDLE firstPrinterChangeNotification = FindFirstPrinterChangeNotification(hPrinter,PRINTER_CHANGE_JOB,0L,NULL);
   WaitForSingleObject(firstPrinterChangeNotification,INFINITE);
   Sleep(2000);
   PostMessage(hwndMainFrame,WM_POTENTIAL_QUIT,0L,0L);
   return 0;
   }
