// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#include <windows.h>
#include <commctrl.h>
#include <wingdi.h>

#include <Richedit.h>

#include <commdlg.h>
#include <stdio.h>
#include <objbase.h>
#include <ShlObj.h>
#include <objsafe.h>
#include <process.h>
#include <olectl.h>

#include <map>
#include <list>

#define MAX_TEXT_RECT_COUNT   128
#define MAX_TEXT_TEXT_SPACE   8192

#include "resultDisposition.h"

#include "PdfEnabler_i.h"
#include "Properties_i.h"

#include "PrintingSupport_i.h"
#include "SignaturePad_i.h"
#include "CursiVision_i.h"

#include "resource.h"
#include "wmUser.h"
#include "timerParameters.h"

#include "PDFiumControl_i.h"

#include "directories.h"

#include "writingLocation.h"

#include "SignatureProcess.h"
#include "doodleOptions.h"

#include "signatureGraphic.h"
#include "templateDocument.h"

#include "utilities.h"

//#define SHOW_SIGNING_CONTROL_PANEL  ( ( pCursiVision -> SignaturePad() && pCursiVision -> SignaturePad() -> IsFullPage() ) ? SW_SHOW : SW_HIDE )
#define SHOW_SIGNING_CONTROL_PANEL  SW_SHOW

#define PRODUCE_BITMAP_FILES

#define IMPLEMENT_BACKENDS

#define PROFILE_BUFFER_SIZE   65535
#define PDF_RECT_LEFT_SLOP        4
#define PDF_RECT_TOP_SLOP         8

#define BAR_BUTTON_HEIGHT   24
#define BAR_BUTTON_WIDTH   64

//#define DRAW_PAD_OUTLINE
//#define DRAW_WINDOW_BORDERS

#define IMPROVE_TOP_BORDER_PAINTING

#define DISPLAY_WAITING_TIMEOUT      5000

#define NO_DOCUMENT_OPEN_STATUS_MESSAGE "To sign a PDF document, drag it onto CursiVision"

#define IDX_SETTINGS          0
#define IDX_OPEN              1
#define IDX_PLAY              2
#define IDX_PRINT             3
#define IDX_FILENAME          4
#define IDX_FILESAVEASNAME    5
#define IDX_REGISTER          6
#define IDX_UNREGISTER        7
#define IDX_FORWARD_TO_SERVER 8
#define IDX_PRINT_TO          9
#define IDX_DOODLE            10
#define IDX_PASS_THRU         11
#define IDX_USE_BACK_END_NAME 12
#define IDX_VIEW_ONLY         13
#define IDX_REGISTER_COMPONENTS  14
#define IDX_UNREGISTER_COMPONENTS   15
#define IDX_INSTALL_PRINTER   16
#define IDX_GRANT_FULL_ACCESS 17
#define IDX_FIXUP_LINKS       18

#define OPTION_ID_CLEAR 0
#define OPTION_ID_OK    1

#define APPLY_INK_WEIGHT   0

#define PREP_PROPERTIES_FILE(pbe)                                                                                                \
{                                                                                                                                \
   char szCurrentSettingsFile[MAX_PATH];                                                                                         \
   char szCurrentBackendSettingsFile[MAX_PATH];                                                                                  \
   pbe -> ServicesAdvise(pCursiVision -> pICursiVisionServices);                                                                 \
   pCursiVision -> cookSettingsFileName(pCursiVision -> szActiveDocument,szCurrentSettingsFile,pbe,szCurrentBackendSettingsFile);\
   BSTR bstrPropertiesFileName = SysAllocStringLen(NULL,MAX_PATH);                                                               \
   MultiByteToWideChar(CP_ACP,0,szCurrentBackendSettingsFile,-1,bstrPropertiesFileName,MAX_PATH);                                \
   pbe -> put_ParentWindow(hwndMainFrame);                                                                                       \
   pbe -> put_PrintingSupportProfile(pCursiVision -> PrintingProfile());                                                         \
   BSTR defaultPropertiesFileName;                                                                                               \
   pbe -> get_PropertiesFileName(&defaultPropertiesFileName);                                                                    \
   FILE *fX = _wfopen(bstrPropertiesFileName,L"rb");                                                                             \
   if ( ! fX )                                                                                                                   \
      CopyFileW(defaultPropertiesFileName,bstrPropertiesFileName,TRUE);                                                          \
   else                                                                                                                          \
      fclose(fX);                                                                                                                \
   SysFreeString(defaultPropertiesFileName);                                                                                     \
   pbe -> put_PropertiesFileName(bstrPropertiesFileName);                                                                        \
   SysFreeString(bstrPropertiesFileName);                                                                                        \
}

struct mousePosition {
   mousePosition() : x(0),y(0) {};
   long x,y;
};

   extern bool isAdministrator;
   extern bool enforceAdministratorRestrictions;

   class CursiVision : public IGPropertiesClient {

   public:

      CursiVision(IUnknown *pIUnknownOuter = NULL);

      ~CursiVision();

      ISignaturePad *SignaturePad() { if ( ! GetSignatureProcess() ) return NULL; return GetSignatureProcess() -> SignaturePad(); };

      IPrintingSupportProfile *PrintingProfile(IPrintingSupportProfile *psp = NULL);

      long padWidthPixels();
      long padHeightPixels();
      double padWidthInches();

      void SetPropertiesFile(char *pszFileName) { strcpy(szPropertiesFile,pszFileName); };
      void SetForwardServer(char *pszServerName) { strcpy(szForwardToServer,pszServerName); };

      char *ActivePDFDocument() { return szActiveDocument; };

      std::map<long,signatureGraphic *> *SignatureGraphics() { return &signatureGraphics; };

      signatureGraphic *RealTimeSignatureGraphic() { 
         if ( ! pRealTimeSignatureGraphic ) 
            pRealTimeSignatureGraphic = new signatureGraphic(true); 
         return pRealTimeSignatureGraphic; 
      };

      void ResetRealTimeSignatureGraphic() {
         if ( pRealTimeSignatureGraphic )
            pRealTimeSignatureGraphic -> totalPoints = 0;
      };

      void ClearSignatureGraphics();

      STDMETHOD(PushProperties)();
      STDMETHOD(PopProperties)();
      STDMETHOD(DiscardProperties)();
      STDMETHOD(SaveProperties)();

      //   IUnknown

      STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
      STDMETHOD_ (ULONG, AddRef)();
      STDMETHOD_ (ULONG, Release)();

      //   IPropertiesClient

      STDMETHOD(SavePrep)();
      STDMETHOD(InitNew)();
      STDMETHOD(Loaded)();
      STDMETHOD(IsDirty)();
      STDMETHOD(GetClassID)(BYTE *pCLSID);

      //   IPropertyPageClient

      class _IGPropertyPageClient : public IGPropertyPageClient {
      public:

         _IGPropertyPageClient(CursiVision *p) : pParent(p),refCount(0) {};
         ~_IGPropertyPageClient() {};

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

      private:

         STDMETHOD(BeforeAllPropertyPages)();
         STDMETHOD(GetPropertyPagesInfo)(long* countPages,SAFEARRAY** stringDescriptions,SAFEARRAY** stringHelpDirs,SAFEARRAY** pSize);
         STDMETHOD(CreatePropertyPage)(long,HWND,RECT *,BOOL,HWND *pHwndPropertyPage);
         STDMETHOD(Apply)();
         STDMETHOD(IsPageDirty)(long,BOOL*);
         STDMETHOD(Help)(BSTR bstrHelpDir);
         STDMETHOD(TranslateAccelerator)(long,long*);
         STDMETHOD(AfterAllPropertyPages)(BOOL);
         STDMETHOD(DestroyPropertyPage)(long);

         STDMETHOD(GetPropertySheetHeader)(void *pHeader);
         STDMETHOD(get_PropertyPageCount)(long *pCount);
         STDMETHOD(GetPropertySheets)(void *pSheets); 

         CursiVision *pParent;
         long refCount;

      } *pIGPropertyPageClient;

#ifdef EMBEDDED_OBJECT_EMBEDDER_CLASS
#undef EMBEDDED_OBJECT_EMBEDDER_CLASS
#endif

#define EMBEDDED_OBJECT_EMBEDDER_CLASS CursiVision

#include "interfacesToSupportAnEmbeddedObject.h"

      // DWebBrowserEvents2

      class _DWebBrowserEvents2 : public DWebBrowserEvents2 {

      public:

         _DWebBrowserEvents2(CursiVision *pp) : pParent(pp) {};

         //   IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHODIMP_(ULONG) AddRef();
         STDMETHODIMP_(ULONG) Release();

         // IDispatch

         STDMETHOD(GetTypeInfoCount)(UINT *pctinfo);
         STDMETHOD(GetTypeInfo)(UINT itinfo, LCID lcid, ITypeInfo **pptinfo);
         STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgdispid);
         STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pdispparams, VARIANT *pvarResult, EXCEPINFO *pexcepinfo, UINT *puArgErr);

      private:

         CursiVision *pParent;

      } *pDWebBrowserEvents_HTML_Host;

      class _IPDFiumControlEvents : public IPDFiumControlEvents {

      public:

         _IPDFiumControlEvents(CursiVision *pp) : pParent(pp) {};

         // IUnknown

         STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
         STDMETHOD_ (ULONG, AddRef)();
         STDMETHOD_ (ULONG, Release)();

         // IPDFiumControlEvents

         STDMETHOD(MouseMessage)(UINT msg,WPARAM wParam,LPARAM lParam);

         STDMETHOD(Size)(SIZE *pSize);

         STDMETHOD(Paint)(HDC hdc,RECT *prcUpdate);

      private:

         CursiVision *pParent;

      } *pIPDFiumControlEvents{NULL};

      //      IConnectionPointContainer

      struct _IConnectionPointContainer : public IConnectionPointContainer {

      public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

        STDMETHOD(FindConnectionPoint)(REFIID riid,IConnectionPoint **);
        STDMETHOD(EnumConnectionPoints)(IEnumConnectionPoints **);

        _IConnectionPointContainer(CursiVision *pp);
		  ~_IConnectionPointContainer();

        void fire_PropertyChanged();

     private:

		  CursiVision *pParent;

     } connectionPointContainer;

     struct _IConnectionPoint : IConnectionPoint {

	  public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

        STDMETHOD (GetConnectionInterface)(IID *);
        STDMETHOD (GetConnectionPointContainer)(IConnectionPointContainer **ppCPC);
        STDMETHOD (Advise)(IUnknown *pUnk,DWORD *pdwCookie);
        STDMETHOD (Unadvise)(DWORD);
        STDMETHOD (EnumConnections)(IEnumConnections **ppEnum);

        _IConnectionPoint(CursiVision *pp);
		  ~_IConnectionPoint();
		  IUnknown *AdviseSink() { return adviseSink; };

     private:

        int getSlot();
        int findSlot(DWORD dwCookie);

		  IUnknown *adviseSink;
		  CursiVision *pParent;
        DWORD nextCookie;
		  int countConnections,countLiveConnections;

        CONNECTDATA *connections;

     } connectionPoint;


	  struct _IEnumConnectionPoints : IEnumConnectionPoints {

	  public:

        STDMETHOD (QueryInterface)(REFIID riid,void **ppv);
        STDMETHOD_ (ULONG, AddRef)();
        STDMETHOD_ (ULONG, Release)();

 	     STDMETHOD (Next)(ULONG cConnections,IConnectionPoint **rgpcn,ULONG *pcFetched);
        STDMETHOD (Skip)(ULONG cConnections);
        STDMETHOD (Reset)();
        STDMETHOD (Clone)(IEnumConnectionPoints **);

	     _IEnumConnectionPoints(CursiVision *pp,_IConnectionPoint **cp,int connectionPointCount);
       ~_IEnumConnectionPoints();

     private:

        int cpCount,enumeratorIndex;
		  CursiVision *pParent;
		  _IConnectionPoint **connectionPoints;

     } *enumConnectionPoints;

     struct _IEnumerateConnections : public IEnumConnections {

     public:

        _IEnumerateConnections(IUnknown* pParentUnknown,ULONG cConnections,CONNECTDATA* paConnections,ULONG initialIndex);
        ~_IEnumerateConnections();

         STDMETHOD(QueryInterface)(REFIID, void **);
         STDMETHODIMP_(ULONG) AddRef();
         STDMETHODIMP_(ULONG) Release();
         STDMETHOD(Next)(ULONG, CONNECTDATA*, ULONG*);
         STDMETHOD(Skip)(ULONG);
         STDMETHOD(Reset)();
         STDMETHOD(Clone)(IEnumConnections**);

      private:

        ULONG refCount;
        IUnknown *pParentUnknown;
        ULONG enumeratorIndex;
        ULONG countConnections;
        CONNECTDATA *connections;

      } *enumConnections;

      class _ICursiVisionServices : public ICursiVisionServices {

      public:

         _ICursiVisionServices(CursiVision *pp);

         HRESULT __stdcall QueryInterface(REFIID riid,void **ppv) { return pParent -> QueryInterface(riid,ppv); };
         ULONG __stdcall AddRef() { return pParent -> AddRef(); };
         ULONG __stdcall Release() { return pParent -> Release(); };

         STDMETHOD(get_SignaturePad)(ISignaturePad **);
         STDMETHOD(get_ParentWindow)(HWND *);

         STDMETHOD(get_PrintingSupportProfile)(IPrintingSupportProfile **);
         STDMETHOD(put_PrintingSupportProfile)(IPrintingSupportProfile *);

         STDMETHOD(RegisterSignaturePad)(BSTR bstrOCXName);

         long __stdcall SavedSignatureGraphicCount();
         HRESULT __stdcall SaveSignatureGraphic(void *pvSignatureData);
         void * __stdcall GetSignatureGraphic(long index);
         void __stdcall FreeSignatureGraphic(void *pvSignatureGraphic);

         void __stdcall ResetTextOutlines();

         void * __stdcall GetTextOutlines(long pageNumber,long *pReportedPageWidth,long *pReportedPageHeight);

         void __stdcall FreeTextOutlines(void *pvTextOutlines);

         char * __stdcall DocumentName();

         HRESULT __stdcall GenerateOutlines(char *pszPrintedDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage,long *pFound);

         HRESULT __stdcall ConvertPixelsToPDF(char *pszPrintedDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage);

         long __stdcall FieldCount();
         char * __stdcall FieldLabels();
         char * __stdcall FieldValue(long index);
         char * __stdcall FieldValueFromLabel(char *pszFieldName);

         long __stdcall SignatureLocationCount();
         oleWritingLocation * __stdcall SignatureLocations();

         long generateOutlines(char *pszPrintedDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage);

         long convertPixelsToPDF(char *pszPrintedDocument,char *pszCurrentOutlinesFile,long startPage,long maxPDFPage);

         bool readOutlines(RECT **ppPDFEntries,long **ppPDFPages,
                              char *pszOriginalDocument = NULL,char *pszOutlinesFile = NULL,
                              char **ppszTextValues = NULL,long *pCountEntries = NULL,
                              long *pCXPage = NULL,long *pCYPage = NULL);

         BOOL __stdcall IsAdministrator() { if ( ! enforceAdministratorRestrictions ) return TRUE; return isAdministrator; };

         char * __stdcall GlobalDataStore() { return szGlobalDataStore; };

         void __stdcall SetIsAdministrator(BOOL v) { isAdministrator = v ? true : false; };

         BOOL __stdcall EnforceNonAdministrativeRestrictions() { return enforceAdministratorRestrictions; }

      private:

         void setupPDFEnabler(char *pszDocument);
         void closePDFEnabler();

         long retrieveOrFindPDFFields(char *pszTargetOutlinesFile,long pageNumber);
         long findPDFFields(char *pszTargetOutlinesFile,long pageNumber);

         IPdfEnabler *pIPdfEnabler;
         IPdfDocument *pIPdfDocument;
         HMODULE hModule_PDFEnabler;

         CursiVision *pParent;

      } *pICursiVisionServices;

#include "doodleOptionsProperties.h"

      SignatureProcess *GetSignatureProcess() { return pSignatureProcess; };

      BOOL IsContinuousDoodle();
      long DoodleCount() { return doodleCount; };

      void FinalizeSignatureProcess(bool deferProcessDisposition = false);
      void CancelSignatureProcess();

      void SetPageNumber(long pageNumber);
      double XPixelsPerPageUnit();
      double YPixelsPerPageUnit();
      void startTracking();
      void pauseTracking();
      void resumeTracking();
      void quitTracking();
      void notifyDocumentIsTracked();
      void notifyTrackCycleEnded();
      bool IsDocumentTracked();
	  
      long handleUnsavedDocument(bool isForExit = false);

      void toDocumentCoordinates(RECT *);
      void toDocumentCoordinates(RECT *pInput,RECT *pOutput);
      void toDocumentCoordinates(POINTL *);
      void toWindowCoordinates(RECT *);
      void toWindowCoordinates(RECT *pInput,RECT *pOutput);
      void toWindowCoordinates(POINTL *);
      long toPixels(double inPageCoordinates);

      long essentialInitialization();
      long initialize(bool okayToAutoOpen,bool loadWelcomePage);
      long initWindows(bool loadWelcomePage);

      long openDocument(char *pszFileName,long pageNumber = 1L,bool setActive = true,bool ignore1 = false,bool ignore2 = false,bool resetPrintProfile = false);
      long openDocument(BSTR bstrFileName,long pageNumber = 1L,bool setActive = true,bool ignore1 = false,bool ignore2 = false,bool resetPrintProfile = false);
      long openDocumentFast(char *pszFileName,long pageNumber);
      long printDocument();
      long closeDocument();

      long updateDocument(long writingLocationIndex,bool deferProcessDisposition);
      long saveDocument(long reOpenedPageNumber = -1L);

      long undoUpdate();

      void processDisposition(resultDisposition *,bool deferBackEnds,long reOpenedPageNumber,char *szResultFile,char *pszGraphicDataFileName = NULL,char *pszSettingsFileName = NULL);
      void processBackends(resultDisposition *pDisposition,BSTR *pBstrResultsFile,bool isTempFile,bool *pHandledByBackend,bool *pReOpenedByBackEnd,char *pszGraphicDataFileName,char *pszSettingsFileName,BSTR specificBackendGUID = NULL,BSTR specificBackendClass = NULL);
      void beDispose(ICursiVisionBackEnd *,char *pszBESettingsFile,BSTR *pBstrResultsFile,bool isTempFile,bool *pReOpendByBackEnd,char *pszGraphicDataFileName,char *pszSettingsFileName);
      void processPrinting();

      void resized();

      void createDoodleOptions();

      resultDisposition *currentProcessDisposition();

      struct doodleOptionProperties *currentDoodleOptionProperties();

      char *currentSettingsFileName();

      void cookSettingsFileName(char *pszDocumentName,char *pszResult,ICursiVisionBackEnd *pBackEnd = NULL,char *pszBackEndSettingsFile = NULL);

      char *GetDispositionSettingsFileName();

      void raiseMouseVellum();
      void lowerMouseVellum();

      bool documentIsOpen() { return isDocumentOpen; };
      bool documentIsModified() { return isDocumentModified; };

      HRESULT createPrintingSupport();
      HRESULT reCreatePrintingSupport();
      void createPrintingSupportThreaded();
      static void __stdcall printSupportInitialized(void *);
      HANDLE printingSupportInitialized;

      long registerSignaturePad(char *pszOCXFileName);

      void findKnownBackEnds();

      long pingSignaturePad();

      BSTR getBackEndDescription(GUID theBackEnd);

      SignatureProcess *pSignatureProcess;

      char szActiveDocument[MAX_PATH];
      char szActiveDocumentRootName[MAX_PATH];
      char szSignedDocument[MAX_PATH];
      char szPropertiesFile[MAX_PATH];
      char szInterimFileName[MAX_PATH];
      char szLastActiveDocument[MAX_PATH];
      char szWelcomeURL[MAX_PATH];

      BSTR bstrActiveDocument;

      long refCount;

      RECT rectFrame,rectDoodleOptions;

      long activeMonitor,monitorCount;
      bool wasMaximized;
      RECT rcMaximizedRestore;

      bool adviseWaiting;

      bool isImmediateDoodle;
      bool isRestored;
      bool isDocumentOpen;
      bool isDocumentOpenInProgress;
      bool isDocumentModified;
      bool isInterimDocument;
      bool isPrintJob;
      bool hasExitDeferral;
      bool signatureEndTimerSet;
      bool isOkayToAutoOpen;
      bool isPrintProfileTraining;
      bool isDoodleProfileTraining;
      bool reRunDoodle;
      bool cancelOpenInProgress;
      bool ignoreDocumentOpenProcessingSteps;
      bool isWritingEnabled;

      bool isGlobalPropertiesSetup;

      bool quitAfterSaving;
      bool openAfterSaving;

      WPARAM wParamAfterSaving;
      LPARAM lParamAfterSaving;
      bool handledByBackEnd;

      long optionChoice,optionPaneWidth,optionPaneHeight;
      long optionWidths[8];
      long optionHeight;
      long defaultOption;

      char szOptionText[1024];
      char szOptions[512];
      char szForwardToServer[128];

      resultDisposition processingDisposition;

      //
      // Objects Used to host PDFiumControl
      //

      IOleInPlaceObject *pIOleInPlaceObject_HTML;
      IOleObject *pIOleObject_HTML;
      IOleInPlaceActiveObject *pIOleInPlaceActiveObject_HTML;

      IConnectionPoint *pIConnectionPoint_HTML;
      DWORD connectionCookie_HTML;

      IConnectionPoint *pIConnectionPoint_PDFiumControlEvents;
      DWORD connectionCookie_PDFiumControlEvents;

      IPDFiumControl *pIPDFiumControl{NULL};

      long doodleCount;

      double xPixelsPerPageUnit,yPixelsPerPageUnit;

      double padToPDFScale;
      double lastPadToPDFScale;

      POINTL pdfDocumentUpperLeft;
      POINTL pdfDocumentLowerLeft;
      POINTL pdfDocumentUpperRight;
      POINTL pdfDocumentLowerRight;

      long pdfPageWidth,pdfPageHeight;
      long pdfPageCount;

      long pdfPageNumber;
      long pdfAdobePageNumber;
      char pdfPageLabel[32];

      bool autoScrollMode;
      bool expectingPostPaint;
      bool isProcessingSaveAndReopen;
      bool signActivityCanceled;

      long pdfPageNumberInAbbayence;

      long cachedPadWidthPixels;
      long cachedPadHeightPixels;
      double cachedPadWidthInches;

      std::map<long,signatureGraphic *> signatureGraphics;
      signatureGraphic *pRealTimeSignatureGraphic;
      signatureGraphic *pLastSignatureGraphic;

      std::list<BSTR> menuBackends;
      std::list<backEndPackage *> allBackEnds;

      HWND hwndDispositionSettings;
      HWND hwndAdditionalBackEnds;

      HACCEL hAccelerator;

      CRITICAL_SECTION graphicListAccess;

      IGProperties *pIGProperties;

      IPdfEnabler *pIPdfEnabler;
      IPdfDocument *pIPdfDocument;
      IPdfPage *pIPdfPage;
      doodleOptions *pDoodleOptions;

      IPrintingSupport *pIPrintingSupport;
      IPrintingSupportProfile *pIPrintingSupportProfile;

      ISignaturePad *pISignaturePad_PropertiesOnly;
      bool signaturePadHasBeenPinged;

      CRITICAL_SECTION trackPageNumberAccess;
      HANDLE trackPageNumberDone;

      void protectedTrackPageNumber();

      static VOID CALLBACK trackPageNumber();
      static void trackPageNumberUIThread();

      unsigned int threadTrackPageNumberAddress;
      HANDLE hThreadTrackPageNumber;

      static unsigned int __stdcall trackPageNumberMonitor(void *);

      static IClassFactory *pIClassFactory_SignaturePads;

      static LRESULT CALLBACK globalSettingsStoreHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK dispositionSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK dispositionEmailSettingsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalBackEndsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK listViewHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK aboutHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK optionsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK additionalSaveOptionsHandler(HWND,UINT,WPARAM,LPARAM);
      static LRESULT CALLBACK displayWaitingHandler(HWND,UINT,WPARAM,LPARAM);


   };

#ifndef MIDL_DEFINE_GUID
#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}
#endif

MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x00);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionBackEndPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x02);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionControlPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x03);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionTemplatePropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x04);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionRecognitionPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x05);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionSigningLocationsPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x06);
MIDL_DEFINE_GUID(CLSID,CLSID_CursiVisionFieldsPropertyPage,0xA64AB7AF,0x8A26,0x4f07,0x88,0x77,0x56,0xFE,0x99,0x57,0x04,0x07);

#define EOL "\x0D\x0A"

extern "C" int GetCommonAppDataLocation(HWND hwnd,char *);
extern "C" int GetDocumentsLocation(HWND hwnd,char *);
int GetLocation(HWND hwnd,long key,char *szFolderLocation);

void logEvent(LPTSTR szMessage);

int pixelsToHiMetric(SIZEL *pPixels,SIZEL *phiMetric);
int hiMetricToPixel(SIZEL *phiMetric,SIZEL *pPixels);

LRESULT CALLBACK timerHandler(HWND hwnd,WPARAM wParam,LPARAM lParam);
LRESULT CALLBACK commandHandler(HWND hwnd,WPARAM wParam,LPARAM lParam);
LRESULT CALLBACK paintHandler(HWND hwnd,WPARAM wParam,LPARAM lParam);
LRESULT CALLBACK sizeHandler(HWND hwnd,WPARAM wParam,LPARAM lParam);

LRESULT CALLBACK handler(HWND,UINT,WPARAM,LPARAM);
LRESULT CALLBACK wmUserHandler(UINT,WPARAM,LPARAM);
LRESULT CALLBACK mouseVellumHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
LRESULT CALLBACK htmlHostHandler(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

long hashCode(char *pszInput);

#ifdef DEFINE_DATA

   HWND hwndMainFrame = NULL,hwndHTMLHost = NULL,hwndStatusBar = NULL,hwndToolBar = NULL;

   HWND hwndMouseVellum = NULL;
   HBITMAP hbmSuperVellum = NULL;
   HDC hdcSuperVellum = NULL;
   
   HWND hwndSigningControl = NULL;
   HWND hwndDisplayWaiting = NULL;

   HBITMAP hBitmapCancel,hBitmapSetup,hBitmapHelp,hBitmapForget,hBitmapUndo,hBitmapSeal,hBitmapRepeat,hBitmapCreate;
   HDC hdcGDIPlus = NULL;

   HDC hdcPDFSource = NULL;
   HBITMAP hBitmapPDFPage = NULL;

   long statusBarHeight = 0L;
   long toolBarHeight = 0L;
   long scrollBarWidth = 0L;
   long scrollBarHeight = 0L;

   RECT rcHTMLHost;
   long cxHTMLHost = 0L,cyHTMLHost = 0L;
   long deviceCount = 0;
   char deviceName[64];

   CursiVision *pCursiVision = NULL;

   char szArguments[16][MAX_PATH];

   HMENU hMenuSetup = NULL;
   HMENU hMenuDeviceList = NULL;
   HMENU hMenuDoodle = NULL;
   HMENU hMenuFile = NULL;
   HMENU hMenuTools = NULL;
   HMENU hMenuBackends = NULL;

   WNDPROC nativeListViewHandler = NULL;

   OLECHAR wstrModuleName[256];
   
   HMODULE hModule = NULL;

   char szModuleName[MAX_PATH];

   bool waitForPDFPaint = false;

   char szPageNumberText[64];

   bool isAdministrator = false;
   bool enforceAdministratorRestrictions = true;

   WCHAR szwTemporaryHTMLDocument[MAX_PATH];

#else

   extern HWND hwndMainFrame,hwndHTMLHost,hwndStatusBar,hwndToolBar;
 
   extern HWND hwndMouseVellum;
   extern HBITMAP hbmSuperVellum;
   extern HDC hdcSuperVellum;

   extern HWND hwndSigningControl;
   extern HWND hwndDisplayWaiting;

   extern HBITMAP hBitmapCancel,hBitmapSetup,hBitmapHelp,hBitmapForget,hBitmapUndo,hBitmapSeal,hBitmapRepeat,hBitmapCreate;
   extern HDC hdcGDIPlus;

   extern HDC hdcPDFSource;
   extern HBITMAP hBitmapPDFPage;

   extern long statusBarHeight;
   extern long toolBarHeight;
   extern long scrollBarWidth;
   extern long scrollBarHeight;

   extern RECT rcHTMLHost;
   extern long cxHTMLHost,cyHTMLHost;
   extern long deviceCount;
   extern char deviceName[];

   extern CursiVision *pCursiVision;

   extern char szArguments[16][MAX_PATH];

   extern HMENU hMenuSetup;
   extern HMENU hMenuDeviceList;
   extern HMENU hMenuDoodle;
   extern HMENU hMenuFile;
   extern HMENU hMenuTools;
   extern HMENU hMenuBackends;

   extern WNDPROC nativeListViewHandler;

   extern mousePosition mouseStart,mouseEnd;

   extern OLECHAR wstrModuleName[];
   
   extern HMODULE hModule;

   extern char szModuleName[];

   extern HANDLE hReaderMonitor;

   extern bool waitForPDFPaint;

   extern char szPageNumberText[64];

   extern bool isAdministrator;
   extern bool enforceAdministratorRestrictions;

   extern WCHAR szwTemporaryHTMLDocument[];

#endif

