// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

#include <process.h>

   unsigned int __stdcall monitorPrinter(void *);

   extern long pageNumberForContinuation;

   LRESULT CALLBACK timerHandler(HWND hwnd,WPARAM wParam,LPARAM lParam) {

   if ( TIMER_EVENT_DISPLAY_WAITING == wParam ) {
      if ( ! pCursiVision -> adviseWaiting ) {
         KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      }
      if ( ! pCursiVision -> pSignatureProcess )
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      if ( REGION_CAPTURE_STAGE_DONE == pCursiVision -> pSignatureProcess -> RegionCaptureStage() ) {
         KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);
         if ( hwndDisplayWaiting )
            DestroyWindow(hwndDisplayWaiting);
         hwndDisplayWaiting = NULL;
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      }
      if ( REGION_CAPTURE_STAGE_ONE != pCursiVision -> pSignatureProcess -> RegionCaptureStage() )
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      if ( hwndDisplayWaiting )
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);

      hwndDisplayWaiting = CreateDialog(hModule,MAKEINTRESOURCE(IDD_DISPLAY_WAITING),hwndMainFrame,(DLGPROC)CursiVision::displayWaitingHandler);
      MessageBeep(MB_ICONASTERISK);
      SetFocus(hwndMainFrame);

      return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
   }

   if ( TIMER_EVENT_START_DOODLE_PART2 == wParam ) {

      KillTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE_PART2);

      if ( ! pCursiVision -> pSignatureProcess )
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);

      if ( NULL == hwndSigningControl ) {

         pCursiVision -> pDoodleOptions -> CreateControlWindow();

         if ( pCursiVision -> pISignaturePad_PropertiesOnly -> IsFullPage() ) {

            RECT *pRect = &pCursiVision -> currentProcessDisposition() -> rcOptions;
         
            long cyScreen = GetSystemMetrics(SM_CYSCREEN);
            long cxScreen = GetSystemMetrics(SM_CXSCREEN);
            RECT rcDoodleOptions;
         
            GetClientRect(hwndSigningControl,&rcDoodleOptions);
         
            if ( ( 0 == pRect -> left && 0 == pRect -> right ) || 
                     pRect -> left > (cxScreen - (rcDoodleOptions.right - rcDoodleOptions.left) ) || 
                     pRect -> top > (cyScreen - (rcDoodleOptions.bottom - rcDoodleOptions.top) ) )
               memcpy(pRect,&pCursiVision -> rectDoodleOptions,sizeof(RECT));
         
            RECT rcFrame;
            GetWindowRect(hwndHTMLHost,&rcFrame);

            if ( ( 0 == pRect -> left && 0 == pRect -> right ) || 
                     pRect -> left > (cxScreen - (rcDoodleOptions.right - rcDoodleOptions.left) ) || 
                     pRect -> top > (cyScreen - (rcDoodleOptions.bottom - rcDoodleOptions.top) ) ) {
               pRect -> left = rcFrame.right - (rcDoodleOptions.right + rcDoodleOptions.left / 2);
               pRect -> top = rcFrame.top + 8;
               pRect -> right = pRect -> left + (rcDoodleOptions.right - rcDoodleOptions.left);
               pRect -> bottom = pRect -> top + (rcDoodleOptions.bottom - rcDoodleOptions.top);
            }

            SetWindowPos(hwndSigningControl,HWND_TOP,pRect -> left,pRect -> top,0,0,SWP_NOSIZE);

         }

      } else

         ShowWindow(hwndSigningControl,SW_SHOW);

      if ( 0 == pCursiVision -> DoodleCount() ) 
         pCursiVision -> pDoodleOptions -> ClearSignaturesFile();

      writingLocation writingLocation;
      long pageNumber;
      long adobePageNumber;

      if ( S_OK == pCursiVision -> pDoodleOptions -> GetLocation(pCursiVision -> DoodleCount(),&writingLocation.documentRect,&pageNumber,&adobePageNumber,false) ) {

         pCursiVision -> toWindowCoordinates(&writingLocation.documentRect);
         pCursiVision -> SetPageNumber(pageNumber);
         pCursiVision -> pdfPageNumberInAbbayence = pageNumber;

//         strcpy(pCursiVision -> pdfPageLabelInAbbayence,pageLabel);
         pCursiVision -> pSignatureProcess -> OuterRect(&writingLocation.documentRect);

         KillTimer(hwndMainFrame,TIMER_EVENT_DISPLAY_WAITING);
         if ( hwndDisplayWaiting ) {
            ShowWindow(hwndDisplayWaiting,SW_HIDE);
            DestroyWindow(hwndDisplayWaiting);
            hwndDisplayWaiting = NULL;
         }

         pCursiVision -> isDoodleProfileTraining = false;
         pCursiVision -> isPrintProfileTraining = false;

         PostMessage(hwndMainFrame,WM_START_ADHOC,(WPARAM)0L,0L);

      } else {

         pCursiVision -> raiseMouseVellum();

         pCursiVision -> pdfPageNumberInAbbayence = -1L;
         pCursiVision -> pSignatureProcess -> RegionCaptureStage(REGION_CAPTURE_STAGE_ONE);

         if ( pCursiVision -> pISignaturePad_PropertiesOnly -> IsFullPage() ) {
            //pCursiVision -> protectedTrackPageNumber();
            pCursiVision -> pSignatureProcess -> OuterRect(NULL);
            pCursiVision -> pSignatureProcess -> RegionCaptureStage(REGION_CAPTURE_STAGE_DONE);
         } else 
            SetWindowText(hwndStatusBar,"Move the mouse over the document. The outline shows the size of the pad's display. Click when done.");

         pCursiVision -> isDoodleProfileTraining = true;

         if ( pCursiVision -> PrintingProfile() ) 
            pCursiVision -> isPrintProfileTraining = true;

      }

      return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);

   }

   if ( TIMER_EVENT_PRINT_ONLY == wParam ) {

      KillTimer(hwndMainFrame,TIMER_EVENT_PRINT_ONLY);

      if ( ! pCursiVision -> documentIsOpen() ) {
         SetTimer(hwndMainFrame,TIMER_EVENT_PRINT_ONLY,200,0L);
         return (LRESULT)0;
      }

      DWORD dwSize = MAX_PATH;

      GetDefaultPrinter(szDefaultPrinter,&dwSize);

      SetDefaultPrinter(szArguments[IDX_PRINT_TO]);

      HANDLE hPrinter;

      if ( OpenPrinter(szArguments[IDX_PRINT_TO],&hPrinter,NULL) ) {

         BYTE *pDevMode = new BYTE[4096];

         char szPrinterSettingsFile[MAX_PATH];

         sprintf(szPrinterSettingsFile,"%s\\Settings\\printerSettings",szApplicationDataDirectory);

         FILE *fPrinterSettings = fopen(szPrinterSettingsFile,"rb");

         if ( fPrinterSettings ) {

            fread(pDevMode,4096,1,fPrinterSettings);
            fclose(fPrinterSettings);

            PRINTER_INFO_1 printerInfo;
            DWORD cb;
            GetPrinter(hPrinter,1,(BYTE *)&printerInfo,sizeof(PRINTER_INFO_1),&cb);
            DocumentProperties(NULL,hPrinter,printerInfo.pName,NULL,(DEVMODE *)pDevMode,DM_IN_BUFFER);

            delete [] pDevMode;

         }

      }

      pCursiVision -> pIPDFiumControl -> PrintCurrentDocument(FALSE);

      unsigned int threadAddr;
      HANDLE hNotification = (HANDLE)_beginthreadex(NULL,4096,monitorPrinter,(void *)NULL,CREATE_SUSPENDED,&threadAddr);
      ResumeThread(hNotification);

      return (LRESULT)0;

   }

//NTC: 12-3-2012
//
// The following directives WERE active before today, but I commented them out.
// The Topaz pad was doing the flash thing, and this may have been why (the
// signature process was not getting deleted and rebuilt).
//
//#ifdef DO_PROCESSES
   if ( TIMER_EVENT_CONTINUE_PLAY == wParam ) {

      KillTimer(hwnd,TIMER_EVENT_CONTINUE_PLAY);

      if ( ! pCursiVision -> documentIsOpen() ) {
         SetTimer(hwnd,TIMER_EVENT_CONTINUE_PLAY,100,0L);
         return (LRESULT)0;
      }

      pCursiVision -> pSignatureProcess = new SignatureProcess();
      pCursiVision -> pSignatureProcess -> ContinueNextPage(pageNumberForContinuation);

      return (LRESULT)0;
   }
//#endif

   if ( TIMER_EVENT_REDOODLE == wParam ) {

      KillTimer(hwndMainFrame,TIMER_EVENT_REDOODLE);
#if 1
      if ( pCursiVision -> documentIsOpen() )
#else
      if ( ! pCursiVision -> pSignatureProcess && pInsertPDF -> documentIsOpen() )
#endif
         PostMessage(hwndMainFrame,WM_COMMAND,MAKEWPARAM(ID_DOODLE,1),0L);
      else
         SetTimer(hwndMainFrame,TIMER_EVENT_REDOODLE,RE_DOODLE_DELAY,0L);

      return (LRESULT)0;
   }

   if ( TIMER_EVENT_START_DOODLE == wParam ) {

      KillTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE);

      if ( pCursiVision -> signActivityCanceled )
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);

      if ( pCursiVision -> expectingPostPaint ) {
         SetTimer(hwndMainFrame,TIMER_EVENT_START_DOODLE,DOODLE_START_DELAY,NULL);
         return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
      }

      RedrawWindow(hwnd,NULL,NULL,RDW_INVALIDATE | RDW_UPDATENOW);
      PostMessage(hwnd,WM_COMMAND,MAKEWPARAM(ID_DOODLE,0),0L);

      return (LRESULT)0;
   }

   if ( TIMER_EVENT_END_SIGNATURE == wParam ) {
      pCursiVision -> signatureEndTimerSet = false;
      KillTimer(hwndMainFrame,TIMER_EVENT_END_SIGNATURE);
      pCursiVision -> SignaturePad() -> FireOption(1);
      return (LRESULT)0;
   }

   return DefWindowProc(hwnd,WM_TIMER,wParam,lParam);
   }

