// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   LRESULT CALLBACK sizeHandler(HWND hwnd,WPARAM wParam,LPARAM lParam) {

   if ( SIZE_MINIMIZED == wParam )
      return DefWindowProc(hwnd,WM_SIZE,wParam,lParam);

   if ( ! hwndHTMLHost )
      return DefWindowProc(hwnd,WM_SIZE,wParam,lParam);

   RECT rcToolBar;

   GetWindowRect(hwndToolBar,&rcToolBar);

   SetWindowPos(hwndStatusBar,HWND_TOP,0,HIWORD(lParam) - statusBarHeight,LOWORD(lParam),statusBarHeight,0L);

   toolBarHeight = rcToolBar.bottom - rcToolBar.top;

   SetWindowPos(hwndHTMLHost,HWND_BOTTOM,0,toolBarHeight,LOWORD(lParam),HIWORD(lParam) - statusBarHeight - toolBarHeight,0L);

   SetWindowPos(hwndToolBar,HWND_TOP,0,0,LOWORD(lParam),toolBarHeight,0L);

   int parts[] = {LOWORD(lParam) - 32 - 128 - 8 - 32 - 8,LOWORD(lParam) - 32 - 64 - 8,LOWORD(lParam) - 16,-1,0};

   SendMessage(hwndStatusBar,SB_SETPARTS,(WPARAM)3,(LPARAM)&parts);

   GetClientRect(hwndHTMLHost,&rcHTMLHost);

   cxHTMLHost = rcHTMLHost.right - rcHTMLHost.left;

   cyHTMLHost = rcHTMLHost.bottom - rcHTMLHost.top;

   pCursiVision -> resized();

   if ( pCursiVision -> pIOleInPlaceObject_HTML ) 
      pCursiVision -> pIOleInPlaceObject_HTML -> SetObjectRects(&rcHTMLHost,&rcHTMLHost);

   GetWindowRect(hwndMainFrame,&pCursiVision -> rectFrame);

   return (LRESULT)0L;
   }
