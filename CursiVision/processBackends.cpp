// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CursiVision.h"

   void CursiVision::processBackends(resultDisposition *pDisposition,BSTR *pBstrResultsFile,bool isTempFile,
                                       bool *pHandledByBackend,bool *pReOpenedByBackEnd,
                                       char *pszGraphicDataFileName,char *pszSettingsFileName,BSTR specificBackendGUID,BSTR specificBackendClass) {

   if ( specificBackendGUID || szArguments[IDX_USE_BACK_END_NAME][0] ) {

      CLSID clsId;
      char szBackEndSettingsFile[MAX_PATH];

      if ( ! specificBackendGUID ) {

         long beIndex = 0L;
         for ( std::list<backEndPackage *>::iterator it = allBackEnds.begin(); it != allBackEnds.end(); it++ ) {
            if ( 0 == strcmp(szArguments[IDX_USE_BACK_END_NAME],(*it) -> szDescription) )
               break;
            beIndex++;
         }

         OLECHAR oleString[MAX_PATH];
         MultiByteToWideChar(CP_ACP,0,szArguments[IDX_USE_BACK_END_NAME],-1,oleString,MAX_PATH);

         strcpy(szBackEndSettingsFile,pDisposition -> backEndSettingsFiles[beIndex]);

         CLSIDFromString(oleString,&clsId);

      } else {

         CLSIDFromString(specificBackendGUID,&clsId);

      }

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(clsId,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( ! ( S_OK == rc ) ) {
         OLECHAR szMessage[1024];
         BSTR bstrDescription = getBackEndDescription(clsId);
         swprintf(szMessage,L"The CursiVision Back End processor \n\n\t%s\n\nIs not registered correctly or is non-functional",bstrDescription);
         SysFreeString(bstrDescription);
         MessageBoxW(hwndMainFrame,szMessage,L"Error!",MB_ICONEXCLAMATION);
         return;
      }

      pICursiVisionBackEnd -> ServicesAdvise(pICursiVisionServices);

      if ( pszSettingsFileName && pszSettingsFileName[0] ) {

         char szCodeName[32];
         BSTR codeName;

         pICursiVisionBackEnd -> get_CodeName(&codeName);

         WideCharToMultiByte(CP_ACP,0,codeName,-1,szCodeName,32,0,0);

         SysFreeString(codeName);

         strcpy(szBackEndSettingsFile,pszSettingsFileName);

         char *p = strrchr(szBackEndSettingsFile,'.');
         if ( p )
            *p = '\0';

         sprintf(szBackEndSettingsFile + strlen(szBackEndSettingsFile),"_%s.settings",szCodeName);

      }

      beDispose(pICursiVisionBackEnd,szBackEndSettingsFile,
                        pBstrResultsFile,isTempFile,pReOpenedByBackEnd,pszGraphicDataFileName,pszSettingsFileName);

      pICursiVisionBackEnd -> Release();                                                                                                  

      if ( pHandledByBackend )
         *pHandledByBackend = true;

      return;

   } 

   GUID specificClassGUID = GUID_NULL;

   if ( ! ( NULL == specificBackendClass ) )
      CLSIDFromString(specificBackendClass,&specificClassGUID);

   BOOL didSpecificBackEnd = FALSE;

   for ( long k = 0; k < pDisposition -> countBackEnds; k++ ) {

      if ( ! ( NULL == specificBackendClass ) && ! IsEqualGUID(specificClassGUID,pDisposition -> backEndGUIDS[k]) )
         continue;

      if ( ! ( NULL == specificBackendClass ) && IsEqualGUID(specificClassGUID,pDisposition -> backEndGUIDS[k]) )
         didSpecificBackEnd = TRUE;

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(pDisposition -> backEndGUIDS[k],NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( S_OK != rc ) {
         char szMessage[1024];
         sprintf(szMessage,"The CursiVision Back End processor \n\n\t%s\n\nIs not registered correctly or is non-functional",pDisposition -> backEndDescriptions[k]);
         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);
         continue;        
      }

      pICursiVisionBackEnd -> ServicesAdvise(pICursiVisionServices);

      beDispose(pICursiVisionBackEnd,pDisposition -> backEndSettingsFiles[k],pBstrResultsFile,isTempFile,pReOpenedByBackEnd,pszGraphicDataFileName,pszSettingsFileName);

      pICursiVisionBackEnd -> Release();                                                                                                  

      if ( pHandledByBackend )
         *pHandledByBackend = true;

   }

   if ( ! ( GUID_NULL == specificClassGUID ) && ! didSpecificBackEnd ) {

      ICursiVisionBackEnd *pICursiVisionBackEnd = NULL;

      HRESULT rc = CoCreateInstance(specificClassGUID,NULL,CLSCTX_INPROC_SERVER,IID_ICursiVisionBackEnd,reinterpret_cast<void **>(&pICursiVisionBackEnd));

      if ( S_OK != rc ) {

         char szMessage[1024];
         sprintf(szMessage,"The CursiVision Back End processor specified is not registered correctly or is non-functional");
         MessageBox(hwndMainFrame,szMessage,"Error!",MB_ICONEXCLAMATION);

      } else {

         PREP_PROPERTIES_FILE(pICursiVisionBackEnd)

         pICursiVisionBackEnd -> ServicesAdvise(pICursiVisionServices);
         beDispose(pICursiVisionBackEnd,NULL,/*pDisposition -> backEndSettingsFiles[k],*/pBstrResultsFile,isTempFile,pReOpenedByBackEnd,pszGraphicDataFileName,pszSettingsFileName);
         pICursiVisionBackEnd -> Release(); 

      }

   }

   return;
   }


   void CursiVision::beDispose(ICursiVisionBackEnd *pICursiVisionBackEnd,char *pszBESettingsFile,BSTR *pBstrResultsFile,bool isTempFile,bool *pReOpenedByBackEnd,
                                 char *pszGraphicDataFileName,char *pszSettingsFileName) {

   if ( pszBESettingsFile && pszBESettingsFile[0] ) {
      BSTR bstrSettingsFile = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,pszBESettingsFile,-1,bstrSettingsFile,MAX_PATH);
      pICursiVisionBackEnd -> put_PropertiesFileName(bstrSettingsFile);
      SysFreeString(bstrSettingsFile);
   }

   pICursiVisionBackEnd -> put_PrintingSupportProfile(PrintingProfile());

   BSTR bstrOriginalFile = NULL;
   if ( isTempFile ) {
      bstrOriginalFile = SysAllocStringLen(NULL,MAX_PATH);
      MultiByteToWideChar(CP_ACP,0,szActiveDocument,-1,bstrOriginalFile,MAX_PATH);
   } else
      bstrOriginalFile = SysAllocString(*pBstrResultsFile);

   pICursiVisionBackEnd -> put_ParentWindow(hwndMainFrame);

   if ( szArguments[IDX_PASS_THRU][0] ) {
      BSTR bstrPassThru = SysAllocStringLen(NULL,(DWORD)strlen(szArguments[IDX_PASS_THRU]) + 1);
      MultiByteToWideChar(CP_ACP,0,szArguments[IDX_PASS_THRU],-1,bstrPassThru,(DWORD)strlen(szArguments[IDX_PASS_THRU]) + 1);
      pICursiVisionBackEnd -> put_CommandLine(bstrPassThru);                                                          
      SysFreeString(bstrPassThru);                                                                                    
   }
                                                                                                                
   if ( pszGraphicDataFileName || pszSettingsFileName ) {                                                          
                                                                                                                   
      BSTR bstrGraphicDataFileName = NULL;                                                                         
      if ( pszGraphicDataFileName ) {                                                                              
         bstrGraphicDataFileName = SysAllocStringLen(NULL,MAX_PATH);                                               
         MultiByteToWideChar(CP_ACP,0,pszGraphicDataFileName,-1,bstrGraphicDataFileName,MAX_PATH);                 
      }                                                                                                            
                                                                                                                   
      BSTR bstrDispositionSettingsFileName = NULL;                                                                 
      if ( pszSettingsFileName ) {                                                                                 
         bstrDispositionSettingsFileName = SysAllocStringLen(NULL,MAX_PATH);                                       
         MultiByteToWideChar(CP_ACP,0,pszSettingsFileName,-1,bstrDispositionSettingsFileName,MAX_PATH);            
      }                                                                                                            
                                                                                                                   
      pICursiVisionBackEnd -> Dispose(bstrOriginalFile,*pBstrResultsFile,bstrGraphicDataFileName,bstrDispositionSettingsFileName,isTempFile);
                                                                                             
      if ( bstrGraphicDataFileName )                                                         
         SysFreeString(bstrGraphicDataFileName);                                             
                                                                                             
      if ( bstrDispositionSettingsFileName )                                                 
         SysFreeString(bstrDispositionSettingsFileName);                                     
                                                                                             
   } else                                                                                    
                                                                                             
      pICursiVisionBackEnd -> Dispose(bstrOriginalFile,*pBstrResultsFile,NULL,NULL,isTempFile);
                                                                                             
   BSTR bstrNewName;                                                                         
   HRESULT didSave = pICursiVisionBackEnd -> get_SavedDocumentName(&bstrNewName);            
   if ( S_OK == didSave ) {                                                                  
      if ( wcslen(bstrNewName) ) {                                                           
         if ( *pBstrResultsFile )                                                            
            SysFreeString(*pBstrResultsFile);                                                
         *pBstrResultsFile = SysAllocString(bstrNewName);                                    
         openDocument(*pBstrResultsFile,pdfPageNumber,false,false,true,false);                                                    
         if ( pReOpenedByBackEnd )
            *pReOpenedByBackEnd = true;
         SysFreeString(bstrNewName);                                                         
      }                                                                                      
   }                                                                                         
                                                                                             
   SysFreeString(bstrOriginalFile);                                                          

   return;
   }