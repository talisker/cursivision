
#include <windows.h>
#include <stdio.h>
#include <conio.h>

#include "CursiVisionReceptor.h"

///install CursiVisionReceptor.exe "CursiVision Receptor"  PDF 17638
///uninstall "CursiVision Receptor"
///install CursiVisionReceptor.exe "CursiVision Receptor"  PDF 17638

   int __stdcall WinMain(HINSTANCE hInst,HINSTANCE hInstancePrevious,LPSTR lpCmdLine,int nCmdShow) {

   char *pCommandLine = GetCommandLine();
   char **argv;
   int argc = 0;

   long n = (DWORD)strlen(pCommandLine) + 1;
   BSTR bstrCommandLine = SysAllocStringLen(NULL,n);
   MultiByteToWideChar(CP_ACP,0,pCommandLine,-1,bstrCommandLine,n);
   BSTR *pArgs = CommandLineToArgvW(bstrCommandLine,&argc);
   argv = new char *[argc];

   for ( long k = 0; k < argc; k++ ) {
      argv[k] = new char[wcslen(pArgs[k]) + 1];
      memset(argv[k],0,(wcslen(pArgs[k]) + 1) * sizeof(char));
      WideCharToMultiByte(CP_ACP,0,pArgs[k],-1,argv[k],(DWORD)wcslen(pArgs[k]),0,0);
   }

   SC_HANDLE hServiceManager;
   SC_HANDLE hService;
   char szPath[MAX_PATH];

   memset(szPath,0,sizeof(szPath));

   szPath[0] = '\"';

   GetModuleFileName(NULL,szPath + 1,MAX_PATH);
 
   if ( 2 > argc ) {
      printf("Usage: \n\nTo install the front end control service:\n\n\tFrontEndControl /install CursiVisionReceptor.exe \"CursiVision Receptor\" PDF 17638"
                  "\n\n\tThe literal is the name of the service you want to show in services, and 17638 is the port number"
                  "\n\nTo uninstall the service:\n\n\tFrontEndControl /uninstall \"CursiVision Receptor\"\n");

      _getch();
      exit(0);
   }

   char *p = strrchr(szPath,'\\');
   if ( ! p )
      p = strrchr(szPath,'/');
   if ( p ) {
      *p = '\0';
      sprintf(szPath + strlen(szPath),"\\%s",argv[2]);
   }

   szPath[strlen(szPath) + 1] = '\0';
   szPath[strlen(szPath)] = '\"';

   hServiceManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);

   if ( 0 == stricmp(argv[1],"/install") ) {

      hService = OpenService(hServiceManager,argv[3],SERVICE_ALL_ACCESS );

      if ( hService ) {
         SERVICE_STATUS serviceStatus;
         ControlService(hService,SERVICE_CONTROL_STOP,&serviceStatus);
         DeleteService(hService);
         CloseServiceHandle(hService); 
      }

      hService = CreateService( hServiceManager,argv[3],argv[3],SERVICE_ALL_ACCESS,
                                    SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
                                    SERVICE_AUTO_START,SERVICE_ERROR_NORMAL,szPath,NULL,NULL,NULL,NULL,NULL);

      SERVICE_DESCRIPTION serviceDescription;
      char szDescription[1024];
      sprintf(szDescription,"Send %s documents to this service via port %s to get them signed by the user logged on to this machine.",argv[4],argv[5]);
      serviceDescription.lpDescription = szDescription;

      long rc = ChangeServiceConfig2(hService,SERVICE_CONFIG_DESCRIPTION,(void *)&serviceDescription);

      StartService(hService,0,NULL);

      CloseServiceHandle(hService); 

      printf("Service installed successfully\n"); 

      return 0;

   }

   if ( 0 == stricmp(argv[1],"/uninstall") ) {

      hService = OpenService(hServiceManager,argv[2],SERVICE_ALL_ACCESS);

      if ( hService ) {
         SERVICE_STATUS serviceStatus;
         ControlService(hService,SERVICE_CONTROL_STOP,&serviceStatus);
         BOOL rc = DeleteService(hService);
         CloseServiceHandle(hService); 
         printf("Service uninstalled successfully\n"); 
         return 0;
      } else 
         printf("The Service did not uninstall\n"); 

   }

   CloseServiceHandle(hServiceManager);

   return GetLastError();
   }
